1
00:00:00,080 --> 00:00:04,240
and that's it that's the end of the

2
00:00:01,839 --> 00:00:06,000
class this is what you've learned

3
00:00:04,240 --> 00:00:08,080
and hopefully you've transitioned from

4
00:00:06,000 --> 00:00:11,840
saying this is madness

5
00:00:08,080 --> 00:00:14,400
just saying wonderful wonderful

6
00:00:11,840 --> 00:00:16,080
I love interrupt descriptor tables

7
00:00:14,400 --> 00:00:19,600
global descriptor tables

8
00:00:16,080 --> 00:00:22,640
local descriptor tables because

9
00:00:19,600 --> 00:00:26,000
this is architecture 2001

10
00:00:22,640 --> 00:00:27,920
yeah so

11
00:00:26,000 --> 00:00:30,560
you know big picture you know what you

12
00:00:27,920 --> 00:00:32,239
saw you saw logical addresses which were

13
00:00:30,560 --> 00:00:33,920
segment selectors and offsets you saw

14
00:00:32,239 --> 00:00:35,920
global descriptor tables

15
00:00:33,920 --> 00:00:38,160
the means by which logical addresses

16
00:00:35,920 --> 00:00:40,480
were converted to linear addresses

17
00:00:38,160 --> 00:00:41,840
you saw linear addresses being used as

18
00:00:40,480 --> 00:00:43,440
virtual addresses

19
00:00:41,840 --> 00:00:45,680
when they were translated through page

20
00:00:43,440 --> 00:00:46,399
tables to find your way to physical

21
00:00:45,680 --> 00:00:49,600
addresses

22
00:00:46,399 --> 00:00:50,079
out there on RAM and we saw virtual

23
00:00:49,600 --> 00:00:52,640
memory

24
00:00:50,079 --> 00:00:54,559
in six fruity flavors we saw the

25
00:00:52,640 --> 00:00:56,879
simplest original version which was

26
00:00:54,559 --> 00:00:58,239
32-bit linear address space to 32-bit

27
00:00:56,879 --> 00:01:00,399
physical address space

28
00:00:58,239 --> 00:01:02,719
and we saw how you could skip a level of

29
00:01:00,399 --> 00:01:03,199
paging in order to get a 4 megabyte

30
00:01:02,719 --> 00:01:06,080
page

31
00:01:03,199 --> 00:01:08,880
instead of a 4 kilobyte page then we

32
00:01:06,080 --> 00:01:09,680
saw the 64-bit paging the 4-level

33
00:01:08,880 --> 00:01:11,360
pages

34
00:01:09,680 --> 00:01:13,439
and again we saw how you could skip

35
00:01:11,360 --> 00:01:16,240
levels in order to get 2 megabyte

36
00:01:13,439 --> 00:01:18,400
and 1 gigabyte pages and if you watch

37
00:01:16,240 --> 00:01:20,560
the optional material you also saw the

38
00:01:18,400 --> 00:01:23,119
up and coming 5-level paging

39
00:01:20,560 --> 00:01:26,320
to eventually allow intel processors to

40
00:01:23,119 --> 00:01:28,640
support 57-bit linear address spaces

41
00:01:26,320 --> 00:01:30,880
and while we didn't actually see it

42
00:01:28,640 --> 00:01:32,960
behind the scenes these fruits existed I

43
00:01:30,880 --> 00:01:34,320
just skipped the fact that you can skip

44
00:01:32,960 --> 00:01:35,920
the page levels

45
00:01:34,320 --> 00:01:38,079
in order to get the same sort of 2

46
00:01:35,920 --> 00:01:41,200
megabyte and 1 gigabyte pages

47
00:01:38,079 --> 00:01:42,960
in 57-bit paging as well

48
00:01:41,200 --> 00:01:44,399
so we picked up a whole bunch of friends

49
00:01:42,960 --> 00:01:46,240
along the way we weren't going out

50
00:01:44,399 --> 00:01:47,840
looking for them we just you know met

51
00:01:46,240 --> 00:01:50,560
them organically

52
00:01:47,840 --> 00:01:51,920
cpuid as a mechanism to find out what

53
00:01:50,560 --> 00:01:52,880
hardware features were actually

54
00:01:51,920 --> 00:01:55,520
supported

55
00:01:52,880 --> 00:01:57,600
read and write msr in order to configure

56
00:01:55,520 --> 00:01:58,719
the enablement or disablement of those

57
00:01:57,600 --> 00:02:01,439
features

58
00:01:58,719 --> 00:02:03,840
push and pop fq in order to read and

59
00:02:01,439 --> 00:02:06,799
write the rflags register

60
00:02:03,840 --> 00:02:08,800
moving to segment registers in order to

61
00:02:06,799 --> 00:02:10,000
change around what was actually being

62
00:02:08,800 --> 00:02:12,720
used as the

63
00:02:10,000 --> 00:02:13,920
code section or data section although we

64
00:02:12,720 --> 00:02:16,160
saw with 64-bit

65
00:02:13,920 --> 00:02:17,840
segmentation they're not really doesn't

66
00:02:16,160 --> 00:02:20,000
really matter anymore for those

67
00:02:17,840 --> 00:02:20,959
so it was really more the fs and gs that

68
00:02:20,000 --> 00:02:22,720
mattered

69
00:02:20,959 --> 00:02:24,959
and beyond the mov versions there was

70
00:02:22,720 --> 00:02:26,640
the push and pop ways to read and write

71
00:02:24,959 --> 00:02:28,879
the segment registers

72
00:02:26,640 --> 00:02:30,959
and then store and load the gdtr store

73
00:02:28,879 --> 00:02:33,920
and load the ldtr store and load the

74
00:02:30,959 --> 00:02:36,000
task register store and load the idtr

75
00:02:33,920 --> 00:02:38,000
these many tables that made up the

76
00:02:36,000 --> 00:02:38,879
various information that we covered in

77
00:02:38,000 --> 00:02:40,720
this class

78
00:02:38,879 --> 00:02:42,640
as well as the set and clear interrupt

79
00:02:40,720 --> 00:02:46,160
flag which changed and

80
00:02:42,640 --> 00:02:46,959
masked interrupts as necessary we saw a

81
00:02:46,160 --> 00:02:49,360
bunch of different

82
00:02:46,959 --> 00:02:51,680
software interrupt mechanisms software

83
00:02:49,360 --> 00:02:53,200
assembly instructions to invoke hardware

84
00:02:51,680 --> 00:02:54,640
interrupt vectors from the interrupt

85
00:02:53,200 --> 00:02:57,680
descriptor table

86
00:02:54,640 --> 00:03:00,400
we saw the system call functions

87
00:02:57,680 --> 00:03:01,519
sorry system call assembly instructions

88
00:03:00,400 --> 00:03:04,239
which were used

89
00:03:01,519 --> 00:03:04,800
both on 32 and 64-bit systems and we saw

90
00:03:04,239 --> 00:03:06,560
the

91
00:03:04,800 --> 00:03:08,879
the back and forth between what intel

92
00:03:06,560 --> 00:03:10,000
supported and what AMD supported and how

93
00:03:08,879 --> 00:03:12,159
some are supported

94
00:03:10,000 --> 00:03:13,040
some are preferred on 64-bit versus

95
00:03:12,159 --> 00:03:14,879
32-bit

96
00:03:13,040 --> 00:03:16,159
because of their hardware support in the

97
00:03:14,879 --> 00:03:18,239
different platforms

98
00:03:16,159 --> 00:03:20,000
and then those peripherally applicable

99
00:03:18,239 --> 00:03:23,519
things like swapgs and read

100
00:03:20,000 --> 00:03:24,959
write fs and gsbase because basically

101
00:03:23,519 --> 00:03:27,200
the whole 64-bit

102
00:03:24,959 --> 00:03:28,799
segmentation system was designed

103
00:03:27,200 --> 00:03:30,799
specifically to allow people to keep

104
00:03:28,799 --> 00:03:31,360
using what they were using anyways which

105
00:03:30,799 --> 00:03:34,879
was

106
00:03:31,360 --> 00:03:36,640
fs and gs as information such as you

107
00:03:34,879 --> 00:03:39,680
know thread local information

108
00:03:36,640 --> 00:03:42,080
or kernel data structures

109
00:03:39,680 --> 00:03:44,239
rdtsc that was an assembly instruction I

110
00:03:42,080 --> 00:03:45,680
just threw in for the heck of it because

111
00:03:44,239 --> 00:03:47,440
you know you needed to break at that

112
00:03:45,680 --> 00:03:48,319
point in the class you need to rest your

113
00:03:47,440 --> 00:03:50,560
brain

114
00:03:48,319 --> 00:03:52,400
so that's just a fun way to find out how

115
00:03:50,560 --> 00:03:53,599
many ticks have elapsed at a given point

116
00:03:52,400 --> 00:03:54,799
in time

117
00:03:53,599 --> 00:03:57,360
reading and writing the control

118
00:03:54,799 --> 00:03:59,120
registers which all were useful in the

119
00:03:57,360 --> 00:04:00,480
context of paging that's where we saw it

120
00:03:59,120 --> 00:04:02,239
but they've got all sorts of other

121
00:04:00,480 --> 00:04:04,799
little bits you know you should go read

122
00:04:02,239 --> 00:04:06,640
and check out what the other bits do

123
00:04:04,799 --> 00:04:08,400
only although there were like a giant

124
00:04:06,640 --> 00:04:10,080
ton of tables for paging

125
00:04:08,400 --> 00:04:11,760
there was only one assembly instruction

126
00:04:10,080 --> 00:04:13,519
that you know specifically was related

127
00:04:11,760 --> 00:04:16,000
to that that we picked up and that was

128
00:04:13,519 --> 00:04:17,680
invalidate page which was the way to

129
00:04:16,000 --> 00:04:19,199
kick things out of the page table if

130
00:04:17,680 --> 00:04:20,560
they were marked as global because

131
00:04:19,199 --> 00:04:23,040
everything gets kicked out

132
00:04:20,560 --> 00:04:24,479
when you change CR3 except the global

133
00:04:23,040 --> 00:04:25,919
pages they need to be kicked out

134
00:04:24,479 --> 00:04:28,080
specifically

135
00:04:25,919 --> 00:04:29,680
and a little bit of debug register fun

136
00:04:28,080 --> 00:04:33,280
and a little bit of port io

137
00:04:29,680 --> 00:04:35,280
fun so that was the class you know some

138
00:04:33,280 --> 00:04:38,240
closing thoughts about it

139
00:04:35,280 --> 00:04:38,960
this class you know I used windows as a

140
00:04:38,240 --> 00:04:41,520
means to an

141
00:04:38,960 --> 00:04:42,560
end to exemplify the particular hardware

142
00:04:41,520 --> 00:04:44,320
mechanisms

143
00:04:42,560 --> 00:04:46,160
was not you know this was not meant to

144
00:04:44,320 --> 00:04:47,840
be a windows internals class that'll

145
00:04:46,160 --> 00:04:49,120
come out later that'll build on this

146
00:04:47,840 --> 00:04:51,199
type of class

147
00:04:49,120 --> 00:04:53,280
but so now that you understand like the

148
00:04:51,199 --> 00:04:54,320
actual mechanisms as described in the

149
00:04:53,280 --> 00:04:56,240
intel manuals

150
00:04:54,320 --> 00:04:58,479
you can go out and read the manuals

151
00:04:56,240 --> 00:05:00,400
they're fun read the fun manuals

152
00:04:58,479 --> 00:05:01,600
in order to learn more about how these

153
00:05:00,400 --> 00:05:03,280
mechanisms work

154
00:05:01,600 --> 00:05:04,720
you can also learn how they work in your

155
00:05:03,280 --> 00:05:07,199
favorite operating system

156
00:05:04,720 --> 00:05:08,400
go grab an internals book and see how

157
00:05:07,199 --> 00:05:10,400
that OS uses it

158
00:05:08,400 --> 00:05:12,400
if it happens to have some open source

159
00:05:10,400 --> 00:05:15,600
like the xnu for macOS

160
00:05:12,400 --> 00:05:17,360
or Linux kernel freebsd openbsd

161
00:05:15,600 --> 00:05:18,880
you can go find out how those different

162
00:05:17,360 --> 00:05:20,479
operating systems use all of these

163
00:05:18,880 --> 00:05:22,479
mechanisms

164
00:05:20,479 --> 00:05:25,199
also you're much better suited now to

165
00:05:22,479 --> 00:05:28,880
try to learn things like virtualization

166
00:05:25,199 --> 00:05:31,919
because OS is to process as

167
00:05:28,880 --> 00:05:33,840
hypervisor is to OS because

168
00:05:31,919 --> 00:05:35,680
basically an operating system

169
00:05:33,840 --> 00:05:37,440
manipulates the hardware resources

170
00:05:35,680 --> 00:05:38,960
on behalf of you know the memory

171
00:05:37,440 --> 00:05:40,080
management for different processes

172
00:05:38,960 --> 00:05:41,600
things like that they're context

173
00:05:40,080 --> 00:05:42,080
switching they're swapping things around

174
00:05:41,600 --> 00:05:43,919
their

175
00:05:42,080 --> 00:05:45,199
time division multiplexing amongst

176
00:05:43,919 --> 00:05:47,840
various processes

177
00:05:45,199 --> 00:05:48,720
and virtualization has to be able to do

178
00:05:47,840 --> 00:05:51,520
the same thing

179
00:05:48,720 --> 00:05:52,479
amongst the multiple operating systems

180
00:05:51,520 --> 00:05:54,240
so if you start

181
00:05:52,479 --> 00:05:55,600
thinking to yourself you know the

182
00:05:54,240 --> 00:05:58,000
operating system

183
00:05:55,600 --> 00:06:00,080
is or sorry the virtualization system is

184
00:05:58,000 --> 00:06:02,720
playing games with an operating system

185
00:06:00,080 --> 00:06:03,280
it starts to raise interesting questions

186
00:06:02,720 --> 00:06:05,680
about

187
00:06:03,280 --> 00:06:06,319
how can you actually lie to an operating

188
00:06:05,680 --> 00:06:07,840
system

189
00:06:06,319 --> 00:06:09,520
about you know the contents of the

190
00:06:07,840 --> 00:06:12,560
global descriptor table register

191
00:06:09,520 --> 00:06:15,759
control register 3, 4, 0, 1

192
00:06:12,560 --> 00:06:16,080
whatever so the different you know so

193
00:06:15,759 --> 00:06:17,840
this

194
00:06:16,080 --> 00:06:19,199
this should like give you a sense of

195
00:06:17,840 --> 00:06:21,360
like how hard it is

196
00:06:19,199 --> 00:06:23,039
to like bolt on virtualization support

197
00:06:21,360 --> 00:06:24,479
to a architecture like intel which

198
00:06:23,039 --> 00:06:26,400
didn't have it to begin with

199
00:06:24,479 --> 00:06:29,039
this also should give you a sense of why

200
00:06:26,400 --> 00:06:30,639
there's these leaky mechanisms by which

201
00:06:29,039 --> 00:06:32,400
if you know malicious software really

202
00:06:30,639 --> 00:06:34,720
wants to detect that it's in a virtual

203
00:06:32,400 --> 00:06:36,400
machine it probably can

204
00:06:34,720 --> 00:06:38,960
and it also should start to give you a

205
00:06:36,400 --> 00:06:41,840
sense of how malicious software might

206
00:06:38,960 --> 00:06:44,319
use virtualization to its advantage we

207
00:06:41,840 --> 00:06:46,560
covered red pill in this class which was

208
00:06:44,319 --> 00:06:48,319
you know a mechanism that was used in

209
00:06:46,560 --> 00:06:50,880
order to detect virtualization

210
00:06:48,319 --> 00:06:52,080
later on that was followed with a talk

211
00:06:50,880 --> 00:06:54,160
called blue pill

212
00:06:52,080 --> 00:06:55,840
which was using the virtualization in

213
00:06:54,160 --> 00:06:57,840
order to blue pill or you know

214
00:06:55,840 --> 00:06:59,680
take and put the operating system in the

215
00:06:57,840 --> 00:07:01,759
matrix keep it in the matrix

216
00:06:59,680 --> 00:07:03,199
use the virtualization system in order

217
00:07:01,759 --> 00:07:05,199
to take control

218
00:07:03,199 --> 00:07:06,800
and then lie to the operating system

219
00:07:05,199 --> 00:07:08,560
about the state of the world and whether

220
00:07:06,800 --> 00:07:11,039
it was infected or not

221
00:07:08,560 --> 00:07:12,479
so this this sort of class is the the

222
00:07:11,039 --> 00:07:13,680
baseline information that you need to

223
00:07:12,479 --> 00:07:14,639
know to start digging into

224
00:07:13,680 --> 00:07:17,680
virtualization

225
00:07:14,639 --> 00:07:20,080
which will be you know future classes

226
00:07:17,680 --> 00:07:20,800
also this is just you know pontification

227
00:07:20,080 --> 00:07:22,639
you know you can

228
00:07:20,800 --> 00:07:23,840
skip and be done at this point this all

229
00:07:22,639 --> 00:07:25,360
these closing thoughts are just

230
00:07:23,840 --> 00:07:27,520
pontification

231
00:07:25,360 --> 00:07:29,199
but uh you know in my opinion it's very

232
00:07:27,520 --> 00:07:31,599
frequently the case that there are tool

233
00:07:29,199 --> 00:07:33,039
users and there are tool understanders

234
00:07:31,599 --> 00:07:34,720
and of course there are tool builders

235
00:07:33,039 --> 00:07:37,599
who must necessarily be

236
00:07:34,720 --> 00:07:39,680
tool understanders and you know I think

237
00:07:37,599 --> 00:07:40,160
that everyone always starts as a tool

238
00:07:39,680 --> 00:07:41,919
user

239
00:07:40,160 --> 00:07:43,440
of course if you start doing reverse

240
00:07:41,919 --> 00:07:45,840
engineering you just have to

241
00:07:43,440 --> 00:07:47,759
take a debugger be shown how it works be

242
00:07:45,840 --> 00:07:48,319
shown how to read assembly things like

243
00:07:47,759 --> 00:07:49,840
that

244
00:07:48,319 --> 00:07:52,000
and that's the appropriate starting

245
00:07:49,840 --> 00:07:52,960
point but as you want to mature in your

246
00:07:52,000 --> 00:07:54,560
capabilities

247
00:07:52,960 --> 00:07:56,000
you need to move into being a tool

248
00:07:54,560 --> 00:07:57,599
understander you have to say how does

249
00:07:56,000 --> 00:08:00,560
the debugger actually work

250
00:07:57,599 --> 00:08:01,919
what is it doing behind the scenes and

251
00:08:00,560 --> 00:08:03,680
so if you look at the people who are

252
00:08:01,919 --> 00:08:05,199
really you know always pushing research

253
00:08:03,680 --> 00:08:06,080
forward and pushing you know the

254
00:08:05,199 --> 00:08:08,000
industry forward

255
00:08:06,080 --> 00:08:10,240
it's people who are understanders and

256
00:08:08,000 --> 00:08:11,440
builders so you should always try to

257
00:08:10,240 --> 00:08:13,759
move your way towards being an

258
00:08:11,440 --> 00:08:16,000
understander as an aside

259
00:08:13,759 --> 00:08:17,280
if you ever have to find yourself

260
00:08:16,000 --> 00:08:18,560
interviewing someone and you're trying

261
00:08:17,280 --> 00:08:19,840
to get a sense of

262
00:08:18,560 --> 00:08:21,759
you know interviewing someone for

263
00:08:19,840 --> 00:08:22,319
perhaps a security architect type

264
00:08:21,759 --> 00:08:24,960
position

265
00:08:22,319 --> 00:08:26,400
a low-level security type person and you

266
00:08:24,960 --> 00:08:28,080
need to find out you know is this really

267
00:08:26,400 --> 00:08:30,479
a tool builder is it a tool

268
00:08:28,080 --> 00:08:32,560
understander is it just a tool user

269
00:08:30,479 --> 00:08:34,479
something like how does a debugger work

270
00:08:32,560 --> 00:08:35,680
is a great question that can you know

271
00:08:34,479 --> 00:08:37,519
elicit uh

272
00:08:35,680 --> 00:08:39,279
details of how deeply they understand

273
00:08:37,519 --> 00:08:40,640
the system you can say how does debugger

274
00:08:39,279 --> 00:08:42,479
work and they say

275
00:08:40,640 --> 00:08:44,399
oh well you know it uses a software

276
00:08:42,479 --> 00:08:46,480
interrupt okay which software interrupt

277
00:08:44,399 --> 00:08:47,519
and you know okay well what's the

278
00:08:46,480 --> 00:08:48,720
specific you know

279
00:08:47,519 --> 00:08:50,320
byte that's used for the software

280
00:08:48,720 --> 00:08:51,920
interrupt how does the debugger you know

281
00:08:50,320 --> 00:08:52,480
rewrite to the assembly instruction on

282
00:08:51,920 --> 00:08:54,080
the fly

283
00:08:52,480 --> 00:08:55,839
that kind of thing so it's a good

284
00:08:54,080 --> 00:08:56,800
question to like drill down on how deep

285
00:08:55,839 --> 00:08:58,720
people know things

286
00:08:56,800 --> 00:09:02,399
and of course you at this point know

287
00:08:58,720 --> 00:09:04,480
things much better than most people do

288
00:09:02,399 --> 00:09:05,440
and you know just thinking about things

289
00:09:04,480 --> 00:09:08,560
from an adverse

290
00:09:05,440 --> 00:09:10,720
adversarial perspective you know

291
00:09:08,560 --> 00:09:12,560
people who build software like you know

292
00:09:10,720 --> 00:09:15,440
hypervisor rootkits

293
00:09:12,560 --> 00:09:17,040
kernel mode rootkits firmware attacks

294
00:09:15,440 --> 00:09:18,800
these are people who come from a

295
00:09:17,040 --> 00:09:20,800
perspective of really understanding the

296
00:09:18,800 --> 00:09:23,120
system at a very deep level

297
00:09:20,800 --> 00:09:25,440
and so if you are a defender if your job

298
00:09:23,120 --> 00:09:27,120
is to defend as opposed to attack

299
00:09:25,440 --> 00:09:28,320
then you have to think like you know

300
00:09:27,120 --> 00:09:30,240
what would it mean if you're trying to

301
00:09:28,320 --> 00:09:31,440
defend a system and the attacker has

302
00:09:30,240 --> 00:09:34,640
spent you know

303
00:09:31,440 --> 00:09:36,560
days weeks months years reading intel

304
00:09:34,640 --> 00:09:37,920
manuals understanding the system at an

305
00:09:36,560 --> 00:09:40,080
extremely deep level

306
00:09:37,920 --> 00:09:41,839
you know are you hopelessly outclassed

307
00:09:40,080 --> 00:09:44,320
and outmatched at that point

308
00:09:41,839 --> 00:09:45,600
possibly you know the point is it's

309
00:09:44,320 --> 00:09:47,519
really hard to defend

310
00:09:45,600 --> 00:09:49,200
against an attacker who understands the

311
00:09:47,519 --> 00:09:50,720
system much better than you do

312
00:09:49,200 --> 00:09:52,480
so that's again why you need to be a

313
00:09:50,720 --> 00:09:54,560
tool understander that's why you have to

314
00:09:52,480 --> 00:09:56,399
love reading the fun manuals

315
00:09:54,560 --> 00:09:57,839
and why you have to you know always want

316
00:09:56,399 --> 00:09:59,440
to go deep which is

317
00:09:57,839 --> 00:10:01,920
why open security training is trying to

318
00:09:59,440 --> 00:10:04,560
help you go deeper faster

319
00:10:01,920 --> 00:10:05,360
all right well that's it for this class

320
00:10:04,560 --> 00:10:07,760
goodbye

321
00:10:05,360 --> 00:10:11,360
I'll hopefully see you again in another

322
00:10:07,760 --> 00:10:11,360
class coming up soon

