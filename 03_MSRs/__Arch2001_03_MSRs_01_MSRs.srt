1
00:00:00,560 --> 00:00:04,799
In the cpuid section we talked about the

2
00:00:03,120 --> 00:00:07,680
idea that there's going to be an

3
00:00:04,799 --> 00:00:09,679
ever increasing number of features as a

4
00:00:07,680 --> 00:00:12,160
processor evolves

5
00:00:09,679 --> 00:00:14,320
and so generically any sort of processor

6
00:00:12,160 --> 00:00:16,640
maker is going to need some way to

7
00:00:14,320 --> 00:00:18,640
let software query the hardware and say

8
00:00:16,640 --> 00:00:21,439
you know what features do you support

9
00:00:18,640 --> 00:00:23,119
cpuid is that way that software finds

10
00:00:21,439 --> 00:00:25,199
out what features are supported

11
00:00:23,119 --> 00:00:26,800
but then after the fact after you find

12
00:00:25,199 --> 00:00:27,199
out that some feature that you want to

13
00:00:26,800 --> 00:00:29,039
use

14
00:00:27,199 --> 00:00:31,760
is supported what do you do how do you

15
00:00:29,039 --> 00:00:34,399
enable it well this mechanism on intel

16
00:00:31,760 --> 00:00:37,360
systems of model specific registers

17
00:00:34,399 --> 00:00:38,800
is the way that intel can allow features

18
00:00:37,360 --> 00:00:39,760
after they're checked to be present with

19
00:00:38,800 --> 00:00:43,120
cpuid

20
00:00:39,760 --> 00:00:45,120
to be enabled and configured specific to

21
00:00:43,120 --> 00:00:47,760
this particular hardware

22
00:00:45,120 --> 00:00:50,160
so over time this list of model specific

23
00:00:47,760 --> 00:00:52,079
registers has just grown so large that

24
00:00:50,160 --> 00:00:53,199
it became its own volume of the intel

25
00:00:52,079 --> 00:00:55,600
manuals

26
00:00:53,199 --> 00:00:56,480
and so what you'll see in there is a mix

27
00:00:55,600 --> 00:00:59,120
of things called

28
00:00:56,480 --> 00:00:59,680
architectural msrs these are the things

29
00:00:59,120 --> 00:01:01,680
that are

30
00:00:59,680 --> 00:01:03,199
essentially so common that they should

31
00:01:01,680 --> 00:01:05,760
be treated as

32
00:01:03,199 --> 00:01:08,000
belonging to all processor families and

33
00:01:05,760 --> 00:01:10,240
then you get the actual model specific

34
00:01:08,000 --> 00:01:11,760
registers so then there's a giant list

35
00:01:10,240 --> 00:01:14,960
of you know for this model it supports

36
00:01:11,760 --> 00:01:17,200
this for this model it supports this

37
00:01:14,960 --> 00:01:18,960
and there's going to be a naming caveat

38
00:01:17,200 --> 00:01:20,560
that you should be aware of when you

39
00:01:18,960 --> 00:01:21,040
start looking at the manual you'll see

40
00:01:20,560 --> 00:01:24,479
things

41
00:01:21,040 --> 00:01:28,080
start with ia32_ if they're

42
00:01:24,479 --> 00:01:29,680
an architectural MSR so if you see ia32_

43
00:01:28,080 --> 00:01:31,680
you should think to yourself

44
00:01:29,680 --> 00:01:33,280
this means it's just a MSR that

45
00:01:31,680 --> 00:01:34,000
everybody everywhere should be able to

46
00:01:33,280 --> 00:01:36,400
look up

47
00:01:34,000 --> 00:01:37,840
it's not specifically limited to 32-bit

48
00:01:36,400 --> 00:01:39,360
execution

49
00:01:37,840 --> 00:01:41,360
so how would you look up, how would you

50
00:01:39,360 --> 00:01:42,479
read and write these model specific

51
00:01:41,360 --> 00:01:45,119
registers?

52
00:01:42,479 --> 00:01:46,720
Well you start with the rdmsr

53
00:01:45,119 --> 00:01:49,680
assembly instruction

54
00:01:46,720 --> 00:01:50,960
and here we're going to add a new

55
00:01:49,680 --> 00:01:53,200
terminology or a new

56
00:01:50,960 --> 00:01:54,880
indication every single assembly

57
00:01:53,200 --> 00:01:55,280
instruction we've seen thus far has been

58
00:01:54,880 --> 00:01:58,399
a

59
00:01:55,280 --> 00:02:00,320
star that's a little yellow star and now

60
00:01:58,399 --> 00:02:01,840
what we need is some way to indicate

61
00:02:00,320 --> 00:02:03,600
that this particular assembly

62
00:02:01,840 --> 00:02:05,920
instruction is

63
00:02:03,600 --> 00:02:06,880
only executable from privileged

64
00:02:05,920 --> 00:02:09,039
environment

65
00:02:06,880 --> 00:02:10,399
so the red star does not indicate that

66
00:02:09,039 --> 00:02:11,599
it's you know seeking to overthrow the

67
00:02:10,399 --> 00:02:13,599
bourgeoisie

68
00:02:11,599 --> 00:02:15,280
actually the red star in this class

69
00:02:13,599 --> 00:02:16,160
means that this particular assembly

70
00:02:15,280 --> 00:02:18,560
instruction

71
00:02:16,160 --> 00:02:19,680
can only be used by the privileged elite

72
00:02:18,560 --> 00:02:21,040
in kernel mode

73
00:02:19,680 --> 00:02:22,720
it's practically a tool of the

74
00:02:21,040 --> 00:02:25,680
oppressors. So

75
00:02:22,720 --> 00:02:27,680
rdmsr, this cannot be used in user

76
00:02:25,680 --> 00:02:28,640
space code it can only be used in kernel

77
00:02:27,680 --> 00:02:30,959
code

78
00:02:28,640 --> 00:02:33,040
so what does it do well it reads the MSR

79
00:02:30,959 --> 00:02:35,760
specified by ecx so you just

80
00:02:33,040 --> 00:02:37,200
stick some value into ecx execute the

81
00:02:35,760 --> 00:02:39,360
rdmsr command

82
00:02:37,200 --> 00:02:40,560
and expect that the output will come

83
00:02:39,360 --> 00:02:44,000
into edx

84
00:02:40,560 --> 00:02:47,280
eax so again just like cpuid

85
00:02:44,000 --> 00:02:48,160
it's still using 32-bit values it hasn't

86
00:02:47,280 --> 00:02:50,400
changed over to

87
00:02:48,160 --> 00:02:53,280
64-bit registers so these are just the

88
00:02:50,400 --> 00:02:55,760
registers that it's going to use.

89
00:02:53,280 --> 00:02:58,159
Alright, so the correspondent to

90
00:02:55,760 --> 00:02:59,920
reading the MSR is writing the MSR

91
00:02:58,159 --> 00:03:02,159
this is how something like an operating

92
00:02:59,920 --> 00:03:03,680
system would configure these security

93
00:03:02,159 --> 00:03:05,200
features and enable these security

94
00:03:03,680 --> 00:03:08,319
features or just general

95
00:03:05,200 --> 00:03:10,319
system features. So of course if you know

96
00:03:08,319 --> 00:03:12,319
normal user-space software could

97
00:03:10,319 --> 00:03:13,599
reconfigure the system to turn off some

98
00:03:12,319 --> 00:03:15,519
security functionality

99
00:03:13,599 --> 00:03:17,200
that would be extremely bad and that

100
00:03:15,519 --> 00:03:18,400
would be sort of an indefensible

101
00:03:17,200 --> 00:03:20,400
security position

102
00:03:18,400 --> 00:03:22,560
so that is why these things are

103
00:03:20,400 --> 00:03:25,920
restricted to only being usable

104
00:03:22,560 --> 00:03:27,760
in kernel space. So wrmsr is the

105
00:03:25,920 --> 00:03:29,760
you know inverse of rdmsr it's going

106
00:03:27,760 --> 00:03:32,239
to take a 64-bit value in these 2

107
00:03:29,760 --> 00:03:33,519
concatenated 32-bit registers and it's

108
00:03:32,239 --> 00:03:36,319
going to write it to whatever

109
00:03:33,519 --> 00:03:38,400
MSR you specify by sticking some number

110
00:03:36,319 --> 00:03:40,720
in ecx.

111
00:03:38,400 --> 00:03:41,680
So what could we read what could we

112
00:03:40,720 --> 00:03:43,680
write? Let's go

113
00:03:41,680 --> 00:03:44,879
take a look at something interesting.

114
00:03:43,680 --> 00:03:47,040
Well this class is about

115
00:03:44,879 --> 00:03:48,400
64-bit architectural support for

116
00:03:47,040 --> 00:03:51,200
operating systems

117
00:03:48,400 --> 00:03:51,599
so if we look at the man page and we

118
00:03:51,200 --> 00:03:54,239
find

119
00:03:51,599 --> 00:03:59,280
the architectural MSRs we scan down

120
00:03:54,239 --> 00:04:02,640
eventually we find ourselves at c000_0080

121
00:03:59,280 --> 00:04:04,480
and this is the ia32_efer register

122
00:04:02,640 --> 00:04:06,519
now I'm going to have another little

123
00:04:04,480 --> 00:04:09,519
indication in this class of a orange

124
00:04:06,519 --> 00:04:12,480
icosahedron for MSRs why

125
00:04:09,519 --> 00:04:14,080
why not so whenever you see a orange

126
00:04:12,480 --> 00:04:16,239
icosahedron that means we've got a

127
00:04:14,080 --> 00:04:20,320
little MSR somewhere on the page.

128
00:04:16,239 --> 00:04:21,120
So ia32_efer is the extended features

129
00:04:20,320 --> 00:04:24,240
enables

130
00:04:21,120 --> 00:04:26,840
MSR and so amongst these features

131
00:04:24,240 --> 00:04:28,000
are bit 8 talks about whether or not

132
00:04:26,840 --> 00:04:31,280
ia32e

133
00:04:28,000 --> 00:04:32,880
mode is enabled right so this also was

134
00:04:31,280 --> 00:04:35,680
known as i32_efer

135
00:04:32,880 --> 00:04:36,560
lme long mode enable and it is a read

136
00:04:35,680 --> 00:04:38,560
and write

137
00:04:36,560 --> 00:04:40,720
bit and this is the thing that actually

138
00:04:38,560 --> 00:04:42,479
enables 64-bit mode

139
00:04:40,720 --> 00:04:44,320
so we showed before on that you know

140
00:04:42,479 --> 00:04:46,160
finite state machine that there's some

141
00:04:44,320 --> 00:04:48,000
lme long mode enable thing that gets you

142
00:04:46,160 --> 00:04:49,199
from normal protective mode into actual

143
00:04:48,000 --> 00:04:51,680
64-bit mode

144
00:04:49,199 --> 00:04:52,880
and here's the bit it's this bit in this

145
00:04:51,680 --> 00:04:55,120
MSR.

146
00:04:52,880 --> 00:04:56,080
Furthermore, there's another bit here bit

147
00:04:55,120 --> 00:04:58,720
10, ia

148
00:04:56,080 --> 00:04:59,440
32e mode active and this tells you

149
00:04:58,720 --> 00:05:00,880
whether or not

150
00:04:59,440 --> 00:05:04,479
you know the mode is actually active

151
00:05:00,880 --> 00:05:04,479
right now this is a read-only thing.

152
00:05:04,960 --> 00:05:11,039
Right, so back to this state finite state

153
00:05:08,240 --> 00:05:12,720
machine we saw real mode make your way

154
00:05:11,039 --> 00:05:13,120
up to protective mode and then right

155
00:05:12,720 --> 00:05:16,560
here

156
00:05:13,120 --> 00:05:19,360
efer.lme equals 1 was

157
00:05:16,560 --> 00:05:20,880
one of the bits that gets you too long

158
00:05:19,360 --> 00:05:21,600
not everything so we've still got more

159
00:05:20,880 --> 00:05:23,280
to learn

160
00:05:21,600 --> 00:05:26,320
but you know we've we've found one

161
00:05:23,280 --> 00:05:27,840
element of the picture so far

162
00:05:26,320 --> 00:05:29,440
and this is again why I don't like the

163
00:05:27,840 --> 00:05:30,960
intel version because it just says lme

164
00:05:29,440 --> 00:05:33,919
it doesn't tell you where it is

165
00:05:30,960 --> 00:05:35,840
at least the AMD one says it's in the efer

166
00:05:33,919 --> 00:05:39,039
register

167
00:05:35,840 --> 00:05:41,039
oh there's black manta there we go.

168
00:05:39,039 --> 00:05:42,720
Alright, well, if we go back and we look

169
00:05:41,039 --> 00:05:47,360
at the man page here though there's

170
00:05:42,720 --> 00:05:47,360
some comment over here that says if

171
00:05:47,400 --> 00:05:52,560
cpuid 8000001

172
00:05:49,120 --> 00:05:56,720
edx bit 20 or

173
00:05:52,560 --> 00:05:59,120
edx bit 29. So this kind of implies that

174
00:05:56,720 --> 00:05:59,919
this model specific register is probably

175
00:05:59,120 --> 00:06:03,520
only available

176
00:05:59,919 --> 00:06:07,280
if cpuid returns you know true

177
00:06:03,520 --> 00:06:13,280
returns one for this bit 20 or bit 29

178
00:06:07,280 --> 00:06:15,280
in a cpuid query of eax equals 8000001.

179
00:06:13,280 --> 00:06:19,280
So we could go back and look at the cpuid

180
00:06:15,280 --> 00:06:22,720
man page and we'd see for 8000001.

181
00:06:19,280 --> 00:06:25,120
edx output of bit 20

182
00:06:22,720 --> 00:06:26,400
is execute disable bit available all

183
00:06:25,120 --> 00:06:29,199
right well that doesn't seem to have

184
00:06:26,400 --> 00:06:30,319
anything to do with this but bit 29 is

185
00:06:29,199 --> 00:06:31,919
intel 64

186
00:06:30,319 --> 00:06:34,000
architecture available so there you go

187
00:06:31,919 --> 00:06:37,120
there's that in one

188
00:06:34,000 --> 00:06:39,120
one page it says ia-32e and another page

189
00:06:37,120 --> 00:06:42,000
it calls it intel 64.

190
00:06:39,120 --> 00:06:43,440
so bit 29 is going to be the bit that we

191
00:06:42,000 --> 00:06:47,039
really care about to find out

192
00:06:43,440 --> 00:06:50,639
whether or not we can check that ia64e

193
00:06:47,039 --> 00:06:52,639
enabled. So let's go ahead and go over

194
00:06:50,639 --> 00:06:55,520
and see what it looks like for instance

195
00:06:52,639 --> 00:06:57,680
if we run one of these restricted

196
00:06:55,520 --> 00:06:59,280
instructions like rdmsr from user

197
00:06:57,680 --> 00:07:02,800
space which I just told you should only

198
00:06:59,280 --> 00:07:05,840
be possible to run in kernel space.

199
00:07:02,800 --> 00:07:09,039
So I'm going to start up my VM here

200
00:07:05,840 --> 00:07:11,039
hop on over to user MSRs

201
00:07:09,039 --> 00:07:12,880
there is I don't need to use any raw

202
00:07:11,039 --> 00:07:14,319
assembly here because there is a

203
00:07:12,880 --> 00:07:16,880
compiler intrinsic for

204
00:07:14,319 --> 00:07:19,440
rdmsr and this thing will happily let

205
00:07:16,880 --> 00:07:21,599
me compile it in user space.

206
00:07:19,440 --> 00:07:23,440
So let's go ahead and build that and

207
00:07:21,599 --> 00:07:25,759
let's set a breakpoint at the

208
00:07:23,440 --> 00:07:26,800
beginning and the end of this particular

209
00:07:25,759 --> 00:07:31,280
code

210
00:07:26,800 --> 00:07:32,720
let's run it in the debugger and let's

211
00:07:31,280 --> 00:07:35,120
just go ahead and you know continue

212
00:07:32,720 --> 00:07:37,120
let's see what happens all right well we

213
00:07:35,120 --> 00:07:37,520
didn't make our way to this final break

214
00:07:37,120 --> 00:07:40,960
point

215
00:07:37,520 --> 00:07:44,000
instead we stopped here and it said

216
00:07:40,960 --> 00:07:46,639
exception unhandled exception privileged

217
00:07:44,000 --> 00:07:48,639
instruction was executed and so you know

218
00:07:46,639 --> 00:07:50,240
it's not going to let us run this code

219
00:07:48,639 --> 00:07:52,000
and that exactly jives with what we

220
00:07:50,240 --> 00:07:53,680
expect. Okay

221
00:07:52,000 --> 00:07:56,000
well having established that we can't

222
00:07:53,680 --> 00:07:57,759
run the rdmsr command from within

223
00:07:56,000 --> 00:07:59,199
user space let's go ahead and do it from

224
00:07:57,759 --> 00:08:01,199
within kernel space

225
00:07:59,199 --> 00:08:02,319
we have a separate project,

226
00:08:01,199 --> 00:08:04,319
K_MSRs for

227
00:08:02,319 --> 00:08:05,440
building the kernel driver for that and

228
00:08:04,319 --> 00:08:07,680
you should look at

229
00:08:05,440 --> 00:08:09,440
the website for the instructions to

230
00:08:07,680 --> 00:08:10,879
follow along and do this after I show

231
00:08:09,440 --> 00:08:14,080
you how

232
00:08:10,879 --> 00:08:17,280
oops so over here k_msrs

233
00:08:14,080 --> 00:08:18,960
same thing uh as the user space version

234
00:08:17,280 --> 00:08:21,720
it's got the exact same you know use of

235
00:08:18,960 --> 00:08:23,199
the intrinsic in order to read from

236
00:08:21,720 --> 00:08:25,199
C0000080

237
00:08:23,199 --> 00:08:26,879
and it's checking again the long mode

238
00:08:25,199 --> 00:08:29,840
enable bit the long mode

239
00:08:26,879 --> 00:08:32,159
active bit. So let's go ahead and build

240
00:08:29,840 --> 00:08:34,719
that

241
00:08:32,159 --> 00:08:35,279
and then after it's done building you'll

242
00:08:34,719 --> 00:08:37,919
see the

243
00:08:35,279 --> 00:08:38,640
absolute path to the locations where the

244
00:08:37,919 --> 00:08:41,279
output is

245
00:08:38,640 --> 00:08:42,240
so go ahead and copy you'll copy that

246
00:08:41,279 --> 00:08:45,839
ultimately

247
00:08:42,240 --> 00:08:49,760
you'll go to that location

248
00:08:45,839 --> 00:08:53,120
and you copy the kernel driver itself

249
00:08:49,760 --> 00:08:56,640
go over to your vm drop it into

250
00:08:53,120 --> 00:08:59,040
c:\ost2

251
00:08:56,640 --> 00:08:59,839
and from there you're then going to run

252
00:08:59,040 --> 00:09:03,600
the

253
00:08:59,839 --> 00:09:05,120
devcon command in order to install it.

254
00:09:03,600 --> 00:09:07,279
Before you install it you want to make

255
00:09:05,120 --> 00:09:08,640
sure your kernel debugging is set up and

256
00:09:07,279 --> 00:09:11,440
that you can see

257
00:09:08,640 --> 00:09:12,000
debug print output so this again is just

258
00:09:11,440 --> 00:09:14,640
like

259
00:09:12,000 --> 00:09:16,640
you're done in past labs in the

260
00:09:14,640 --> 00:09:19,440
intermediate WinDbg

261
00:09:16,640 --> 00:09:21,760
class mini course so you need to make

262
00:09:19,440 --> 00:09:25,519
sure that you have the

263
00:09:21,760 --> 00:09:29,680
verbose debugging set

264
00:09:25,519 --> 00:09:32,640
so I go ahead and do that and hit go.

265
00:09:29,680 --> 00:09:34,320
Alright and now I can go ahead and load

266
00:09:32,640 --> 00:09:36,800
this kernel driver it's going to ask me

267
00:09:34,320 --> 00:09:38,640
if I want to install it I say yes I do

268
00:09:36,800 --> 00:09:41,680
it's going to take a little bit in order

269
00:09:38,640 --> 00:09:43,680
to process it before it executes it

270
00:09:41,680 --> 00:09:45,519
but once we get back to a command prompt

271
00:09:43,680 --> 00:09:46,399
here then the kernel driver should have

272
00:09:45,519 --> 00:09:48,080
been loaded

273
00:09:46,399 --> 00:09:49,519
and we should see the output of the

274
00:09:48,080 --> 00:09:52,720
driver over in the

275
00:09:49,519 --> 00:09:54,640
WinDbg screen command window.

276
00:09:52,720 --> 00:09:56,720
So what do we see as output? Well we see

277
00:09:54,640 --> 00:09:57,279
some of our stock you know driver entry

278
00:09:56,720 --> 00:10:00,160
print

279
00:09:57,279 --> 00:10:01,040
and the event add device but what we

280
00:10:00,160 --> 00:10:04,640
care about is

281
00:10:01,040 --> 00:10:07,600
it took ia32_efer and it printed out the

282
00:10:04,640 --> 00:10:09,120
value which is d01

283
00:10:07,600 --> 00:10:11,680
and then specifically it looked at the

284
00:10:09,120 --> 00:10:14,240
bits LME and it says that's 1 so

285
00:10:11,680 --> 00:10:16,480
it's running in 64-bit mode and LMA and

286
00:10:14,240 --> 00:10:18,959
that's 1 so 64-bit mode was active

287
00:10:16,480 --> 00:10:21,600
when this code ran

288
00:10:18,959 --> 00:10:22,959
so that's the output that we expect and

289
00:10:21,600 --> 00:10:25,120
you know you should already know this

290
00:10:22,959 --> 00:10:27,519
but there is an easy mode here in that

291
00:10:25,120 --> 00:10:29,040
WinDbg does have a rdmsr and a

292
00:10:27,519 --> 00:10:32,000
wrmsr

293
00:10:29,040 --> 00:10:33,360
option so you could get the same thing

294
00:10:32,000 --> 00:10:34,800
you don't need to use a kernel driver in

295
00:10:33,360 --> 00:10:36,160
order to do this you just need to be

296
00:10:34,800 --> 00:10:39,440
doing kernel debugging

297
00:10:36,160 --> 00:10:42,560
and from there you could do rdmsr

298
00:10:39,440 --> 00:10:46,079
rdmsr alright that's right and C000

299
00:10:42,560 --> 00:10:49,519
0080

300
00:10:46,079 --> 00:10:50,399
and you see the same value d01. So go

301
00:10:49,519 --> 00:10:52,640
ahead and

302
00:10:50,399 --> 00:10:53,760
now run this lab yourself just to make

303
00:10:52,640 --> 00:10:56,880
sure that you have

304
00:10:53,760 --> 00:10:57,760
kernel mode driver execution working

305
00:10:56,880 --> 00:11:00,000
that you have

306
00:10:57,760 --> 00:11:04,640
debug print working and that you can use

307
00:11:00,000 --> 00:11:04,640
the rdmsr command from within WinDbg

