1
00:00:00,04 --> 00:00:01,96
So what's the conclusion in this section,

2
00:00:01,97 --> 00:00:06,78
paging is complicated yo! so I'm really aware that it

3
00:00:06,79 --> 00:00:09,97
is extremely difficult to absorb all of this in one

4
00:00:09,97 --> 00:00:10,46
sitting

5
00:00:10,84 --> 00:00:12,12
The reality is of course,

6
00:00:12,12 --> 00:00:16,1
people learned this after many iterations of reading the manual

7
00:00:16,1 --> 00:00:16,97
fiddling with things,

8
00:00:16,97 --> 00:00:18,76
reading manual fiddling with things

9
00:00:19,14 --> 00:00:21,58
The hope is that this class serves as a sort

10
00:00:21,58 --> 00:00:25,73
of guided tour to help you get bootstrapped faster in

11
00:00:25,73 --> 00:00:26,63
a perfect world,

12
00:00:26,63 --> 00:00:31,26
You would watch the entire class twice because basically just

13
00:00:31,26 --> 00:00:33,25
the second time you're primed and you know,

14
00:00:33,25 --> 00:00:35,64
you understand what you're in for and you have a

15
00:00:35,64 --> 00:00:36,96
good sense of what's coming

16
00:00:37,34 --> 00:00:39,71
But if you don't have enough time to watch the

17
00:00:39,71 --> 00:00:41,11
entire class twice,

18
00:00:41,19 --> 00:00:44,83
I still recommend at least watching this particular section twice

19
00:00:44,83 --> 00:00:47,15
because it's very difficult to grasp

20
00:00:47,54 --> 00:00:48,72
Like I said,

21
00:00:48,72 --> 00:00:49,45
most people,

22
00:00:49,45 --> 00:00:51,1
they either read the manual and iterate

23
00:00:51,11 --> 00:00:55,17
Usually that takes the form of taking an operating systems

24
00:00:55,17 --> 00:00:58,17
class where you actually have to build an operating system

25
00:00:58,17 --> 00:00:59,75
that has paging support

26
00:01:00,54 --> 00:01:00,85
Now,

27
00:01:00,85 --> 00:01:04,26
some operating system classes don't actually teach that way

28
00:01:04,26 --> 00:01:05,7
When I was an undergrad,

29
00:01:05,7 --> 00:01:08,22
my OS class at the University of Minnesota,

30
00:01:08,22 --> 00:01:10,95
didn't teach by forcing us to make an operating system

31
00:01:11,54 --> 00:01:12,95
When I was in grad school,

32
00:01:13,34 --> 00:01:15,94
the undergrad class from cmu,

33
00:01:15,94 --> 00:01:17,48
which I took as a grad student,

34
00:01:17,49 --> 00:01:19,84
did force me to make an OS class

35
00:01:19,84 --> 00:01:21,22
And quite frankly,

36
00:01:21,22 --> 00:01:24,9
I attribute that class two significant growth in my technical

37
00:01:24,9 --> 00:01:27,11
capability because it's sort of,

38
00:01:27,11 --> 00:01:27,56
you know,

39
00:01:27,94 --> 00:01:30,66
beat me into understanding the architecture very well

40
00:01:31,04 --> 00:01:33,95
So if you don't have time for taking a full

41
00:01:33,95 --> 00:01:35,45
operating systems class,

42
00:01:35,81 --> 00:01:38,55
then you should at least try to have time to

43
00:01:38,55 --> 00:01:40,06
take this class twice

44
00:01:40,07 --> 00:01:41,01
So other than that,

45
00:01:41,02 --> 00:01:43,2
we mostly just looked at a ton of tables and

46
00:01:43,2 --> 00:01:45,13
a ton of entries in those tables

47
00:01:45,13 --> 00:01:45,97
In this section,

48
00:01:45,97 --> 00:01:47,86
there's not a lot of new assembly instructions

49
00:01:48,24 --> 00:01:49,88
Just the new mov register,

50
00:01:49,88 --> 00:01:54,26
64-bit into control registers or out of control registers

51
00:01:55,04 --> 00:01:59,89
and the invalidate page assembly instruction for forcing out global

52
00:01:59,89 --> 00:02:01,86
pages from the TLB

