1
00:00:00,080 --> 00:00:04,080
now in the interest of time I'm not

2
00:00:02,240 --> 00:00:06,720
going to go into the

3
00:00:04,080 --> 00:00:07,919
page table level view of this particular

4
00:00:06,720 --> 00:00:09,840
permutation

5
00:00:07,919 --> 00:00:11,360
I just want to give you the intuition of

6
00:00:09,840 --> 00:00:13,679
what the point of it was

7
00:00:11,360 --> 00:00:15,920
because this was the original way to

8
00:00:13,679 --> 00:00:17,920
move from 32-bit linear address space to

9
00:00:15,920 --> 00:00:18,560
a larger than 32-bit linear address

10
00:00:17,920 --> 00:00:21,199
space

11
00:00:18,560 --> 00:00:22,240
originally 36 and now whatever

12
00:00:21,199 --> 00:00:23,920
MAXPHYSADDR is

13
00:00:22,240 --> 00:00:25,519
so this was the physical address

14
00:00:23,920 --> 00:00:28,160
extensions

15
00:00:25,519 --> 00:00:29,199
and so let's see and understand what the

16
00:00:28,160 --> 00:00:31,760
point of this was

17
00:00:29,199 --> 00:00:32,399
because eventually I mean I'll just tell

18
00:00:31,760 --> 00:00:34,880
you the

19
00:00:32,399 --> 00:00:36,640
the point was that eventually 32-bit

20
00:00:34,880 --> 00:00:37,440
systems started running up against the

21
00:00:36,640 --> 00:00:39,920
fact that

22
00:00:37,440 --> 00:00:41,680
people could afford more than thirty

23
00:00:39,920 --> 00:00:43,760
4 gigabytes of RAM

24
00:00:41,680 --> 00:00:46,480
and consequently they were essentially

25
00:00:43,760 --> 00:00:48,239
limited that their processor could only

26
00:00:46,480 --> 00:00:50,480
do you know under these sort of paging

27
00:00:48,239 --> 00:00:51,360
mechanisms your processor can only do 32

28
00:00:50,480 --> 00:00:53,600
to 32

29
00:00:51,360 --> 00:00:56,000
so you could never even access more than

30
00:00:53,600 --> 00:00:58,239
32 bits of physical RAM

31
00:00:56,000 --> 00:00:59,680
so this was basically a sort of

32
00:00:58,239 --> 00:01:02,800
workaround until

33
00:00:59,680 --> 00:01:04,400
people got full 64-bit systems and also

34
00:01:02,800 --> 00:01:06,400
from an operating system maker's

35
00:01:04,400 --> 00:01:07,920
perspective it was nice because they

36
00:01:06,400 --> 00:01:09,280
didn't have to rewrite their entire

37
00:01:07,920 --> 00:01:10,479
operating system to support all the

38
00:01:09,280 --> 00:01:12,240
64-bit stuff

39
00:01:10,479 --> 00:01:14,000
they could make a much smaller set of

40
00:01:12,240 --> 00:01:15,040
changes just to the paging

41
00:01:14,000 --> 00:01:17,200
infrastructure

42
00:01:15,040 --> 00:01:19,119
in order to ultimately access more than

43
00:01:17,200 --> 00:01:22,000
4 gigabytes of memory

44
00:01:19,119 --> 00:01:22,720
so the intuition here is I said you know

45
00:01:22,000 --> 00:01:24,479
originally

46
00:01:22,720 --> 00:01:26,000
everyone had less than you know 4

47
00:01:24,479 --> 00:01:28,560
gigs of RAM and

48
00:01:26,000 --> 00:01:29,360
so this you know first notion of virtual

49
00:01:28,560 --> 00:01:31,360
address space

50
00:01:29,360 --> 00:01:32,560
was the notion that you could ask for

51
00:01:31,360 --> 00:01:34,880
something in the

52
00:01:32,560 --> 00:01:36,000
full four 4 gigabyte uh linear

53
00:01:34,880 --> 00:01:38,799
address space

54
00:01:36,000 --> 00:01:39,280
you could ask for ffff for instance but

55
00:01:38,799 --> 00:01:41,119
then

56
00:01:39,280 --> 00:01:42,560
through the magic of page tables that

57
00:01:41,119 --> 00:01:44,399
would get translated into

58
00:01:42,560 --> 00:01:46,799
something that was actually within the

59
00:01:44,399 --> 00:01:49,520
space that you had physical RAM for

60
00:01:46,799 --> 00:01:49,840
such as 7fff and that would give

61
00:01:49,520 --> 00:01:52,000
you

62
00:01:49,840 --> 00:01:54,399
you know accessing the 2 megabytes 2

63
00:01:52,000 --> 00:01:56,479
gigabytes of RAM

64
00:01:54,399 --> 00:01:58,079
now this page table expansion is not

65
00:01:56,479 --> 00:02:00,240
meant to indicate that you know

66
00:01:58,079 --> 00:02:01,119
your page tables got bigger which they

67
00:02:00,240 --> 00:02:02,960
might have but

68
00:02:01,119 --> 00:02:04,560
all I'm trying to show here is how much

69
00:02:02,960 --> 00:02:06,719
of the linear address space that

70
00:02:04,560 --> 00:02:08,560
this page table can actually access and

71
00:02:06,719 --> 00:02:10,479
it has to do with you know the

72
00:02:08,560 --> 00:02:11,599
size of your RAM versus the size of your

73
00:02:10,479 --> 00:02:13,680
linear address space

74
00:02:11,599 --> 00:02:15,520
so this is the easy case when people

75
00:02:13,680 --> 00:02:16,480
could eventually afford 4 gigabytes

76
00:02:15,520 --> 00:02:18,879
of RAM

77
00:02:16,480 --> 00:02:20,160
then you had the option and should the

78
00:02:18,879 --> 00:02:21,599
OS want to

79
00:02:20,160 --> 00:02:23,360
to make it so that there was you know

80
00:02:21,599 --> 00:02:25,040
one-to-one mapping so the operating

81
00:02:23,360 --> 00:02:28,239
system would say give me

82
00:02:25,040 --> 00:02:28,640
memory at ffff and that could go exactly

83
00:02:28,239 --> 00:02:31,360
through

84
00:02:28,640 --> 00:02:32,640
to the RAM at ffff modulo stuff that

85
00:02:31,360 --> 00:02:34,879
you'll learn about

86
00:02:32,640 --> 00:02:36,720
in the architecture 4000 series about

87
00:02:34,879 --> 00:02:38,000
how bios works so that would never

88
00:02:36,720 --> 00:02:40,640
actually be mapped to RAM

89
00:02:38,000 --> 00:02:41,040
but that's neither here nor there the

90
00:02:40,640 --> 00:02:43,920
point

91
00:02:41,040 --> 00:02:45,599
is we want to talk about the case of

92
00:02:43,920 --> 00:02:49,120
8 gigabytes of RAM

93
00:02:45,599 --> 00:02:51,200
on a 32-bit system you've only got a

94
00:02:49,120 --> 00:02:53,200
32-bit linear address space so there's

95
00:02:51,200 --> 00:02:54,560
no way you can possibly do a one-to-one

96
00:02:53,200 --> 00:02:59,280
mapping of 32-bit

97
00:02:54,560 --> 00:03:02,159
addresses to you know 33 bits of RAM

98
00:02:59,280 --> 00:03:02,560
so you know someone like gilgamesh has

99
00:03:02,159 --> 00:03:05,920
his

100
00:03:02,560 --> 00:03:07,680
extra RAM now the processor sorry the

101
00:03:05,920 --> 00:03:09,599
operating system the processor could say

102
00:03:07,680 --> 00:03:11,040
you know give me memory at address

103
00:03:09,599 --> 00:03:13,360
0xC0000000 which is the 3

104
00:03:11,040 --> 00:03:15,360
gigabyte and that could go through the

105
00:03:13,360 --> 00:03:16,000
set first set of page tables fine and

106
00:03:15,360 --> 00:03:18,800
that would

107
00:03:16,000 --> 00:03:20,480
you know work out just fine but if the

108
00:03:18,800 --> 00:03:22,800
processor said you know that

109
00:03:20,480 --> 00:03:24,000
the processor literally cannot say

110
00:03:22,800 --> 00:03:26,080
please give me

111
00:03:24,000 --> 00:03:28,159
memory at address 7 gigabytes because

112
00:03:26,080 --> 00:03:31,040
this is greater than a 32-bit

113
00:03:28,159 --> 00:03:32,480
address so if it tries that then a whole

114
00:03:31,040 --> 00:03:34,239
bunch of nothing's going to happen it

115
00:03:32,480 --> 00:03:36,239
literally you know registers don't hold

116
00:03:34,239 --> 00:03:38,799
more than 32 bits it would have just

117
00:03:36,239 --> 00:03:40,480
wrapped around and truncated anyways but

118
00:03:38,799 --> 00:03:42,799
the point of this physical address

119
00:03:40,480 --> 00:03:44,080
extensions in the original 32-bit

120
00:03:42,799 --> 00:03:47,200
incarnation

121
00:03:44,080 --> 00:03:50,239
was the notion that an operating system

122
00:03:47,200 --> 00:03:52,400
could create a page table

123
00:03:50,239 --> 00:03:54,319
that could actually do the work of

124
00:03:52,400 --> 00:03:58,000
translating something like give me

125
00:03:54,319 --> 00:04:00,560
memory at 0xC0000000 which would be

126
00:03:58,000 --> 00:04:02,159
originally 3 gigabytes but the page

127
00:04:00,560 --> 00:04:03,519
table could say you know what I'm

128
00:04:02,159 --> 00:04:07,040
actually going to make that

129
00:04:03,519 --> 00:04:09,280
access physical RAM at 7 gigabytes

130
00:04:07,040 --> 00:04:10,400
and in so doing now all of a sudden your

131
00:04:09,280 --> 00:04:13,599
32-bit you know

132
00:04:10,400 --> 00:04:15,519
nominally 32-bit processor is actually

133
00:04:13,599 --> 00:04:16,880
able to access all the RAM and your

134
00:04:15,519 --> 00:04:19,120
operating system can

135
00:04:16,880 --> 00:04:21,280
not have to use virtual memory in the

136
00:04:19,120 --> 00:04:23,040
swapping to disk sense of the word

137
00:04:21,280 --> 00:04:24,320
and you know people are obviously happy

138
00:04:23,040 --> 00:04:26,160
that you know

139
00:04:24,320 --> 00:04:27,440
they put more RAM into their system and

140
00:04:26,160 --> 00:04:29,360
it's actually using it

141
00:04:27,440 --> 00:04:30,639
whereas if you didn't have an operating

142
00:04:29,360 --> 00:04:31,919
system that was using this physical

143
00:04:30,639 --> 00:04:33,919
address extensions

144
00:04:31,919 --> 00:04:35,199
it was straight up just sitting there

145
00:04:33,919 --> 00:04:36,639
doing nothing

146
00:04:35,199 --> 00:04:38,240
so that was the original point of

147
00:04:36,639 --> 00:04:38,639
physical address extensions if you'd

148
00:04:38,240 --> 00:04:41,199
like

149
00:04:38,639 --> 00:04:42,880
after later on you can definitely go

150
00:04:41,199 --> 00:04:46,880
back and look at the manual to see how

151
00:04:42,880 --> 00:04:46,880
the page tables all break down for that

