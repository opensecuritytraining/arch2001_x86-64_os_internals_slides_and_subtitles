1
00:00:00,080 --> 00:00:04,319
okay so let's do a quick little lab to

2
00:00:02,240 --> 00:00:05,359
have you look at the control register

3
00:00:04,319 --> 00:00:07,120
values

4
00:00:05,359 --> 00:00:08,639
so I'm going to want you to answer

5
00:00:07,120 --> 00:00:10,719
questions again it'll be the ones on the

6
00:00:08,639 --> 00:00:12,160
website not on these slides but

7
00:00:10,719 --> 00:00:14,799
you know go check which of these bits

8
00:00:12,160 --> 00:00:18,000
are set paging write protect

9
00:00:14,799 --> 00:00:18,560
protection enable six 57-bit linear

10
00:00:18,000 --> 00:00:20,960
addresses

11
00:00:18,560 --> 00:00:22,080
and so forth so to do that we're going

12
00:00:20,960 --> 00:00:24,000
to go into our

13
00:00:22,080 --> 00:00:25,359
WinDbg registers window and you can

14
00:00:24,000 --> 00:00:26,400
either scroll all the way down and

15
00:00:25,359 --> 00:00:27,680
you'll start seeing the control

16
00:00:26,400 --> 00:00:30,080
registers or you can

17
00:00:27,680 --> 00:00:31,920
edit it and force them up to the top and

18
00:00:30,080 --> 00:00:32,880
furthermore to make things a little bit

19
00:00:31,920 --> 00:00:36,079
easier on you

20
00:00:32,880 --> 00:00:38,800
you can use this dx command and at

21
00:00:36,079 --> 00:00:40,559
in a control register in binary format

22
00:00:38,800 --> 00:00:42,000
and that'll make it easy it'll just spit

23
00:00:40,559 --> 00:00:43,520
out the register in binary format to

24
00:00:42,000 --> 00:00:45,440
make it easier for you to

25
00:00:43,520 --> 00:00:46,640
go back and check you know what is bit

26
00:00:45,440 --> 00:00:51,280
0 what is bit

27
00:00:46,640 --> 00:00:54,800
you know whatever okay so let's do that

28
00:00:51,280 --> 00:00:59,120
all right so break into your

29
00:00:54,800 --> 00:00:59,520
debuggee and if you scroll all the way

30
00:00:59,120 --> 00:01:01,680
down

31
00:00:59,520 --> 00:01:03,039
you'll get from your general purpose

32
00:01:01,680 --> 00:01:05,119
registers down here

33
00:01:03,039 --> 00:01:06,400
you'll find the control register 0,

34
00:01:05,119 --> 00:01:08,960
2, 3, 4

35
00:01:06,400 --> 00:01:10,640
also you could just hit customize and at

36
00:01:08,960 --> 00:01:13,439
the very top you could put

37
00:01:10,640 --> 00:01:13,840
CR0 something like that but I prefer to

38
00:01:13,439 --> 00:01:16,560
just

39
00:01:13,840 --> 00:01:16,960
scroll all the way down and then like I

40
00:01:16,560 --> 00:01:20,880
said

41
00:01:16,960 --> 00:01:24,080
the alternative is use dx at cr0

42
00:01:20,880 --> 00:01:26,240
comma b for binary and it'll spit it out

43
00:01:24,080 --> 00:01:27,680
all in binary for you so that you can

44
00:01:26,240 --> 00:01:31,920
answer the questions that

45
00:01:27,680 --> 00:01:31,920
are in the website

