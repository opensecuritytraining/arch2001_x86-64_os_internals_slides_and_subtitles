1
00:00:00,000 --> 00:00:03,679
so what did the page map level 4 entry

2
00:00:02,480 --> 00:00:07,120
give us it gave us

3
00:00:03,679 --> 00:00:09,679
40 bits of a physical address where the

4
00:00:07,120 --> 00:00:11,280
MMU could find the page directory

5
00:00:09,679 --> 00:00:14,480
pointer table

6
00:00:11,280 --> 00:00:17,199
the MMU can take 9 bits from here

7
00:00:14,480 --> 00:00:18,560
and use it to index to here the page

8
00:00:17,199 --> 00:00:21,359
directory pointer table

9
00:00:18,560 --> 00:00:22,720
entry I feel like we should enhance so

10
00:00:21,359 --> 00:00:25,199
let's go ahead and

11
00:00:22,720 --> 00:00:25,199
enhance

12
00:00:27,359 --> 00:00:31,359
but what's this instead of some dry page

13
00:00:30,480 --> 00:00:33,360
table bits

14
00:00:31,359 --> 00:00:34,399
we are presented with this charming

15
00:00:33,360 --> 00:00:36,880
scene

16
00:00:34,399 --> 00:00:37,760
childhood in which a child asks her

17
00:00:36,880 --> 00:00:40,320
mother

18
00:00:37,760 --> 00:00:41,760
mommy where do 1 gigabyte pages come

19
00:00:40,320 --> 00:00:44,000
from

20
00:00:41,760 --> 00:00:45,760
and so I was just looking for a picture

21
00:00:44,000 --> 00:00:47,360
like this where a child could ask you

22
00:00:45,760 --> 00:00:49,840
know the parent a question

23
00:00:47,360 --> 00:00:50,719
and this particular picture really stood

24
00:00:49,840 --> 00:00:52,559
out to me

25
00:00:50,719 --> 00:00:54,879
most of the pictures go ahead and go do

26
00:00:52,559 --> 00:00:57,600
this yourself go search google images

27
00:00:54,879 --> 00:00:59,680
for you know mother talking to child

28
00:00:57,600 --> 00:01:01,359
and this particular picture jumps out

29
00:00:59,680 --> 00:01:03,600
because most pictures are so

30
00:01:01,359 --> 00:01:05,680
like smiling mother just unreasonably

31
00:01:03,600 --> 00:01:07,840
happy smiling children

32
00:01:05,680 --> 00:01:08,720
but let's look at this mother this

33
00:01:07,840 --> 00:01:10,320
mother's face

34
00:01:08,720 --> 00:01:12,000
is quite interesting and now my wife

35
00:01:10,320 --> 00:01:16,000
just said no no she's just looking

36
00:01:12,000 --> 00:01:19,200
at the child with love I'm not so sure

37
00:01:16,000 --> 00:01:22,560
enhance this lady

38
00:01:19,200 --> 00:01:25,040
is not smiling this is a slight

39
00:01:22,560 --> 00:01:28,080
downward turn of the lip not an upward

40
00:01:25,040 --> 00:01:31,200
turn there are no smile wrinkles here

41
00:01:28,080 --> 00:01:33,920
around the eyes this woman

42
00:01:31,200 --> 00:01:35,119
is questioning her life choices this

43
00:01:33,920 --> 00:01:37,600
woman is asking

44
00:01:35,119 --> 00:01:39,119
where it all went wrong and you know

45
00:01:37,600 --> 00:01:40,000
what's wrong with this child that she

46
00:01:39,119 --> 00:01:43,280
doesn't understand

47
00:01:40,000 --> 00:01:45,520
where 1 gigabyte pages come from

48
00:01:43,280 --> 00:01:47,600
now that's just my interpretation it's

49
00:01:45,520 --> 00:01:49,600
entirely possible that she's just having

50
00:01:47,600 --> 00:01:50,560
flashbacks to her operating systems

51
00:01:49,600 --> 00:01:53,920
class in which

52
00:01:50,560 --> 00:01:56,320
PTEs PML4Es PDEs are

53
00:01:53,920 --> 00:01:57,200
dropping out of the system you know left

54
00:01:56,320 --> 00:01:59,360
and right and

55
00:01:57,200 --> 00:02:02,000
you know she's utterly traumatized from

56
00:01:59,360 --> 00:02:02,000
this event

57
00:02:02,620 --> 00:02:05,769
[Music]

58
00:02:07,280 --> 00:02:10,959
so there's multiple different

59
00:02:09,200 --> 00:02:12,160
interpretations for what's going on in

60
00:02:10,959 --> 00:02:15,920
this particular picture

61
00:02:12,160 --> 00:02:18,000
but ultimately she she sighs

62
00:02:15,920 --> 00:02:20,000
and she thinks she's going to break but

63
00:02:18,000 --> 00:02:23,280
then she puts on a brave face and

64
00:02:20,000 --> 00:02:27,120
she tells her child PTD

65
00:02:23,280 --> 00:02:28,800
PE.PS equals 1 child

66
00:02:27,120 --> 00:02:30,720
but maybe I was over interpreting it

67
00:02:28,800 --> 00:02:32,400
anyways so there's actually 3

68
00:02:30,720 --> 00:02:34,720
possible interpretations

69
00:02:32,400 --> 00:02:36,560
for the page directory pointer table

70
00:02:34,720 --> 00:02:39,120
entry there is the

71
00:02:36,560 --> 00:02:40,800
present bit is 0 version of this as

72
00:02:39,120 --> 00:02:41,760
well but I of course just left that out

73
00:02:40,800 --> 00:02:43,840
because that's again

74
00:02:41,760 --> 00:02:46,000
if it's 0 then all of this is just

75
00:02:43,840 --> 00:02:47,440
ignored and any attempt at usage leads

76
00:02:46,000 --> 00:02:50,400
to a page fault

77
00:02:47,440 --> 00:02:51,599
so let's see these 2 interpretations

78
00:02:50,400 --> 00:02:53,519
the difference between these 2

79
00:02:51,599 --> 00:02:56,959
interpretations is bit 7

80
00:02:53,519 --> 00:02:58,720
the page size bit and the page size bit

81
00:02:56,959 --> 00:03:00,879
is what indicates whether or not this

82
00:02:58,720 --> 00:03:02,000
entire entry is pointing at a 1

83
00:03:00,879 --> 00:03:04,319
gigabyte

84
00:03:02,000 --> 00:03:06,640
page frame or whether it is pointing to

85
00:03:04,319 --> 00:03:09,680
the next level of table translation

86
00:03:06,640 --> 00:03:13,120
the page directory so if

87
00:03:09,680 --> 00:03:15,840
PS is 1 then these bits so the bottom

88
00:03:13,120 --> 00:03:18,319
30 bits are assumed to be 0 so 0

89
00:03:15,840 --> 00:03:21,440
through 29 are all assumed to be 0

90
00:03:18,319 --> 00:03:23,519
and the top bits from bit 30 up to

91
00:03:21,440 --> 00:03:25,360
M-1 are going to be the address

92
00:03:23,519 --> 00:03:27,360
of the one gigabyte page frame

93
00:03:25,360 --> 00:03:30,080
other bits of interest when it happens

94
00:03:27,360 --> 00:03:33,360
to be this 1 gigabyte version

95
00:03:30,080 --> 00:03:34,080
are the global flag which indicates

96
00:03:33,360 --> 00:03:36,799
whether or not

97
00:03:34,080 --> 00:03:38,480
this virtual to physical translation

98
00:03:36,799 --> 00:03:40,159
that's occurring right now the MMU's

99
00:03:38,480 --> 00:03:43,519
walking through this page table

100
00:03:40,159 --> 00:03:44,560
the G flag is used to do caching and

101
00:03:43,519 --> 00:03:46,879
make sure that

102
00:03:44,560 --> 00:03:48,480
the cache is filled in and left filled

103
00:03:46,879 --> 00:03:50,239
in with the translation

104
00:03:48,480 --> 00:03:52,000
of the particular linear address that we

105
00:03:50,239 --> 00:03:54,400
came to to the ultimate

106
00:03:52,000 --> 00:03:56,239
physical address that this indicates of

107
00:03:54,400 --> 00:03:58,480
a 1 gigabyte page frame

108
00:03:56,239 --> 00:04:00,159
so we'll talk more about the TLB later

109
00:03:58,480 --> 00:04:03,840
on the basic point is

110
00:04:00,159 --> 00:04:04,560
if G is 1 this will be cached and if G

111
00:04:03,840 --> 00:04:06,640
is 0

112
00:04:04,560 --> 00:04:09,519
then it'll still be cached but it'll be

113
00:04:06,640 --> 00:04:12,720
evicted every time CR3 is changed so

114
00:04:09,519 --> 00:04:16,000
whenever you change processes non-global

115
00:04:12,720 --> 00:04:19,440
pages get evicted out of the cache

116
00:04:16,000 --> 00:04:21,759
the D flag the dirty bit flag is used by

117
00:04:19,440 --> 00:04:24,080
operating systems to know whether or not

118
00:04:21,759 --> 00:04:25,600
the particular memory that is pointed to

119
00:04:24,080 --> 00:04:28,400
by this particular frame

120
00:04:25,600 --> 00:04:29,520
has actually been written to by some

121
00:04:28,400 --> 00:04:31,919
code that's run

122
00:04:29,520 --> 00:04:32,800
and it's frequently used for things like

123
00:04:31,919 --> 00:04:35,360
understanding

124
00:04:32,800 --> 00:04:37,040
RAM eviction when the operating system

125
00:04:35,360 --> 00:04:39,280
starts running low on RAM

126
00:04:37,040 --> 00:04:40,320
and it just has to start paging memory

127
00:04:39,280 --> 00:04:42,560
to disk

128
00:04:40,320 --> 00:04:44,560
so you can read more about that there so

129
00:04:42,560 --> 00:04:45,680
intel was actually trying to give us a

130
00:04:44,560 --> 00:04:47,680
hint here

131
00:04:45,680 --> 00:04:49,560
when we had the 1 gigabyte version of

132
00:04:47,680 --> 00:04:53,199
this picture it said

133
00:04:49,560 --> 00:04:55,040
PDPTE with PS equals 1 so I was trying

134
00:04:53,199 --> 00:04:58,080
to say with PS equals 1

135
00:04:55,040 --> 00:04:58,880
then 30 bits are used to index into a 2

136
00:04:58,080 --> 00:05:02,000
to the 30

137
00:04:58,880 --> 00:05:04,720
bit size 1 gigabyte page if

138
00:05:02,000 --> 00:05:05,360
on the other hand the PS bit is equal to

139
00:05:04,720 --> 00:05:08,479
0

140
00:05:05,360 --> 00:05:11,039
then the entry is interpreted this way

141
00:05:08,479 --> 00:05:12,960
all of the present bit read/write bit

142
00:05:11,039 --> 00:05:15,199
user/supervisor bit and XD

143
00:05:12,960 --> 00:05:16,080
bits are interpreted exactly the same

144
00:05:15,199 --> 00:05:19,280
way as we saw

145
00:05:16,080 --> 00:05:22,320
in the PML4E

146
00:05:19,280 --> 00:05:23,840
if the PS is 0 then that also means

147
00:05:22,320 --> 00:05:25,360
that this is going to be the physical

148
00:05:23,840 --> 00:05:28,160
address it's going to be larger than you

149
00:05:25,360 --> 00:05:28,160
saw before before

150
00:05:33,840 --> 00:05:36,960
before it was a small physical address

151
00:05:35,600 --> 00:05:38,720
for 1 gigabyte page

152
00:05:36,960 --> 00:05:40,400
and now it's a larger physical address

153
00:05:38,720 --> 00:05:42,960
to find the next

154
00:05:40,400 --> 00:05:46,160
table in our cavalcade of tables the

155
00:05:42,960 --> 00:05:46,160
page directory

