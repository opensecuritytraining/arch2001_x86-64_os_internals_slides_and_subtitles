1
00:00:00,160 --> 00:00:05,600
so how does the brand new 57-bit linear

2
00:00:03,439 --> 00:00:08,879
addressing mechanism work

3
00:00:05,600 --> 00:00:08,879
well let's dig into it

4
00:00:09,040 --> 00:00:14,960
so this is why I can't stop calling it

5
00:00:11,759 --> 00:00:18,560
last instead of linear address 57.

6
00:00:14,960 --> 00:00:21,600
if cpuid for eax of 7 and ecx of 0

7
00:00:18,560 --> 00:00:24,000
returns ecx bit 16

8
00:00:21,600 --> 00:00:25,920
of 1 that means your processor supports

9
00:00:24,000 --> 00:00:29,279
57-bit linear addresses

10
00:00:25,920 --> 00:00:31,439
you can feel free to update your cpuid

11
00:00:29,279 --> 00:00:32,320
lab in order to see whether you support

12
00:00:31,439 --> 00:00:35,200
it or not

13
00:00:32,320 --> 00:00:36,640
but mine definitely doesn't support it

14
00:00:35,200 --> 00:00:38,640
when you support this and if the

15
00:00:36,640 --> 00:00:40,800
operating system supported it

16
00:00:38,640 --> 00:00:42,640
then you could use 5 level paging

17
00:00:40,800 --> 00:00:45,360
instead of 4 level paging which

18
00:00:42,640 --> 00:00:45,760
is just another level so how does that

19
00:00:45,360 --> 00:00:49,120
work

20
00:00:45,760 --> 00:00:52,640
well you had 48 bits before

21
00:00:49,120 --> 00:00:55,760
and now you have 57 bits right so 0 to

22
00:00:52,640 --> 00:00:58,559
47 is 48 bits 0 to 56

23
00:00:55,760 --> 00:01:00,480
is 57 bits so they just tacked on

24
00:00:58,559 --> 00:01:02,960
another 9 bits on the end

25
00:01:00,480 --> 00:01:05,600
in order to say we will use that to

26
00:01:02,960 --> 00:01:08,479
index into a page map level 5

27
00:01:05,600 --> 00:01:09,200
to find a page map level 5 entry so

28
00:01:08,479 --> 00:01:12,880
CR3

29
00:01:09,200 --> 00:01:17,600
MAXPHYADDR-12 to find the

30
00:01:12,880 --> 00:01:19,840
page map level 5 9-bit index

31
00:01:17,600 --> 00:01:21,280
and then from there same thing you know

32
00:01:19,840 --> 00:01:23,600
MAXPHYADDR-12

33
00:01:21,280 --> 00:01:24,400
to make your way to the page map

34
00:01:23,600 --> 00:01:27,680
level 4

35
00:01:24,400 --> 00:01:29,520
etc I can feel the you know intel manual

36
00:01:27,680 --> 00:01:31,200
people just screaming as they

37
00:01:29,520 --> 00:01:33,520
run out of room and they're like ah we

38
00:01:31,200 --> 00:01:35,439
don't have enough space

39
00:01:33,520 --> 00:01:37,680
that's why they also don't have this as

40
00:01:35,439 --> 00:01:40,079
MAXPHYADDR-12

41
00:01:37,680 --> 00:01:42,320
but uh but yeah so it's it's not

42
00:01:40,079 --> 00:01:44,399
conceptually difficult at all it's just

43
00:01:42,320 --> 00:01:45,439
the notion of you add more bits and your

44
00:01:44,399 --> 00:01:48,159
processor

45
00:01:45,439 --> 00:01:49,759
MMU is updated to understand it and

46
00:01:48,159 --> 00:01:51,759
walk it

47
00:01:49,759 --> 00:01:53,520
and other than that it's exactly

48
00:01:51,759 --> 00:01:54,000
everything's the same as we learned in

49
00:01:53,520 --> 00:01:57,840
the very

50
00:01:54,000 --> 00:01:59,360
beginning 2 level paging

51
00:01:57,840 --> 00:02:01,439
now the one little point I would make

52
00:01:59,360 --> 00:02:03,600
here is that when you turn on page size

53
00:02:01,439 --> 00:02:05,439
extensions at least for now

54
00:02:03,600 --> 00:02:07,040
it doesn't support anything larger than

55
00:02:05,439 --> 00:02:09,679
1 gigabyte pages

56
00:02:07,040 --> 00:02:10,479
so they could definitely have done again

57
00:02:09,679 --> 00:02:13,120
the same

58
00:02:10,479 --> 00:02:14,160
you know game of accumulating up some

59
00:02:13,120 --> 00:02:17,280
number of bits

60
00:02:14,160 --> 00:02:22,720
and having a super super giant thing for

61
00:02:17,280 --> 00:02:25,040
512 gigabyte pages but they didn't

62
00:02:22,720 --> 00:02:27,120
now just as optional material I will

63
00:02:25,040 --> 00:02:29,520
point out that I eventually found

64
00:02:27,120 --> 00:02:30,160
this table which basically is exactly

65
00:02:29,520 --> 00:02:32,640
the same

66
00:02:30,160 --> 00:02:33,200
as the table that I just made myself and

67
00:02:32,640 --> 00:02:34,800
so

68
00:02:33,200 --> 00:02:36,480
I was super pissed when I eventually

69
00:02:34,800 --> 00:02:37,280
found this after having done all the

70
00:02:36,480 --> 00:02:40,000
work to

71
00:02:37,280 --> 00:02:40,800
make that table myself but here you go

72
00:02:40,000 --> 00:02:42,720
this table

73
00:02:40,800 --> 00:02:44,560
it's in the manual somewhere you can

74
00:02:42,720 --> 00:02:47,360
check it out and it's got a bunch of

75
00:02:44,560 --> 00:02:50,160
nice notes for you

76
00:02:47,360 --> 00:02:51,840
finally there's this diagram just

77
00:02:50,160 --> 00:02:54,000
another finite state machine

78
00:02:51,840 --> 00:02:55,920
which is you know beautiful you know I

79
00:02:54,000 --> 00:02:59,440
don't know what you see there but

80
00:02:55,920 --> 00:03:00,879
I think it's just great it's a hypercube

81
00:02:59,440 --> 00:03:04,000
I think it's a hypercube

82
00:03:00,879 --> 00:03:05,760
anyways so it uh it starts out

83
00:03:04,000 --> 00:03:07,120
with no paging and so the question is

84
00:03:05,760 --> 00:03:08,080
you know how do you get to these various

85
00:03:07,120 --> 00:03:10,000
paging states

86
00:03:08,080 --> 00:03:11,360
and you know this would be completely

87
00:03:10,000 --> 00:03:13,360
incomprehensible

88
00:03:11,360 --> 00:03:14,720
had we not covered some of this material

89
00:03:13,360 --> 00:03:17,200
but it's actually

90
00:03:14,720 --> 00:03:20,239
not too bad here so start out with no

91
00:03:17,200 --> 00:03:22,800
paging CR4 is 0 so this is your reset

92
00:03:20,239 --> 00:03:25,840
state if you reset the processor

93
00:03:22,800 --> 00:03:28,239
CR4 is 0 CR4 page is 0 so

94
00:03:25,840 --> 00:03:29,040
paging is 0 physical address

95
00:03:28,239 --> 00:03:32,319
extensions

96
00:03:29,040 --> 00:03:34,319
is 0 long mode enable is 0 if you

97
00:03:32,319 --> 00:03:37,040
just set paging by itself

98
00:03:34,319 --> 00:03:38,080
boom you're into 32-bit paging and it's

99
00:03:37,040 --> 00:03:40,000
all good that's the

100
00:03:38,080 --> 00:03:41,920
simple 2-level paging that's what

101
00:03:40,000 --> 00:03:44,000
everybody used for a long time

102
00:03:41,920 --> 00:03:46,080
from there if you set physical address

103
00:03:44,000 --> 00:03:50,319
extensions now you've got this option

104
00:03:46,080 --> 00:03:52,319
for 32 bits to originally 36 bits and

105
00:03:50,319 --> 00:03:54,799
now you know 52 bits

106
00:03:52,319 --> 00:03:55,599
so backing up a second if you want to

107
00:03:54,799 --> 00:03:57,599
get to 4-

108
00:03:55,599 --> 00:03:58,799
level paging there's multiple routes

109
00:03:57,599 --> 00:04:01,200
here so

110
00:03:58,799 --> 00:04:03,040
you can first set the long mode enable

111
00:04:01,200 --> 00:04:04,879
in the register and then you can set

112
00:04:03,040 --> 00:04:06,080
physical address extensions and then you

113
00:04:04,879 --> 00:04:07,840
can set paging

114
00:04:06,080 --> 00:04:09,920
and then you'll be in 4-level paging

115
00:04:07,840 --> 00:04:11,920
or instead you can fit set physical

116
00:04:09,920 --> 00:04:12,480
address extensions then set long mode

117
00:04:11,920 --> 00:04:14,640
enable

118
00:04:12,480 --> 00:04:15,760
then set paging and you'll be in

119
00:04:14,640 --> 00:04:19,440
4-level paging so

120
00:04:15,760 --> 00:04:21,040
two routes same direction to get into

121
00:04:19,440 --> 00:04:23,199
4-level paging

122
00:04:21,040 --> 00:04:24,479
and I believe this wasn't on here I'm

123
00:04:23,199 --> 00:04:26,800
pretty sure I added this in

124
00:04:24,479 --> 00:04:29,040
so this diagram is from the manual and

125
00:04:26,800 --> 00:04:30,639
here's my little extra addition here

126
00:04:29,040 --> 00:04:32,720
so if you want to get to 5-level

127
00:04:30,639 --> 00:04:33,840
paging you would start from this

128
00:04:32,720 --> 00:04:36,320
intermediate state

129
00:04:33,840 --> 00:04:38,639
you would set LA57 and you would still

130
00:04:36,320 --> 00:04:39,840
be in a no paging state and finally as a

131
00:04:38,639 --> 00:04:42,240
last thing you would

132
00:04:39,840 --> 00:04:46,080
set paging equal to 1 and now you

133
00:04:42,240 --> 00:04:46,080
would be in 5-level paging

