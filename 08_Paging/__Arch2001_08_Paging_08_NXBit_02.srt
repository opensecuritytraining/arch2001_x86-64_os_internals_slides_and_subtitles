1
00:00:00,000 --> 00:00:03,120
all right so now a couple labs in order

2
00:00:02,720 --> 00:00:06,319
to

3
00:00:03,120 --> 00:00:09,200
understand the NX bit so the first thing

4
00:00:06,319 --> 00:00:11,519
is to go run rdmsr on that EFER

5
00:00:09,200 --> 00:00:13,280
register and check the bits to find out

6
00:00:11,519 --> 00:00:14,320
whether or not it says that XD is

7
00:00:13,280 --> 00:00:15,679
enabled

8
00:00:14,320 --> 00:00:17,840
and then also while you're in the

9
00:00:15,679 --> 00:00:20,000
debugger go ahead and run pte

10
00:00:17,840 --> 00:00:21,359
rsp to find out whether or not your

11
00:00:20,000 --> 00:00:24,000
kernel stack

12
00:00:21,359 --> 00:00:24,960
is non-executable so let's go ahead and

13
00:00:24,000 --> 00:00:28,560
do that now

14
00:00:24,960 --> 00:00:31,840
so let's go ahead and start our debugger

15
00:00:28,560 --> 00:00:31,840
and break in

16
00:00:33,120 --> 00:00:40,399
gonna do rdmsr c0000080

17
00:00:41,120 --> 00:00:44,160
and then what are we looking for we're

18
00:00:42,879 --> 00:00:49,039
looking for

19
00:00:44,160 --> 00:00:49,039
whether or not bit 11 is set

20
00:00:53,280 --> 00:00:59,840
so we go ahead and interpret this

21
00:00:56,480 --> 00:00:59,840
as bits

22
00:01:02,399 --> 00:01:09,520
and so this is bit 7

23
00:01:05,760 --> 00:01:10,640
and so 8 9 10 11. this bit is set and

24
00:01:09,520 --> 00:01:13,360
therefore NX

25
00:01:10,640 --> 00:01:13,920
is enabled now is our stack in the

26
00:01:13,360 --> 00:01:16,320
kernel

27
00:01:13,920 --> 00:01:18,320
non-executable so we can just go ahead

28
00:01:16,320 --> 00:01:21,040
and do pte

29
00:01:18,320 --> 00:01:21,040
rsp

30
00:01:21,439 --> 00:01:24,880
and if we look at the last entry we see

31
00:01:24,240 --> 00:01:28,000
that the

32
00:01:24,880 --> 00:01:30,320
E for execute is not set you can see

33
00:01:28,000 --> 00:01:33,360
that higher pages have execute so

34
00:01:30,320 --> 00:01:35,360
that does not have the NX bit set this

35
00:01:33,360 --> 00:01:37,040
doesn't have the NX bit set this doesn't

36
00:01:35,360 --> 00:01:38,320
have the NX bit set so they all show up

37
00:01:37,040 --> 00:01:41,360
as executable

38
00:01:38,320 --> 00:01:42,560
but the last page table entry does have

39
00:01:41,360 --> 00:01:44,640
the NX bit set

40
00:01:42,560 --> 00:01:47,280
and so our stack is non-executable in

41
00:01:44,640 --> 00:01:47,280
the kernel

