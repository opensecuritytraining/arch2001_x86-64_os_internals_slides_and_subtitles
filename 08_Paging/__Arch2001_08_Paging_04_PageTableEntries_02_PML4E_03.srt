1
00:00:00,240 --> 00:00:06,319
okay for my page map level 4 entry

2
00:00:03,280 --> 00:00:06,960
I can see that bit 1 is set or sorry

3
00:00:06,319 --> 00:00:09,120
bit 0

4
00:00:06,960 --> 00:00:10,480
is set so this is 3 so that would be

5
00:00:09,120 --> 00:00:14,000
00

6
00:00:10,480 --> 00:00:17,199
11 so present bit is set

7
00:00:14,000 --> 00:00:20,080
that is as expected read/write bit

8
00:00:17,199 --> 00:00:22,240
is 1 because this is 3 so 11

9
00:00:20,080 --> 00:00:23,600
so 1 means that it's writable so it's

10
00:00:22,240 --> 00:00:25,840
readable/writable

11
00:00:23,600 --> 00:00:28,080
and sorry I should say that the present

12
00:00:25,840 --> 00:00:31,199
bit of equal to 1 up here

13
00:00:28,080 --> 00:00:32,800
is referenced as a V here in WinDbg's

14
00:00:31,199 --> 00:00:34,480
output valid output

15
00:00:32,800 --> 00:00:36,640
so they provide you a little sort of

16
00:00:34,480 --> 00:00:38,160
mnemonics here to try to make it easier

17
00:00:36,640 --> 00:00:41,440
so that you don't have to always

18
00:00:38,160 --> 00:00:42,079
parse the entries using the raw bit

19
00:00:41,440 --> 00:00:45,120
fields

20
00:00:42,079 --> 00:00:48,320
so V for valid and W

21
00:00:45,120 --> 00:00:50,559
for writable and user/supervisor bit

22
00:00:48,320 --> 00:00:51,600
what's set there well that is 0 so

23
00:00:50,559 --> 00:00:54,719
that means not

24
00:00:51,600 --> 00:00:58,239
user but only supervisor so

25
00:00:54,719 --> 00:01:00,239
0 is supervisor which is K

26
00:00:58,239 --> 00:01:01,600
for kernel down here in the WinDbg

27
00:01:00,239 --> 00:01:04,400
output

28
00:01:01,600 --> 00:01:06,720
and then XD at the most significant bit

29
00:01:04,400 --> 00:01:09,360
you can see that that is not set

30
00:01:06,720 --> 00:01:09,840
and therefore it is executable so it's

31
00:01:09,360 --> 00:01:13,040
not

32
00:01:09,840 --> 00:01:15,600
not executable it's not execute disabled

33
00:01:13,040 --> 00:01:17,439
so it's executable and then what is the

34
00:01:15,600 --> 00:01:19,680
physical address of the page directory

35
00:01:17,439 --> 00:01:22,159
pointer table based on this entry

36
00:01:19,680 --> 00:01:24,159
well it was these middle bits here and

37
00:01:22,159 --> 00:01:25,759
we said the 12 bits right here are used

38
00:01:24,159 --> 00:01:27,040
as flags so you can just basically

39
00:01:25,759 --> 00:01:30,799
ignore the 12 bits

40
00:01:27,040 --> 00:01:34,000
so drop off the last 4 hex nibbles

41
00:01:30,799 --> 00:01:36,240
and it's just 4909000

42
00:01:34,000 --> 00:01:38,159
so we just assume zero's for

43
00:01:36,240 --> 00:01:40,560
the bottom that's the physical address

44
00:01:38,159 --> 00:01:42,000
4909000 so that's

45
00:01:40,560 --> 00:01:45,439
how you would interpret

46
00:01:42,000 --> 00:01:48,799
this page map level 4 entry

47
00:01:45,439 --> 00:01:50,399
there's also this pfn page frame number

48
00:01:48,799 --> 00:01:53,439
4909

49
00:01:50,399 --> 00:01:55,360
4909 and that is a

50
00:01:53,439 --> 00:01:57,119
different sort of windows data structure

51
00:01:55,360 --> 00:01:59,360
that can talk about you know how that

52
00:01:57,119 --> 00:02:01,200
particular physical frame is used

53
00:01:59,360 --> 00:02:02,960
and you would learn more about that in a

54
00:02:01,200 --> 00:02:05,920
future windows operating systems

55
00:02:02,960 --> 00:02:05,920
internals class

