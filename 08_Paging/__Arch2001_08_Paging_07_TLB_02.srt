1
00:00:00,080 --> 00:00:03,600
but here's the thing I'm not actually so

2
00:00:02,240 --> 00:00:06,160
big of a nerd that I am

3
00:00:03,600 --> 00:00:06,879
interested in TLBs for their own sake I

4
00:00:06,160 --> 00:00:08,800
don't go

5
00:00:06,879 --> 00:00:10,639
saying to myself oh wow let's check out

6
00:00:08,800 --> 00:00:12,400
this caching mechanism it's so cool it

7
00:00:10,639 --> 00:00:15,440
makes it so efficient

8
00:00:12,400 --> 00:00:18,560
no this was a means to an end

9
00:00:15,440 --> 00:00:20,240
to explain shadow walker to you so

10
00:00:18,560 --> 00:00:22,000
shadow walker is what's actually cool

11
00:00:20,240 --> 00:00:24,480
because shadow walker is TLB

12
00:00:22,000 --> 00:00:26,880
manipulation for fun and profit

13
00:00:24,480 --> 00:00:28,880
basically the idea is that if you had

14
00:00:26,880 --> 00:00:31,760
some malware and it was trying to

15
00:00:28,880 --> 00:00:32,800
hide itself from a memory scanner it

16
00:00:31,760 --> 00:00:34,559
could actually

17
00:00:32,800 --> 00:00:35,840
exploit the fact that there is a

18
00:00:34,559 --> 00:00:38,160
different data

19
00:00:35,840 --> 00:00:38,879
translation lookaside buffer from an

20
00:00:38,160 --> 00:00:41,760
instruction

21
00:00:38,879 --> 00:00:42,879
translation lookaside buffer and in so doing

22
00:00:41,760 --> 00:00:45,920
it could essentially

23
00:00:42,879 --> 00:00:48,320
force to be cached a translation

24
00:00:45,920 --> 00:00:50,239
which means that attempts to execute

25
00:00:48,320 --> 00:00:52,480
would yield the rootkit code

26
00:00:50,239 --> 00:00:53,920
and attempts to read by some security

27
00:00:52,480 --> 00:00:57,199
scanner would read

28
00:00:53,920 --> 00:00:58,079
some benign garbage code or garbage data

29
00:00:57,199 --> 00:01:00,000
doesn't matter what

30
00:00:58,079 --> 00:01:01,359
just doesn't let you see the rootkit so

31
00:01:00,000 --> 00:01:02,800
basically you can think of this as a

32
00:01:01,359 --> 00:01:04,799
stealth malware technique or an

33
00:01:02,800 --> 00:01:07,200
anti-memory forensic technique

34
00:01:04,799 --> 00:01:08,880
so in order to do this what it needs to

35
00:01:07,200 --> 00:01:09,920
do is it has to have some way to

36
00:01:08,880 --> 00:01:12,960
differentiate

37
00:01:09,920 --> 00:01:14,880
execution where it wants to feed

38
00:01:12,960 --> 00:01:16,720
its own assembly instructions so that

39
00:01:14,880 --> 00:01:18,960
its own code runs

40
00:01:16,720 --> 00:01:20,640
from reading of memory which would be

41
00:01:18,960 --> 00:01:23,600
something that it would expect to occur

42
00:01:20,640 --> 00:01:26,080
instead via a security tool such as a

43
00:01:23,600 --> 00:01:27,360
memory scanner or a memory dumper in

44
00:01:26,080 --> 00:01:29,520
order to do this

45
00:01:27,360 --> 00:01:32,240
it actually specifically manipulates the

46
00:01:29,520 --> 00:01:34,479
page table entries for its own code

47
00:01:32,240 --> 00:01:35,280
in order to set the present bit equal to

48
00:01:34,479 --> 00:01:37,920
0

49
00:01:35,280 --> 00:01:38,400
and it essentially wants a page fault to

50
00:01:37,920 --> 00:01:40,720
occur

51
00:01:38,400 --> 00:01:42,640
every single time its own code would be

52
00:01:40,720 --> 00:01:45,840
accessed or its own code would be

53
00:01:42,640 --> 00:01:47,759
read then they have to actually replace

54
00:01:45,840 --> 00:01:48,399
the page fault handler with their own

55
00:01:47,759 --> 00:01:50,240
code

56
00:01:48,399 --> 00:01:52,720
that is going to have special logic to

57
00:01:50,240 --> 00:01:54,479
basically say is it me trying to execute

58
00:01:52,720 --> 00:01:56,079
myself or is it some other tool

59
00:01:54,479 --> 00:01:58,240
trying to read me which I need to hide

60
00:01:56,079 --> 00:02:00,240
from so how would it determine that

61
00:01:58,240 --> 00:02:02,479
it would have the modified page fault

62
00:02:00,240 --> 00:02:03,280
handler examine the eip that was saved

63
00:02:02,479 --> 00:02:05,200
onto the stack

64
00:02:03,280 --> 00:02:07,280
I'm going to be speaking here in 32-bit

65
00:02:05,200 --> 00:02:10,640
terms because that was the original code

66
00:02:07,280 --> 00:02:12,560
32-bit system so it examines the eip

67
00:02:10,640 --> 00:02:13,760
which is pointed onto which is pushed

68
00:02:12,560 --> 00:02:16,720
onto the stack

69
00:02:13,760 --> 00:02:17,040
and if the eip itself is inside of its

70
00:02:16,720 --> 00:02:19,280
own

71
00:02:17,040 --> 00:02:20,720
code memory range it knows that it

72
00:02:19,280 --> 00:02:23,040
itself is trying to

73
00:02:20,720 --> 00:02:25,840
execute itself and therefore it should

74
00:02:23,040 --> 00:02:28,959
go ahead and have the page fault handler

75
00:02:25,840 --> 00:02:30,000
update the page table entry for its

76
00:02:28,959 --> 00:02:32,160
particular code

77
00:02:30,000 --> 00:02:33,920
to point at the real physical address

78
00:02:32,160 --> 00:02:37,120
where its code resides

79
00:02:33,920 --> 00:02:37,760
if instead someone is trying to access

80
00:02:37,120 --> 00:02:40,239
this

81
00:02:37,760 --> 00:02:42,480
and if the assembly instruction is not

82
00:02:40,239 --> 00:02:44,160
within the range of its own code

83
00:02:42,480 --> 00:02:46,080
that means it's some sort of security

84
00:02:44,160 --> 00:02:49,200
scanner coming along and trying to

85
00:02:46,080 --> 00:02:50,160
read its memory if this occurs then it's

86
00:02:49,200 --> 00:02:52,720
going to go ahead

87
00:02:50,160 --> 00:02:54,720
and map the page table entry to some

88
00:02:52,720 --> 00:02:56,480
other garbage piece of memory

89
00:02:54,720 --> 00:02:57,840
so that they don't actually get to see

90
00:02:56,480 --> 00:03:00,159
the attacker code

91
00:02:57,840 --> 00:03:02,080
so here's the ASCII art of doom

92
00:03:00,159 --> 00:03:02,720
basically saying that the page fault

93
00:03:02,080 --> 00:03:05,840
handler

94
00:03:02,720 --> 00:03:08,080
is going to always do this check when a

95
00:03:05,840 --> 00:03:09,680
page fault occurs it's going to say is

96
00:03:08,080 --> 00:03:11,519
this page fault in the range of my

97
00:03:09,680 --> 00:03:13,519
memory that I want to hide let's

98
00:03:11,519 --> 00:03:14,640
imagine that that memory is in frame

99
00:03:13,519 --> 00:03:16,560
number one

100
00:03:14,640 --> 00:03:18,319
if it's in that range then it's going to

101
00:03:16,560 --> 00:03:20,879
fill in the page table entry

102
00:03:18,319 --> 00:03:22,640
that entry this mapping from the virtual

103
00:03:20,879 --> 00:03:23,280
address to this particular physical

104
00:03:22,640 --> 00:03:26,000
address

105
00:03:23,280 --> 00:03:26,959
will be cached inside the ITLB

106
00:03:26,000 --> 00:03:29,120
consequently

107
00:03:26,959 --> 00:03:30,799
the you know page fault handler won't be

108
00:03:29,120 --> 00:03:33,280
invoked the rest of the time

109
00:03:30,799 --> 00:03:35,040
on attempts to execute this code so

110
00:03:33,280 --> 00:03:36,000
it'll actually be you know reasonably

111
00:03:35,040 --> 00:03:37,680
performant

112
00:03:36,000 --> 00:03:40,000
in that it'll just run the code just

113
00:03:37,680 --> 00:03:43,280
fine but the page fault handler

114
00:03:40,000 --> 00:03:44,959
having seen that there was a attempt to

115
00:03:43,280 --> 00:03:46,640
read the data instead of execute the

116
00:03:44,959 --> 00:03:49,599
data would have

117
00:03:46,640 --> 00:03:52,159
filled in the page table entry with a

118
00:03:49,599 --> 00:03:55,040
pointer to some random frame of garbage

119
00:03:52,159 --> 00:03:56,879
and consequently the DTLB would cache

120
00:03:55,040 --> 00:03:58,560
this mapping of a particular virtual

121
00:03:56,879 --> 00:03:59,360
address to that particular physical

122
00:03:58,560 --> 00:04:00,879
address

123
00:03:59,360 --> 00:04:02,640
and now the fact that there's two

124
00:04:00,879 --> 00:04:05,280
different TLBs

125
00:04:02,640 --> 00:04:07,200
is going to mean that there's execute

126
00:04:05,280 --> 00:04:07,760
accesses just always translate through

127
00:04:07,200 --> 00:04:10,080
this one

128
00:04:07,760 --> 00:04:10,799
and automatically performantly execute

129
00:04:10,080 --> 00:04:13,200
the code

130
00:04:10,799 --> 00:04:14,239
and data accesses always just translate

131
00:04:13,200 --> 00:04:16,320
through this one

132
00:04:14,239 --> 00:04:17,600
and automatically hide the code from

133
00:04:16,320 --> 00:04:19,120
security tools

134
00:04:17,600 --> 00:04:20,720
so there are of course a number of

135
00:04:19,120 --> 00:04:21,600
different ways that shadow walker could

136
00:04:20,720 --> 00:04:23,520
be defeated

137
00:04:21,600 --> 00:04:25,280
things like checking the IDT for the

138
00:04:23,520 --> 00:04:26,880
page fault handler to make sure that it

139
00:04:25,280 --> 00:04:28,160
hasn't been redirected out to some

140
00:04:26,880 --> 00:04:29,919
rootkit code and

141
00:04:28,160 --> 00:04:32,240
reality things technologies like

142
00:04:29,919 --> 00:04:32,720
patchguard on on microsoft windows at

143
00:04:32,240 --> 00:04:34,880
least

144
00:04:32,720 --> 00:04:37,040
sort of automatically do this today of

145
00:04:34,880 --> 00:04:37,840
course defeating patchguard is its own

146
00:04:37,040 --> 00:04:40,800
specialty

147
00:04:37,840 --> 00:04:41,120
so if you combined a patchguard defeat

148
00:04:40,800 --> 00:04:43,520
with

149
00:04:41,120 --> 00:04:45,040
a IDT overwrite then you know you could

150
00:04:43,520 --> 00:04:46,400
still open up this technique

151
00:04:45,040 --> 00:04:48,960
beyond the fact that you could change

152
00:04:46,400 --> 00:04:49,600
the IDT entry itself instead an attacker

153
00:04:48,960 --> 00:04:52,400
could have

154
00:04:49,600 --> 00:04:54,080
hooked the actual code the assembly for

155
00:04:52,400 --> 00:04:55,440
the existing page fault handler instead

156
00:04:54,080 --> 00:04:58,080
of changing the table entry

157
00:04:55,440 --> 00:05:00,320
so again you could integrity check that

158
00:04:58,080 --> 00:05:01,280
security tools could manually flush the

159
00:05:00,320 --> 00:05:03,600
data TLB

160
00:05:01,280 --> 00:05:05,280
before or sorry flush the TLB in general

161
00:05:03,600 --> 00:05:06,400
using you know the invalidate page

162
00:05:05,280 --> 00:05:08,639
assembly instruction

163
00:05:06,400 --> 00:05:10,479
before scanning memory security tools

164
00:05:08,639 --> 00:05:12,639
could make their own virtual to physical

165
00:05:10,479 --> 00:05:13,919
address translations to basically walk

166
00:05:12,639 --> 00:05:14,880
through all of the different physical

167
00:05:13,919 --> 00:05:17,360
addresses

168
00:05:14,880 --> 00:05:18,240
there's paging profiling that could

169
00:05:17,360 --> 00:05:21,120
occur

170
00:05:18,240 --> 00:05:22,880
or the security software could use DMA

171
00:05:21,120 --> 00:05:24,639
direct memory access or

172
00:05:22,880 --> 00:05:26,240
cold boot where you physically yank out

173
00:05:24,639 --> 00:05:28,320
the RAM and read the data

174
00:05:26,240 --> 00:05:30,479
because neither of those mechanisms are

175
00:05:28,320 --> 00:05:31,360
subject to TLB translation the same way

176
00:05:30,479 --> 00:05:33,120
that a

177
00:05:31,360 --> 00:05:35,440
security software running from within

178
00:05:33,120 --> 00:05:37,039
the kernel is and just in general

179
00:05:35,440 --> 00:05:39,039
you know those techniques are more

180
00:05:37,039 --> 00:05:40,720
forensically sound the only reason that

181
00:05:39,039 --> 00:05:42,479
I go through this you know laundry list

182
00:05:40,720 --> 00:05:43,440
of ways to defeat it is just to say that

183
00:05:42,479 --> 00:05:45,120
while things like

184
00:05:43,440 --> 00:05:47,440
patchguard do make it so that this is

185
00:05:45,120 --> 00:05:48,160
not a trivial thing on microsoft windows

186
00:05:47,440 --> 00:05:50,560
today

187
00:05:48,160 --> 00:05:52,000
the reality is you know security

188
00:05:50,560 --> 00:05:54,000
software is not actually

189
00:05:52,000 --> 00:05:56,560
trying to mitigate against this type of

190
00:05:54,000 --> 00:05:56,960
attack other operating systems don't

191
00:05:56,560 --> 00:05:58,880
have

192
00:05:56,960 --> 00:06:00,160
mechanisms like patchguard protecting

193
00:05:58,880 --> 00:06:02,080
your IDT you know

194
00:06:00,160 --> 00:06:04,000
Linux there's definitely nothing

195
00:06:02,080 --> 00:06:04,800
protecting the IDT and so an attacker

196
00:06:04,000 --> 00:06:06,560
could do

197
00:06:04,800 --> 00:06:08,319
exactly this thing that was described

198
00:06:06,560 --> 00:06:10,080
back in 2005

199
00:06:08,319 --> 00:06:12,400
and you know security software would be

200
00:06:10,080 --> 00:06:13,840
none the wiser so in general I just like

201
00:06:12,400 --> 00:06:15,440
shadow walker even if it's

202
00:06:13,840 --> 00:06:17,280
you know defeatable once you know about

203
00:06:15,440 --> 00:06:18,319
it I like it because it's again this

204
00:06:17,280 --> 00:06:20,080
example of

205
00:06:18,319 --> 00:06:22,639
an attacker who really understands the

206
00:06:20,080 --> 00:06:24,240
system is going to hands down beat

207
00:06:22,639 --> 00:06:26,240
any defender who doesn't really

208
00:06:24,240 --> 00:06:28,400
understand the system doesn't understand

209
00:06:26,240 --> 00:06:30,400
what they need to be mitigating against

210
00:06:28,400 --> 00:06:32,560
now for what it's worth shadow walker as

211
00:06:30,400 --> 00:06:34,960
made originally in 2005 has actually

212
00:06:32,560 --> 00:06:37,120
been broken for quite a while by

213
00:06:34,960 --> 00:06:39,120
things that like intel made the shared

214
00:06:37,120 --> 00:06:42,000
TLBs around 2008

215
00:06:39,120 --> 00:06:43,120
and that would have broken it so in 2014

216
00:06:42,000 --> 00:06:45,199
Jacob Torrey did

217
00:06:43,120 --> 00:06:46,880
a talk where he specifically talked

218
00:06:45,199 --> 00:06:48,639
about updating shadow walker

219
00:06:46,880 --> 00:06:50,160
capabilities to work on more modern

220
00:06:48,639 --> 00:06:53,599
hardware so check that out if you're

221
00:06:50,160 --> 00:06:53,599
interested in this kind of attack

