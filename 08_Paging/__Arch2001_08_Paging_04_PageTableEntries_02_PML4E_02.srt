1
00:00:00,080 --> 00:00:03,120
all right so now I want you to do a

2
00:00:01,520 --> 00:00:05,440
little exercise and we're going to be

3
00:00:03,120 --> 00:00:07,359
reusing the contents of this exercise

4
00:00:05,440 --> 00:00:10,160
for future exercises so

5
00:00:07,359 --> 00:00:10,800
keep keep the contents handy I want you

6
00:00:10,160 --> 00:00:13,920
to run

7
00:00:10,800 --> 00:00:15,839
lml in order to list out the modules

8
00:00:13,920 --> 00:00:17,039
that are loaded and get the address of

9
00:00:15,839 --> 00:00:19,760
the starting point

10
00:00:17,039 --> 00:00:20,640
of the nt module which is the kernel so

11
00:00:19,760 --> 00:00:23,359
it would look like this

12
00:00:20,640 --> 00:00:24,320
lml and this is the start address of the

13
00:00:23,359 --> 00:00:25,920
kernel

14
00:00:24,320 --> 00:00:27,599
then I want you to run the WinDbg

15
00:00:25,920 --> 00:00:29,279
command bang pte

16
00:00:27,599 --> 00:00:30,960
on that address that you just got for

17
00:00:29,279 --> 00:00:34,160
the start of nt

18
00:00:30,960 --> 00:00:36,719
and I want you to analyze the output

19
00:00:34,160 --> 00:00:39,920
specifically I want you to look at the

20
00:00:36,719 --> 00:00:42,480
place where it says PXE at blah contains

21
00:00:39,920 --> 00:00:44,160
foo so whatever this foo is right here

22
00:00:42,480 --> 00:00:47,120
what is contained here

23
00:00:44,160 --> 00:00:48,239
the PXE is WinDbg's term for the page

24
00:00:47,120 --> 00:00:51,120
map level 4

25
00:00:48,239 --> 00:00:53,280
entry so this can be interpreted

26
00:00:51,120 --> 00:00:54,800
according to those bits in the slides

27
00:00:53,280 --> 00:00:56,879
from just a second ago

28
00:00:54,800 --> 00:00:58,800
so I want you to go look at those bits

29
00:00:56,879 --> 00:01:00,800
manually and I want you to

30
00:00:58,800 --> 00:01:02,399
figure out which of the present, read/

31
00:01:00,800 --> 00:01:04,799
write, user/supervisor

32
00:01:02,399 --> 00:01:05,519
and XD bits are set in this particular

33
00:01:04,799 --> 00:01:07,439
entry

34
00:01:05,519 --> 00:01:09,360
I also want you to tell me what the

35
00:01:07,439 --> 00:01:10,560
physical address of the page directory

36
00:01:09,360 --> 00:01:12,640
pointer table is

37
00:01:10,560 --> 00:01:14,320
based on this entry so yours are going

38
00:01:12,640 --> 00:01:17,200
to be different of course

39
00:01:14,320 --> 00:01:19,040
so go do this for yours and then in the

40
00:01:17,200 --> 00:01:19,360
next video I'll just you know explain

41
00:01:19,040 --> 00:01:23,280
for

42
00:01:19,360 --> 00:01:23,280
mine what the answer was

