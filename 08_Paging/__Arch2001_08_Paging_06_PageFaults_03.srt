1
00:00:00,160 --> 00:00:03,120
so just two other quick miscellaneous

2
00:00:02,159 --> 00:00:06,160
points while we're

3
00:00:03,120 --> 00:00:07,680
tailing off on page faulting so

4
00:00:06,160 --> 00:00:09,920
you of course can have sensitive

5
00:00:07,680 --> 00:00:13,120
information in your RAM things such as

6
00:00:09,920 --> 00:00:13,440
crypto keys passwords and sexy nudes and

7
00:00:13,120 --> 00:00:15,599
so

8
00:00:13,440 --> 00:00:18,160
if that's in RAM and then the operating

9
00:00:15,599 --> 00:00:20,400
system page fault handler decides to

10
00:00:18,160 --> 00:00:22,400
page that information out to disk that

11
00:00:20,400 --> 00:00:23,279
could potentially mean that an attacker

12
00:00:22,400 --> 00:00:25,680
who has disk

13
00:00:23,279 --> 00:00:27,119
access whether because of physical or

14
00:00:25,680 --> 00:00:29,039
logical access

15
00:00:27,119 --> 00:00:30,880
could ultimately get access to the

16
00:00:29,039 --> 00:00:32,800
sensitive information

17
00:00:30,880 --> 00:00:34,960
now most operating systems try to

18
00:00:32,800 --> 00:00:37,680
mitigate this particular vulnerability

19
00:00:34,960 --> 00:00:39,040
by encrypting information as it's being

20
00:00:37,680 --> 00:00:41,760
paged out to disk

21
00:00:39,040 --> 00:00:43,680
and then decrypting it as it's paged in

22
00:00:41,760 --> 00:00:45,200
and this is a reasonable mitigation

23
00:00:43,680 --> 00:00:47,440
under the assumption of a threat model

24
00:00:45,200 --> 00:00:48,719
where the attacker has full physical

25
00:00:47,440 --> 00:00:50,559
hard drive access

26
00:00:48,719 --> 00:00:51,760
also as we'll learn later in the

27
00:00:50,559 --> 00:00:53,680
debugging section

28
00:00:51,760 --> 00:00:55,520
there's only 4 possible read and

29
00:00:53,680 --> 00:00:57,520
write hardware breakpoints

30
00:00:55,520 --> 00:00:59,840
so these are the mechanism by which you

31
00:00:57,520 --> 00:01:01,760
stop when there's a read or write to

32
00:00:59,840 --> 00:01:03,359
memory not the mechanisms where you stop

33
00:01:01,760 --> 00:01:05,280
when there's execution

34
00:01:03,359 --> 00:01:08,240
and so that's extremely limiting to only

35
00:01:05,280 --> 00:01:11,200
be able to monitor 4 memory locations

36
00:01:08,240 --> 00:01:13,200
so sometimes either researchers or more

37
00:01:11,200 --> 00:01:15,439
advanced debugging software

38
00:01:13,200 --> 00:01:17,280
have attempted to increase the

39
00:01:15,439 --> 00:01:20,479
capability of someone to

40
00:01:17,280 --> 00:01:21,840
monitor memory by basically manipulating

41
00:01:20,479 --> 00:01:24,880
the page tables

42
00:01:21,840 --> 00:01:25,439
so for every location that someone wants

43
00:01:24,880 --> 00:01:28,560
to

44
00:01:25,439 --> 00:01:30,400
monitor for reads and writes essentially

45
00:01:28,560 --> 00:01:33,040
they can go ahead and manipulate the

46
00:01:30,400 --> 00:01:33,840
page table set the present bit equal to

47
00:01:33,040 --> 00:01:35,600
0

48
00:01:33,840 --> 00:01:37,680
and consequently every single time

49
00:01:35,600 --> 00:01:38,880
there's a read or a write to that memory

50
00:01:37,680 --> 00:01:40,320
location

51
00:01:38,880 --> 00:01:42,159
then it will invoke the page fault

52
00:01:40,320 --> 00:01:43,040
handler and then you know the software

53
00:01:42,159 --> 00:01:45,759
has to

54
00:01:43,040 --> 00:01:47,680
find its way into the the execution flow

55
00:01:45,759 --> 00:01:48,560
so that it can say okay a page fault has

56
00:01:47,680 --> 00:01:50,399
just occurred

57
00:01:48,560 --> 00:01:52,799
what am I going to do about it okay is

58
00:01:50,399 --> 00:01:55,200
it an access to this particular address

59
00:01:52,799 --> 00:01:56,399
they would you know check the CR2 in

60
00:01:55,200 --> 00:01:58,079
order to figure out whether it was an

61
00:01:56,399 --> 00:01:59,920
access to an address that the

62
00:01:58,079 --> 00:02:02,240
researcher or the debugger you know

63
00:01:59,920 --> 00:02:04,320
person doing debugging wanted to

64
00:02:02,240 --> 00:02:06,000
monitor and if so then they would let

65
00:02:04,320 --> 00:02:07,520
the human look at it if not then they

66
00:02:06,000 --> 00:02:10,000
would continue execution

67
00:02:07,520 --> 00:02:11,920
so that's just to say like as a very

68
00:02:10,000 --> 00:02:13,840
large hammer

69
00:02:11,920 --> 00:02:16,400
just straight up marking pages as

70
00:02:13,840 --> 00:02:17,680
non-present can be used at extreme

71
00:02:16,400 --> 00:02:19,920
performance cost

72
00:02:17,680 --> 00:02:23,520
to essentially monitor memory anywhere

73
00:02:19,920 --> 00:02:23,520
within that particular page

