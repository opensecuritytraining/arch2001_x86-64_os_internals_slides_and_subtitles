1
00:00:00,080 --> 00:00:04,960
okay what did we learn in this section

2
00:00:03,199 --> 00:00:06,720
well we learned about the control

3
00:00:04,960 --> 00:00:08,400
registers zero through four we're not

4
00:00:06,720 --> 00:00:10,000
going to care about control register

5
00:00:08,400 --> 00:00:12,240
8 for our purposes

6
00:00:10,000 --> 00:00:13,599
and then we saw control register 3

7
00:00:12,240 --> 00:00:15,519
reappears down here

8
00:00:13,599 --> 00:00:19,920
it's actually going to be the base where

9
00:00:15,519 --> 00:00:19,920
the page tables start being indexed from

