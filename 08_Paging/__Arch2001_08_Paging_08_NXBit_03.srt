1
00:00:00,160 --> 00:00:05,839
next we're going to go ahead and try to

2
00:00:03,439 --> 00:00:06,799
force our way into a userspace process

3
00:00:05,839 --> 00:00:09,040
and check whether or not

4
00:00:06,799 --> 00:00:10,719
it's return whether it's code is

5
00:00:09,040 --> 00:00:12,400
non-executable or whether it's stack is

6
00:00:10,719 --> 00:00:13,519
not executable you would expect the code

7
00:00:12,400 --> 00:00:15,200
to be executable

8
00:00:13,519 --> 00:00:18,240
and hopefully the stack is not

9
00:00:15,200 --> 00:00:20,480
executable so let's go ahead and do that

10
00:00:18,240 --> 00:00:22,960
to do that you have to first open

11
00:00:20,480 --> 00:00:24,800
notepad inside of your VM I already have

12
00:00:22,960 --> 00:00:26,800
it open so if you don't then go ahead

13
00:00:24,800 --> 00:00:29,840
and continue your debugger and then

14
00:00:26,800 --> 00:00:31,119
go open notepad but since I have notepad

15
00:00:29,840 --> 00:00:35,559
open then I want to

16
00:00:31,119 --> 00:00:39,120
search for it so !process 0 0

17
00:00:35,559 --> 00:00:42,079
notepad.exe and this is going to then

18
00:00:39,120 --> 00:00:44,559
search the process list for the kernel

19
00:00:42,079 --> 00:00:45,920
information about notepad.exe

20
00:00:44,559 --> 00:00:47,600
all right and eventually that

21
00:00:45,920 --> 00:00:49,600
information will show up

22
00:00:47,600 --> 00:00:52,079
and so it says you know here is the

23
00:00:49,600 --> 00:00:54,239
information about notepad.exe

24
00:00:52,079 --> 00:00:56,239
this is what we're looking for this is

25
00:00:54,239 --> 00:00:58,160
what's called the EPROCESS structure

26
00:00:56,239 --> 00:01:00,079
but it just calls it process here

27
00:00:58,160 --> 00:01:02,559
so we're going to take that data

28
00:01:00,079 --> 00:01:06,320
structure and we are going to

29
00:01:02,559 --> 00:01:09,200
run .process and

30
00:01:06,320 --> 00:01:10,320
/i /p and then that process

31
00:01:09,200 --> 00:01:13,920
address

32
00:01:10,320 --> 00:01:16,159
so /i /p and then the address

33
00:01:13,920 --> 00:01:18,880
of that process data structure

34
00:01:16,159 --> 00:01:20,960
when we do that the i says invasive and

35
00:01:18,880 --> 00:01:21,600
so it's basically saying we invasively

36
00:01:20,960 --> 00:01:23,920
want to

37
00:01:21,600 --> 00:01:24,960
force our context to be that particular

38
00:01:23,920 --> 00:01:26,799
process

39
00:01:24,960 --> 00:01:28,320
so it says the kernel debugger says you

40
00:01:26,799 --> 00:01:29,680
need to continue and then the context

41
00:01:28,320 --> 00:01:30,560
will be switched and then it'll break

42
00:01:29,680 --> 00:01:34,880
back up

43
00:01:30,560 --> 00:01:34,880
so we hit g to go and continue

44
00:01:35,040 --> 00:01:38,159
and then it goes ahead and it breaks

45
00:01:36,560 --> 00:01:41,439
back again so if we did

46
00:01:38,159 --> 00:01:43,439
!process -1

47
00:01:41,439 --> 00:01:45,439
and we're gonna do f so that it prints

48
00:01:43,439 --> 00:01:46,320
out actually before we do that I need to

49
00:01:45,439 --> 00:01:50,399
do a

50
00:01:46,320 --> 00:01:52,240
.reload /f so I need to reload the

51
00:01:50,399 --> 00:01:54,320
kernel debug symbols and everything else

52
00:01:52,240 --> 00:01:55,040
so that in the context of this notepad

53
00:01:54,320 --> 00:01:57,200
process

54
00:01:55,040 --> 00:02:00,000
we get the symbols for this userspace

55
00:01:57,200 --> 00:02:00,000
process as well

56
00:02:03,360 --> 00:02:06,399
all right after the symbols are done

57
00:02:05,680 --> 00:02:08,879
reloading

58
00:02:06,399 --> 00:02:10,399
then we're going to do !process

59
00:02:08,879 --> 00:02:11,120
-1 and we're actually going to

60
00:02:10,399 --> 00:02:13,040
do

61
00:02:11,120 --> 00:02:14,160
0xf for the flags here so that it

62
00:02:13,040 --> 00:02:16,400
gives us the

63
00:02:14,160 --> 00:02:17,760
sort of bunch of flags including the

64
00:02:16,400 --> 00:02:19,200
thread state

65
00:02:17,760 --> 00:02:21,520
so this will print out all sorts of

66
00:02:19,200 --> 00:02:24,239
information about the various

67
00:02:21,520 --> 00:02:25,920
kernel or sorry the various stack traces

68
00:02:24,239 --> 00:02:26,560
of the different threads so there you

69
00:02:25,920 --> 00:02:28,800
see the

70
00:02:26,560 --> 00:02:30,239
information scrolling by we're going to

71
00:02:28,800 --> 00:02:31,920
let this complete

72
00:02:30,239 --> 00:02:33,519
and then we're going to walk our way

73
00:02:31,920 --> 00:02:35,200
back up through these threads and we're

74
00:02:33,519 --> 00:02:37,760
going to look for one that has

75
00:02:35,200 --> 00:02:39,599
notepad in the backtrace so if we look

76
00:02:37,760 --> 00:02:42,000
here we can see that there's no

77
00:02:39,599 --> 00:02:43,120
notepad anywhere in this particular back

78
00:02:42,000 --> 00:02:46,400
trace

79
00:02:43,120 --> 00:02:49,120
go up to the next one no notepad

80
00:02:46,400 --> 00:02:50,800
found here and I just know that in

81
00:02:49,120 --> 00:02:55,120
general it tends to be the

82
00:02:50,800 --> 00:02:57,680
first one up here so too far

83
00:02:55,120 --> 00:02:58,640
there we go right here here is notepad

84
00:02:57,680 --> 00:03:01,760
in the backtrace

85
00:02:58,640 --> 00:03:04,080
so we want to focus on this line and we

86
00:03:01,760 --> 00:03:06,720
want to look at the addresses

87
00:03:04,080 --> 00:03:08,000
for both its stack so this first field

88
00:03:06,720 --> 00:03:09,840
this first column

89
00:03:08,000 --> 00:03:12,159
is the child stack pointer and the

90
00:03:09,840 --> 00:03:15,360
second column is the return address

91
00:03:12,159 --> 00:03:17,599
so we basically want to find out the

92
00:03:15,360 --> 00:03:19,840
for the page tables of both the stack

93
00:03:17,599 --> 00:03:22,560
and the

94
00:03:19,840 --> 00:03:24,799
code address are they executable so I'm

95
00:03:22,560 --> 00:03:26,239
going to do !pte I just copied both

96
00:03:24,799 --> 00:03:26,560
of them so that I don't have to scroll

97
00:03:26,239 --> 00:03:28,560
back

98
00:03:26,560 --> 00:03:30,560
up again so then I'm just going to do

99
00:03:28,560 --> 00:03:32,640
this this is going to be the stack

100
00:03:30,560 --> 00:03:34,720
and !pte this is going to be the

101
00:03:32,640 --> 00:03:37,920
code so what do we see

102
00:03:34,720 --> 00:03:40,879
well we see non-executable for the stack

103
00:03:37,920 --> 00:03:42,959
and we see executable for the code which

104
00:03:40,879 --> 00:03:45,040
is exactly what we would expect

105
00:03:42,959 --> 00:03:48,480
so we've successfully forced our way

106
00:03:45,040 --> 00:03:51,120
into the userspace notepad.exe process

107
00:03:48,480 --> 00:03:52,640
and we've examined the stack and the

108
00:03:51,120 --> 00:03:55,840
executable section

109
00:03:52,640 --> 00:03:58,000
and we found that as hoped for microsoft

110
00:03:55,840 --> 00:04:00,760
windows is using the NX bit

111
00:03:58,000 --> 00:04:03,760
the XD bit in order to mark the stack

112
00:04:00,760 --> 00:04:03,760
non-executable

