1
00:00:00,160 --> 00:00:04,480
in this section we're going to dig into

2
00:00:02,399 --> 00:00:06,560
what are in all of these various entries

3
00:00:04,480 --> 00:00:09,280
like the page map level 4 entry

4
00:00:06,560 --> 00:00:10,800
page directory pointer table entry etc

5
00:00:09,280 --> 00:00:12,719
unfortunately I have to apologize

6
00:00:10,800 --> 00:00:14,400
in advance this is going to be pretty

7
00:00:12,719 --> 00:00:16,000
dry thus far

8
00:00:14,400 --> 00:00:17,440
I haven't found any particularly good

9
00:00:16,000 --> 00:00:19,920
way to spice it up

10
00:00:17,440 --> 00:00:21,840
the only way out is through so let's get

11
00:00:19,920 --> 00:00:25,599
to it

12
00:00:21,840 --> 00:00:28,240
so we have to start with CR3 actually

13
00:00:25,599 --> 00:00:31,119
because that has a particular structure

14
00:00:28,240 --> 00:00:32,880
that will be inside the register

15
00:00:31,119 --> 00:00:34,480
so first of all if you see anything

16
00:00:32,880 --> 00:00:35,840
listed as reserved you should assume

17
00:00:34,480 --> 00:00:39,280
that's 0

18
00:00:35,840 --> 00:00:41,440
M in this bit field up here is

19
00:00:39,280 --> 00:00:42,800
indicating the MAXPHYADDR I'm going

20
00:00:41,440 --> 00:00:44,239
to pretend it's 52

21
00:00:42,800 --> 00:00:46,800
even though I told you that on my

22
00:00:44,239 --> 00:00:48,800
particular system it's actually 39

23
00:00:46,800 --> 00:00:50,239
so on my system this would be 38 and

24
00:00:48,800 --> 00:00:52,559
this would be 39

25
00:00:50,239 --> 00:00:55,360
but let's pretend it's 52 is if you had

26
00:00:52,559 --> 00:00:58,559
something that had the maximum support

27
00:00:55,360 --> 00:01:01,280
if it was 52 then bits 51

28
00:00:58,559 --> 00:01:02,079
through 12 are the most significant bits

29
00:01:01,280 --> 00:01:04,720
of the

30
00:01:02,079 --> 00:01:05,519
physical address where the page map

31
00:01:04,720 --> 00:01:07,760
level 4

32
00:01:05,519 --> 00:01:09,280
table can be found and just ignore the

33
00:01:07,760 --> 00:01:09,760
fact that this would look like reserve

34
00:01:09,280 --> 00:01:11,280
you just

35
00:01:09,760 --> 00:01:13,040
have to slide it up and then that

36
00:01:11,280 --> 00:01:15,360
wouldn't be reserved anymore

37
00:01:13,040 --> 00:01:16,479
so one thing that I have to caution

38
00:01:15,360 --> 00:01:18,799
that's oftentimes

39
00:01:16,479 --> 00:01:20,240
very confusing for people certainly was

40
00:01:18,799 --> 00:01:22,240
confusing for me when I was first

41
00:01:20,240 --> 00:01:24,560
learning an operating systems class

42
00:01:22,240 --> 00:01:26,799
you know you turn on paging and all of a

43
00:01:24,560 --> 00:01:29,759
sudden every single address

44
00:01:26,799 --> 00:01:31,520
is running through page tables and so

45
00:01:29,759 --> 00:01:33,360
it's sort of like in some sense before

46
00:01:31,520 --> 00:01:35,680
paging is on you have access to physical

47
00:01:33,360 --> 00:01:37,840
addresses but after paging is on

48
00:01:35,680 --> 00:01:38,720
you don't have direct access to physical

49
00:01:37,840 --> 00:01:40,880
addresses ever

50
00:01:38,720 --> 00:01:42,720
the MMU has access so you can go ahead

51
00:01:40,880 --> 00:01:45,520
and stick addresses in here

52
00:01:42,720 --> 00:01:46,320
and the MMU will happily be able to go

53
00:01:45,520 --> 00:01:49,439
fetch from

54
00:01:46,320 --> 00:01:51,360
RAM at that physical address but you as

55
00:01:49,439 --> 00:01:53,920
a person in a debugger

56
00:01:51,360 --> 00:01:55,520
will not be able to just easily fetch

57
00:01:53,920 --> 00:01:57,759
from that physical address

58
00:01:55,520 --> 00:01:58,799
so when you see what's in CR3 later you

59
00:01:57,759 --> 00:02:01,040
might see an address

60
00:01:58,799 --> 00:02:02,000
and that's a physical address but if you

61
00:02:01,040 --> 00:02:04,240
try to look at it

62
00:02:02,000 --> 00:02:05,840
with memory you know memory dd and

63
00:02:04,240 --> 00:02:08,239
WinDbg or whatever

64
00:02:05,840 --> 00:02:10,239
you're requesting memory for a virtual

65
00:02:08,239 --> 00:02:12,239
address not a physical address because

66
00:02:10,239 --> 00:02:13,840
paging is undoubtedly on

67
00:02:12,239 --> 00:02:15,520
in the debugger that you're running so

68
00:02:13,840 --> 00:02:17,760
that's just to say that

69
00:02:15,520 --> 00:02:19,440
accessing and reading and seeing things

70
00:02:17,760 --> 00:02:20,800
manually at any given physical address

71
00:02:19,440 --> 00:02:23,280
like if you want to go look at the page

72
00:02:20,800 --> 00:02:25,520
map level 4 that is non-trivial

73
00:02:23,280 --> 00:02:27,040
WinDbg makes it easier for us to see

74
00:02:25,520 --> 00:02:27,680
the bits that we actually care about

75
00:02:27,040 --> 00:02:29,120
seeing so

76
00:02:27,680 --> 00:02:32,160
that's again part of the reason why I

77
00:02:29,120 --> 00:02:35,760
like to use windows for this particular

78
00:02:32,160 --> 00:02:38,879
class so because this is again these

79
00:02:35,760 --> 00:02:42,000
upper bits so 51 to 12 then the bottom

80
00:02:38,879 --> 00:02:44,879
0 to 11 are assumed to be 0

81
00:02:42,000 --> 00:02:45,680
so this page map level 4 needs to be

82
00:02:44,879 --> 00:02:47,840
0x1000

83
00:02:45,680 --> 00:02:50,080
or page aligned you can see that there's

84
00:02:47,840 --> 00:02:50,560
a bunch of ignored bits here and 2

85
00:02:50,080 --> 00:02:52,959
bits

86
00:02:50,560 --> 00:02:54,160
that do actually have names but those

87
00:02:52,959 --> 00:02:56,000
have to do with caching

88
00:02:54,160 --> 00:02:57,840
which we mostly don't cover in this

89
00:02:56,000 --> 00:02:59,680
course so we're going to ignore those

90
00:02:57,840 --> 00:03:02,000
too

91
00:02:59,680 --> 00:03:02,879
now you should be aware that when an

92
00:03:02,000 --> 00:03:04,959
operating system

93
00:03:02,879 --> 00:03:06,959
changes context between different

94
00:03:04,959 --> 00:03:08,560
processes in order to

95
00:03:06,959 --> 00:03:10,239
represent different processes with

96
00:03:08,560 --> 00:03:12,480
different virtual memory address spaces

97
00:03:10,239 --> 00:03:14,319
with different physical memory mapped

98
00:03:12,480 --> 00:03:15,760
potentially even the same virtual memory

99
00:03:14,319 --> 00:03:18,239
address the

100
00:03:15,760 --> 00:03:20,400
the the operating system is going to

101
00:03:18,239 --> 00:03:22,159
basically be rewriting CR3

102
00:03:20,400 --> 00:03:23,599
each time it's swapping in and out

103
00:03:22,159 --> 00:03:26,799
different processes or

104
00:03:23,599 --> 00:03:29,120
kernelspace context and so essentially

105
00:03:26,799 --> 00:03:30,560
by changing this out there will be you

106
00:03:29,120 --> 00:03:32,720
know potentially many

107
00:03:30,560 --> 00:03:34,000
page map level 4s that point at a

108
00:03:32,720 --> 00:03:36,879
bunch of different

109
00:03:34,000 --> 00:03:39,280
page page directory pointer tables and

110
00:03:36,879 --> 00:03:40,959
so forth

111
00:03:39,280 --> 00:03:43,440
so there's this nice picture in the

112
00:03:40,959 --> 00:03:45,360
manuals that I liked but which I have to

113
00:03:43,440 --> 00:03:48,080
alter slightly before I can reuse

114
00:03:45,360 --> 00:03:49,440
because this paint this picture first of

115
00:03:48,080 --> 00:03:50,799
all it was just you know the typical

116
00:03:49,440 --> 00:03:52,720
32-bit paging so

117
00:03:50,799 --> 00:03:54,080
level 2 depth and then second of all

118
00:03:52,720 --> 00:03:57,360
it talks about tasks

119
00:03:54,080 --> 00:04:00,080
instead of processes so task A

120
00:03:57,360 --> 00:04:01,120
shared memory task B so if I go ahead

121
00:04:00,080 --> 00:04:02,959
and update this

122
00:04:01,120 --> 00:04:04,239
being lazy so that I only have to add

123
00:04:02,959 --> 00:04:06,560
2 levels

124
00:04:04,239 --> 00:04:08,959
we have this notion that CR3 when it's

125
00:04:06,560 --> 00:04:11,200
running in the context of process A

126
00:04:08,959 --> 00:04:13,519
will point at one page map level 4

127
00:04:11,200 --> 00:04:15,439
and for any given virtual address it'll

128
00:04:13,519 --> 00:04:16,799
walk its way through these entries and

129
00:04:15,439 --> 00:04:19,199
find some particular

130
00:04:16,799 --> 00:04:20,320
physical frame so when it's running

131
00:04:19,199 --> 00:04:22,880
process A

132
00:04:20,320 --> 00:04:25,120
it's all this memory right here is

133
00:04:22,880 --> 00:04:27,919
process A's memory

134
00:04:25,120 --> 00:04:28,960
when it changes to process B it changes

135
00:04:27,919 --> 00:04:31,520
the CR3

136
00:04:28,960 --> 00:04:32,720
and that can change which page map it

137
00:04:31,520 --> 00:04:34,960
actually points at

138
00:04:32,720 --> 00:04:36,000
and consequently it will now all of a

139
00:04:34,960 --> 00:04:37,840
sudden only see

140
00:04:36,000 --> 00:04:39,360
virtual memory in these particular

141
00:04:37,840 --> 00:04:40,560
ranges that it's defined in these

142
00:04:39,360 --> 00:04:42,240
particular tables

143
00:04:40,560 --> 00:04:44,000
and if we wanted to go back to the

144
00:04:42,240 --> 00:04:46,639
notion that it was trying to show

145
00:04:44,000 --> 00:04:49,120
you can imagine that process a has some

146
00:04:46,639 --> 00:04:51,280
page tables that ultimately have a page

147
00:04:49,120 --> 00:04:54,479
directory entry that points at

148
00:04:51,280 --> 00:04:57,680
a shared page table and that page table

149
00:04:54,479 --> 00:05:00,080
is shared with process B so process B

150
00:04:57,680 --> 00:05:01,680
has a page directory entry that points

151
00:05:00,080 --> 00:05:04,080
at that particular page table

152
00:05:01,680 --> 00:05:06,080
and consequently this RAM right here

153
00:05:04,080 --> 00:05:06,639
could actually appear in both of these

154
00:05:06,080 --> 00:05:08,240
2

155
00:05:06,639 --> 00:05:10,240
processes it may not be at the same

156
00:05:08,240 --> 00:05:12,240
virtual memory address space but the

157
00:05:10,240 --> 00:05:13,600
physical RAM could be accessible

158
00:05:12,240 --> 00:05:15,600
within the context of each of these

159
00:05:13,600 --> 00:05:17,280
processes and we'll talk a little bit

160
00:05:15,600 --> 00:05:19,840
more about shared memory later

161
00:05:17,280 --> 00:05:23,280
and this notion of page tables as a

162
00:05:19,840 --> 00:05:23,280
mechanism for sharing memory

