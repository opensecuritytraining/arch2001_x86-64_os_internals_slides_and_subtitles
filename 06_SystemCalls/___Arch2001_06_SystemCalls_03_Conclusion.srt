1
00:00:00,000 --> 00:00:03,840
So what's the conclusion about system

2
00:00:01,520 --> 00:00:05,040
calls well these are the things that are

3
00:00:03,840 --> 00:00:07,040
the primary way

4
00:00:05,040 --> 00:00:08,800
that a operating system gets from user

5
00:00:07,040 --> 00:00:10,639
space to kernel and back again

6
00:00:08,800 --> 00:00:12,160
no longer are people tending to use call

7
00:00:10,639 --> 00:00:13,599
gates or interrupts

8
00:00:12,160 --> 00:00:15,440
it's these system call assembly

9
00:00:13,599 --> 00:00:16,400
instructions primarily for performance

10
00:00:15,440 --> 00:00:17,760
reasons now

11
00:00:16,400 --> 00:00:19,279
if you're one of the students who's

12
00:00:17,760 --> 00:00:21,199
taking this class because you're

13
00:00:19,279 --> 00:00:22,480
following the primrose path on your way

14
00:00:21,199 --> 00:00:24,160
to exploitation

15
00:00:22,480 --> 00:00:26,400
you should know that this also means

16
00:00:24,160 --> 00:00:28,960
that system calls are one of the primary

17
00:00:26,400 --> 00:00:30,400
attack surfaces of operating systems

18
00:00:28,960 --> 00:00:32,079
these are the things where there's

19
00:00:30,400 --> 00:00:33,680
literally hundreds of functions that a

20
00:00:32,079 --> 00:00:36,000
given operating system

21
00:00:33,680 --> 00:00:37,600
allows userspace to call and if the

22
00:00:36,000 --> 00:00:38,719
operating system mishandles the

23
00:00:37,600 --> 00:00:40,640
parameters that the

24
00:00:38,719 --> 00:00:42,160
attacker controlled code in userspace

25
00:00:40,640 --> 00:00:44,320
sends it that could mean that the

26
00:00:42,160 --> 00:00:45,600
operating system could be compromised

27
00:00:44,320 --> 00:00:47,440
so you're going to learn more about

28
00:00:45,600 --> 00:00:49,120
system calls in future os specific

29
00:00:47,440 --> 00:00:50,000
classes because there's actually quite a

30
00:00:49,120 --> 00:00:52,000
bit of depth

31
00:00:50,000 --> 00:00:53,520
to what happens beyond just you know

32
00:00:52,000 --> 00:00:54,960
what parameters it feeds

33
00:00:53,520 --> 00:00:57,199
operating systems do a whole bunch of

34
00:00:54,960 --> 00:00:58,960
complicated things down this path

35
00:00:57,199 --> 00:01:00,640
so we picked up a few more assembly

36
00:00:58,960 --> 00:01:03,280
instructions along the way the primary

37
00:01:00,640 --> 00:01:04,000
one for 64-bit systems being syscall and

38
00:01:03,280 --> 00:01:05,519
sysret.

39
00:01:04,000 --> 00:01:07,520
If you watch the optional material

40
00:01:05,519 --> 00:01:09,280
you'll see about how the 32-bit

41
00:01:07,520 --> 00:01:09,760
prefer sysenter and sysexit

42
00:01:09,280 --> 00:01:12,320
work

43
00:01:09,760 --> 00:01:14,400
swapgs was used for system calls or

44
00:01:12,320 --> 00:01:16,080
potentially interrupts in order to

45
00:01:14,400 --> 00:01:18,240
swap around data structures so the

46
00:01:16,080 --> 00:01:19,680
kernel can quickly get access to

47
00:01:18,240 --> 00:01:21,439
something when it's just transitioned in

48
00:01:19,680 --> 00:01:23,840
from userspace and then

49
00:01:21,439 --> 00:01:25,119
the read and write fsbase and read and

50
00:01:23,840 --> 00:01:27,439
write gsbase

51
00:01:25,119 --> 00:01:28,400
are means by which userspace can now

52
00:01:27,439 --> 00:01:29,840
have more control

53
00:01:28,400 --> 00:01:33,759
over these data structures that are

54
00:01:29,840 --> 00:01:33,759
referenced by fs and gs

