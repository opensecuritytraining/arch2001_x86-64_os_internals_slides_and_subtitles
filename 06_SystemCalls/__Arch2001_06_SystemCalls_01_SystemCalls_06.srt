1
00:00:00,080 --> 00:00:03,439
The little connect 4 diagram before told

2
00:00:02,159 --> 00:00:05,920
us that while syscall

3
00:00:03,439 --> 00:00:08,160
sysret are what's preferred on 64-bit

4
00:00:05,920 --> 00:00:10,320
sysenter sysexit are what's con

5
00:00:08,160 --> 00:00:13,200
preferred for maximum compatibility on

6
00:00:10,320 --> 00:00:15,440
32-bit AMD and intel CPUs

7
00:00:13,200 --> 00:00:17,359
so this was created before the AMD

8
00:00:15,440 --> 00:00:18,080
extensions and that's why both of them

9
00:00:17,359 --> 00:00:20,880
support it

10
00:00:18,080 --> 00:00:22,800
so let's see exactly how those work okay

11
00:00:20,880 --> 00:00:24,720
so sysenter how does it work

12
00:00:22,800 --> 00:00:26,480
well as before there's some model

13
00:00:24,720 --> 00:00:29,439
specific registers involved

14
00:00:26,480 --> 00:00:30,160
love those icosahedrons so the cs value

15
00:00:29,439 --> 00:00:33,280
gets set

16
00:00:30,160 --> 00:00:36,480
to ia32_sysenter_cs msr

17
00:00:33,280 --> 00:00:40,320
so the bottom 16 bits so

18
00:00:36,480 --> 00:00:42,640
174. now ss uses the same msr

19
00:00:40,320 --> 00:00:44,000
it's named cs but then it does that sort

20
00:00:42,640 --> 00:00:46,800
of plus 8 math

21
00:00:44,000 --> 00:00:48,320
like we saw before with the syscall esp

22
00:00:46,800 --> 00:00:51,199
is just going to get set to

23
00:00:48,320 --> 00:00:51,920
whatever this msr says to set it to and

24
00:00:51,199 --> 00:00:54,160
eip

25
00:00:51,920 --> 00:00:56,399
is just set to whatever this msr says to

26
00:00:54,160 --> 00:00:57,280
set it to this exit then should be the

27
00:00:56,399 --> 00:00:59,920
opposite

28
00:00:57,280 --> 00:01:01,280
so it should be changing the cs back to

29
00:00:59,920 --> 00:01:03,920
what it was before

30
00:01:01,280 --> 00:01:04,879
well it kind of does that and you hope

31
00:01:03,920 --> 00:01:08,159
that it does it

32
00:01:04,879 --> 00:01:11,040
what it does is it sets it to cs plus

33
00:01:08,159 --> 00:01:12,720
16 so whatever's in this msr plus 16 so

34
00:01:11,040 --> 00:01:13,760
it's going to again increment up that

35
00:01:12,720 --> 00:01:16,240
GDT table

36
00:01:13,760 --> 00:01:16,880
and pick the thing from there likewise

37
00:01:16,240 --> 00:01:20,960
ss

38
00:01:16,880 --> 00:01:25,119
gets this msr plus 24. esp

39
00:01:20,960 --> 00:01:27,119
is esp is set to the value that is

40
00:01:25,119 --> 00:01:29,840
stored in ecx and eip

41
00:01:27,119 --> 00:01:31,680
is set to the value stored in edx so

42
00:01:29,840 --> 00:01:32,880
that does not seem symmetrical with what

43
00:01:31,680 --> 00:01:35,119
we just learned about

44
00:01:32,880 --> 00:01:36,720
sysenter so that means there's this

45
00:01:35,119 --> 00:01:38,720
implicit expectation

46
00:01:36,720 --> 00:01:41,040
that sysenter may have not have

47
00:01:38,720 --> 00:01:44,000
stored it but the sysexit

48
00:01:41,040 --> 00:01:46,159
better have stored the esp value and

49
00:01:44,000 --> 00:01:46,880
replaced it into ecx so that it can be

50
00:01:46,159 --> 00:01:48,960
put back

51
00:01:46,880 --> 00:01:50,720
by the sysexit so no matter what

52
00:01:48,960 --> 00:01:52,880
whatever is laying around in these two

53
00:01:50,720 --> 00:01:53,600
registers that's going back into esp and

54
00:01:52,880 --> 00:01:55,439
eip

55
00:01:53,600 --> 00:01:57,520
when the sysexit is called so just

56
00:01:55,439 --> 00:02:01,280
visualizing these registers a little bit

57
00:01:57,520 --> 00:02:03,759
sysenter cs bottom 15 or 16 bits rather

58
00:02:01,280 --> 00:02:06,079
are set to the cs selector and then

59
00:02:03,759 --> 00:02:09,280
that's again used with some math to find

60
00:02:06,079 --> 00:02:09,759
the other the ring 0 ss the ring

61
00:02:09,280 --> 00:02:12,879
3

62
00:02:09,759 --> 00:02:16,480
cs and the ring 0 sorry ring 3

63
00:02:12,879 --> 00:02:17,040
ss and esp and eip are one way they'll

64
00:02:16,480 --> 00:02:19,840
get you

65
00:02:17,040 --> 00:02:21,120
a value to put for the kernel but they

66
00:02:19,840 --> 00:02:23,840
don't give you a way back

67
00:02:21,120 --> 00:02:24,720
so visualizing that quickly we have the

68
00:02:23,840 --> 00:02:27,680
bottom bits

69
00:02:24,720 --> 00:02:29,520
8 cs just takes them exactly as is

70
00:02:27,680 --> 00:02:31,680
when going into kernel space

71
00:02:29,520 --> 00:02:33,440
that would be interpreted as index 1

72
00:02:31,680 --> 00:02:36,239
here ring 0 code index 1

73
00:02:33,440 --> 00:02:37,599
all right ss is just cs plus eight for

74
00:02:36,239 --> 00:02:41,440
all intents purposes

75
00:02:37,599 --> 00:02:42,879
so that would be index 2 the sysexit

76
00:02:41,440 --> 00:02:43,519
trying to get you back out of kernel

77
00:02:42,879 --> 00:02:46,400
space

78
00:02:43,519 --> 00:02:47,200
is starting from this exact same 8

79
00:02:46,400 --> 00:02:50,400
and then

80
00:02:47,200 --> 00:02:52,560
adding 16 to it so now we get 0x18

81
00:02:50,400 --> 00:02:53,440
interpreted like this you get ring

82
00:02:52,560 --> 00:02:56,160
three's code

83
00:02:53,440 --> 00:02:56,879
starting right there but this is even

84
00:02:56,160 --> 00:02:59,760
more of an

85
00:02:56,879 --> 00:03:00,159
uh what's going on here than it was for

86
00:02:59,760 --> 00:03:02,800
the

87
00:03:00,159 --> 00:03:04,000
syscall because syscall at least there

88
00:03:02,800 --> 00:03:06,560
was like separate

89
00:03:04,000 --> 00:03:08,560
slots for going in implicitly going to

90
00:03:06,560 --> 00:03:09,840
the ring 0 and going out implicitly

91
00:03:08,560 --> 00:03:11,599
going back to ring 3

92
00:03:09,840 --> 00:03:13,200
here there's not two slots there's just

93
00:03:11,599 --> 00:03:14,959
a single slot and that means

94
00:03:13,200 --> 00:03:17,360
you know necessarily when you were

95
00:03:14,959 --> 00:03:18,640
specifying the ring 0 code and stack

96
00:03:17,360 --> 00:03:20,400
you would have wanted to leave the

97
00:03:18,640 --> 00:03:22,879
bottom two bits equal to 0

98
00:03:20,400 --> 00:03:24,480
and here again they would be 0 if

99
00:03:22,879 --> 00:03:26,319
nothing else is happening so

100
00:03:24,480 --> 00:03:28,080
what's happening here well just like

101
00:03:26,319 --> 00:03:30,080
before we had to dig into the

102
00:03:28,080 --> 00:03:32,239
nitty-gritty details of sysexit the

103
00:03:30,080 --> 00:03:34,239
operations section in the manual so

104
00:03:32,239 --> 00:03:36,480
if it's 64 bits no we're not looking at

105
00:03:34,239 --> 00:03:39,680
64 right now we're looking at 32

106
00:03:36,480 --> 00:03:43,519
so 32 bits 32 bits it says

107
00:03:39,680 --> 00:03:45,200
it's the ia-32 sysenter cs plus 16

108
00:03:43,519 --> 00:03:47,440
and then here we go that's what we're

109
00:03:45,200 --> 00:03:50,159
looking for cs selector or

110
00:03:47,440 --> 00:03:51,599
3 and that forces you on sysexit

111
00:03:50,159 --> 00:03:54,480
going back to userspace

112
00:03:51,599 --> 00:03:55,200
to go to ring 3 and likewise again

113
00:03:54,480 --> 00:03:57,200
the DPL

114
00:03:55,200 --> 00:03:59,599
is forced to 3 so you're back in

115
00:03:57,200 --> 00:04:00,080
userspace this has been ored with 3

116
00:03:59,599 --> 00:04:03,519
and so

117
00:04:00,080 --> 00:04:06,560
you have the RPL of 3 and the cs

118
00:04:03,519 --> 00:04:09,200
is again just the ss plus

119
00:04:06,560 --> 00:04:09,840
sorry the ss is again cs plus eight and

120
00:04:09,200 --> 00:04:12,720
so

121
00:04:09,840 --> 00:04:13,439
basically you end up with according to

122
00:04:12,720 --> 00:04:16,400
the way that

123
00:04:13,439 --> 00:04:17,600
things must be set up for sysenter and

124
00:04:16,400 --> 00:04:19,680
sysexit work

125
00:04:17,600 --> 00:04:21,359
you must have effectively ring 0 code

126
00:04:19,680 --> 00:04:23,360
followed by ring 0 stack followed by

127
00:04:21,359 --> 00:04:23,840
ring 3 code followed by ring 3

128
00:04:23,360 --> 00:04:25,040
stack

129
00:04:23,840 --> 00:04:26,880
all right so there's this interesting

130
00:04:25,040 --> 00:04:28,960
little point in the

131
00:04:26,880 --> 00:04:30,880
manuals that says sysenter and

132
00:04:28,960 --> 00:04:32,800
sysexit instructions are companion

133
00:04:30,880 --> 00:04:34,240
instructions but they do not constitute

134
00:04:32,800 --> 00:04:36,000
a call/return pair

135
00:04:34,240 --> 00:04:37,759
when executing a sysenter instruction

136
00:04:36,000 --> 00:04:39,280
the processor does not save state

137
00:04:37,759 --> 00:04:41,199
information for the user code

138
00:04:39,280 --> 00:04:42,479
for instance the instruction pointer

139
00:04:41,199 --> 00:04:44,000
right I just said that

140
00:04:42,479 --> 00:04:46,479
you know the instruction pointer gets

141
00:04:44,000 --> 00:04:49,120
restored from whatever sitting around in

142
00:04:46,479 --> 00:04:49,680
the edx register so it's not explicitly

143
00:04:49,120 --> 00:04:51,520
saved

144
00:04:49,680 --> 00:04:53,199
so someone somewhere needs to save it

145
00:04:51,520 --> 00:04:55,120
that ultimately becomes up to the

146
00:04:53,199 --> 00:04:55,680
operating systems to figure out what

147
00:04:55,120 --> 00:04:58,320
their

148
00:04:55,680 --> 00:04:58,880
personal convention is to save and

149
00:04:58,320 --> 00:05:02,160
restore

150
00:04:58,880 --> 00:05:05,360
state so that means that unlike syscall

151
00:05:02,160 --> 00:05:06,160
sysret these are unbalanced and

152
00:05:05,360 --> 00:05:08,400
what do you do

153
00:05:06,160 --> 00:05:09,360
when you have an unbalanced situation

154
00:05:08,400 --> 00:05:11,280
you throw

155
00:05:09,360 --> 00:05:14,160
chunks of a moon at people words to live

156
00:05:11,280 --> 00:05:14,160
your life by

