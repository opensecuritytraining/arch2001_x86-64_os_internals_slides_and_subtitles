1
00:00:00,240 --> 00:00:04,000
When an architecture gets a protection

2
00:00:01,839 --> 00:00:05,440
mechanism such as intel's privilege

3
00:00:04,000 --> 00:00:07,279
separation rings

4
00:00:05,440 --> 00:00:08,639
this means that now you've got some

5
00:00:07,279 --> 00:00:11,280
situation where you want

6
00:00:08,639 --> 00:00:12,240
the kernel or the executive code to

7
00:00:11,280 --> 00:00:14,240
fully control

8
00:00:12,240 --> 00:00:16,000
certain resources and you want to

9
00:00:14,240 --> 00:00:18,400
restrict their access from the lower

10
00:00:16,000 --> 00:00:20,400
privileged code like userspace

11
00:00:18,400 --> 00:00:21,600
so we've seen thus far a couple of

12
00:00:20,400 --> 00:00:23,199
different ways to

13
00:00:21,600 --> 00:00:25,119
transition between low privilege and

14
00:00:23,199 --> 00:00:26,000
high privilege code the call gates and

15
00:00:25,119 --> 00:00:27,279
the interrupts

16
00:00:26,000 --> 00:00:29,279
but those are not currently the

17
00:00:27,279 --> 00:00:32,719
preferred methods on x86

18
00:00:29,279 --> 00:00:35,920
system calls are and specifically newer

19
00:00:32,719 --> 00:00:38,640
instruction sets have added the syscall

20
00:00:35,920 --> 00:00:39,520
and sysenter instructions and these are

21
00:00:38,640 --> 00:00:41,920
currently the

22
00:00:39,520 --> 00:00:45,840
most preferred way to transition from

23
00:00:41,920 --> 00:00:45,840
ring 3 to ring 0

24
00:00:53,520 --> 00:00:57,199
so just to back up and you know talk

25
00:00:55,680 --> 00:00:59,199
about sort of the evolution

26
00:00:57,199 --> 00:01:01,039
of computing systems and operating

27
00:00:59,199 --> 00:01:03,920
systems if you think like

28
00:01:01,039 --> 00:01:04,720
way back to an operating system like dos

29
00:01:03,920 --> 00:01:06,960
where the

30
00:01:04,720 --> 00:01:08,080
os and the applications are all sort of

31
00:01:06,960 --> 00:01:10,159
mixed in

32
00:01:08,080 --> 00:01:12,000
one memory space and there's no true

33
00:01:10,159 --> 00:01:13,600
separation between them

34
00:01:12,000 --> 00:01:15,439
you would have the hardware and you

35
00:01:13,600 --> 00:01:16,799
would have the software and some

36
00:01:15,439 --> 00:01:19,280
particular application may

37
00:01:16,799 --> 00:01:20,240
call through the OS in order to call

38
00:01:19,280 --> 00:01:22,640
some particular

39
00:01:20,240 --> 00:01:24,640
function of the hardware to you know for

40
00:01:22,640 --> 00:01:26,000
instance read a sector off of a spinning

41
00:01:24,640 --> 00:01:27,520
disk hard drive

42
00:01:26,000 --> 00:01:29,439
but the problem with these older

43
00:01:27,520 --> 00:01:31,840
operating systems is that

44
00:01:29,439 --> 00:01:33,920
the application could just go around the

45
00:01:31,840 --> 00:01:36,479
operating system if it knew how

46
00:01:33,920 --> 00:01:37,360
so there were particular interfaces and

47
00:01:36,479 --> 00:01:40,479
apps could just

48
00:01:37,360 --> 00:01:42,560
go directly to the hardware so once

49
00:01:40,479 --> 00:01:43,600
ring 0 ring 3 type separation

50
00:01:42,560 --> 00:01:45,280
comes into play

51
00:01:43,600 --> 00:01:47,759
you've got the hardware which is hard

52
00:01:45,280 --> 00:01:49,439
you've got the software which is softer

53
00:01:47,759 --> 00:01:50,320
and you've got now userspace which is

54
00:01:49,439 --> 00:01:52,079
fluid

55
00:01:50,320 --> 00:01:54,399
so I don't know I just came up with

56
00:01:52,079 --> 00:01:55,360
these with these colorations a long time

57
00:01:54,399 --> 00:01:57,920
ago just in one

58
00:01:55,360 --> 00:01:58,880
plain white boxes so got me some slack

59
00:01:57,920 --> 00:02:01,840
here

60
00:01:58,880 --> 00:02:03,759
so ring 0 is taking control of the

61
00:02:01,840 --> 00:02:04,960
system and it wants to restrict things

62
00:02:03,759 --> 00:02:06,880
from userspace

63
00:02:04,960 --> 00:02:08,879
so the first and most important thing it

64
00:02:06,880 --> 00:02:09,599
does is it takes control of the hardware

65
00:02:08,879 --> 00:02:11,280
and says

66
00:02:09,599 --> 00:02:12,720
dear userspace you're not allowed to

67
00:02:11,280 --> 00:02:14,160
just talk to the hardware you have to go

68
00:02:12,720 --> 00:02:15,760
through me

69
00:02:14,160 --> 00:02:17,599
because that's how you can enforce

70
00:02:15,760 --> 00:02:19,920
things like

71
00:02:17,599 --> 00:02:21,520
privileges on files privileges for

72
00:02:19,920 --> 00:02:23,599
executing things

73
00:02:21,520 --> 00:02:24,640
capability to read and write to I/O

74
00:02:23,599 --> 00:02:27,760
devices like

75
00:02:24,640 --> 00:02:29,440
keyboards or monitors so if userspace

76
00:02:27,760 --> 00:02:32,400
wants to open a file to

77
00:02:29,440 --> 00:02:34,080
execute some code or something like that

78
00:02:32,400 --> 00:02:35,280
then it has to call through the kernel

79
00:02:34,080 --> 00:02:36,000
and the kernel will be the one

80
00:02:35,280 --> 00:02:39,599
responsible

81
00:02:36,000 --> 00:02:41,360
for opening that file and the hardware

82
00:02:39,599 --> 00:02:43,519
may send the data back to the kernel but

83
00:02:41,360 --> 00:02:45,680
then the kernel can abstract that data

84
00:02:43,519 --> 00:02:47,440
in some way if it wants to provide a

85
00:02:45,680 --> 00:02:50,000
more meaningful interface between

86
00:02:47,440 --> 00:02:51,360
the kernel and userspace something that

87
00:02:50,000 --> 00:02:54,879
it can actually guarantee

88
00:02:51,360 --> 00:02:57,519
in terms of a stable API between

89
00:02:54,879 --> 00:02:58,480
kernel land and userspace so that

90
00:02:57,519 --> 00:03:00,400
stable API

91
00:02:58,480 --> 00:03:01,840
is typically called the system call

92
00:03:00,400 --> 00:03:04,560
interface

93
00:03:01,840 --> 00:03:06,480
and what it is is a set of functions

94
00:03:04,560 --> 00:03:07,200
that the kernel would expose to user

95
00:03:06,480 --> 00:03:09,599
space so

96
00:03:07,200 --> 00:03:10,959
system function 1 system function 2

97
00:03:09,599 --> 00:03:12,959
3 and 4

98
00:03:10,959 --> 00:03:14,000
so the kernel the operating system

99
00:03:12,959 --> 00:03:15,920
decides what

100
00:03:14,000 --> 00:03:18,159
the functionality is that it needs to

101
00:03:15,920 --> 00:03:20,239
provide to userspace and

102
00:03:18,159 --> 00:03:22,640
it you know makes those available

103
00:03:20,239 --> 00:03:24,640
somehow so that userspace can call into

104
00:03:22,640 --> 00:03:26,879
the kernel kernel does the work for user

105
00:03:24,640 --> 00:03:28,959
space and then it transfers back

106
00:03:26,879 --> 00:03:31,040
but a slightly more realistic version of

107
00:03:28,959 --> 00:03:32,720
that system call interface is not things

108
00:03:31,040 --> 00:03:34,159
going one to one direct

109
00:03:32,720 --> 00:03:36,000
it's usually that there's going to be

110
00:03:34,159 --> 00:03:37,599
some sort of centralized dispatching

111
00:03:36,000 --> 00:03:39,200
mechanism some sort of centralized

112
00:03:37,599 --> 00:03:42,080
dispatching library

113
00:03:39,200 --> 00:03:42,959
down in userspace that all the various

114
00:03:42,080 --> 00:03:45,680
system calls

115
00:03:42,959 --> 00:03:46,000
call into from that central code then it

116
00:03:45,680 --> 00:03:48,000
goes

117
00:03:46,000 --> 00:03:49,440
up to a central location in the kernel

118
00:03:48,000 --> 00:03:51,280
space and from there

119
00:03:49,440 --> 00:03:53,280
it dispatches to the particular function

120
00:03:51,280 --> 00:03:54,159
of interest and so to make that slightly

121
00:03:53,280 --> 00:03:56,560
more accurate

122
00:03:54,159 --> 00:03:57,840
instead of being random system calls it

123
00:03:56,560 --> 00:04:00,879
could be things like this

124
00:03:57,840 --> 00:04:02,879
from Windows ReadKey has a NtReadKey

125
00:04:00,879 --> 00:04:05,680
up in the kernelspace WriteFile

126
00:04:02,879 --> 00:04:08,799
NtWriteFile ShutdownSystem and Open

127
00:04:05,680 --> 00:04:10,480
Semaphore these are just functions

128
00:04:08,799 --> 00:04:12,400
functionality that the kernel wants to

129
00:04:10,480 --> 00:04:14,080
provide to userspace that it knows

130
00:04:12,400 --> 00:04:16,239
most programs are going to need and so

131
00:04:14,080 --> 00:04:18,400
it provides it

132
00:04:16,239 --> 00:04:20,320
so then the question is what are options

133
00:04:18,400 --> 00:04:22,800
to bridge this chasm

134
00:04:20,320 --> 00:04:24,080
between the userspace and kernelspace

135
00:04:22,800 --> 00:04:26,080
well we saw the first one

136
00:04:24,080 --> 00:04:29,199
call gates but that was really used

137
00:04:26,080 --> 00:04:32,479
circa Windows 95 or so

138
00:04:29,199 --> 00:04:34,320
the next one are interrupts and because

139
00:04:32,479 --> 00:04:36,160
this is just because the interrupts

140
00:04:34,320 --> 00:04:39,199
below 31 are reserved

141
00:04:36,160 --> 00:04:41,280
for intel and above it our you know

142
00:04:39,199 --> 00:04:42,240
any operating system can do whatever it

143
00:04:41,280 --> 00:04:44,000
wants with it

144
00:04:42,240 --> 00:04:45,520
different operating systems can choose

145
00:04:44,000 --> 00:04:48,400
different interrupts it's really just

146
00:04:45,520 --> 00:04:50,000
up to them and so historically Windows

147
00:04:48,400 --> 00:04:52,560
had used interrupt 2e

148
00:04:50,000 --> 00:04:53,440
and unix type systems had used interrupt

149
00:04:52,560 --> 00:04:55,040
80.

150
00:04:53,440 --> 00:04:56,880
and it was really just you know up to

151
00:04:55,040 --> 00:04:58,639
the you know operating system

152
00:04:56,880 --> 00:05:00,400
they make the code down here for user

153
00:04:58,639 --> 00:05:01,360
space they make the code for up here in

154
00:05:00,400 --> 00:05:02,639
kernelspace

155
00:05:01,360 --> 00:05:04,800
and so they just have to agree with

156
00:05:02,639 --> 00:05:06,479
themselves about what this interrupt is

157
00:05:04,800 --> 00:05:07,440
that gets them from userspace to kernel

158
00:05:06,479 --> 00:05:09,520
space

159
00:05:07,440 --> 00:05:11,440
interrupt 2e was used for a long time on

160
00:05:09,520 --> 00:05:13,199
Windows and then it was removed

161
00:05:11,440 --> 00:05:15,680
and then it was recently added back in

162
00:05:13,199 --> 00:05:17,120
again for reasons doing to do with uh

163
00:05:15,680 --> 00:05:20,000
virtualization and so

164
00:05:17,120 --> 00:05:20,800
finally the last mechanism is the system

165
00:05:20,000 --> 00:05:24,080
call

166
00:05:20,800 --> 00:05:27,120
from assembly instructions so

167
00:05:24,080 --> 00:05:29,680
from about intel pentium twos or so

168
00:05:27,120 --> 00:05:31,520
there was a sysenter assembly

169
00:05:29,680 --> 00:05:33,039
instruction which would transition from

170
00:05:31,520 --> 00:05:35,360
userspace to kernelspace

171
00:05:33,039 --> 00:05:37,120
and a sysexit which would return you

172
00:05:35,360 --> 00:05:41,199
back to userspace

173
00:05:37,120 --> 00:05:43,520
and so about Windows XP, Linux 2.5 or so

174
00:05:41,199 --> 00:05:45,680
those are where these were adopted for

175
00:05:43,520 --> 00:05:48,320
32-bit systems

176
00:05:45,680 --> 00:05:49,360
for 64-bit systems we said that AMD you

177
00:05:48,320 --> 00:05:52,000
know extended

178
00:05:49,360 --> 00:05:52,560
the architecture and so it added

179
00:05:52,000 --> 00:05:55,440
separate

180
00:05:52,560 --> 00:05:55,759
competing syscall and sysret instead of

181
00:05:55,440 --> 00:05:58,160
just

182
00:05:55,759 --> 00:06:00,000
extending these to 64-bit they came up

183
00:05:58,160 --> 00:06:02,080
with their own that have similar

184
00:06:00,000 --> 00:06:03,520
but slightly different semantics and

185
00:06:02,080 --> 00:06:06,560
those were used from around

186
00:06:03,520 --> 00:06:07,840
Windows 8 to you know Linux I don't know

187
00:06:06,560 --> 00:06:09,840
because

188
00:06:07,840 --> 00:06:10,960
because the interface itself is called

189
00:06:09,840 --> 00:06:13,680
syscall it's

190
00:06:10,960 --> 00:06:14,720
somewhat difficult to google for when

191
00:06:13,680 --> 00:06:17,120
did Linux use

192
00:06:14,720 --> 00:06:18,319
syscall assembly instruction so if

193
00:06:17,120 --> 00:06:20,240
someone wants to go dig

194
00:06:18,319 --> 00:06:22,240
through the Linux commit logs and find

195
00:06:20,240 --> 00:06:24,319
that out for me please go ahead and let

196
00:06:22,240 --> 00:06:26,160
me know

197
00:06:24,319 --> 00:06:28,080
so an interesting data point that I

198
00:06:26,160 --> 00:06:29,039
found randomly as I was googling around

199
00:06:28,080 --> 00:06:32,000
for things

200
00:06:29,039 --> 00:06:32,479
is that performance wise in interrupt

201
00:06:32,000 --> 00:06:34,080
gate

202
00:06:32,479 --> 00:06:35,759
is the most compatible thing but it's

203
00:06:34,080 --> 00:06:37,600
also the slowest thing

204
00:06:35,759 --> 00:06:39,840
so you know order of magnitude 600

205
00:06:37,600 --> 00:06:42,479
cycles for some particular hardware

206
00:06:39,840 --> 00:06:44,240
call gate is not as bad but still pretty

207
00:06:42,479 --> 00:06:46,560
bad in comparison to

208
00:06:44,240 --> 00:06:49,280
the syscall and sysenter instructions

209
00:06:46,560 --> 00:06:51,520
so that's again why wherever possible

210
00:06:49,280 --> 00:06:53,199
wherever the hardware supports it and

211
00:06:51,520 --> 00:06:55,840
wherever the you know operating system

212
00:06:53,199 --> 00:06:57,840
mode of 32 or 64-bit supports it

213
00:06:55,840 --> 00:07:02,080
operating systems will generally try to

214
00:06:57,840 --> 00:07:02,080
use the syscall or sysenter

