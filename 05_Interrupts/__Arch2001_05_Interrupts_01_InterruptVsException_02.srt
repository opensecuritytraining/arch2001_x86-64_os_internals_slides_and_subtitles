1
00:00:00,560 --> 00:00:03,280
so the software generation so there

2
00:00:02,320 --> 00:00:05,040
could be the hardware

3
00:00:03,280 --> 00:00:06,640
generated interrupts which as I said you

4
00:00:05,040 --> 00:00:08,240
know tends to deal with you know

5
00:00:06,640 --> 00:00:09,120
external peripheral hardware network

6
00:00:08,240 --> 00:00:12,559
cards

7
00:00:09,120 --> 00:00:14,480
video cards keyboards USB

8
00:00:12,559 --> 00:00:16,480
devices things like that that basically

9
00:00:14,480 --> 00:00:17,279
say like hey something occurred with my

10
00:00:16,480 --> 00:00:18,960
hardware please

11
00:00:17,279 --> 00:00:21,920
deal with it kernel but there's also

12
00:00:18,960 --> 00:00:23,760
these software-generated interrupts

13
00:00:21,920 --> 00:00:25,359
so one of the things that can cause an

14
00:00:23,760 --> 00:00:26,400
interrupt is literally the interrupt

15
00:00:25,359 --> 00:00:29,039
instruction

16
00:00:26,400 --> 00:00:30,560
and it can do interrupt n where n is a

17
00:00:29,039 --> 00:00:32,000
particular interrupt number so there's

18
00:00:30,560 --> 00:00:32,880
many different interrupts we'll talk

19
00:00:32,000 --> 00:00:35,920
about the interrupt

20
00:00:32,880 --> 00:00:39,040
vector table here in a second but

21
00:00:35,920 --> 00:00:41,680
basically or interrupt descriptor table

22
00:00:39,040 --> 00:00:43,680
basically this will invoke a specific

23
00:00:41,680 --> 00:00:44,000
interrupt from this interrupt descriptor

24
00:00:43,680 --> 00:00:45,760
table

25
00:00:44,000 --> 00:00:47,680
now normally the table is used to handle

26
00:00:45,760 --> 00:00:48,879
various hardware conditions but you can

27
00:00:47,680 --> 00:00:51,039
invoke those conditions

28
00:00:48,879 --> 00:00:52,320
with a software interrupt instruction

29
00:00:51,039 --> 00:00:54,879
but you should be aware

30
00:00:52,320 --> 00:00:58,079
that essentially while it is possible to

31
00:00:54,879 --> 00:00:59,840
software invoke these interrupts

32
00:00:58,079 --> 00:01:01,760
the int instruction does not actually

33
00:00:59,840 --> 00:01:02,640
push an error code so if you happen to

34
00:01:01,760 --> 00:01:05,040
be

35
00:01:02,640 --> 00:01:05,760
forcefully software invoking some

36
00:01:05,040 --> 00:01:09,200
interrupt

37
00:01:05,760 --> 00:01:11,920
number whatever 13 and

38
00:01:09,200 --> 00:01:13,280
the interrupt handler assumes that

39
00:01:11,920 --> 00:01:13,920
there's going to be an error code on the

40
00:01:13,280 --> 00:01:15,200
stack

41
00:01:13,920 --> 00:01:16,400
well then you're going to your stack's

42
00:01:15,200 --> 00:01:18,080
going to be off and that's probably

43
00:01:16,400 --> 00:01:19,600
going to screw up and just not really

44
00:01:18,080 --> 00:01:22,159
work

45
00:01:19,600 --> 00:01:23,280
so you know just if for some reason you

46
00:01:22,159 --> 00:01:25,759
have to invoke

47
00:01:23,280 --> 00:01:27,119
interrupts by software keep that in mind

48
00:01:25,759 --> 00:01:28,799
iret as we just said

49
00:01:27,119 --> 00:01:30,560
is the interrupt return assembly

50
00:01:28,799 --> 00:01:31,680
instruction and this is always going to

51
00:01:30,560 --> 00:01:34,000
be found at the end of

52
00:01:31,680 --> 00:01:36,400
interrupt handlers to basically get you

53
00:01:34,000 --> 00:01:38,320
back to the code that was running before

54
00:01:36,400 --> 00:01:41,119
popping all of these save state back

55
00:01:38,320 --> 00:01:43,759
into the relevant registers

56
00:01:41,119 --> 00:01:44,960
there's also some more relevant software

57
00:01:43,759 --> 00:01:48,000
generated interrupts

58
00:01:44,960 --> 00:01:50,880
particular INT3 so INT3

59
00:01:48,000 --> 00:01:51,600
it's not like INT space 3 it's INT3

60
00:01:50,880 --> 00:01:53,520
that's a

61
00:01:51,600 --> 00:01:55,520
specific assembly instruction and quite

62
00:01:53,520 --> 00:01:56,640
frankly took me a long time to realize

63
00:01:55,520 --> 00:01:58,799
that was actually

64
00:01:56,640 --> 00:02:00,079
literally a completely separate assembly

65
00:01:58,799 --> 00:02:02,640
instruction mnemonic

66
00:02:00,079 --> 00:02:03,920
so INT3 not INT space 3 has a

67
00:02:02,640 --> 00:02:07,040
special one-byte

68
00:02:03,920 --> 00:02:08,319
form 0xCC and so you will very

69
00:02:07,040 --> 00:02:10,319
frequently see this

70
00:02:08,319 --> 00:02:12,319
being used I'm sorry you always see this

71
00:02:10,319 --> 00:02:14,640
being used as a

72
00:02:12,319 --> 00:02:16,239
software breakpoint but you know

73
00:02:14,640 --> 00:02:18,319
sometimes if you see someone like

74
00:02:16,239 --> 00:02:19,520
throwing in manual 0xCC's

75
00:02:18,319 --> 00:02:21,760
what they're doing there is they're

76
00:02:19,520 --> 00:02:23,920
specifically trying to say if the

77
00:02:21,760 --> 00:02:25,520
rip ever points at this data then I want

78
00:02:23,920 --> 00:02:26,720
it to be treated like a software

79
00:02:25,520 --> 00:02:27,520
breakpoint and I want you know a

80
00:02:26,720 --> 00:02:30,400
debugger to be

81
00:02:27,520 --> 00:02:31,760
invoked something like that so we'll see

82
00:02:30,400 --> 00:02:34,000
in a lab here in a bit

83
00:02:31,760 --> 00:02:35,440
that CC is always going to be the

84
00:02:34,000 --> 00:02:37,040
software breakpoint so if you happen to

85
00:02:35,440 --> 00:02:39,200
be in a debugger that's going to have

86
00:02:37,040 --> 00:02:41,680
the debugger catch it

87
00:02:39,200 --> 00:02:43,440
there's also an INT1 and this is

88
00:02:41,680 --> 00:02:46,400
much more rarely

89
00:02:43,440 --> 00:02:47,040
occurring but it has a one byte form

90
00:02:46,400 --> 00:02:49,920
0xF1

91
00:02:47,040 --> 00:02:51,920
and this can be used for hardware makers

92
00:02:49,920 --> 00:02:52,959
who want to invoke a hardware breakpoint

93
00:02:51,920 --> 00:02:54,400
so there's actually

94
00:02:52,959 --> 00:02:55,680
different uh when we see the interrupt

95
00:02:54,400 --> 00:02:56,879
descriptor table we'll see there's

96
00:02:55,680 --> 00:02:59,040
different sort of

97
00:02:56,879 --> 00:03:00,560
uh debugging mechanisms for INT1

98
00:02:59,040 --> 00:03:03,760
versus int3

99
00:03:00,560 --> 00:03:04,000
and so you know intel says that the

100
00:03:03,760 --> 00:03:06,159
INT1

101
00:03:04,000 --> 00:03:08,000
can be used by hardware makers to invoke

102
00:03:06,159 --> 00:03:09,920
a sort of hardware breakpoint so

103
00:03:08,000 --> 00:03:11,680
this might be like a you know bios

104
00:03:09,920 --> 00:03:13,840
vendor or something like that who

105
00:03:11,680 --> 00:03:15,680
sort of has like hardware attached at

106
00:03:13,840 --> 00:03:17,360
the very earliest part of the system

107
00:03:15,680 --> 00:03:17,760
when there's really no operating system

108
00:03:17,360 --> 00:03:19,280
up

109
00:03:17,760 --> 00:03:21,840
there's nothing really to handle these

110
00:03:19,280 --> 00:03:22,239
things and so sticking in an INT1

111
00:03:21,840 --> 00:03:24,000
will

112
00:03:22,239 --> 00:03:25,360
invoke the hardware breakpoint to let

113
00:03:24,000 --> 00:03:26,879
the hardware trap on it

114
00:03:25,360 --> 00:03:28,879
and allow them to debug the early

115
00:03:26,879 --> 00:03:31,120
execution of the system

116
00:03:28,879 --> 00:03:32,239
but you won't see this very often in

117
00:03:31,120 --> 00:03:35,040
most normal

118
00:03:32,239 --> 00:03:36,000
operating system execution environments

119
00:03:35,040 --> 00:03:38,080
and then finally for

120
00:03:36,000 --> 00:03:39,200
completeness we have these other ones

121
00:03:38,080 --> 00:03:41,280
INTO which

122
00:03:39,200 --> 00:03:42,319
invokes the overflow interrupt which is

123
00:03:41,280 --> 00:03:44,720
INT4

124
00:03:42,319 --> 00:03:46,480
if the overflow flag in the rflags

125
00:03:44,720 --> 00:03:47,599
register is set to 1 so it's sort of

126
00:03:46,480 --> 00:03:49,920
like a conditional

127
00:03:47,599 --> 00:03:51,760
INT4 and so you might for instance be

128
00:03:49,920 --> 00:03:52,959
doing some math and you want to find out

129
00:03:51,760 --> 00:03:55,519
or interrupt if an

130
00:03:52,959 --> 00:03:57,120
overflow just occurred uh and so

131
00:03:55,519 --> 00:03:58,159
basically you might do some code and

132
00:03:57,120 --> 00:04:00,799
then have an INTO

133
00:03:58,159 --> 00:04:01,680
there and that will you know invoke a

134
00:04:00,799 --> 00:04:03,599
interrupt if

135
00:04:01,680 --> 00:04:05,920
an overflow actually occurred in the

136
00:04:03,599 --> 00:04:09,040
math and then UD2

137
00:04:05,920 --> 00:04:10,879
is called the invalid opcode interrupt

138
00:04:09,040 --> 00:04:12,480
and so there's a thing in the interrupt

139
00:04:10,879 --> 00:04:14,560
description table interrupt 6

140
00:04:12,480 --> 00:04:16,720
which basically says someone somewhere

141
00:04:14,560 --> 00:04:18,400
tried to execute an assembly instruction

142
00:04:16,720 --> 00:04:20,079
that was actually completely invalid

143
00:04:18,400 --> 00:04:21,680
opcode that's not something the

144
00:04:20,079 --> 00:04:23,440
processor knows anything about

145
00:04:21,680 --> 00:04:24,960
and so while normally that might occur

146
00:04:23,440 --> 00:04:26,080
if someone has some garbage code they're

147
00:04:24,960 --> 00:04:27,840
trying to execute

148
00:04:26,080 --> 00:04:29,840
this is you know sometimes used for

149
00:04:27,840 --> 00:04:32,080
someone just as a convenient way

150
00:04:29,840 --> 00:04:33,520
in order to suggest like okay well let's

151
00:04:32,080 --> 00:04:36,639
let's raise an interrupt and

152
00:04:33,520 --> 00:04:38,160
what interrupt should we do okay UD2

153
00:04:36,639 --> 00:04:39,840
so these are infrequently seen but I

154
00:04:38,160 --> 00:04:42,240
just list them as you know other sources

155
00:04:39,840 --> 00:04:44,320
of software interrupts

156
00:04:42,240 --> 00:04:46,400
all right so just like we saw with call

157
00:04:44,320 --> 00:04:47,680
gates back in the call gate section when

158
00:04:46,400 --> 00:04:49,440
we're talking about segmentation

159
00:04:47,680 --> 00:04:51,040
interrupts are another mechanism that

160
00:04:49,440 --> 00:04:53,759
can be used to perform

161
00:04:51,040 --> 00:04:55,520
this privilege level escalation so

162
00:04:53,759 --> 00:04:57,360
basically your code can be running along

163
00:04:55,520 --> 00:04:59,440
at current privilege level 3

164
00:04:57,360 --> 00:05:01,680
and interrupt occurs and then it's going

165
00:04:59,440 --> 00:05:02,000
to break into the kernel and it's going

166
00:05:01,680 --> 00:05:04,000
to

167
00:05:02,000 --> 00:05:06,000
execute the kernel's interrupt handler

168
00:05:04,000 --> 00:05:07,440
at a privileged level of 0

169
00:05:06,000 --> 00:05:09,440
and that's why there was that sort of

170
00:05:07,440 --> 00:05:10,880
difference of stacks of is there a

171
00:05:09,440 --> 00:05:12,479
privilege level transfer is there no

172
00:05:10,880 --> 00:05:14,720
privilege level transfer

173
00:05:12,479 --> 00:05:16,800
this is actually a very common way that

174
00:05:14,720 --> 00:05:18,560
operating systems used to invoke their

175
00:05:16,800 --> 00:05:20,560
system call mechanism in order to go

176
00:05:18,560 --> 00:05:21,280
from userspace calling explicitly into

177
00:05:20,560 --> 00:05:23,600
the kernel

178
00:05:21,280 --> 00:05:24,720
it's not so common anymore but it is a

179
00:05:23,600 --> 00:05:28,400
way for

180
00:05:24,720 --> 00:05:30,000
privilege level transfer to occur

181
00:05:28,400 --> 00:05:31,680
all right now so the question is going

182
00:05:30,000 --> 00:05:33,440
back to this we said okay

183
00:05:31,680 --> 00:05:35,440
well the stack pointer is pointing

184
00:05:33,440 --> 00:05:36,880
somewhere and then an interrupt occurs

185
00:05:35,440 --> 00:05:37,360
and information gets pushed onto the

186
00:05:36,880 --> 00:05:39,440
stack

187
00:05:37,360 --> 00:05:40,479
and then an iret occurs and it pops it

188
00:05:39,440 --> 00:05:42,320
back off the stack

189
00:05:40,479 --> 00:05:44,560
the question is where does this

190
00:05:42,320 --> 00:05:46,720
information get pushed onto the stack

191
00:05:44,560 --> 00:05:48,639
and so in order to understand if there's

192
00:05:46,720 --> 00:05:50,720
this privilege level transfer and we go

193
00:05:48,639 --> 00:05:52,000
from ring 3 to ring 0

194
00:05:50,720 --> 00:05:53,759
you know where does the information

195
00:05:52,000 --> 00:05:55,600
actually get pushed to find out about

196
00:05:53,759 --> 00:05:57,840
that we're going to have to learn about

197
00:05:55,600 --> 00:06:00,080
a very old legacy mechanism

198
00:05:57,840 --> 00:06:01,360
of intel hardware called tasks and

199
00:06:00,080 --> 00:06:02,639
specifically we're going to want to

200
00:06:01,360 --> 00:06:06,400
learn about the task

201
00:06:02,639 --> 00:06:06,400
state segment

