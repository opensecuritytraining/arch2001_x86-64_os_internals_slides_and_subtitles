1
00:00:00,080 --> 00:00:04,160
okay so what did we learn in this

2
00:00:01,599 --> 00:00:05,040
section well we finally got to dig into

3
00:00:04,160 --> 00:00:06,879
the IDT

4
00:00:05,040 --> 00:00:09,200
and see what interrupt gates looked like

5
00:00:06,879 --> 00:00:11,840
and those interrupt gates can

6
00:00:09,200 --> 00:00:13,679
pass through and reference some segment

7
00:00:11,840 --> 00:00:15,120
descriptors because they have

8
00:00:13,679 --> 00:00:17,039
far pointers in there so there's a

9
00:00:15,120 --> 00:00:19,840
segment selector in the interrupt

10
00:00:17,039 --> 00:00:21,039
descriptor which references a segment

11
00:00:19,840 --> 00:00:23,680
descriptor

12
00:00:21,039 --> 00:00:24,160
which in turn ultimately says here is

13
00:00:23,680 --> 00:00:25,840
some

14
00:00:24,160 --> 00:00:27,920
segment of memory which practically

15
00:00:25,840 --> 00:00:28,400
speaking is always going to be base of 0

16
00:00:27,920 --> 00:00:31,039
and

17
00:00:28,400 --> 00:00:32,320
maximum size of 2 to the 64-1 but the

18
00:00:31,039 --> 00:00:33,200
different interrupts could have

19
00:00:32,320 --> 00:00:36,640
different

20
00:00:33,200 --> 00:00:38,719
privilege levels we also saw a trap gate

21
00:00:36,640 --> 00:00:39,120
is the exact same thing as an interrupt

22
00:00:38,719 --> 00:00:41,040
gate

23
00:00:39,120 --> 00:00:42,640
except that it's going to behave

24
00:00:41,040 --> 00:00:44,000
differently with the interrupt flag that

25
00:00:42,640 --> 00:00:46,320
we'll talk about next

26
00:00:44,000 --> 00:00:48,320
so in the IDT you can have a trap gate

27
00:00:46,320 --> 00:00:49,680
and we already covered you know this

28
00:00:48,320 --> 00:00:50,640
particular one they're showing pointing

29
00:00:49,680 --> 00:00:53,840
at an LDT

30
00:00:50,640 --> 00:00:56,000
LDT pointing out some segment so let's

31
00:00:53,840 --> 00:00:58,719
go ahead and fill that in as well

32
00:00:56,000 --> 00:00:59,920
also these interrupts we saw the IST and

33
00:00:58,719 --> 00:01:03,039
the TSS

34
00:00:59,920 --> 00:01:06,320
being used for one way to set the stack

35
00:01:03,039 --> 00:01:06,320
pointer on the interrupt

