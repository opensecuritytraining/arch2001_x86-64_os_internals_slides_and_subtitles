1
00:00:00,04 --> 00:00:02,83
So here's a slide that I'm borrowing from the class

2
00:00:02,83 --> 00:00:05,42
that will become the architecture 4001 class

3
00:00:05,43 --> 00:00:08,97
And it talks about a basic methodology for Port

4
00:00:08,97 --> 00:00:09,13
I/O

5
00:00:09,13 --> 00:00:10,85
Access that some devices uses,

6
00:00:10,85 --> 00:00:12,28
including the one we're going to look at in a

7
00:00:12,28 --> 00:00:12,8
second

8
00:00:12,81 --> 00:00:16,96
And this methodology is called using a index/data pair

9
00:00:17,04 --> 00:00:19,13
So the idea is that the

10
00:00:19,13 --> 00:00:19,29
I/O

11
00:00:19,29 --> 00:00:23,33
Device that you're trying to access is nominally organized into

12
00:00:23,33 --> 00:00:26,2
some sort of like memory array type thing

13
00:00:26,2 --> 00:00:26,57
And you know,

14
00:00:26,57 --> 00:00:29,01
maybe some things are control registers and some things are

15
00:00:29,01 --> 00:00:29,35
data

16
00:00:29,94 --> 00:00:33,32
But there's a notion of first you have to write

17
00:00:33,32 --> 00:00:37,33
to an index port to say what particular index in

18
00:00:37,33 --> 00:00:40,93
this notional array you want to access and then you

19
00:00:40,93 --> 00:00:42,76
read or write from a data port

20
00:00:43,24 --> 00:00:46,2
And that's going to basically specify like are you trying

21
00:00:46,2 --> 00:00:48,06
to read from that index for write to that index

22
00:00:48,74 --> 00:00:50,07
So here in this diagram,

23
00:00:50,07 --> 00:00:50,75
for instance,

24
00:00:51,14 --> 00:00:54,27
the port that we might write to for the index

25
00:00:54,28 --> 00:00:55,46
is port 4,

26
00:00:55,47 --> 00:00:56,2
sorry,

27
00:00:56,21 --> 00:00:57,87
is port 70

28
00:00:57,88 --> 00:01:01,07
So the index is port 70 because that's moved into dx

29
00:01:01,08 --> 00:01:03,54
and then a particular immediate value

30
00:01:03,54 --> 00:01:08,6
We would write there is 4 so 0, 1, 2, 3, 4 So you're

31
00:01:08,6 --> 00:01:11,29
saying the index that I want to read or write

32
00:01:11,29 --> 00:01:12,76
from is index 4

33
00:01:13,14 --> 00:01:15,95
So I'm going to take 4 and I'm going to

34
00:01:15,95 --> 00:01:16,98
output it to

35
00:01:16,98 --> 00:01:17,29
dx

36
00:01:17,29 --> 00:01:20,05
Which was set to 70 which is the index port

37
00:01:20,05 --> 00:01:21,56
for this particular chunk of hardware

38
00:01:22,34 --> 00:01:25,06
Then if I want to read in from that,

39
00:01:25,32 --> 00:01:28,96
I would take 71 which is the data port and

40
00:01:28,96 --> 00:01:31,36
I would write that into dx and then I would

41
00:01:31,36 --> 00:01:34,58
run the in instruction and I would read in a single

42
00:01:34,58 --> 00:01:35,02
byte

43
00:01:35,02 --> 00:01:36,85
So this is the al so it's going to be

44
00:01:36,85 --> 00:01:40,22
a single byte from dx which is port 71,

45
00:01:40,23 --> 00:01:41,16
the data port

46
00:01:41,17 --> 00:01:43,64
And because I already set up the index in the

47
00:01:43,64 --> 00:01:44,67
previous step,

48
00:01:44,68 --> 00:01:47,52
it knows that it's trying to read in a single

49
00:01:47,52 --> 00:01:51,76
byte from index 4 of this black box device

50
00:01:52,54 --> 00:01:55,74
So there's various devices that use this sort of thing

51
00:01:55,74 --> 00:01:56,38
CMOS,

52
00:01:56,38 --> 00:01:58,39
which will see next PCI,

53
00:01:58,39 --> 00:01:59,79
which you'll see more in the architecture

54
00:01:59,79 --> 00:02:01,86
4000 class or for instance,

55
00:02:01,86 --> 00:02:03,8
here is a very good publication that I like

56
00:02:03,8 --> 00:02:08,29
That was examining the Dell E6400 embedded controller

57
00:02:08,33 --> 00:02:10,9
Did it a whole bunch of reversing and they showed

58
00:02:10,9 --> 00:02:13,48
in that how it used port I/O in order to access

59
00:02:13,48 --> 00:02:13,64
it

60
00:02:13,65 --> 00:02:15,52
But the device we want to look at now is

61
00:02:15,52 --> 00:02:18,15
called the real time clock or CMOS

62
00:02:18,54 --> 00:02:20,93
And this is a thing going way back to some

63
00:02:20,93 --> 00:02:24,27
of the original PCs And what it has is 14

64
00:02:24,27 --> 00:02:26,07
bytes of date info

65
00:02:26,08 --> 00:02:27,96
Like so this is the clock side of things

66
00:02:27,96 --> 00:02:28,76
Real-time clock,

67
00:02:29,24 --> 00:02:31,95
so date info and then it has a few configuration

68
00:02:31,95 --> 00:02:33,46
registers that we're not going to care about

69
00:02:33,84 --> 00:02:38,41
And after that there is 114 bytes of just arbitrary

70
00:02:38,45 --> 00:02:40,36
an operating system or BIOS,

71
00:02:40,36 --> 00:02:43,02
anybody can do anything with it as long as they

72
00:02:43,02 --> 00:02:43,57
have port

73
00:02:43,57 --> 00:02:43,69
I/O

74
00:02:43,69 --> 00:02:44,25
Capability,

75
00:02:45,04 --> 00:02:46,26
114 Bytes

76
00:02:46,26 --> 00:02:49,51
That was later expanded with another extra 128 bytes

77
00:02:50,14 --> 00:02:51,66
Why is this called CMOS?

78
00:02:51,74 --> 00:02:55,23
It's because the memory that you're actually accessing when you

79
00:02:55,23 --> 00:02:58,46
do this port I/O when you go past these date time

80
00:02:58,47 --> 00:03:00,18
information and go into the bytes

81
00:03:00,46 --> 00:03:04,51
The memory itself is physically called static RAM or

82
00:03:04,51 --> 00:03:07,88
SRAM and SRAM is made up of CMOS

83
00:03:07,88 --> 00:03:10,83
or complementary metal-oxide semiconductor

84
00:03:10,83 --> 00:03:12,56
So it's a type of transistor

85
00:03:13,04 --> 00:03:16,45
And so these transistors can be used for storing information

86
00:03:16,45 --> 00:03:19,57
It's actually the same thing that's used for registers in

87
00:03:19,58 --> 00:03:23,39
the CPU but just like registers when the power goes

88
00:03:23,39 --> 00:03:24,14
away from it

89
00:03:24,15 --> 00:03:29,6
So specifically CMOS is typically backed by a physical battery

90
00:03:29,6 --> 00:03:33,44
an extra battery inside of a motherboard

91
00:03:33,45 --> 00:03:37,0
And so that essentially ensures that this will retain its

92
00:03:37,0 --> 00:03:39,48
contents and it sort of acts as a non-volatile

93
00:03:39,48 --> 00:03:39,85
RAM

94
00:03:40,24 --> 00:03:42,52
It's non-volatile up until the point where this battery

95
00:03:42,52 --> 00:03:43,48
dies essentially

96
00:03:43,58 --> 00:03:44,42
That's also why,

97
00:03:44,42 --> 00:03:47,54
you know certain BIOS makers in the past when they

98
00:03:47,54 --> 00:03:49,34
had BIOS passwords and things like that,

99
00:03:49,35 --> 00:03:51,96
you could clear the BIOS password by just removing the

100
00:03:51,96 --> 00:03:55,37
coin cell because it meant that their BIOS password implementation

101
00:03:55,37 --> 00:03:57,85
was using the CMOS memory behind the scenes

102
00:03:58,24 --> 00:04:00,9
And so when some lock bit or something went away

103
00:04:00,9 --> 00:04:01,74
from the CMOS,

104
00:04:02,03 --> 00:04:04,25
then it lost that particular setting

105
00:04:04,26 --> 00:04:06,99
So what the first indices of CMOS look like

106
00:04:06,99 --> 00:04:08,01
If you were to access them,

107
00:04:08,01 --> 00:04:10,02
if you were to access index 0,

108
00:04:10,02 --> 00:04:13,48
you would be getting the second time according to the

109
00:04:13,48 --> 00:04:14,48
real-time clock

110
00:04:14,55 --> 00:04:15,79
Index 2 is the minutes,

111
00:04:15,79 --> 00:04:17,05
the next 4 is the hours

112
00:04:17,34 --> 00:04:18,66
And then year month,

113
00:04:19,14 --> 00:04:20,07
day of the month,

114
00:04:20,08 --> 00:04:20,76
day of the week

115
00:04:21,14 --> 00:04:23,45
So basically this is the clock information

116
00:04:23,45 --> 00:04:25,57
And when you initially set up your clock in your

117
00:04:25,57 --> 00:04:27,7
operating system or your BIOS,

118
00:04:27,73 --> 00:04:33,03
it will configure this and then ever after because that

119
00:04:33,04 --> 00:04:34,76
power is being provided to the hardware,

120
00:04:34,76 --> 00:04:36,66
it will just keep ticking forward

121
00:04:36,67 --> 00:04:39,47
Even when the system is nominally shut down,

122
00:04:39,54 --> 00:04:41,7
this hardware is still powered and it's still going to

123
00:04:41,7 --> 00:04:44,39
just keep track of what time it is until you

124
00:04:44,39 --> 00:04:46,06
remove one of those batteries for instance

125
00:04:46,54 --> 00:04:47,36
And then after that,

126
00:04:47,36 --> 00:04:50,53
once you get to indices E through 7F

127
00:04:50,54 --> 00:04:53,62
That's where you just have this arbitrary SRAM

128
00:04:53,62 --> 00:04:55,88
Which any operating system or BIOS,

129
00:04:55,88 --> 00:04:59,37
anyone with port i/O can go ahead and put anything into

130
00:04:59,38 --> 00:05:02,77
So just here's a quick screenshot from one of the

131
00:05:02,78 --> 00:05:03,55
data sheets,

132
00:05:03,94 --> 00:05:06,46
which was nice because it was all on one page

133
00:05:06,46 --> 00:05:07,32
basically saying,

134
00:05:07,32 --> 00:05:07,54
you know,

135
00:05:07,54 --> 00:05:08,26
there's two banks,

136
00:05:08,26 --> 00:05:11,42
128 bytes each standard and extended bank

137
00:05:11,43 --> 00:05:14,14
And then it says right here that the ports for

138
00:05:14,14 --> 00:05:14,63
the port

139
00:05:14,63 --> 00:05:15,01
I/O

140
00:05:15,13 --> 00:05:19,15
It supports 70 for the index register and port 71

141
00:05:19,15 --> 00:05:20,95
for the data or target register

142
00:05:21,34 --> 00:05:22,67
And that's for the first bank,

143
00:05:22,67 --> 00:05:25,75
that's the clock information and the first bits of CMOS

144
00:05:25,82 --> 00:05:30,32
And then for its port 72 for the extended RAM

145
00:05:30,32 --> 00:05:31,26
index register,

146
00:05:31,94 --> 00:05:35,36
And 73 for the extended RAM target or data port

