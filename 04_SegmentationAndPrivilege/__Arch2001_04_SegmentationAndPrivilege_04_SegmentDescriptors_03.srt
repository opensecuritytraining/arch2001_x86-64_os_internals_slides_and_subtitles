1
00:00:00,080 --> 00:00:04,160
okay so in this section what did we

2
00:00:02,480 --> 00:00:06,799
learn

3
00:00:04,160 --> 00:00:08,000
well we saw now the definition of

4
00:00:06,799 --> 00:00:11,440
segment descriptors

5
00:00:08,000 --> 00:00:13,679
inside the things like GDT's and LDT's

6
00:00:11,440 --> 00:00:14,799
segment descriptor can be pointing at a

7
00:00:13,679 --> 00:00:17,359
code or data or

8
00:00:14,799 --> 00:00:19,359
stack segment which just is some chunk

9
00:00:17,359 --> 00:00:19,680
of memory but practically speaking if

10
00:00:19,359 --> 00:00:23,680
it's

11
00:00:19,680 --> 00:00:25,279
cs, ds, es, or ss it may have information in

12
00:00:23,680 --> 00:00:26,960
there but the only information that's

13
00:00:25,279 --> 00:00:28,000
actually being used is the access

14
00:00:26,960 --> 00:00:29,920
information

15
00:00:28,000 --> 00:00:31,599
right we said the base and the limit are

16
00:00:29,920 --> 00:00:35,920
for all intents and purposes 0

17
00:00:31,599 --> 00:00:38,239
and the entire 64-bit address space

18
00:00:35,920 --> 00:00:39,520
in the LDT you can also have segment

19
00:00:38,239 --> 00:00:41,680
descriptors which will

20
00:00:39,520 --> 00:00:42,960
also have the option to create code and

21
00:00:41,680 --> 00:00:44,960
data segments or

22
00:00:42,960 --> 00:00:46,079
you know various gates of different

23
00:00:44,960 --> 00:00:47,680
types we're going to learn about in a

24
00:00:46,079 --> 00:00:50,000
bit

25
00:00:47,680 --> 00:00:50,719
and then we briefly saw the LDT

26
00:00:50,000 --> 00:00:53,920
descriptor

27
00:00:50,719 --> 00:00:56,399
itself is a 16 byte descriptor which is

28
00:00:53,920 --> 00:01:00,079
extended so that it can have a 64-bit

29
00:00:56,399 --> 00:01:01,600
value to point at the base of the LDT

30
00:01:00,079 --> 00:01:04,000
so this should really be pointing at the

31
00:01:01,600 --> 00:01:05,600
base of the LDT but maybe they're again

32
00:01:04,000 --> 00:01:08,560
saying you know this information gets

33
00:01:05,600 --> 00:01:12,159
cached into the ld the hidden portion

34
00:01:08,560 --> 00:01:12,159
of the LDT register

