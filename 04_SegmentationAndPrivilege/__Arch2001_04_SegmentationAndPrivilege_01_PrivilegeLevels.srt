1
00:00:00,640 --> 00:00:04,000
In this section I want to introduce you

2
00:00:02,320 --> 00:00:06,160
to one of the most fundamental

3
00:00:04,000 --> 00:00:07,440
security mechanisms that exist on modern

4
00:00:06,160 --> 00:00:10,800
operating systems

5
00:00:07,440 --> 00:00:12,240
privilege rings I also want to apologize

6
00:00:10,800 --> 00:00:14,000
for all the Sonic the Hedgehog

7
00:00:12,240 --> 00:00:14,719
references which are going to appear in

8
00:00:14,000 --> 00:00:16,320
this section

9
00:00:14,719 --> 00:00:17,920
when I was redoing the slides there was

10
00:00:16,320 --> 00:00:20,320
this single slide of

11
00:00:17,920 --> 00:00:22,240
Sonic the Hedgehog and everything after

12
00:00:20,320 --> 00:00:23,119
this was inspired by this one little

13
00:00:22,240 --> 00:00:25,119
picture here

14
00:00:23,119 --> 00:00:27,680
trying to use Sonic as a reference for

15
00:00:25,119 --> 00:00:30,560
how you can get those privilege rings

16
00:00:27,680 --> 00:00:31,519
so first of all privilege rings were

17
00:00:30,560 --> 00:00:34,640
introduced by

18
00:00:31,519 --> 00:00:36,480
Multics in 1960s which was the first

19
00:00:34,640 --> 00:00:37,200
operating system that supported hardware

20
00:00:36,480 --> 00:00:39,680
enforced

21
00:00:37,200 --> 00:00:41,840
privileged rings and intel eventually

22
00:00:39,680 --> 00:00:44,000
added rings as well and they are also

23
00:00:41,840 --> 00:00:46,079
enforced by the hardware

24
00:00:44,000 --> 00:00:47,120
and so what you'll often hear is someone

25
00:00:46,079 --> 00:00:49,600
referenced that

26
00:00:47,120 --> 00:00:51,600
you know user space code or user mode

27
00:00:49,600 --> 00:00:53,840
code runs in "ring 3"

28
00:00:51,600 --> 00:00:55,360
and so that's a reference to the

29
00:00:53,840 --> 00:00:57,920
privilege with which it runs

30
00:00:55,360 --> 00:00:59,199
also you'll hear that kernel ring runs

31
00:00:57,920 --> 00:01:00,719
in ring 0

32
00:00:59,199 --> 00:01:02,480
and so we're going to understand and

33
00:01:00,719 --> 00:01:04,000
explore what these rings are and how

34
00:01:02,480 --> 00:01:06,479
they're enforced

35
00:01:04,000 --> 00:01:07,920
so specifically on intel systems the

36
00:01:06,479 --> 00:01:09,840
lower the ring number the more

37
00:01:07,920 --> 00:01:12,640
privileged the code is intended to be

38
00:01:09,840 --> 00:01:13,119
so ring 0 is the most privileged code

39
00:01:12,640 --> 00:01:15,280
and then

40
00:01:13,119 --> 00:01:18,159
ring 1, ring 2, ring 3 are

41
00:01:15,280 --> 00:01:21,280
subsequently less privileged.

42
00:01:18,159 --> 00:01:23,360
Now intel envisioned things like this

43
00:01:21,280 --> 00:01:24,960
initially that ring 0 would be the

44
00:01:23,360 --> 00:01:27,439
operating system kernel

45
00:01:24,960 --> 00:01:28,880
and then in order to de-privilege the

46
00:01:27,439 --> 00:01:30,320
other code in the system so that it

47
00:01:28,880 --> 00:01:32,000
didn't have to you know have the

48
00:01:30,320 --> 00:01:34,560
capability to do everything

49
00:01:32,000 --> 00:01:36,000
you might have operating system services

50
00:01:34,560 --> 00:01:38,240
at rings 1 or

51
00:01:36,000 --> 00:01:40,479
2 and then the applications would run

52
00:01:38,240 --> 00:01:42,560
with the least privilege at ring 3.

53
00:01:40,479 --> 00:01:43,520
So you could imagine some basic you know

54
00:01:42,560 --> 00:01:46,079
operating system

55
00:01:43,520 --> 00:01:46,880
service like you know memory copy

56
00:01:46,079 --> 00:01:48,640
between

57
00:01:46,880 --> 00:01:50,240
processes or something like that and

58
00:01:48,640 --> 00:01:51,920
that might be code that the

59
00:01:50,240 --> 00:01:53,680
operating system would have run at ring

60
00:01:51,920 --> 00:01:55,040
level 3 so that if that code was

61
00:01:53,680 --> 00:01:56,880
compromised it couldn't do

62
00:01:55,040 --> 00:01:59,119
everything everywhere the way that it

63
00:01:56,880 --> 00:02:02,159
could if it was at level 0

64
00:01:59,119 --> 00:02:04,799
now that was the goal in practical

65
00:02:02,159 --> 00:02:05,200
uh in practical terms everyone pretty

66
00:02:04,799 --> 00:02:07,439
much

67
00:02:05,200 --> 00:02:09,520
all runs all of their kernel information

68
00:02:07,439 --> 00:02:10,160
all of the operating system services as

69
00:02:09,520 --> 00:02:12,480
it were

70
00:02:10,160 --> 00:02:14,400
at ring level 0 so basically you've

71
00:02:12,480 --> 00:02:15,680
got this giant blob of code running with

72
00:02:14,400 --> 00:02:17,520
the most privilege

73
00:02:15,680 --> 00:02:18,720
and that's why you know operating system

74
00:02:17,520 --> 00:02:20,400
security is difficult

75
00:02:18,720 --> 00:02:22,400
because there's so much code so much

76
00:02:20,400 --> 00:02:24,560
attack surface that

77
00:02:22,400 --> 00:02:25,440
users and attackers running at ring

78
00:02:24,560 --> 00:02:27,200
level 3

79
00:02:25,440 --> 00:02:29,520
have many places they can try to

80
00:02:27,200 --> 00:02:31,840
compromise the kernel.

81
00:02:29,520 --> 00:02:34,000
So you will occasionally see systems try

82
00:02:31,840 --> 00:02:36,480
to make use of these different privilege

83
00:02:34,000 --> 00:02:38,800
levels to try to de-privilege code

84
00:02:36,480 --> 00:02:41,440
and so paravirtualized xen tries to do

85
00:02:38,800 --> 00:02:41,840
this. "Paravirtualized" refers to the idea

86
00:02:41,440 --> 00:02:44,160
of

87
00:02:41,840 --> 00:02:46,000
modifying an operating system to make it

88
00:02:44,160 --> 00:02:48,640
easier to virtualize, to make it

89
00:02:46,000 --> 00:02:50,319
work in concert with the virtualization

90
00:02:48,640 --> 00:02:51,440
system instead of normal virtualization

91
00:02:50,319 --> 00:02:53,599
where you just try to

92
00:02:51,440 --> 00:02:54,720
completely you know take an existing

93
00:02:53,599 --> 00:02:56,319
thing as is

94
00:02:54,720 --> 00:02:59,040
and you know put it in a virtual

95
00:02:56,319 --> 00:03:00,800
environment so paravirtualized xen

96
00:02:59,040 --> 00:03:02,319
has a modified guest operating system

97
00:03:00,800 --> 00:03:04,400
and in that sort of environment

98
00:03:02,319 --> 00:03:05,920
you might have your ring zero being the

99
00:03:04,400 --> 00:03:08,640
the true hypervisor

100
00:03:05,920 --> 00:03:10,480
uh the virtual machine manager and then

101
00:03:08,640 --> 00:03:10,879
at a lower privilege level you might

102
00:03:10,480 --> 00:03:13,200
have

103
00:03:10,879 --> 00:03:15,200
for instance the management domain uh

104
00:03:13,200 --> 00:03:16,800
that does not have you know ring 0

105
00:03:15,200 --> 00:03:18,480
access doesn't can't do everything

106
00:03:16,800 --> 00:03:20,400
everywhere but it still has some

107
00:03:18,480 --> 00:03:22,480
privileges that are much more privileged

108
00:03:20,400 --> 00:03:23,920
than you know normal operating system

109
00:03:22,480 --> 00:03:27,519
itself for the

110
00:03:23,920 --> 00:03:29,200
uh or the user space programs that run

111
00:03:27,519 --> 00:03:30,560
so that's an example of how you could

112
00:03:29,200 --> 00:03:32,480
try to push some

113
00:03:30,560 --> 00:03:33,840
privileged code into ring 1 or ring

114
00:03:32,480 --> 00:03:36,000
2 but

115
00:03:33,840 --> 00:03:38,720
practically speaking most systems don't

116
00:03:36,000 --> 00:03:41,200
actually utilize this

117
00:03:38,720 --> 00:03:42,239
so in order to fully understand

118
00:03:41,200 --> 00:03:44,799
privilege rings

119
00:03:42,239 --> 00:03:46,080
we're going to have to take a large and

120
00:03:44,799 --> 00:03:48,799
lengthy detour

121
00:03:46,080 --> 00:03:49,280
through a technology called segmentation

122
00:03:48,799 --> 00:03:51,440
and

123
00:03:49,280 --> 00:03:53,519
in doing so we will find the you know

124
00:03:51,440 --> 00:03:54,720
presumably 2 bits which encode the

125
00:03:53,519 --> 00:03:57,920
4 ring levels

126
00:03:54,720 --> 00:04:00,879
0, 1, 2, 3 so let's go ahead and

127
00:03:57,920 --> 00:04:00,879
dig into that now.

