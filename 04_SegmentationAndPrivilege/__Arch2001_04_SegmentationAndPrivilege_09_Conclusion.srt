1
00:00:00,160 --> 00:00:03,280
So segmentation: Why did we even bother

2
00:00:02,399 --> 00:00:05,440
learning it?

3
00:00:03,280 --> 00:00:07,040
Well it's still influential on the

4
00:00:05,440 --> 00:00:07,919
system because of things like backwards

5
00:00:07,040 --> 00:00:09,040
compatibility

6
00:00:07,919 --> 00:00:11,120
anytime you're going to be running

7
00:00:09,040 --> 00:00:12,559
32-bit code on the 64-bit system it's

8
00:00:11,120 --> 00:00:14,000
definitely going to be in play

9
00:00:12,559 --> 00:00:15,679
but the most important reason for

10
00:00:14,000 --> 00:00:17,119
covering it was the reason that I gave

11
00:00:15,679 --> 00:00:19,039
it the very beginning of the section

12
00:00:17,119 --> 00:00:20,640
we want to understand at a deep level

13
00:00:19,039 --> 00:00:22,720
how privileged rings

14
00:00:20,640 --> 00:00:23,840
and permission separation works on the

15
00:00:22,720 --> 00:00:25,359
intel architecture

16
00:00:23,840 --> 00:00:28,000
and you can't do that if you don't

17
00:00:25,359 --> 00:00:30,640
understand segmentation

18
00:00:28,000 --> 00:00:32,239
it also is going to go on to influence a

19
00:00:30,640 --> 00:00:33,920
bunch of the topics that we cover in the

20
00:00:32,239 --> 00:00:35,360
rest of the class things like interrupts

21
00:00:33,920 --> 00:00:37,360
and system calls

22
00:00:35,360 --> 00:00:39,920
interrupts will ultimately be selecting

23
00:00:37,360 --> 00:00:41,920
things in the GDT or LDT

24
00:00:39,920 --> 00:00:43,440
system calls will ultimately be using

25
00:00:41,920 --> 00:00:47,360
the fs and gs

26
00:00:43,440 --> 00:00:49,920
registers as the msrs in particular

27
00:00:47,360 --> 00:00:51,199
so it's still important even though it

28
00:00:49,920 --> 00:00:54,160
you know ostensibly

29
00:00:51,199 --> 00:00:55,120
got neutered as part of the x86-64

30
00:00:54,160 --> 00:00:57,039
upgrades.

31
00:00:55,120 --> 00:00:58,960
Of course it's also very important to

32
00:00:57,039 --> 00:00:59,760
just understand how things work at a

33
00:00:58,960 --> 00:01:01,359
deep level

34
00:00:59,760 --> 00:01:03,840
and this is where you find the weird

35
00:01:01,359 --> 00:01:07,119
corner cases like I talked about in that

36
00:01:03,840 --> 00:01:08,960
Ionescu blog post also you're going to be

37
00:01:07,119 --> 00:01:10,799
seeing and you you know you may not

38
00:01:08,960 --> 00:01:11,520
understand it had you not see these kind

39
00:01:10,799 --> 00:01:13,040
of things

40
00:01:11,520 --> 00:01:15,520
you'll see that if you reverse engineer

41
00:01:13,040 --> 00:01:16,080
Windows and Linux you'll often times run

42
00:01:15,520 --> 00:01:19,200
into

43
00:01:16,080 --> 00:01:20,240
explicit usage of the fs and gs segment

44
00:01:19,200 --> 00:01:21,520
registers

45
00:01:20,240 --> 00:01:22,720
and now that you've understand the

46
00:01:21,520 --> 00:01:24,080
segments a little better you'll

47
00:01:22,720 --> 00:01:26,880
understand that AMD

48
00:01:24,080 --> 00:01:28,799
left fs and gs working specifically so

49
00:01:26,880 --> 00:01:31,600
operating system makers could use

50
00:01:28,799 --> 00:01:33,520
and put data structures at those

51
00:01:31,600 --> 00:01:34,720
segments because they had just already

52
00:01:33,520 --> 00:01:37,360
been doing it before

53
00:01:34,720 --> 00:01:39,040
they did the 64-bit systems so some

54
00:01:37,360 --> 00:01:40,000
miscellaneous instructions we picked up

55
00:01:39,040 --> 00:01:42,000
along the way

56
00:01:40,000 --> 00:01:43,920
things like the mov instruction the

57
00:01:42,000 --> 00:01:46,000
fact that it has a specific segment

58
00:01:43,920 --> 00:01:48,640
register version that would move

59
00:01:46,000 --> 00:01:50,399
into and out of a segment register the

60
00:01:48,640 --> 00:01:52,320
push and the pop of a segment register

61
00:01:50,399 --> 00:01:55,600
although we don't have a

62
00:01:52,320 --> 00:01:58,560
pop cs version the sgdt

63
00:01:55,600 --> 00:02:00,479
lgdt which load which store and load the

64
00:01:58,560 --> 00:02:01,360
gdtr the global descriptor table

65
00:02:00,479 --> 00:02:04,719
register

66
00:02:01,360 --> 00:02:08,640
and the sldt lldt which store and load

67
00:02:04,719 --> 00:02:08,640
the local descriptor table register

