1
00:00:00,000 --> 00:00:04,640
now let's look at the local descriptor

2
00:00:02,159 --> 00:00:07,359
table register ldtr

3
00:00:04,640 --> 00:00:08,800
so I said that this is a register that

4
00:00:07,359 --> 00:00:10,880
has a 16-bit

5
00:00:08,800 --> 00:00:12,480
segment selector like we already learned

6
00:00:10,880 --> 00:00:15,040
about which you had seen

7
00:00:12,480 --> 00:00:16,240
placed into the segment registers like

8
00:00:15,040 --> 00:00:18,720
cs, ds, es,

9
00:00:16,240 --> 00:00:19,279
etc and so it has a 16 bit segment

10
00:00:18,720 --> 00:00:21,279
selector

11
00:00:19,279 --> 00:00:24,320
and then it has a hidden portion just

12
00:00:21,279 --> 00:00:26,320
like those cs ss registers

13
00:00:24,320 --> 00:00:28,640
the hidden portion again acts as a sort

14
00:00:26,320 --> 00:00:30,000
of cache to take information from the

15
00:00:28,640 --> 00:00:32,719
table which here will be the

16
00:00:30,000 --> 00:00:34,160
GDT table and cache it here so that it

17
00:00:32,719 --> 00:00:36,160
doesn't have to be looked up from memory

18
00:00:34,160 --> 00:00:38,719
every time

19
00:00:36,160 --> 00:00:39,600
so the segment selector itself because

20
00:00:38,719 --> 00:00:41,440
it is a

21
00:00:39,600 --> 00:00:43,280
you know has a table indicator bit the

22
00:00:41,440 --> 00:00:44,079
table indicator bit must always point at

23
00:00:43,280 --> 00:00:46,640
0 to

24
00:00:44,079 --> 00:00:47,360
say it selects from the GDT because the

25
00:00:46,640 --> 00:00:50,000
ldtr

26
00:00:47,360 --> 00:00:52,160
is how you find the LDT and so you can't

27
00:00:50,000 --> 00:00:53,600
have a self-rent referential thing

28
00:00:52,160 --> 00:00:55,920
saying oh yeah I'm the

29
00:00:53,600 --> 00:00:58,960
ldtr and I point at the LDT otherwise

30
00:00:55,920 --> 00:01:01,520
you wouldn't be able to find the LDT

31
00:00:58,960 --> 00:01:03,120
so just like the gdtr there are special

32
00:01:01,520 --> 00:01:06,159
assembly instructions to

33
00:01:03,120 --> 00:01:08,880
read and write the lldt is

34
00:01:06,159 --> 00:01:09,920
load a 16-bit segment selector so again

35
00:01:08,880 --> 00:01:11,760
you can only

36
00:01:09,920 --> 00:01:14,000
hit this visible portion you can only

37
00:01:11,760 --> 00:01:17,439
load up the visible portion

38
00:01:14,000 --> 00:01:19,360
of the register and then sldt is again

39
00:01:17,439 --> 00:01:21,759
something that's not privileged and

40
00:01:19,360 --> 00:01:23,439
which will take the 16-bit value and

41
00:01:21,759 --> 00:01:27,840
store it out to memory if someone wants

42
00:01:23,439 --> 00:01:27,840
to read it

43
00:01:34,320 --> 00:01:37,680
all right now showing that slightly

44
00:01:35,759 --> 00:01:40,320
differently using the diagrams that I

45
00:01:37,680 --> 00:01:42,159
showed before if the LDT register the

46
00:01:40,320 --> 00:01:44,560
16-bit visible portion

47
00:01:42,159 --> 00:01:45,360
has a segment selector has some rpl I

48
00:01:44,560 --> 00:01:47,200
said that the

49
00:01:45,360 --> 00:01:48,960
table indicator must always point at the

50
00:01:47,200 --> 00:01:51,840
GDT because it can't be

51
00:01:48,960 --> 00:01:53,360
finding the LDT via itself and then

52
00:01:51,840 --> 00:01:54,880
let's just say the index you know

53
00:01:53,360 --> 00:01:56,560
pointed at 3

54
00:01:54,880 --> 00:01:58,640
all right so index pointing at 3

55
00:01:56,560 --> 00:01:59,680
would select some data structure from

56
00:01:58,640 --> 00:02:00,960
the GDT

57
00:01:59,680 --> 00:02:03,360
which we learn about in the next

58
00:02:00,960 --> 00:02:05,680
section and that would talk about you

59
00:02:03,360 --> 00:02:08,560
know here is the base here is the size

60
00:02:05,680 --> 00:02:09,440
of the LDT and then behind the scenes

61
00:02:08,560 --> 00:02:11,280
also

62
00:02:09,440 --> 00:02:13,360
this is filling in the hidden portion

63
00:02:11,280 --> 00:02:14,879
just caching the information about

64
00:02:13,360 --> 00:02:17,200
what is the base what is the limit what

65
00:02:14,879 --> 00:02:18,560
are the attributes of the LDT so that

66
00:02:17,200 --> 00:02:20,720
the hidden portion

67
00:02:18,560 --> 00:02:22,879
when you look at the LDT register

68
00:02:20,720 --> 00:02:24,160
someone's accessing LDT information it's

69
00:02:22,879 --> 00:02:27,599
all just cached here

70
00:02:24,160 --> 00:02:30,480
in the register for looking up the LDT

71
00:02:27,599 --> 00:02:32,160
okay so one more time the table

72
00:02:30,480 --> 00:02:34,239
indicator the segment selector

73
00:02:32,160 --> 00:02:36,640
is going to be some sort of thing that

74
00:02:34,239 --> 00:02:38,319
is stored in a register like cs ss and the

75
00:02:36,640 --> 00:02:42,319
segment registers we learned about

76
00:02:38,319 --> 00:02:43,920
or ldtr now we've learned about

77
00:02:42,319 --> 00:02:45,360
and we've got these special purpose

78
00:02:43,920 --> 00:02:46,720
registers that you know

79
00:02:45,360 --> 00:02:49,040
a table indicator might say it's

80
00:02:46,720 --> 00:02:50,959
pointing at some particular

81
00:02:49,040 --> 00:02:52,239
table but the special purpose registers

82
00:02:50,959 --> 00:02:54,560
are how the hardware actually finds

83
00:02:52,239 --> 00:02:57,280
those tables

84
00:02:54,560 --> 00:02:59,360
and then the tables themselves are a set

85
00:02:57,280 --> 00:03:01,680
of data structures that are stored in

86
00:02:59,360 --> 00:03:01,680
ram

87
00:03:01,920 --> 00:03:06,239
so what is the point of the LDT well the

88
00:03:04,480 --> 00:03:08,400
original point was to give different

89
00:03:06,239 --> 00:03:10,800
processes different views of memory

90
00:03:08,400 --> 00:03:12,560
because the LDT could have you know

91
00:03:10,800 --> 00:03:15,200
8000 different entries so you could

92
00:03:12,560 --> 00:03:17,760
imagine 8000 different processes have

93
00:03:15,200 --> 00:03:19,519
8000 different segments covering 8000

94
00:03:17,760 --> 00:03:21,200
different ways of looking at memory

95
00:03:19,519 --> 00:03:23,519
practically speaking no one actually

96
00:03:21,200 --> 00:03:24,799
uses it for that these days instead they

97
00:03:23,519 --> 00:03:26,560
use a mechanism called

98
00:03:24,799 --> 00:03:28,319
paging that we'll learn about later on

99
00:03:26,560 --> 00:03:30,400
in the class

100
00:03:28,319 --> 00:03:31,840
so there's a good blog post here which I

101
00:03:30,400 --> 00:03:33,200
recommend you check out which talks a

102
00:03:31,840 --> 00:03:35,599
little bit about how

103
00:03:33,200 --> 00:03:36,560
windows used the LDT for user mode

104
00:03:35,599 --> 00:03:39,280
scheduling

105
00:03:36,560 --> 00:03:41,280
but ultimately it was removed in windows

106
00:03:39,280 --> 00:03:43,599
10 update at some point

107
00:03:41,280 --> 00:03:44,879
in favor of another mechanism which you

108
00:03:43,599 --> 00:03:46,959
know we'll actually see

109
00:03:44,879 --> 00:03:48,959
about later on in this class but this is

110
00:03:46,959 --> 00:03:51,280
good sort of historical perspective

111
00:03:48,959 --> 00:03:52,400
and it'll show just how you know you can

112
00:03:51,280 --> 00:03:54,720
only read certain

113
00:03:52,400 --> 00:03:56,239
you know os internals type blog posts if

114
00:03:54,720 --> 00:03:59,680
you have the kind of information that's

115
00:03:56,239 --> 00:03:59,680
talked about in this class

