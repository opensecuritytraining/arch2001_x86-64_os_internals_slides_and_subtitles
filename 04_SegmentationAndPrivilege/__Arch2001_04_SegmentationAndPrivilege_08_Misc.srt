1
00:00:00,000 --> 00:00:04,080
So this is just a quick little video for

2
00:00:02,159 --> 00:00:05,920
a little miscellaneous fact that I

3
00:00:04,080 --> 00:00:07,359
couldn't find a good place to fit into

4
00:00:05,920 --> 00:00:08,880
the flows of the rest of the videos

5
00:00:07,359 --> 00:00:09,760
which were already quite frankly too

6
00:00:08,880 --> 00:00:11,759
long

7
00:00:09,760 --> 00:00:12,880
so now that we know a bit about

8
00:00:11,759 --> 00:00:14,880
segmentation

9
00:00:12,880 --> 00:00:16,560
it turns out that you've been using

10
00:00:14,880 --> 00:00:17,440
segmentation all along without even

11
00:00:16,560 --> 00:00:19,520
knowing it

12
00:00:17,440 --> 00:00:20,560
so segmentation can either be used

13
00:00:19,520 --> 00:00:22,960
implicitly or

14
00:00:20,560 --> 00:00:25,039
explicitly and the most common way is to

15
00:00:22,960 --> 00:00:27,199
actually be using it implicitly

16
00:00:25,039 --> 00:00:29,679
so the processor will actually be

17
00:00:27,199 --> 00:00:30,320
automatically implicitly choosing a

18
00:00:29,679 --> 00:00:32,880
segment

19
00:00:30,320 --> 00:00:34,480
register based on what type of access

20
00:00:32,880 --> 00:00:35,520
it's doing so if you're doing some sort

21
00:00:34,480 --> 00:00:37,920
of code

22
00:00:35,520 --> 00:00:39,760
like instruction fetches implicitly the

23
00:00:37,920 --> 00:00:40,480
processor is always using the code

24
00:00:39,760 --> 00:00:42,160
segment

25
00:00:40,480 --> 00:00:43,680
if you're doing any sort of pushing and

26
00:00:42,160 --> 00:00:44,239
popping from the stack or if you're

27
00:00:43,680 --> 00:00:47,840
using

28
00:00:44,239 --> 00:00:48,160
ebp or esp then it's implicitly using

29
00:00:47,840 --> 00:00:50,640
the

30
00:00:48,160 --> 00:00:52,399
stack segment register data access

31
00:00:50,640 --> 00:00:55,520
implicitly uses the ds

32
00:00:52,399 --> 00:00:59,120
for everything except there's special

33
00:00:55,520 --> 00:01:00,640
string destination things and also es is

34
00:00:59,120 --> 00:01:01,280
used when you're dealing with string

35
00:01:00,640 --> 00:01:03,039
instructions

36
00:01:01,280 --> 00:01:04,799
so what is a string instruction you

37
00:01:03,039 --> 00:01:07,280
recall that there was movs

38
00:01:04,799 --> 00:01:09,200
and rep movs those were those move

39
00:01:07,280 --> 00:01:10,320
string to string or repeated move string

40
00:01:09,200 --> 00:01:12,880
to string

41
00:01:10,320 --> 00:01:14,640
so I kind of, you know, didn't dwell on

42
00:01:12,880 --> 00:01:15,280
this fact when we were talking about

43
00:01:14,640 --> 00:01:17,920
them

44
00:01:15,280 --> 00:01:18,880
but behind the scenes those instructions

45
00:01:17,920 --> 00:01:20,960
are actually

46
00:01:18,880 --> 00:01:22,960
explicitly saying that they are moving

47
00:01:20,960 --> 00:01:26,720
from the data segment

48
00:01:22,960 --> 00:01:28,799
to the e segment the extra segment

49
00:01:26,720 --> 00:01:30,799
I feel like a long time ago I had

50
00:01:28,799 --> 00:01:33,200
actually seen some disassemblers that

51
00:01:30,799 --> 00:01:34,400
left off the ds and es and so the first time

52
00:01:33,200 --> 00:01:36,240
that I ever went and looked at the

53
00:01:34,400 --> 00:01:36,799
manual and saw these references I was

54
00:01:36,240 --> 00:01:38,079
like oh

55
00:01:36,799 --> 00:01:40,079
weird you know I never noticed that

56
00:01:38,079 --> 00:01:41,520
before and you know sometimes you might

57
00:01:40,079 --> 00:01:43,280
just straight up look at it

58
00:01:41,520 --> 00:01:44,720
you'll see the ds you'll see the es you

59
00:01:43,280 --> 00:01:45,600
won't know what they are and so you'll

60
00:01:44,720 --> 00:01:48,159
just kind of

61
00:01:45,600 --> 00:01:49,280
ignore it in your mind but but really

62
00:01:48,159 --> 00:01:51,680
it's using

63
00:01:49,280 --> 00:01:53,840
certainly on 32-bit systems it's using

64
00:01:51,680 --> 00:01:56,880
these segment registers

65
00:01:53,840 --> 00:01:58,079
behind the scenes now actually on 64-bit

66
00:01:56,880 --> 00:02:01,119
systems it is not

67
00:01:58,079 --> 00:02:02,640
implicitly using those anymore and so

68
00:02:01,119 --> 00:02:04,960
that's why it's funny that gdb

69
00:02:02,640 --> 00:02:06,560
actually still shows that the ds and es

70
00:02:04,960 --> 00:02:08,000
are used because according to the manual

71
00:02:06,560 --> 00:02:09,520
they're not actually used

72
00:02:08,000 --> 00:02:11,280
and even if they were they would just be

73
00:02:09,520 --> 00:02:12,720
zeros so you could really leave those

74
00:02:11,280 --> 00:02:15,920
off here but this is probably just

75
00:02:12,720 --> 00:02:18,160
left over from the disassembly

76
00:02:15,920 --> 00:02:19,840
from 32-bit disassembly as applied to

77
00:02:18,160 --> 00:02:23,040
64-bit

78
00:02:19,840 --> 00:02:24,800
now beyond the implicit usage of segment

79
00:02:23,040 --> 00:02:26,319
registers you can actually have the

80
00:02:24,800 --> 00:02:29,920
explicit usage

81
00:02:26,319 --> 00:02:32,879
so if you had you know mov from rbx

82
00:02:29,920 --> 00:02:34,640
the memory pointed to by rbx into rax

83
00:02:32,879 --> 00:02:37,519
you can actually have an explicit

84
00:02:34,640 --> 00:02:39,760
mov from the data pointed to by this

85
00:02:37,519 --> 00:02:41,920
logical address this far pointer so if

86
00:02:39,760 --> 00:02:46,000
this is a normal pointer a near pointer

87
00:02:41,920 --> 00:02:48,800
this is a far pointer so from fs:rbx

88
00:02:46,000 --> 00:02:49,760
into rax and you will actually see this

89
00:02:48,800 --> 00:02:52,160
sort of thing

90
00:02:49,760 --> 00:02:54,720
a lot in 32-bit Windows because on

91
00:02:52,160 --> 00:02:57,200
32-bit Windows they use the fs register

92
00:02:54,720 --> 00:02:58,640
we'll come back to that later on Windows

93
00:02:57,200 --> 00:03:01,040
64. they use the gs

94
00:02:58,640 --> 00:03:02,720
register but basically you know

95
00:03:01,040 --> 00:03:04,640
operating systems like we said the whole

96
00:03:02,720 --> 00:03:06,640
reason fs and gs are

97
00:03:04,640 --> 00:03:08,000
still working the same way they used to

98
00:03:06,640 --> 00:03:09,760
work for segmentation is because

99
00:03:08,000 --> 00:03:10,959
operating systems like to put data

100
00:03:09,760 --> 00:03:13,599
structures at

101
00:03:10,959 --> 00:03:15,360
fs or gs and different operating systems

102
00:03:13,599 --> 00:03:16,560
do different things with that

103
00:03:15,360 --> 00:03:18,560
so you know if you're reverse

104
00:03:16,560 --> 00:03:19,120
engineering Windows you'll frequently

105
00:03:18,560 --> 00:03:21,280
see

106
00:03:19,120 --> 00:03:22,560
ref or malware on Windows you'll see

107
00:03:21,280 --> 00:03:24,640
references to fs

108
00:03:22,560 --> 00:03:26,400
and some pointer and that's actually the

109
00:03:24,640 --> 00:03:29,680
start of some data structure plus

110
00:03:26,400 --> 00:03:31,280
some offset into that data structure

111
00:03:29,680 --> 00:03:32,799
so like I said this was just a quick

112
00:03:31,280 --> 00:03:35,680
miscellaneous point that there's

113
00:03:32,799 --> 00:03:37,920
implicit and explicit usage of segment

114
00:03:35,680 --> 00:03:40,720
registers that still goes on today

115
00:03:37,920 --> 00:03:41,200
even though you know 64-bit segmentation

116
00:03:40,720 --> 00:03:45,280
has been

117
00:03:41,200 --> 00:03:45,280
diminished in its capabilities

