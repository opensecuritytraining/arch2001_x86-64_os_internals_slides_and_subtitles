1
00:00:00,399 --> 00:00:03,520
Okay and here's just a quick little

2
00:00:02,240 --> 00:00:06,160
optional video

3
00:00:03,520 --> 00:00:07,040
as a throwback to how segmentation was

4
00:00:06,160 --> 00:00:10,719
slightly different

5
00:00:07,040 --> 00:00:11,120
for the 32-bit systems so I've provided

6
00:00:10,719 --> 00:00:13,759
a

7
00:00:11,120 --> 00:00:14,480
user space segment registers 32 which

8
00:00:13,759 --> 00:00:17,520
you can

9
00:00:14,480 --> 00:00:19,760
go ahead and make a 32-bit windows vm

10
00:00:17,520 --> 00:00:20,800
and then run that on there I just need

11
00:00:19,760 --> 00:00:23,600
to make sure that you

12
00:00:20,800 --> 00:00:24,880
you know run it build it as release and

13
00:00:23,600 --> 00:00:28,080
Win32 or

14
00:00:24,880 --> 00:00:29,840
x86 depending on what Visual Studio says

15
00:00:28,080 --> 00:00:31,279
and then you want to run it on a Windows

16
00:00:29,840 --> 00:00:33,920
10 32-bit system

17
00:00:31,279 --> 00:00:35,280
with the Visual Studio 2019

18
00:00:33,920 --> 00:00:37,840
redistributable

19
00:00:35,280 --> 00:00:39,440
package installed so I'll put a link to

20
00:00:37,840 --> 00:00:42,079
that in the

21
00:00:39,440 --> 00:00:44,480
web page basically you need that to run

22
00:00:42,079 --> 00:00:47,200
code that is compiled from visual studio

23
00:00:44,480 --> 00:00:48,640
with the free version which is what

24
00:00:47,200 --> 00:00:50,160
you're using right now

25
00:00:48,640 --> 00:00:52,879
so you can go ahead and run that on the

26
00:00:50,160 --> 00:00:56,719
32-bit vm and you can also

27
00:00:52,879 --> 00:00:58,879
compile the K_SegRegs32

28
00:00:56,719 --> 00:01:00,960
again making sure that it is compiled as

29
00:00:58,879 --> 00:01:01,760
32-bit from visual studio so you can use

30
00:01:00,960 --> 00:01:03,359
the same

31
00:01:01,760 --> 00:01:04,879
VM that you've been using thus far you

32
00:01:03,359 --> 00:01:07,040
just need to change your compiler

33
00:01:04,879 --> 00:01:09,600
options to compile as 32-bit

34
00:01:07,040 --> 00:01:10,400
and again you can just copy it over and

35
00:01:09,600 --> 00:01:12,000
uh and

36
00:01:10,400 --> 00:01:14,799
invoke it you can use the same kernel

37
00:01:12,000 --> 00:01:16,799
debugger mechanism to attach to your

38
00:01:14,799 --> 00:01:18,320
vm just make sure that you've closed the

39
00:01:16,799 --> 00:01:19,600
other one because they otherwise will

40
00:01:18,320 --> 00:01:22,799
conflict with the

41
00:01:19,600 --> 00:01:25,360
debug port that they're using

42
00:01:22,799 --> 00:01:26,000
so I did that and the net result of what

43
00:01:25,360 --> 00:01:29,600
I saw

44
00:01:26,000 --> 00:01:32,720
was that again for RPL

45
00:01:29,600 --> 00:01:34,000
of cs and ss we do see the difference we

46
00:01:32,720 --> 00:01:35,840
have index 1 2

47
00:01:34,000 --> 00:01:37,439
3 4 so the different indices for

48
00:01:35,840 --> 00:01:40,000
user in kernel space

49
00:01:37,439 --> 00:01:40,560
and we also see RPL of 0's over here

50
00:01:40,000 --> 00:01:43,840
in kernel

51
00:01:40,560 --> 00:01:47,360
and 3 over here in user space

52
00:01:43,840 --> 00:01:49,360
we see that the ds and es segments

53
00:01:47,360 --> 00:01:50,399
are the same between user space and

54
00:01:49,360 --> 00:01:52,159
kernel space

55
00:01:50,399 --> 00:01:54,079
and furthermore we see that the actual

56
00:01:52,159 --> 00:01:58,079
index is the same as the ss

57
00:01:54,079 --> 00:02:01,040
index from user space we see that the

58
00:01:58,079 --> 00:02:03,439
fs is actually different so we've got a

59
00:02:01,040 --> 00:02:05,439
6 over here for the index and a 7 over

60
00:02:03,439 --> 00:02:08,479
here for the index and user space

61
00:02:05,439 --> 00:02:09,679
so even though so these seem to be

62
00:02:08,479 --> 00:02:11,680
actually pointing at

63
00:02:09,679 --> 00:02:14,160
different data potentially different

64
00:02:11,680 --> 00:02:17,360
segments different chunks of memory

65
00:02:14,160 --> 00:02:19,599
and in the gs register we have

66
00:02:17,360 --> 00:02:22,720
0 which is a straight up invalid

67
00:02:19,599 --> 00:02:25,520
value you're never allowed to point at

68
00:02:22,720 --> 00:02:26,319
the GDT entry of 0 that's considered

69
00:02:25,520 --> 00:02:28,319
invalid

70
00:02:26,319 --> 00:02:30,400
and consequently you know that's not

71
00:02:28,319 --> 00:02:32,640
actually being used

72
00:02:30,400 --> 00:02:35,040
so the inferences from this is that

73
00:02:32,640 --> 00:02:37,680
windows maintains different cs, ss

74
00:02:35,040 --> 00:02:39,519
and fs segment selectors for user space

75
00:02:37,680 --> 00:02:42,800
in kernel space

76
00:02:39,519 --> 00:02:44,800
windows doesn't change ds or es it

77
00:02:42,800 --> 00:02:46,640
does seem to change fs as mentioned

78
00:02:44,800 --> 00:02:48,640
right there it doesn't use

79
00:02:46,640 --> 00:02:50,239
gs at all because that's completely

80
00:02:48,640 --> 00:02:52,640
invalid segment selector

81
00:02:50,239 --> 00:02:54,879
and once again the RPL field of cs and

82
00:02:52,640 --> 00:02:58,159
ss seems to correlate to

83
00:02:54,879 --> 00:02:59,120
kernel space and user space. So what's

84
00:02:58,159 --> 00:03:01,840
different from the

85
00:02:59,120 --> 00:03:02,560
windows x86-64 that we just saw a little

86
00:03:01,840 --> 00:03:05,200
bit

87
00:03:02,560 --> 00:03:07,200
well the in 32-bit the fs segment

88
00:03:05,200 --> 00:03:09,120
selector seems to change between kernel

89
00:03:07,200 --> 00:03:10,400
space and user space whereas it didn't

90
00:03:09,120 --> 00:03:12,720
in the 64-bit

91
00:03:10,400 --> 00:03:14,319
and also the gs segment selector was

92
00:03:12,720 --> 00:03:19,280
completely invalid

93
00:03:14,319 --> 00:03:21,599
uh in uh in 32-bit whereas in 64-bit it

94
00:03:19,280 --> 00:03:23,120
seems to be valid and it seems to be

95
00:03:21,599 --> 00:03:24,879
unchanging but again

96
00:03:23,120 --> 00:03:26,799
it seems to be we'll come back to you

97
00:03:24,879 --> 00:03:29,280
later why that may not actually be the

98
00:03:26,799 --> 00:03:29,280
case

