1
00:00:00,080 --> 00:00:04,160
okay well let's go look at that more

2
00:00:01,920 --> 00:00:06,000
concretely so that was kind of the the

3
00:00:04,160 --> 00:00:08,160
quick world one whirlwind tour

4
00:00:06,000 --> 00:00:10,559
of the segment descriptors so we

5
00:00:08,160 --> 00:00:12,400
previously saw the gdtr and we saw the

6
00:00:10,559 --> 00:00:15,040
gdtl we saw how those

7
00:00:12,400 --> 00:00:17,279
uh point at the table itself so now

8
00:00:15,040 --> 00:00:20,000
let's go ahead and go look at the table

9
00:00:17,279 --> 00:00:22,000
manually and we can eyeball parse that

10
00:00:20,000 --> 00:00:22,960
or we could just let WinDbg do the

11
00:00:22,000 --> 00:00:25,359
hard work for us

12
00:00:22,960 --> 00:00:26,080
and we can use the dt display type

13
00:00:25,359 --> 00:00:28,080
command

14
00:00:26,080 --> 00:00:30,000
and we can recursively display type to

15
00:00:28,080 --> 00:00:32,559
dig down into the subtypes and we

16
00:00:30,000 --> 00:00:33,360
can use a type that microsoft gives us

17
00:00:32,559 --> 00:00:36,640
the nt

18
00:00:33,360 --> 00:00:38,719
_KGDTENTRY64 is a data

19
00:00:36,640 --> 00:00:41,440
structure that microsoft uses to define

20
00:00:38,719 --> 00:00:44,640
a GDT entry so we'll essentially cast

21
00:00:41,440 --> 00:00:47,840
the gdtr the base address the base

22
00:00:44,640 --> 00:00:51,840
memory address for the GDT table into

23
00:00:47,840 --> 00:00:54,000
a KGDTENTRY64 and then we'll add some

24
00:00:51,840 --> 00:00:55,440
offset to look at entries in it so you

25
00:00:54,000 --> 00:00:56,719
know we'll see that the first two

26
00:00:55,440 --> 00:00:57,760
entries are 0 and so they're

27
00:00:56,719 --> 00:01:00,239
uninteresting they're

28
00:00:57,760 --> 00:01:00,800
not valid entries and so we'll just

29
00:01:00,239 --> 00:01:02,399
offset

30
00:01:00,800 --> 00:01:04,400
you know we'll assume that each of the

31
00:01:02,399 --> 00:01:04,879
things is 8 this may not actually be

32
00:01:04,400 --> 00:01:06,400
true

33
00:01:04,879 --> 00:01:08,159
because we saw that system segment

34
00:01:06,400 --> 00:01:10,320
selectors can be 16 but let's just

35
00:01:08,159 --> 00:01:13,840
assume they're 8 and go to index 2

36
00:01:10,320 --> 00:01:13,840
let's hop into the kernel debugger

37
00:01:14,799 --> 00:01:18,479
and let's grab those commands

38
00:01:19,520 --> 00:01:26,880
first one is display as quad words

39
00:01:23,680 --> 00:01:29,600
the gdtr so there we go entry 0

40
00:01:26,880 --> 00:01:31,040
is all zeros so that's not present so

41
00:01:29,600 --> 00:01:32,799
that's not interesting

42
00:01:31,040 --> 00:01:34,079
that's all 0 so that's not present

43
00:01:32,799 --> 00:01:37,119
that's not interesting

44
00:01:34,079 --> 00:01:38,640
and then here is the next entry so

45
00:01:37,119 --> 00:01:40,479
we could you know display it as quad

46
00:01:38,640 --> 00:01:41,520
words we could display it as dwords you

47
00:01:40,479 --> 00:01:43,840
know whatever

48
00:01:41,520 --> 00:01:45,520
makes it more understandable to you

49
00:01:43,840 --> 00:01:47,119
keeping in mind that you know see this

50
00:01:45,520 --> 00:01:47,840
has to do with endianness right if

51
00:01:47,119 --> 00:01:51,439
you're displaying

52
00:01:47,840 --> 00:01:53,600
dwords at a time but basically you

53
00:01:51,439 --> 00:01:54,560
could eyeball parse these and you know

54
00:01:53,600 --> 00:01:56,560
as a

55
00:01:54,560 --> 00:01:57,680
exercise for yourself I do recommend

56
00:01:56,560 --> 00:01:59,439
that you go

57
00:01:57,680 --> 00:02:01,200
back and look at the slides and parse

58
00:01:59,439 --> 00:02:02,640
through these these are going to be the

59
00:02:01,200 --> 00:02:04,000
lower bytes these are going to be the

60
00:02:02,640 --> 00:02:06,640
upper bytes

61
00:02:04,000 --> 00:02:08,000
how does that you know parse well these

62
00:02:06,640 --> 00:02:09,679
are the lower bytes these

63
00:02:08,000 --> 00:02:11,360
are the upper bytes because this is

64
00:02:09,679 --> 00:02:15,440
bytes 0 through 3

65
00:02:11,360 --> 00:02:17,280
this is bytes 4 through 7

66
00:02:15,440 --> 00:02:19,360
so you could do that that way or we said

67
00:02:17,280 --> 00:02:22,400
you can do it easier mode

68
00:02:19,360 --> 00:02:26,160
with the dt display type command

69
00:02:22,400 --> 00:02:26,160
so let's go ahead and do that now

70
00:02:27,360 --> 00:02:30,720
so if we do that we saw that the raw

71
00:02:29,760 --> 00:02:33,599
description

72
00:02:30,720 --> 00:02:35,040
of entry 2 is that it's you know all

73
00:02:33,599 --> 00:02:38,400
zeros on the bottom

74
00:02:35,040 --> 00:02:42,480
and then it's 0 0 9 b

75
00:02:38,400 --> 00:02:44,959
2 00 and so if we parse that

76
00:02:42,480 --> 00:02:45,920
it would say okay the limit low is 0

77
00:02:44,959 --> 00:02:48,640
the base low

78
00:02:45,920 --> 00:02:50,000
is 0 uh some bytes some flags in the

79
00:02:48,640 --> 00:02:54,720
middle that's not super useful

80
00:02:50,000 --> 00:02:57,200
names the base middle that would be

81
00:02:54,720 --> 00:02:58,560
this element right here that's the base

82
00:02:57,200 --> 00:03:00,879
middle

83
00:02:58,560 --> 00:03:03,360
base middle is 0 the type which we

84
00:03:00,879 --> 00:03:04,080
said is a 5-byte value in microsoft's

85
00:03:03,360 --> 00:03:07,120
world is

86
00:03:04,080 --> 00:03:10,959
1 so that is a code or data segment

87
00:03:07,120 --> 00:03:14,959
and then it's 1011 so b

88
00:03:10,959 --> 00:03:19,200
so what was b b

89
00:03:14,959 --> 00:03:22,319
1011

90
00:03:19,200 --> 00:03:24,080
is a 64-bit TSS

91
00:03:22,319 --> 00:03:25,840
all right that's the wrong table and

92
00:03:24,080 --> 00:03:29,519
it's like that doesn't make sense

93
00:03:25,840 --> 00:03:33,200
b from this a code or data segment

94
00:03:29,519 --> 00:03:36,400
b is a execute

95
00:03:33,200 --> 00:03:40,319
it's a non-conforming execute/read

96
00:03:36,400 --> 00:03:41,920
code segment right so that is kind of

97
00:03:40,319 --> 00:03:43,680
how you would go through and interpret

98
00:03:41,920 --> 00:03:44,560
these you can see that it says present

99
00:03:43,680 --> 00:03:47,200
is 1

100
00:03:44,560 --> 00:03:47,599
you can see DPL is 0 so this would be a

101
00:03:47,200 --> 00:03:51,280
ring

102
00:03:47,599 --> 00:03:51,840
0 code segment descriptor the limit high

103
00:03:51,280 --> 00:03:55,439
is 0

104
00:03:51,840 --> 00:03:57,200
the system flag is 0. so ok they do

105
00:03:55,439 --> 00:03:58,959
pull that out as well the system flag

106
00:03:57,200 --> 00:04:03,040
despite being in the type they also

107
00:03:58,959 --> 00:04:06,319
pull it out the long mode the L

108
00:04:03,040 --> 00:04:07,760
flag is 0 actually system based on

109
00:04:06,319 --> 00:04:11,519
the bits being there is probably

110
00:04:07,760 --> 00:04:15,599
the available flag they're using that

111
00:04:11,519 --> 00:04:18,239
calling it system there and then the D/B

112
00:04:15,599 --> 00:04:19,600
is 0 and the granularity is 0 and

113
00:04:18,239 --> 00:04:22,400
the base high is 0

114
00:04:19,600 --> 00:04:23,759
so each of those bit fields are just

115
00:04:22,400 --> 00:04:26,840
elements out of the

116
00:04:23,759 --> 00:04:29,120
segment descriptor as shown in these

117
00:04:26,840 --> 00:04:30,560
slides

118
00:04:29,120 --> 00:04:32,400
all right well that's one way that you

119
00:04:30,560 --> 00:04:34,160
can look at things and so again I do

120
00:04:32,400 --> 00:04:35,280
recommend you go through and look at

121
00:04:34,160 --> 00:04:37,360
every single entry

122
00:04:35,280 --> 00:04:38,880
manually parsing it just to you know

123
00:04:37,360 --> 00:04:40,560
confirm that it matches your

124
00:04:38,880 --> 00:04:42,720
expectations

125
00:04:40,560 --> 00:04:44,000
but I tried to make this a little bit

126
00:04:42,720 --> 00:04:46,800
easier for you

127
00:04:44,000 --> 00:04:49,759
so I had told you to download a WinDbg

128
00:04:46,800 --> 00:04:51,600
extension called swishdbgext

129
00:04:49,759 --> 00:04:54,800
so you should have that on your desktop

130
00:04:51,600 --> 00:04:57,759
and if you open up the solution file

131
00:04:54,800 --> 00:04:58,800
you will have this plugin you can

132
00:04:57,759 --> 00:05:02,160
right-click on it

133
00:04:58,800 --> 00:05:02,479
and rebuild it make sure that it's set

134
00:05:02,160 --> 00:05:06,160
for

135
00:05:02,479 --> 00:05:09,520
64-bit if it's not by default

136
00:05:06,160 --> 00:05:12,400
and when this finishes building

137
00:05:09,520 --> 00:05:13,120
what we'll have is a extension that we

138
00:05:12,400 --> 00:05:17,039
can

139
00:05:13,120 --> 00:05:20,720
use to pretty print the GDT entries

140
00:05:17,039 --> 00:05:22,960
so I basically expanded this

141
00:05:20,720 --> 00:05:24,479
so this comes from this github

142
00:05:22,960 --> 00:05:26,000
repository via

143
00:05:24,479 --> 00:05:27,840
comaeio I don't know the right

144
00:05:26,000 --> 00:05:29,840
pronunciation of that

145
00:05:27,840 --> 00:05:32,160
but we're going to use the load command

146
00:05:29,840 --> 00:05:33,280
in order to load the dll this plugin

147
00:05:32,160 --> 00:05:35,680
once it has been

148
00:05:33,280 --> 00:05:38,000
compiled and we can unload it afterwards

149
00:05:35,680 --> 00:05:40,160
and then specifically the ms_gdt

150
00:05:38,000 --> 00:05:41,680
command already existed I just made it a

151
00:05:40,160 --> 00:05:43,280
little bit more verbose

152
00:05:41,680 --> 00:05:46,160
so that it would print out some more of

153
00:05:43,280 --> 00:05:49,120
the fields that I cared about

154
00:05:46,160 --> 00:05:49,520
so once you compile this command or

155
00:05:49,120 --> 00:05:52,960
sorry

156
00:05:49,520 --> 00:05:56,000
compile this extension you can copy the

157
00:05:52,960 --> 00:05:56,800
absolute path where it compiled it to go

158
00:05:56,000 --> 00:06:00,319
into

159
00:05:56,800 --> 00:06:02,000
WinDbg do bang load and the absolute

160
00:06:00,319 --> 00:06:04,319
path to the plugin

161
00:06:02,000 --> 00:06:05,280
and now at this point you can do ms

162
00:06:04,319 --> 00:06:08,960
underscore

163
00:06:05,280 --> 00:06:12,240
gdt and that will print out

164
00:06:08,960 --> 00:06:15,120
not one but actually multiple GDTs

165
00:06:12,240 --> 00:06:16,880
so I had not actually said this

166
00:06:15,120 --> 00:06:21,360
explicitly because I was kind of waiting

167
00:06:16,880 --> 00:06:23,759
for here to spring it on me but GDTs LDT

168
00:06:21,360 --> 00:06:24,560
GDT registers LDT registers task

169
00:06:23,759 --> 00:06:26,800
registers

170
00:06:24,560 --> 00:06:28,240
I guess we haven't seen that yet these

171
00:06:26,800 --> 00:06:31,039
registers are actually

172
00:06:28,240 --> 00:06:32,720
per processor so I've given my virtual

173
00:06:31,039 --> 00:06:34,639
machine 4 processors

174
00:06:32,720 --> 00:06:35,840
and that means each processor is going

175
00:06:34,639 --> 00:06:38,160
to have a different

176
00:06:35,840 --> 00:06:39,840
GDT register in the same way that each

177
00:06:38,160 --> 00:06:40,720
processor has different general purpose

178
00:06:39,840 --> 00:06:42,639
registers

179
00:06:40,720 --> 00:06:45,120
so the operating system actually has to

180
00:06:42,639 --> 00:06:47,440
set up different GDTs for each of the

181
00:06:45,120 --> 00:06:49,360
different processors

182
00:06:47,440 --> 00:06:50,560
so in this pretty print version of it

183
00:06:49,360 --> 00:06:54,080
this first thing is

184
00:06:50,560 --> 00:06:56,000
core so this is core 0 core 1

185
00:06:54,080 --> 00:06:57,680
core 2 core 3 so my 4

186
00:06:56,000 --> 00:06:59,840
processors

187
00:06:57,680 --> 00:07:03,280
index so this is the index in the gt

188
00:06:59,840 --> 00:07:05,280
GDT index is 0 1 2 3 4 through 8

189
00:07:03,280 --> 00:07:07,120
present is it present or not if it's not

190
00:07:05,280 --> 00:07:07,919
present well then it's garbage so who

191
00:07:07,120 --> 00:07:10,120
cares

192
00:07:07,919 --> 00:07:11,599
all right so this is present this is

193
00:07:10,120 --> 00:07:15,360
DPL 0

194
00:07:11,599 --> 00:07:18,160
the raw system flag plus type is 1b

195
00:07:15,360 --> 00:07:18,560
and what does that interpret to it is a

196
00:07:18,160 --> 00:07:21,199
code

197
00:07:18,560 --> 00:07:22,960
segment it is read/execute and it is

198
00:07:21,199 --> 00:07:25,039
accessed

199
00:07:22,960 --> 00:07:26,560
all right so basically then you can just

200
00:07:25,039 --> 00:07:28,400
you know look down the line here we've

201
00:07:26,560 --> 00:07:30,319
got code segment data segment code

202
00:07:28,400 --> 00:07:33,440
segment data segment code segment

203
00:07:30,319 --> 00:07:36,800
we've got DPL 0, 0, 3, 3

204
00:07:33,440 --> 00:07:39,919
so those seem like kernel code data

205
00:07:36,800 --> 00:07:41,039
this seems like uh user space code and

206
00:07:39,919 --> 00:07:43,280
data then we've got

207
00:07:41,039 --> 00:07:44,080
some other thing right here which you'll

208
00:07:43,280 --> 00:07:45,680
have to

209
00:07:44,080 --> 00:07:47,520
figure out you know who's using that

210
00:07:45,680 --> 00:07:49,759
who's accessing it which

211
00:07:47,520 --> 00:07:52,000
segment registers or pointing at that

212
00:07:49,759 --> 00:07:54,319
versus somewhere else

213
00:07:52,000 --> 00:07:55,919
then we've got a not present entry

214
00:07:54,319 --> 00:07:58,560
followed by a TSS

215
00:07:55,919 --> 00:08:00,400
and we'll learn about the TSS entries in

216
00:07:58,560 --> 00:08:02,479
a coming section I can't remember if

217
00:08:00,400 --> 00:08:05,120
it's the next section or not

218
00:08:02,479 --> 00:08:06,720
but so this is basically just a nice way

219
00:08:05,120 --> 00:08:08,879
to dig into the GDT

220
00:08:06,720 --> 00:08:10,240
and see what is there so again I

221
00:08:08,879 --> 00:08:13,280
recommend you do

222
00:08:10,240 --> 00:08:16,319
things the hard way first so go ahead

223
00:08:13,280 --> 00:08:17,120
and look at things with the raw qword

224
00:08:16,319 --> 00:08:19,599
or dword

225
00:08:17,120 --> 00:08:21,520
output go look at the slides and

226
00:08:19,599 --> 00:08:23,120
manually parse those by hand so that you

227
00:08:21,520 --> 00:08:23,759
can see what you think they are write it

228
00:08:23,120 --> 00:08:25,680
down

229
00:08:23,759 --> 00:08:27,039
on a piece of paper or on a tablet off

230
00:08:25,680 --> 00:08:29,120
to the side

231
00:08:27,039 --> 00:08:30,319
then after you've written it down go

232
00:08:29,120 --> 00:08:33,200
ahead and

233
00:08:30,319 --> 00:08:35,440
parse it using this data structure you

234
00:08:33,200 --> 00:08:36,560
know cast use display type to cast it to

235
00:08:35,440 --> 00:08:38,159
a data structure

236
00:08:36,560 --> 00:08:40,240
go look in that make sure you looked at

237
00:08:38,159 --> 00:08:41,839
the fields correctly and that they match

238
00:08:40,240 --> 00:08:43,440
you know what you did before so that you

239
00:08:41,839 --> 00:08:44,880
can see if you have any endianness

240
00:08:43,440 --> 00:08:47,440
issues for

241
00:08:44,880 --> 00:08:49,279
grabbing the fields from there and then

242
00:08:47,440 --> 00:08:52,800
finally after you've done all that

243
00:08:49,279 --> 00:08:55,120
go ahead and run the ms_gdt command after

244
00:08:52,800 --> 00:08:59,200
loading up the plugin and see whether

245
00:08:55,120 --> 00:08:59,200
you were parsing it all correctly or not

