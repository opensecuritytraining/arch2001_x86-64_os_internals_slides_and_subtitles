1
00:00:00,320 --> 00:00:03,840
Stop! We've actually overshot what you

2
00:00:02,720 --> 00:00:06,799
need to know to discuss

3
00:00:03,840 --> 00:00:08,400
protection rings so protection rings are

4
00:00:06,799 --> 00:00:10,480
the interactions between the

5
00:00:08,400 --> 00:00:13,120
requested privilege level the thing that

6
00:00:10,480 --> 00:00:15,120
you saw inside of segment selectors

7
00:00:13,120 --> 00:00:16,320
descriptor privilege level the thing

8
00:00:15,120 --> 00:00:19,600
that you saw inside of

9
00:00:16,320 --> 00:00:22,160
segment descriptors and introducing the

10
00:00:19,600 --> 00:00:23,760
current privilege level the CPL is

11
00:00:22,160 --> 00:00:25,199
defined as the protection level of the

12
00:00:23,760 --> 00:00:29,359
currently executing

13
00:00:25,199 --> 00:00:31,920
code segment so basically it is just

14
00:00:29,359 --> 00:00:33,040
the bits 0 and 1 of the segment

15
00:00:31,920 --> 00:00:34,559
selector

16
00:00:33,040 --> 00:00:37,840
for whatever segment selector is

17
00:00:34,559 --> 00:00:39,600
currently in the code segment register

18
00:00:37,840 --> 00:00:41,440
so when it comes to privilege

19
00:00:39,600 --> 00:00:43,680
enforcement the rings are actually

20
00:00:41,440 --> 00:00:44,719
hardware enforced as we said at the very

21
00:00:43,680 --> 00:00:46,320
very beginning

22
00:00:44,719 --> 00:00:48,079
and the hardware is going to enforce it

23
00:00:46,320 --> 00:00:48,640
when particular things are happening

24
00:00:48,079 --> 00:00:50,399
such as

25
00:00:48,640 --> 00:00:52,399
instruction fetches meaning code

26
00:00:50,399 --> 00:00:54,559
execution and data fetches

27
00:00:52,399 --> 00:00:56,000
meaning just reading and writing data so

28
00:00:54,559 --> 00:00:57,520
if for instance you were trying to do

29
00:00:56,000 --> 00:00:59,600
control flow transition

30
00:00:57,520 --> 00:01:01,440
from one segment to a different segment

31
00:00:59,600 --> 00:01:02,559
via the typical control flow

32
00:01:01,440 --> 00:01:05,600
instructions like

33
00:01:02,559 --> 00:01:07,280
jump conditional jumps call return

34
00:01:05,600 --> 00:01:09,520
the hardware is going to automatically

35
00:01:07,280 --> 00:01:12,640
check for the target segment

36
00:01:09,520 --> 00:01:13,600
is the DPL greater than or equal to the

37
00:01:12,640 --> 00:01:15,360
CPL so

38
00:01:13,600 --> 00:01:16,640
the current privilege level which is

39
00:01:15,360 --> 00:01:19,360
whatever is in the

40
00:01:16,640 --> 00:01:20,400
bottom 2 bits of the code segment

41
00:01:19,360 --> 00:01:22,960
register

42
00:01:20,400 --> 00:01:23,520
is that value less than or equal to the

43
00:01:22,960 --> 00:01:26,880
target

44
00:01:23,520 --> 00:01:29,600
DPL right so if you're in ring 3

45
00:01:26,880 --> 00:01:30,960
is the place that you're going have a

46
00:01:29,600 --> 00:01:33,840
value that is

47
00:01:30,960 --> 00:01:34,479
greater than or equal to 3 if it's

48
00:01:33,840 --> 00:01:36,079
0

49
00:01:34,479 --> 00:01:37,759
well 0 is not greater than or equal

50
00:01:36,079 --> 00:01:39,360
to 3 so it wouldn't allow a

51
00:01:37,759 --> 00:01:41,680
transition from ring 0

52
00:01:39,360 --> 00:01:43,680
to ring 3 unless for instance it was

53
00:01:41,680 --> 00:01:45,759
those conforming segments

54
00:01:43,680 --> 00:01:47,600
sorry from ring 3 to ring 0

55
00:01:45,759 --> 00:01:49,439
unless it was a conforming segment

56
00:01:47,600 --> 00:01:51,759
another place that the hardware enforces

57
00:01:49,439 --> 00:01:53,280
it is privileged instructions

58
00:01:51,759 --> 00:01:55,520
so you can see this location in the

59
00:01:53,280 --> 00:01:56,799
manual where there's a big old list of

60
00:01:55,520 --> 00:01:58,799
privileged instructions

61
00:01:56,799 --> 00:02:00,799
but we've already seen some examples

62
00:01:58,799 --> 00:02:03,360
things like loading the GDT

63
00:02:00,799 --> 00:02:05,119
register and loading the LDT register

64
00:02:03,360 --> 00:02:06,719
are things where we indicate with red

65
00:02:05,119 --> 00:02:08,959
star that those can only be done

66
00:02:06,719 --> 00:02:11,200
from CPL 0 and that's just hard coded

67
00:02:08,959 --> 00:02:12,879
into the way that the processor executes

68
00:02:11,200 --> 00:02:15,200
now you may be saying to yourself well

69
00:02:12,879 --> 00:02:16,800
if CPL is just the lower two bits of cs

70
00:02:15,200 --> 00:02:19,200
I'll just go ahead and write those 2

71
00:02:16,800 --> 00:02:20,879
bits to 0 and I will be in ring 0

72
00:02:19,200 --> 00:02:22,480
and of course intel says yeah right

73
00:02:20,879 --> 00:02:24,800
that's not going to happen

74
00:02:22,480 --> 00:02:27,120
they say both that the mov instruction

75
00:02:24,800 --> 00:02:29,200
cannot be used to load the cs register

76
00:02:27,120 --> 00:02:30,000
otherwise it'll cause an invalid opcode

77
00:02:29,200 --> 00:02:32,560
exception

78
00:02:30,000 --> 00:02:34,640
and there's no way to load up the cs

79
00:02:32,560 --> 00:02:36,319
register via pop because there literally

80
00:02:34,640 --> 00:02:39,680
is no pop cs

81
00:02:36,319 --> 00:02:42,400
though there is a pop ss, ds, etc so

82
00:02:39,680 --> 00:02:43,120
how do you set the cs to become ring

83
00:02:42,400 --> 00:02:44,640
0

84
00:02:43,120 --> 00:02:46,800
well that's a little bit of a mystery

85
00:02:44,640 --> 00:02:48,640
for now we'll see one answer to that

86
00:02:46,800 --> 00:02:50,720
mystery in the next section

87
00:02:48,640 --> 00:02:53,200
but there's many different possible ways

88
00:02:50,720 --> 00:02:55,680
to answer that question

89
00:02:53,200 --> 00:02:57,040
so the conclusion for privilege rings is

90
00:02:55,680 --> 00:02:58,480
that ultimately it's the kernel's

91
00:02:57,040 --> 00:03:01,599
responsibility to set up

92
00:02:58,480 --> 00:03:03,840
userspace before it transitions from

93
00:03:01,599 --> 00:03:05,920
ring 0 to ring 3 it has to set up

94
00:03:03,840 --> 00:03:06,800
userspace in such a way that it's going

95
00:03:05,920 --> 00:03:09,120
to have

96
00:03:06,800 --> 00:03:11,040
a code segment register where the

97
00:03:09,120 --> 00:03:13,200
segment selector has an RPL

98
00:03:11,040 --> 00:03:14,480
of 3 and that'll mean that your CPL

99
00:03:13,200 --> 00:03:17,280
will be 3

100
00:03:14,480 --> 00:03:18,480
and that whatever selector is being used

101
00:03:17,280 --> 00:03:21,680
should be selecting

102
00:03:18,480 --> 00:03:23,440
a descriptor which has a DPL of 3 as

103
00:03:21,680 --> 00:03:25,200
well

104
00:03:23,440 --> 00:03:26,959
and so again like I just said the

105
00:03:25,200 --> 00:03:28,879
segment selector RPL of 3

106
00:03:26,959 --> 00:03:31,200
will mean that it has a CPL current

107
00:03:28,879 --> 00:03:32,799
privilege level of 3

108
00:03:31,200 --> 00:03:34,799
by doing this the kernel will

109
00:03:32,799 --> 00:03:37,680
effectively have created code that's

110
00:03:34,799 --> 00:03:38,879
running in ring 3 aka userspace aka

111
00:03:37,680 --> 00:03:40,159
user mode

112
00:03:38,879 --> 00:03:42,239
likewise it's the kernel's

113
00:03:40,159 --> 00:03:43,920
responsibility to make sure that when it

114
00:03:42,239 --> 00:03:44,480
sets up its own kernelspace so that

115
00:03:43,920 --> 00:03:47,519
when it

116
00:03:44,480 --> 00:03:49,280
exits out down to userspace the less

117
00:03:47,519 --> 00:03:51,440
privileged area that it still has a way

118
00:03:49,280 --> 00:03:52,480
to get back a way to transition back to

119
00:03:51,440 --> 00:03:54,400
kernelspace

120
00:03:52,480 --> 00:03:56,159
and that within kernelspace the cs

121
00:03:54,400 --> 00:03:57,280
register has a segment selector with an

122
00:03:56,159 --> 00:03:58,720
RPL of 0

123
00:03:57,280 --> 00:04:02,000
points at a segment descriptor with a

124
00:03:58,720 --> 00:04:03,920
DPL of 0 and in so doing CPL equals

125
00:04:02,000 --> 00:04:06,239
0 when the code is running

126
00:04:03,920 --> 00:04:07,920
and that's why we say that kernelspace

127
00:04:06,239 --> 00:04:09,439
code runs in ring 0

128
00:04:07,920 --> 00:04:11,599
we can call it kernelspace we can call

129
00:04:09,439 --> 00:04:13,599
it ring kernel mode

130
00:04:11,599 --> 00:04:15,760
but ultimately what it comes down to is

131
00:04:13,599 --> 00:04:17,199
that certain bits are set to 0

132
00:04:15,760 --> 00:04:18,959
to indicate that it has the most

133
00:04:17,199 --> 00:04:19,680
privilege and permission it can execute

134
00:04:18,959 --> 00:04:21,199
the most

135
00:04:19,680 --> 00:04:23,199
every single assembly instruction on the

136
00:04:21,199 --> 00:04:24,720
system and so forth

137
00:04:23,199 --> 00:04:26,800
so there's all sorts of little corner

138
00:04:24,720 --> 00:04:28,160
cases and weird little things about how

139
00:04:26,800 --> 00:04:29,919
all the privileged checks go

140
00:04:28,160 --> 00:04:32,639
so if you really want to know the super

141
00:04:29,919 --> 00:04:35,440
details then you can rtfm volume 3

142
00:04:32,639 --> 00:04:37,199
section 5.5 where it talks about every

143
00:04:35,440 --> 00:04:39,280
little detail that there is

144
00:04:37,199 --> 00:04:40,800
and with that Sonic has successfully

145
00:04:39,280 --> 00:04:43,040
completed his journey

146
00:04:40,800 --> 00:04:44,320
trying to find how privileged rings work

147
00:04:43,040 --> 00:04:48,320
and collecting

148
00:04:44,320 --> 00:04:48,320
this amazing giant ring

