1
00:00:00,54 --> 00:00:00,92
Okay

2
00:00:00,92 --> 00:00:03,64
So we're on the hunt for information that can teach

3
00:00:03,64 --> 00:00:06,51
us about how the intel privilege rings work.

4
00:00:06,51 --> 00:00:07,98
And we said we have to do a bit of

5
00:00:07,98 --> 00:00:09,75
a detour through segmentation

6
00:00:10,24 --> 00:00:13,86
So what is segmentation? Segmentation provides a mechanism for providing

7
00:00:13,86 --> 00:00:18,1
the processors addressable memory space called the linear address space

8
00:00:18,16 --> 00:00:21,82
into smaller protected address spaces called segments

9
00:00:21,83 --> 00:00:24,84
So we're going to be talking about linear address spaces

10
00:00:24,84 --> 00:00:26,96
and we're going to be talking about segments and you

11
00:00:26,96 --> 00:00:29,91
could have segmentation being all super complicated like this,

12
00:00:29,91 --> 00:00:32,2
where you've got this segment in that segment and they're

13
00:00:32,2 --> 00:00:34,51
all just little chunks of the linear address space

14
00:00:34,52 --> 00:00:36,66
But nobody actually does things that way

15
00:00:37,24 --> 00:00:40,75
Could have a less complicated version where you've got one

16
00:00:40,75 --> 00:00:41,65
segment for code,

17
00:00:41,65 --> 00:00:42,71
one segment for data,

18
00:00:42,71 --> 00:00:47,12
one segment for stack and then three extra segments limitations

19
00:00:47,12 --> 00:00:48,96
based on how many segment registers you have

20
00:00:49,34 --> 00:00:51,16
But nobody does it that way either

21
00:00:51,75 --> 00:00:53,68
This is closer to the way people do it,

22
00:00:53,69 --> 00:00:54,82
but not exactly

23
00:00:54,83 --> 00:00:57,61
And you could have a basic flat model where the

24
00:00:57,61 --> 00:01:01,08
linear address space is just one giant flat memory space

25
00:01:01,3 --> 00:01:04,18
This picture implying that it was only four gigabytes,

26
00:01:04,18 --> 00:01:06,74
but in 64 bit systems that would be a much

27
00:01:06,74 --> 00:01:07,46
larger space.

28
00:01:09,04 --> 00:01:12,49
So segment addressing is something that is fundamental and required

29
00:01:12,49 --> 00:01:13,89
for intel hardware,

30
00:01:13,9 --> 00:01:15,93
even though most people don't know about it

31
00:01:15,93 --> 00:01:18,64
And it's the kind of thing that like operating systems

32
00:01:18,64 --> 00:01:21,06
people just set up and then kind of forget.

33
00:01:21,94 --> 00:01:25,83
So segment addressing is that you're going to use what's

34
00:01:25,83 --> 00:01:27,39
called a "logical address",

35
00:01:27,4 --> 00:01:30,71
also known as a far pointer as your starting point

36
00:01:30,71 --> 00:01:33,01
So you start with a logical address and from there

37
00:01:33,01 --> 00:01:36,06
you're trying to get to a physical address and the

38
00:01:36,06 --> 00:01:38,78
physical address space is defined as the range of addresses

39
00:01:38,78 --> 00:01:41,15
that the processor can generate on its address bus.

40
00:01:41,23 --> 00:01:43,44
So that means address bus is

41
00:01:43,44 --> 00:01:46,05
the thing going out to RAM for instance.

42
00:01:46,44 --> 00:01:49,25
And so it's saying you start with a logical address

43
00:01:49,25 --> 00:01:51,37
you're going to need to convert it to something called

44
00:01:51,37 --> 00:01:52,51
a linear address,

45
00:01:52,51 --> 00:01:54,23
which is what we were just talking about

46
00:01:54,24 --> 00:01:57,14
And that's treated as the 64-bit address space

47
00:01:57,15 --> 00:01:59,46
And from that 64-bit address space,

48
00:01:59,46 --> 00:02:02,43
you're going to find some specific address in the physical

49
00:02:02,43 --> 00:02:02,96
range.

50
00:02:04,74 --> 00:02:06,23
So we said logical addresses,

51
00:02:06,23 --> 00:02:09,01
which is where we start is also known as a

52
00:02:09,01 --> 00:02:09,85
far pointer

53
00:02:09,93 --> 00:02:12,64
So just to show you in 32-bit systems,

54
00:02:12,64 --> 00:02:15,59
we normally think of pointers as just some 32-bit

55
00:02:15,59 --> 00:02:15,96
value.

56
00:02:16,34 --> 00:02:18,82
So what is a far pointer then?

57
00:02:18,83 --> 00:02:21,81
A far pointer is a 32-bit value with a

58
00:02:21,81 --> 00:02:24,22
segment selector put on the front of it

59
00:02:24,22 --> 00:02:26,06
So that is a 16-bit value

60
00:02:26,74 --> 00:02:29,68
So similarly in 64-bit we have some notion of

61
00:02:29,68 --> 00:02:31,34
pointers and far pointers

62
00:02:31,46 --> 00:02:35,0
There's actually three forms of far pointer here for

63
00:02:35,0 --> 00:02:35,26
64-bit

64
00:02:35,26 --> 00:02:36,47
But to make things simple,

65
00:02:36,47 --> 00:02:38,56
we're just going to ignore those latter 2

66
00:02:39,04 --> 00:02:40,56
So we're going to think of it like a near

67
00:02:40,56 --> 00:02:40,97
pointer

68
00:02:40,97 --> 00:02:43,22
The pointer that you all know and love is just

69
00:02:43,22 --> 00:02:44,79
some 64-bit value

70
00:02:44,82 --> 00:02:48,9
And a Far pointer is the 64-bit value having

71
00:02:48,9 --> 00:02:51,64
a prefix of the 16-bit segment selector

72
00:02:51,67 --> 00:02:55,52
Basically saying I want this particular segment as specified by

73
00:02:55,52 --> 00:02:59,13
a segment selector And this particular offset as specified by

74
00:02:59,13 --> 00:03:01,81
the 64-bit value that you normally think of as

75
00:03:01,81 --> 00:03:02,26
a pointer

76
00:03:04,24 --> 00:03:07,46
So once again restating what kind of address spaces we're

77
00:03:07,46 --> 00:03:08,32
dealing with here

78
00:03:08,37 --> 00:03:11,78
We've got logical addresses which are far pointers saying I

79
00:03:11,78 --> 00:03:13,66
want this segment and that offset,

80
00:03:14,04 --> 00:03:17,12
Which gets turned into a particular linear address space

81
00:03:17,12 --> 00:03:20,25
Somewhere in the 64 bit or 32-bit on old

82
00:03:20,25 --> 00:03:25,31
systems range of addresses that can then get translated into

83
00:03:25,31 --> 00:03:26,69
a virtual address

84
00:03:26,7 --> 00:03:29,66
And that is something where it takes paging into account

85
00:03:30,04 --> 00:03:32,74
and then that further goes to a physical address which

86
00:03:32,74 --> 00:03:34,73
is the actual location in RAM

87
00:03:34,9 --> 00:03:37,28
That gets set out for lookup in the physical

88
00:03:37,28 --> 00:03:37,75
hardware.

89
00:03:38,44 --> 00:03:41,04
Now we're going to pretend that there is no such

90
00:03:41,04 --> 00:03:42,29
thing as paging right now,

91
00:03:42,3 --> 00:03:44,36
we're just going to pretend it's disabled until we learn

92
00:03:44,36 --> 00:03:45,08
about it later

93
00:03:45,09 --> 00:03:46,46
And in reality,

94
00:03:46,46 --> 00:03:48,93
when the processor starts up in real mode,

95
00:03:48,93 --> 00:03:52,57
when we talked about before the different execution modes of

96
00:03:52,57 --> 00:03:53,56
an intel processor,

97
00:03:53,57 --> 00:03:54,72
When it starts in real mode,

98
00:03:54,72 --> 00:03:55,96
there is no paging,

99
00:03:56,24 --> 00:03:57,85
there is no even support for paging

100
00:03:57,85 --> 00:04:01,05
You don't get that support until you get into protected

101
00:04:01,05 --> 00:04:01,42
mode

102
00:04:01,43 --> 00:04:04,38
So really at the very beginning of code execution,

103
00:04:04,39 --> 00:04:05,86
you've got logical addresses,

104
00:04:05,86 --> 00:04:08,74
you've got some notional linear address space and you've got

105
00:04:08,74 --> 00:04:09,6
physical addresses

106
00:04:09,6 --> 00:04:11,62
So that's what we're going to do for now

107
00:04:11,62 --> 00:04:12,91
We're going to just assume well,

108
00:04:12,91 --> 00:04:14,26
when you get to a linear address,

109
00:04:14,27 --> 00:04:16,19
just treat it like it's going to be exactly the

110
00:04:16,19 --> 00:04:17,35
same physical address

111
00:04:17,94 --> 00:04:19,62
So shown slightly differently

112
00:04:19,62 --> 00:04:23,35
We have segmentation translating logical addresses,

113
00:04:23,36 --> 00:04:24,62
a far pointer,

114
00:04:24,62 --> 00:04:28,5
a segment selector and a offset into linear addresses

115
00:04:28,5 --> 00:04:29,68
So how does it do this?

116
00:04:29,69 --> 00:04:32,47
It does this through some sort of table lookup

117
00:04:32,48 --> 00:04:35,26
And so we set a segment selector basically says what

118
00:04:35,26 --> 00:04:38,12
segment you want to use that is going to be

119
00:04:38,12 --> 00:04:40,86
selecting out of some sort of table that has a

120
00:04:40,86 --> 00:04:42,06
description of the segment

121
00:04:42,44 --> 00:04:44,46
That's going to use that description to find where the

122
00:04:44,46 --> 00:04:46,04
base of the segment is

123
00:04:46,05 --> 00:04:48,68
And it's going to add this offset to the base

124
00:04:48,75 --> 00:04:50,15
to get a linear address

125
00:04:50,74 --> 00:04:53,46
So logical address for a pointer,

126
00:04:54,64 --> 00:04:58,47
64 bit value plus a 16 bit segment selector translated

127
00:04:58,47 --> 00:05:00,31
through a table to a linear address

128
00:05:00,31 --> 00:05:02,35
Somewhere in the 64 bit address space

129
00:05:04,14 --> 00:05:05,62
Same exact picture

130
00:05:05,63 --> 00:05:08,01
Just a slightly bigger version of it

131
00:05:08,05 --> 00:05:09,41
So what do we have?

132
00:05:09,41 --> 00:05:10,89
We have a logical address,

133
00:05:10,89 --> 00:05:14,09
a segment selector and an offset translated through some sort

134
00:05:14,09 --> 00:05:16,31
of table gives you a linear address

135
00:05:16,32 --> 00:05:19,4
And then now we're saying paging is disabled

136
00:05:19,4 --> 00:05:21,06
So I've just hidden it from you

137
00:05:21,07 --> 00:05:23,36
And so we say when there's no paging,

138
00:05:23,54 --> 00:05:24,41
A linear address,

139
00:05:24,41 --> 00:05:25,24
maps directly,

140
00:05:25,24 --> 00:05:28,47
one to one to a physical address and that's your actual

141
00:05:28,47 --> 00:05:28,81
RAM

142
00:05:28,81 --> 00:05:32,12
So this is all about translating these logical addresses

143
00:05:32,12 --> 00:05:34,78
All the way through to physical RAM address is where

144
00:05:34,78 --> 00:05:35,55
you find the data

145
00:05:37,14 --> 00:05:37,44
Now,

146
00:05:37,45 --> 00:05:41,34
here is what the manual says about x86-64

147
00:05:41,35 --> 00:05:44,75
And what it really comes down to is that people

148
00:05:44,75 --> 00:05:47,91
were never using segmentation to its full potential in 32

149
00:05:47,91 --> 00:05:48,8
bit systems

150
00:05:48,81 --> 00:05:52,56
And so when AMD made the x86-64 extensions

151
00:05:52,59 --> 00:05:55,64
they more or less disabled segmentation,

152
00:05:55,69 --> 00:05:59,16
but they kept some particular exceptional cases

153
00:05:59,74 --> 00:06:01,49
So what it says is in 64-bit

154
00:06:01,49 --> 00:06:03,43
mode segmentation is generally,

155
00:06:03,45 --> 00:06:05,33
but not completely disabled

156
00:06:05,42 --> 00:06:09,45
It creates a flat 64-bit linear address space and

157
00:06:09,45 --> 00:06:12,59
the processor treats the segment base of the CS DS

158
00:06:12,59 --> 00:06:14,16
ES and SS as zero

159
00:06:14,17 --> 00:06:16,65
So it basically means there's going to be these registers

160
00:06:16,65 --> 00:06:17,74
We'll talk about in a second

161
00:06:17,75 --> 00:06:20,53
But any time you're using these registers,

162
00:06:20,66 --> 00:06:21,9
the hardware basically says,

163
00:06:21,9 --> 00:06:23,49
I don't care what all the table lookup

164
00:06:23,49 --> 00:06:24,17
junk says,

165
00:06:24,17 --> 00:06:26,15
I'm just going to say the base addresses is zero

166
00:06:26,16 --> 00:06:30,75
So if a logical addresses segment selector plus an offset

167
00:06:30,76 --> 00:06:33,16
and it's finding some base and it's adding the offset

168
00:06:33,16 --> 00:06:33,76
to the base

169
00:06:34,14 --> 00:06:35,04
All that doesn't matter

170
00:06:35,04 --> 00:06:35,67
It's just saying,

171
00:06:35,67 --> 00:06:37,21
you know what treat the base as zero,

172
00:06:37,22 --> 00:06:40,1
so zero plus whatever the 64-bit offset is

173
00:06:40,11 --> 00:06:41,83
So effectively it's no,

174
00:06:41,83 --> 00:06:42,0
uh,

175
00:06:42,01 --> 00:06:44,65
it does nothing and it just treats the 64-bit

176
00:06:44,65 --> 00:06:46,56
value like a 64-bit value

177
00:06:47,84 --> 00:06:51,23
But the exceptions are FS and GS segments

178
00:06:51,26 --> 00:06:55,39
So basically these particular segment registers and the usage of

179
00:06:55,39 --> 00:06:57,82
them are going to continue to function like they did

180
00:06:57,82 --> 00:07:00,13
on 32-bit and they're going to use table lookups

181
00:07:00,14 --> 00:07:04,06
and they're going to use the segments based on that.

182
00:07:04,74 --> 00:07:06,39
So the point is,

183
00:07:06,39 --> 00:07:06,59
you know,

184
00:07:06,59 --> 00:07:08,15
nobody ever used it like this

185
00:07:08,15 --> 00:07:09,82
Nobody ever used it like this

186
00:07:09,83 --> 00:07:12,12
People didn't even really use it like this

187
00:07:12,12 --> 00:07:12,84
Close,

188
00:07:12,84 --> 00:07:14,04
but no cigar

189
00:07:14,05 --> 00:07:17,38
The way people actually used it in operating systems Windows

190
00:07:17,38 --> 00:07:18,00
Linux,

191
00:07:18,01 --> 00:07:18,45
Mac OS

192
00:07:19,04 --> 00:07:21,16
What they did typically is that,

193
00:07:21,16 --> 00:07:21,41
you know,

194
00:07:21,41 --> 00:07:22,16
CS, SS,

195
00:07:22,16 --> 00:07:23,82
these first segments,

196
00:07:23,87 --> 00:07:26,66
they would tend to be set to some completely flat

197
00:07:26,66 --> 00:07:30,3
cover the entire linear address space segment

198
00:07:30,31 --> 00:07:33,36
So they'd make one giant segment for all of 2

199
00:07:33,36 --> 00:07:35,91
to the 32 or all of to the 64-bit

200
00:07:35,92 --> 00:07:37,25
linear address space

201
00:07:37,54 --> 00:07:40,11
And they just point these registers at a big flat

202
00:07:40,11 --> 00:07:40,56
segment

203
00:07:41,14 --> 00:07:45,1
But different operating systems would use these FS and GS

204
00:07:45,1 --> 00:07:48,85
to point out some sort of operating system specific register

205
00:07:48,86 --> 00:07:49,33
Sorry,

206
00:07:49,33 --> 00:07:50,05
data structure

207
00:07:50,64 --> 00:07:53,65
So basically when AMD was doing the 64-bit extensions

208
00:07:53,65 --> 00:07:54,04
they said,

209
00:07:54,04 --> 00:07:54,6
well,

210
00:07:54,61 --> 00:07:57,59
we don't want to break everybody who's using this particular

211
00:07:57,59 --> 00:07:59,66
register to point out some data structure

212
00:07:59,66 --> 00:08:01,43
so we'll leave that alone and we don't want to

213
00:08:01,43 --> 00:08:03,7
break people who are using this particular register to point

214
00:08:03,7 --> 00:08:05,34
at some particular data structure

215
00:08:05,35 --> 00:08:06,15
So we'll leave that alone

216
00:08:06,94 --> 00:08:10,55
So that was kind of just codifying what everybody already

217
00:08:10,55 --> 00:08:13,56
did into the actual behavior of the hardware.

218
00:08:14,14 --> 00:08:17,32
So we said segmentation is about translating logical addresses to

219
00:08:17,32 --> 00:08:18,37
linear addresses

220
00:08:18,37 --> 00:08:19,7
To physical addresses

221
00:08:19,76 --> 00:08:22,5
And we said logical addresses are far pointers and far

222
00:08:22,5 --> 00:08:25,66
pointers are 16 bit segment selector plus the offset

223
00:08:26,14 --> 00:08:27,33
And so what is that?

224
00:08:27,33 --> 00:08:28,74
16 segment selector

225
00:08:28,74 --> 00:08:29,86
What exactly is in there?

226
00:08:31,04 --> 00:08:31,5
Well,

227
00:08:31,51 --> 00:08:35,18
that's 16 segment selector is basically a data structure with

228
00:08:35,18 --> 00:08:36,08
three fields

229
00:08:36,09 --> 00:08:38,53
A thing called RPL: requested privilege level, a thing

230
00:08:38,53 --> 00:08:41,06
called table indicator, and a thing called index

231
00:08:42,24 --> 00:08:45,83
So segment selector is basically going to be looking up

232
00:08:45,83 --> 00:08:49,35
or selecting some segment from some table

233
00:08:49,74 --> 00:08:51,63
There's two tables and we're going to learn more about

234
00:08:51,63 --> 00:08:52,14
them later

235
00:08:52,14 --> 00:08:53,83
So I'm leaving the details out for now

236
00:08:53,96 --> 00:08:56,53
There's a global descriptor table and there's a local descriptor

237
00:08:56,53 --> 00:08:56,85
table.

238
00:08:57,44 --> 00:09:00,53
This index is going to be indexing into these two

239
00:09:00,53 --> 00:09:04,03
tables and these tables are tables of data structures that

240
00:09:04,03 --> 00:09:05,44
describe segments,

241
00:09:05,44 --> 00:09:06,35
chunks of memory

242
00:09:07,64 --> 00:09:08,06
So,

243
00:09:10,25 --> 00:09:12,96
And the last point was because this is a 13

244
00:09:12,96 --> 00:09:13,9
bit index,

245
00:09:13,9 --> 00:09:17,32
that means there can be 8000 particular data structures in

246
00:09:17,32 --> 00:09:18,16
each of these tables

247
00:09:18,64 --> 00:09:22,64
But the interesting thing about this segment selector is this

248
00:09:22,64 --> 00:09:25,51
2-bit RPL, "requested privilege level".

249
00:09:25,52 --> 00:09:29,68
So privilege level sounds suspiciously like rings

250
00:09:29,68 --> 00:09:31,93
So let's keep that in mind for a little bit

251
00:09:31,94 --> 00:09:32,86
we'll come back to it.

252
00:09:34,64 --> 00:09:40,48
So there are 6 segment registers on Intel processors And

253
00:09:40,48 --> 00:09:44,59
each of those segment registers holds a 16-bit segment

254
00:09:44,59 --> 00:09:45,11
selector

255
00:09:45,12 --> 00:09:47,96
So the register looks like a 16-bit register and

256
00:09:47,96 --> 00:09:49,13
it holds that data structure,

257
00:09:49,13 --> 00:09:51,05
that 16-bit data structure we just saw

258
00:09:51,64 --> 00:09:55,67
And so the notional naming convention here was CS for

259
00:09:55,67 --> 00:09:56,7
the code segment

260
00:09:56,71 --> 00:09:58,61
So it would have a segment selector

261
00:09:58,61 --> 00:10:01,03
That segment selector would point at a table

262
00:10:01,03 --> 00:10:04,04
The table would say here is the chunk of linear

263
00:10:04,04 --> 00:10:06,06
address space that corresponds to the code segment

264
00:10:06,64 --> 00:10:08,21
So code segment stack segment,

265
00:10:08,21 --> 00:10:09,16
data segment,

266
00:10:09,33 --> 00:10:11,25
extra segment E F G

267
00:10:11,25 --> 00:10:13,65
We're all just sort of extra data segments that people

268
00:10:13,65 --> 00:10:14,54
could potentially use

269
00:10:14,54 --> 00:10:16,15
But I said they never actually did

270
00:10:18,44 --> 00:10:19,72
So back to the very beginning

271
00:10:19,72 --> 00:10:20,35
The introduction,

272
00:10:20,35 --> 00:10:20,78
we said,

273
00:10:20,78 --> 00:10:21,01
you know,

274
00:10:21,01 --> 00:10:23,12
here's some extra stuff that we're going to introduce in

275
00:10:23,12 --> 00:10:23,72
this class

276
00:10:23,74 --> 00:10:26,9
You've already seen all these general purpose registers

277
00:10:26,95 --> 00:10:29,8
But now we are seeing these segment registers

278
00:10:29,8 --> 00:10:34,0
There are six 16-bit segment registers and address space

279
00:10:34,01 --> 00:10:36,1
we're talking about linear address spaces

280
00:10:36,1 --> 00:10:39,55
This nominal 2 to the 64 bit space,

281
00:10:40,64 --> 00:10:41,66
which you know,

282
00:10:41,66 --> 00:10:44,06
is how we access memory

283
00:10:44,44 --> 00:10:47,67
So we're going to be covering a ton about linear

284
00:10:47,67 --> 00:10:49,76
address space and all the various translations here

285
00:10:51,04 --> 00:10:53,67
So briefly like how do you actually set to these

286
00:10:53,67 --> 00:10:54,56
segment registers,

287
00:10:54,56 --> 00:10:55,17
CS,

288
00:10:55,18 --> 00:10:57,16
DS SS etc?

289
00:10:57,54 --> 00:10:57,79
Well,

290
00:10:57,79 --> 00:11:00,22
there are mov instructions and if you want to find

291
00:11:00,22 --> 00:11:01,14
this in the manual,

292
00:11:01,14 --> 00:11:03,01
they are just under the normal mov

293
00:11:03,02 --> 00:11:06,32
So amongst the many different forms of mov are these

294
00:11:06,33 --> 00:11:09,52
segment register things moving from an

295
00:11:09,52 --> 00:11:09,63
r/m16

296
00:11:09,63 --> 00:11:13,47
to segment register and from segment register to an

297
00:11:13,47 --> 00:11:13,62
r/m16

298
00:11:13,62 --> 00:11:13,74
r/m16

299
00:11:13,74 --> 00:11:14,25
r/m16

300
00:11:16,64 --> 00:11:20,32
There are also push & pop versions of these where you

301
00:11:20,32 --> 00:11:23,23
can push the various segment registers onto the stack and

302
00:11:23,23 --> 00:11:24,98
then you can pop values back on

303
00:11:24,99 --> 00:11:28,52
Although I would note that there is no pop CS

304
00:11:28,53 --> 00:11:31,46
type of pop instruction

305
00:11:31,84 --> 00:11:34,66
And again these pops and pushes are just things that

306
00:11:34,66 --> 00:11:37,34
you will find in the normal push and pop manual

307
00:11:37,34 --> 00:11:37,86
page.

308
00:11:39,34 --> 00:11:39,77
All right now,

309
00:11:39,77 --> 00:11:42,93
there's an interesting thing about segment selectors

310
00:11:42,94 --> 00:11:46,13
We normally talk about these segment registers as if they

311
00:11:46,13 --> 00:11:46,66
are

312
00:11:47,04 --> 00:11:49,85
Uh Sorry I said there's interesting about segments selectors

313
00:11:49,86 --> 00:11:52,26
I meant there's an interesting thing about segment registers

314
00:11:52,64 --> 00:11:55,49
We normally talk about these registers as if they're just

315
00:11:55,49 --> 00:11:58,66
16-bit values that hold a segment selector,

316
00:11:59,14 --> 00:12:01,87
but in reality there is a visible part and then

317
00:12:01,87 --> 00:12:02,95
there's a hidden part

318
00:12:03,34 --> 00:12:06,45
Essentially this hidden part is going to be a cache

319
00:12:06,45 --> 00:12:10,24
of information from those lookup tables that gets stuffed

320
00:12:10,25 --> 00:12:10,93
back in here

321
00:12:10,93 --> 00:12:13,95
So you can never actually directly access the hidden part

322
00:12:13,96 --> 00:12:16,59
All you can do is change the visible part to

323
00:12:16,59 --> 00:12:19,22
point to a different entry in some table to then

324
00:12:19,23 --> 00:12:20,86
fill in these hidden parts

325
00:12:21,54 --> 00:12:24,15
So let's see how exactly that works.

326
00:12:25,24 --> 00:12:30,83
So the six segment selectors have the visible part and

327
00:12:30,83 --> 00:12:31,75
the hidden part.

328
00:12:32,14 --> 00:12:36,86
But we said AMD's x86-64 extensions say

329
00:12:36,87 --> 00:12:39,67
you know what CS, SS, DS, ES those

330
00:12:39,67 --> 00:12:42,61
are always going to go ahead and treat the segment

331
00:12:42,61 --> 00:12:44,35
that they're pointing at as if it had a base

332
00:12:44,35 --> 00:12:46,37
of 0 and a limit of 2 to 64 minus

333
00:12:46,37 --> 00:12:46,56
1.

334
00:12:47,14 --> 00:12:50,52
So that's actually included in this hard code in this

335
00:12:50,52 --> 00:12:51,28
hidden part

336
00:12:51,29 --> 00:12:54,65
So these values for the base that this thing points

337
00:12:54,65 --> 00:12:58,47
at or the limit the size effectively of the segment

338
00:12:58,51 --> 00:13:00,85
that it points at these are never actually going to be

339
00:13:00,85 --> 00:13:02,48
filled in from the table for CS

340
00:13:02,48 --> 00:13:02,85
SS

341
00:13:02,86 --> 00:13:03,17
SS

342
00:13:03,17 --> 00:13:03,37
SS

343
00:13:03,37 --> 00:13:05,76
DS and ES they're always just hard coded

344
00:13:06,04 --> 00:13:10,16
And that's why we say that For a large part

345
00:13:10,16 --> 00:13:12,64
of the functionality on 64-bit systems,

346
00:13:12,64 --> 00:13:16,15
these segment segment mechanism is disabled because these are now hard

347
00:13:16,15 --> 00:13:18,77
coded and we said FS and GS are

348
00:13:18,77 --> 00:13:19,6
not hard coded

349
00:13:19,6 --> 00:13:22,79
Those still behave the way that things used to where

350
00:13:22,83 --> 00:13:26,65
these cached information comes from the tables

351
00:13:27,14 --> 00:13:29,06
So let's look at that further.

352
00:13:29,94 --> 00:13:32,71
So if we look at the visible portion of the

353
00:13:32,71 --> 00:13:32,93
CS

354
00:13:32,93 --> 00:13:33,11
CS

355
00:13:33,11 --> 00:13:37,59
Register the segment selector and we say let's go ahead

356
00:13:37,59 --> 00:13:38,21
and parse that

357
00:13:38,21 --> 00:13:38,69
It's got an

358
00:13:38,69 --> 00:13:39,02
RPL,

359
00:13:39,02 --> 00:13:40,26
a TI, and an index

360
00:13:40,74 --> 00:13:43,17
If the TI happens to be set to 0,

361
00:13:43,17 --> 00:13:44,41
then according to this,

362
00:13:44,41 --> 00:13:46,78
the table indicator of 0 means it's going to point

363
00:13:46,78 --> 00:13:49,69
at a table called the GDT that will learn about

364
00:13:49,69 --> 00:13:50,55
in the next section

365
00:13:50,94 --> 00:13:53,92
So points at some table of data structures and the

366
00:13:53,92 --> 00:13:54,44
index,

367
00:13:54,45 --> 00:13:56,46
let's say that it points at 3

368
00:13:56,84 --> 00:13:59,39
It's going to just be selecting some particular data structure

369
00:13:59,39 --> 00:14:00,0
within that

370
00:14:00,01 --> 00:14:03,07
And that's supposed to say like this data structure says

371
00:14:03,07 --> 00:14:06,5
like where is the memory for the code segment?

372
00:14:06,51 --> 00:14:08,53
But now here in 64-bit land,

373
00:14:08,54 --> 00:14:11,27
it's not really taking a base or a limit from

374
00:14:11,27 --> 00:14:11,46
there,

375
00:14:11,46 --> 00:14:12,76
it's just always assuming 0

376
00:14:13,34 --> 00:14:17,48
But the thing that is taken from these tables is

377
00:14:17,49 --> 00:14:22,97
access information, effectively permissions information saying who is allowed to

378
00:14:22,97 --> 00:14:25,81
access this memory for this particular code segment?

379
00:14:25,87 --> 00:14:28,9
Is the kernel allowed to? Is user space allowed to?

380
00:14:28,91 --> 00:14:29,45
hint-hint?

381
00:14:29,84 --> 00:14:32,78
Uh And so this is information that gets filled into

382
00:14:32,78 --> 00:14:33,65
the hidden portion

383
00:14:34,84 --> 00:14:38,37
If we instead looked at a different segment register,

384
00:14:38,37 --> 00:14:39,5
the FS Register,

385
00:14:39,5 --> 00:14:41,65
we said that one is not just hard coded to

386
00:14:41,65 --> 00:14:43,04
always treat everything like 0

387
00:14:43,04 --> 00:14:44,56
base and 64-bit limit

388
00:14:45,14 --> 00:14:47,48
So if that table indicator just happened to be set

389
00:14:47,48 --> 00:14:48,22
to 1,

390
00:14:48,23 --> 00:14:51,04
that would then be pointing at the different data structure

391
00:14:51,05 --> 00:14:51,61
table,

392
00:14:51,62 --> 00:14:52,55
the LDT

393
00:14:51,96 --> 00:14:52,1


394
00:14:52,1 --> 00:14:52,55


395
00:14:52,94 --> 00:14:55,47
And let's say the index was 1 out of that

396
00:14:55,49 --> 00:14:58,21
then it would just be selecting some data structure and

397
00:14:58,21 --> 00:15:01,49
the information from that data structure would go into the

398
00:15:01,49 --> 00:15:03,25
hidden part of the

399
00:15:03,25 --> 00:15:03,72
FS

400
00:15:03,74 --> 00:15:04,55
Register.

401
00:15:05,74 --> 00:15:08,47
So just sort of different behavior for those

402
00:15:08,47 --> 00:15:08,85
CS

403
00:15:08,86 --> 00:15:09,35
ES

404
00:15:09,36 --> 00:15:09,56
SS

405
00:15:09,56 --> 00:15:10,08


406
00:15:10,09 --> 00:15:10,5
DS

407
00:15:10,5 --> 00:15:10,9


408
00:15:11,02 --> 00:15:13,55
They don't accept the base and limit from the tables

409
00:15:13,55 --> 00:15:17,58
They only have access control information and FS and GS

410
00:15:17,58 --> 00:15:20,32
do still fill in all the information from the table.

411
00:15:20,44 --> 00:15:23,46
That's again a reason why we say that segmentation is

412
00:15:23,46 --> 00:15:24,26
not optional

413
00:15:24,27 --> 00:15:25,56
It is still used,

414
00:15:25,94 --> 00:15:28,9
but but it's used in a much more limited form

415
00:15:28,9 --> 00:15:31,25
than what it was originally designed for.

416
00:15:33,74 --> 00:15:34,09
All right,

417
00:15:34,09 --> 00:15:34,42
well,

418
00:15:34,42 --> 00:15:37,28
Sonic has an hypothesis and that is that if the

419
00:15:37,29 --> 00:15:37,7
RPL

420
00:15:37,7 --> 00:15:38,06


421
00:15:38,06 --> 00:15:40,85
Has something to do with privilege rings as it sort

422
00:15:40,85 --> 00:15:41,92
of suggests,

423
00:15:42,12 --> 00:15:45,88
then it should be different when we're reading segment selectors

424
00:15:45,88 --> 00:15:49,55
out of segment registers in kernel versus user space

425
00:15:50,14 --> 00:15:52,16
So how would we test this hypothesis?

426
00:15:52,16 --> 00:15:52,38
Well,

427
00:15:52,38 --> 00:15:54,98
we need to go read segment selectors and kernel versus

428
00:15:54,98 --> 00:15:55,66
user space

