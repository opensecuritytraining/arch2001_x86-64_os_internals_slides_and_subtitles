1
00:00:00,080 --> 00:00:03,840
então vamos fazer um pequeno laboratório simples aqui I

2
00:00:02,879 --> 00:00:06,399
quero que você

3
00:00:03,840 --> 00:00:08,240
vá em frente e configure um break-on-execute e

4
00:00:06,399 --> 00:00:09,760
um hardware de quebra na leitura e escrita

5
00:00:08,240 --> 00:00:11,519
ponto de parada com o WinDbg

6
00:00:09,760 --> 00:00:13,759
pode simplesmente escolher o seu próprio

7
00:00:11,519 --> 00:00:14,400
locais e eu vou escolher os meus e mostrar

8
00:00:13,759 --> 00:00:17,199
o vídeo

9
00:00:14,400 --> 00:00:18,640
vídeo após o facto, vá em frente e liste

10
00:00:17,199 --> 00:00:19,439
os seus pontos de paragem depois de os ter definido

11
00:00:18,640 --> 00:00:21,920
com ba

12
00:00:19,439 --> 00:00:23,199
só para confirmar que estão definidos agora enquanto

13
00:00:21,920 --> 00:00:24,240
você está sentado lá no depurador você

14
00:00:23,199 --> 00:00:26,480
não verá

15
00:00:24,240 --> 00:00:27,439
os registos de depuração são realmente actualizados

16
00:00:26,480 --> 00:00:29,279
imediatamente

17
00:00:27,439 --> 00:00:30,960
por isso precisa de correr o depurador e depois

18
00:00:29,279 --> 00:00:33,120
pará-lo imediatamente

19
00:00:30,960 --> 00:00:34,880
e então nesse ponto você verá o

20
00:00:33,120 --> 00:00:36,719
o registo de depuração é atualizado

21
00:00:34,880 --> 00:00:38,160
então eu quero que você volte e olhe para

22
00:00:36,719 --> 00:00:41,600
registo de depuração 7

23
00:00:38,160 --> 00:00:44,000
e 8 desculpe 6 e 7 e

24
00:00:41,600 --> 00:00:45,440
também os seus registos de depuração sabem 0,

25
00:00:44,000 --> 00:00:46,879
1, 2 e 3

26
00:00:45,440 --> 00:00:48,640
não se sabe exatamente quais serão

27
00:00:46,879 --> 00:00:49,440
serão preenchidas, depende de você

28
00:00:48,640 --> 00:00:51,920
saber se

29
00:00:49,440 --> 00:00:53,120
havia outros pontos de interrupção de depuração definidos

30
00:00:51,920 --> 00:00:54,800
mas realisticamente

31
00:00:53,120 --> 00:00:56,559
provavelmente não vai ter nenhum

32
00:00:54,800 --> 00:00:58,000
outro conjunto, então provavelmente será debug

33
00:00:56,559 --> 00:01:00,399
registo 0 e 1

34
00:00:58,000 --> 00:01:01,280
que obtém esses dois pontos de interrupção, então vá

35
00:01:00,399 --> 00:01:03,760
e faça isso

36
00:01:01,280 --> 00:01:07,760
e vai interpretar os bits de acordo com

37
00:01:03,760 --> 00:01:07,760
estes campos de registo
