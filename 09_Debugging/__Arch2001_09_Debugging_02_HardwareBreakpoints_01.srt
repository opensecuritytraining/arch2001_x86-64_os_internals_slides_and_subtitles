1
00:00:00,399 --> 00:00:04,960
in addition to a unlimited number of

2
00:00:02,879 --> 00:00:05,920
software breakpoints that can be set by

3
00:00:04,960 --> 00:00:08,320
a debugger

4
00:00:05,920 --> 00:00:09,760
inserting a 0xCC over the top of some

5
00:00:08,320 --> 00:00:11,599
assembly instruction

6
00:00:09,760 --> 00:00:13,280
there's also support for what's called

7
00:00:11,599 --> 00:00:14,719
hardware breakpoints

8
00:00:13,280 --> 00:00:16,800
these are things that are built into the

9
00:00:14,719 --> 00:00:19,039
hardware where when it sees a particular

10
00:00:16,800 --> 00:00:21,119
address it will cause an interrupt

11
00:00:19,039 --> 00:00:23,680
unfortunately there's only support for

12
00:00:21,119 --> 00:00:26,240
4 hardware breakpoints

13
00:00:23,680 --> 00:00:29,119
so the limitation of 4 comes from

14
00:00:26,240 --> 00:00:32,000
some hardware debug registers that exist

15
00:00:29,119 --> 00:00:33,040
where debug registers 0 through 3 have a

16
00:00:32,000 --> 00:00:35,600
linear address

17
00:00:33,040 --> 00:00:37,760
where some breakpoint is to occur there

18
00:00:35,600 --> 00:00:39,760
is a debug register 4 and 5 but they're

19
00:00:37,760 --> 00:00:42,559
reserved and unused for now so

20
00:00:39,760 --> 00:00:44,160
basically debug registers 0 through 3 are

21
00:00:42,559 --> 00:00:45,440
the only things we can put addresses for

22
00:00:44,160 --> 00:00:47,920
breakpoints

23
00:00:45,440 --> 00:00:49,920
debug register 6 is the status

24
00:00:47,920 --> 00:00:51,600
register where when a breakpoint happens

25
00:00:49,920 --> 00:00:53,520
a debugger must look at the status to

26
00:00:51,600 --> 00:00:54,320
see what kind of breakpoint happened and

27
00:00:53,520 --> 00:00:56,879
where

28
00:00:54,320 --> 00:00:57,680
and debug register 7 is the control

29
00:00:56,879 --> 00:00:59,920
register

30
00:00:57,680 --> 00:01:02,399
where debuggers will set things to

31
00:00:59,920 --> 00:01:04,799
enable or disable breakpoints

32
00:01:02,399 --> 00:01:05,840
now all of the debug register accesses

33
00:01:04,799 --> 00:01:09,040
require CPL

34
00:01:05,840 --> 00:01:10,960
0 and it is done through a special form

35
00:01:09,040 --> 00:01:13,200
of the mov assembly instruction just

36
00:01:10,960 --> 00:01:15,200
like with moving to control registers

37
00:01:13,200 --> 00:01:18,000
we also or segment registers for that

38
00:01:15,200 --> 00:01:20,080
matter we also have a version to move to

39
00:01:18,000 --> 00:01:21,759
debug registers

40
00:01:20,080 --> 00:01:23,520
here's an overview of what the debug

41
00:01:21,759 --> 00:01:25,119
registers look like except for the fact

42
00:01:23,520 --> 00:01:26,880
that there's a bug in the manual there

43
00:01:25,119 --> 00:01:27,840
should be a debug register 0 down

44
00:01:26,880 --> 00:01:30,320
here as well

45
00:01:27,840 --> 00:01:31,280
so 0 through 3 just take 64-bit

46
00:01:30,320 --> 00:01:34,560
linear addresses

47
00:01:31,280 --> 00:01:36,000
4 and 5 reserved 6 for status so

48
00:01:34,560 --> 00:01:38,840
some particular bits are going to tell

49
00:01:36,000 --> 00:01:40,159
us what breakpoints fired and 7 for

50
00:01:38,840 --> 00:01:43,759
control

51
00:01:40,159 --> 00:01:47,360
so let's look at the first control bits

52
00:01:43,759 --> 00:01:48,799
L0 through L3 are the local breakpoint

53
00:01:47,360 --> 00:01:51,360
enables

54
00:01:48,799 --> 00:01:53,520
these flags are nominally cleared on

55
00:01:51,360 --> 00:01:55,840
task switches but as we said

56
00:01:53,520 --> 00:01:57,200
no one uses tasks and task switches for

57
00:01:55,840 --> 00:01:59,200
this purpose anymore

58
00:01:57,200 --> 00:02:01,840
so for all intensive purposes these can

59
00:01:59,200 --> 00:02:04,640
just be used as is

60
00:02:01,840 --> 00:02:05,200
G0 through 3 where the global break

61
00:02:04,640 --> 00:02:07,360
points

62
00:02:05,200 --> 00:02:08,399
which were going to survive task

63
00:02:07,360 --> 00:02:10,160
switches

64
00:02:08,399 --> 00:02:11,920
but again because no one does task

65
00:02:10,160 --> 00:02:13,760
switches these are

66
00:02:11,920 --> 00:02:15,760
not the differentiation between local

67
00:02:13,760 --> 00:02:17,520
and global is meaningless at this point

68
00:02:15,760 --> 00:02:19,360
and people tend to just use the local

69
00:02:17,520 --> 00:02:22,640
enable

70
00:02:19,360 --> 00:02:24,720
now there's also a LE and GE

71
00:02:22,640 --> 00:02:28,400
but we don't care about these on 64-bit

72
00:02:24,720 --> 00:02:31,120
processors because they're not supported

73
00:02:28,400 --> 00:02:33,680
then there is the general detect flag

74
00:02:31,120 --> 00:02:36,879
now if this is set to 1

75
00:02:33,680 --> 00:02:39,519
this is set to 1 come on

76
00:02:36,879 --> 00:02:40,160
all right if this is set to 1 then

77
00:02:39,519 --> 00:02:42,640
actually

78
00:02:40,160 --> 00:02:44,879
using those move assembly instructions

79
00:02:42,640 --> 00:02:48,480
to mov into the debug registers

80
00:02:44,879 --> 00:02:50,800
to change DR0 DR1 etc

81
00:02:48,480 --> 00:02:52,560
if you move to those registers and this

82
00:02:50,800 --> 00:02:53,280
is set it will actually cause a

83
00:02:52,560 --> 00:02:55,760
debugging

84
00:02:53,280 --> 00:02:56,480
exception in and of itself a interrupt

85
00:02:55,760 --> 00:03:00,080
1

86
00:02:56,480 --> 00:03:02,239
and so this in some sense is used to

87
00:03:00,080 --> 00:03:03,519
sort of maintain control over the debug

88
00:03:02,239 --> 00:03:05,519
registers so

89
00:03:03,519 --> 00:03:06,879
if the initial debugger gets in there

90
00:03:05,519 --> 00:03:08,720
and it wants to set up

91
00:03:06,879 --> 00:03:10,000
certain controls certain hardware

92
00:03:08,720 --> 00:03:12,000
breakpoints

93
00:03:10,000 --> 00:03:14,159
then if some other random piece of

94
00:03:12,000 --> 00:03:14,560
software that's unexpectedly comes along

95
00:03:14,159 --> 00:03:17,840
and

96
00:03:14,560 --> 00:03:19,840
tries to scribble all over those this

97
00:03:17,840 --> 00:03:20,959
this flag being set will cause an

98
00:03:19,840 --> 00:03:23,680
interrupt which the

99
00:03:20,959 --> 00:03:24,319
software could then catch and you know

100
00:03:23,680 --> 00:03:27,120
either

101
00:03:24,319 --> 00:03:28,720
deny or allow the particular overwrites

102
00:03:27,120 --> 00:03:30,959
to the debug registers

103
00:03:28,720 --> 00:03:32,799
I actually just backed up for a second

104
00:03:30,959 --> 00:03:33,760
because I realized I didn't actually say

105
00:03:32,799 --> 00:03:37,040
explicitly

106
00:03:33,760 --> 00:03:37,840
that the L0 enablement of the local

107
00:03:37,040 --> 00:03:39,920
breakpoint

108
00:03:37,840 --> 00:03:41,599
is going to enable a breakpoint on

109
00:03:39,920 --> 00:03:42,799
whatever linear address is in debug

110
00:03:41,599 --> 00:03:45,599
register 0.

111
00:03:42,799 --> 00:03:48,159
L1 is debug register 1 L2 debug register

112
00:03:45,599 --> 00:03:51,040
2 L3 debug register 3.

113
00:03:48,159 --> 00:03:52,879
so those are each explicitly associated

114
00:03:51,040 --> 00:03:55,680
with the particular number

115
00:03:52,879 --> 00:03:57,840
that is and that's going to be annoying

116
00:03:55,680 --> 00:04:02,480
gotta wait through that again

117
00:03:57,840 --> 00:04:02,480
go go go go go

118
00:04:03,920 --> 00:04:09,280
okay and so then basically if this

119
00:04:07,040 --> 00:04:10,480
read/write 0 is set to a particular

120
00:04:09,280 --> 00:04:13,120
value down here

121
00:04:10,480 --> 00:04:14,640
and if the linear address is if the

122
00:04:13,120 --> 00:04:15,519
break point on that linear address in

123
00:04:14,640 --> 00:04:19,199
debug register

124
00:04:15,519 --> 00:04:22,400
0 is enabled then R/W

125
00:04:19,199 --> 00:04:23,440
of 00 would mean debug register 0 is

126
00:04:22,400 --> 00:04:26,639
treated as

127
00:04:23,440 --> 00:04:29,840
a linear address for a break on execute

128
00:04:26,639 --> 00:04:33,600
so 00 is break on instruction

129
00:04:29,840 --> 00:04:35,040
execution 01 is break on data

130
00:04:33,600 --> 00:04:38,080
writes

131
00:04:35,040 --> 00:04:38,880
skip over to 11 is break on data

132
00:04:38,080 --> 00:04:41,199
reads or

133
00:04:38,880 --> 00:04:43,040
writes so there's a break on write

134
00:04:41,199 --> 00:04:43,680
there's a break on read or write there's

135
00:04:43,040 --> 00:04:47,040
no

136
00:04:43,680 --> 00:04:50,560
form of read-only and then

137
00:04:47,040 --> 00:04:53,360
if CR4.DE

138
00:04:50,560 --> 00:04:53,919
for debugging extensions so here we've

139
00:04:53,360 --> 00:04:56,000
got

140
00:04:53,919 --> 00:04:57,120
control register 4 making an unexpected

141
00:04:56,000 --> 00:05:00,560
appearance here

142
00:04:57,120 --> 00:05:04,160
guest starring if DE is equal to 1

143
00:05:00,560 --> 00:05:05,440
then 10 means break on I/O or port I/O

144
00:05:04,160 --> 00:05:07,520
and that's actually going to be the

145
00:05:05,440 --> 00:05:10,560
topic for the next section

146
00:05:07,520 --> 00:05:12,720
if DE is equal to 0 then

147
00:05:10,560 --> 00:05:14,400
10 is undefined and that's not a valid

148
00:05:12,720 --> 00:05:16,240
type of breakpoint

149
00:05:14,400 --> 00:05:18,880
so it was kind of you know unexpected to

150
00:05:16,240 --> 00:05:19,840
me CR4.DE just popping up out of

151
00:05:18,880 --> 00:05:22,800
the blue

152
00:05:19,840 --> 00:05:24,560
saying you know I'm relevant but then I

153
00:05:22,800 --> 00:05:29,039
was reading this and I was like

154
00:05:24,560 --> 00:05:30,639
cradle crade cradle cradle to the grave

155
00:05:29,039 --> 00:05:32,080
and then I went searching for cradle to

156
00:05:30,639 --> 00:05:33,440
the grave to make sure that wasn't you

157
00:05:32,080 --> 00:05:35,120
know a reference to anything I shouldn't

158
00:05:33,440 --> 00:05:36,400
be referencing I came up with this

159
00:05:35,120 --> 00:05:38,400
awesome picture

160
00:05:36,400 --> 00:05:40,320
and I manipulated it using my mad

161
00:05:38,400 --> 00:05:40,639
photoshop skills which really are just

162
00:05:40,320 --> 00:05:43,759
like

163
00:05:40,639 --> 00:05:45,840
chopping chunks of you know h's and

164
00:05:43,759 --> 00:05:46,880
and copying and pasting them around and

165
00:05:45,840 --> 00:05:50,479
there you go

166
00:05:46,880 --> 00:05:53,520
that is by far the most badass version

167
00:05:50,479 --> 00:05:56,479
of control registers you've ever seen or

168
00:05:53,520 --> 00:05:56,479
you ever will see

169
00:05:56,560 --> 00:06:00,319
all right now back to the non-badass

170
00:05:59,199 --> 00:06:03,520
looking of

171
00:06:00,319 --> 00:06:07,039
particular bits in registers

172
00:06:03,520 --> 00:06:09,600
so if this is the interpretation of how

173
00:06:07,039 --> 00:06:10,160
the break point is supposed to occur

174
00:06:09,600 --> 00:06:13,600
based on

175
00:06:10,160 --> 00:06:15,759
execute, read, read/write or write

176
00:06:13,600 --> 00:06:17,440
sorry based on execute I'm going to go

177
00:06:15,759 --> 00:06:18,479
mentally from top to bottom based on

178
00:06:17,440 --> 00:06:21,600
execute

179
00:06:18,479 --> 00:06:24,800
based on write based on port I/O

180
00:06:21,600 --> 00:06:28,240
or based on read/write then

181
00:06:24,800 --> 00:06:31,440
LEN is going to specify what size

182
00:06:28,240 --> 00:06:32,720
of what size the breakpoint should be

183
00:06:31,440 --> 00:06:35,440
interpreted as

184
00:06:32,720 --> 00:06:37,280
now sizes mostly matter in the case of

185
00:06:35,440 --> 00:06:38,800
brakes on reads and brakes on writes

186
00:06:37,280 --> 00:06:40,400
also matters for port I/O

187
00:06:38,800 --> 00:06:42,080
doesn't matter so much for assembly

188
00:06:40,400 --> 00:06:44,319
instructions so

189
00:06:42,080 --> 00:06:46,479
the sizes can be interpreted as 1 byte 2

190
00:06:44,319 --> 00:06:48,240
byte 4 byte and 8 bytes

191
00:06:46,479 --> 00:06:50,240
and this used to not be valid on

192
00:06:48,240 --> 00:06:52,240
non-64-bit systems

193
00:06:50,240 --> 00:06:54,080
so things like break on execute it

194
00:06:52,240 --> 00:06:54,560
really doesn't matter you can use any of

195
00:06:54,080 --> 00:06:57,120
these

196
00:06:54,560 --> 00:06:59,039
it's really just the the linear address

197
00:06:57,120 --> 00:07:01,440
is going to be treated as

198
00:06:59,039 --> 00:07:03,199
you know the first assembly instruction

199
00:07:01,440 --> 00:07:04,880
so the first address and it doesn't

200
00:07:03,199 --> 00:07:06,560
really care what the size is but the

201
00:07:04,880 --> 00:07:08,000
size matters a lot when you start

202
00:07:06,560 --> 00:07:10,479
talking about reads on

203
00:07:08,000 --> 00:07:11,120
breaks on reads and writes or breaks on

204
00:07:10,479 --> 00:07:14,720
writes

205
00:07:11,120 --> 00:07:17,599
so if you set the size to 1 byte

206
00:07:14,720 --> 00:07:19,599
for a break on write and if an assembly

207
00:07:17,599 --> 00:07:21,520
instruction writes 2 bytes

208
00:07:19,599 --> 00:07:23,280
then the hardware will just not break on

209
00:07:21,520 --> 00:07:25,039
that because it assumes you only wanted

210
00:07:23,280 --> 00:07:28,720
to know if only 1 byte was

211
00:07:25,039 --> 00:07:30,960
was written to all right so that's

212
00:07:28,720 --> 00:07:33,039
the control register this is the status

213
00:07:30,960 --> 00:07:36,240
register you can see it's a lot simpler

214
00:07:33,039 --> 00:07:38,720
you've got your 0 through 3 and when

215
00:07:36,240 --> 00:07:39,599
the breakpoint actually happens these

216
00:07:38,720 --> 00:07:42,400
will tell you

217
00:07:39,599 --> 00:07:44,080
which of the debug registers 0 through 3

218
00:07:42,400 --> 00:07:45,840
actually had a break on it so

219
00:07:44,080 --> 00:07:46,800
technically you could put the same

220
00:07:45,840 --> 00:07:48,720
linear address in

221
00:07:46,800 --> 00:07:50,240
all 4 registers you could enable all

222
00:07:48,720 --> 00:07:51,919
4 registers you could

223
00:07:50,240 --> 00:07:54,319
set all the same lengths and all the

224
00:07:51,919 --> 00:07:56,000
same R/W bits

225
00:07:54,319 --> 00:07:57,840
and when that breakpoint happens you

226
00:07:56,000 --> 00:07:59,440
would see all of these set because they

227
00:07:57,840 --> 00:08:02,160
were all technically

228
00:07:59,440 --> 00:08:03,440
reasons that the breakpoint occurred but

229
00:08:02,160 --> 00:08:05,120
practically speaking these will

230
00:08:03,440 --> 00:08:06,879
typically your debug registers will be

231
00:08:05,120 --> 00:08:08,479
different or you know values that are

232
00:08:06,879 --> 00:08:10,319
just straight up not filled in

233
00:08:08,479 --> 00:08:12,160
and so these are just to tell you okay

234
00:08:10,319 --> 00:08:13,680
it was you know break point 0 that

235
00:08:12,160 --> 00:08:14,720
fired this time break point 3 that

236
00:08:13,680 --> 00:08:16,800
fired that time

237
00:08:14,720 --> 00:08:19,039
a little tricky bit to this for you know

238
00:08:16,800 --> 00:08:19,680
debugger makers is that these bits will

239
00:08:19,039 --> 00:08:22,080
be set

240
00:08:19,680 --> 00:08:23,919
even if this the control register says

241
00:08:22,080 --> 00:08:25,840
that a particular thing is actually not

242
00:08:23,919 --> 00:08:27,360
enabled at the moment so you know the

243
00:08:25,840 --> 00:08:29,039
hardware is just doing something

244
00:08:27,360 --> 00:08:30,800
super trivial and it's just saying oh

245
00:08:29,039 --> 00:08:31,599
yeah you know this particular linear

246
00:08:30,800 --> 00:08:34,719
address

247
00:08:31,599 --> 00:08:36,080
and you know it wouldn't actually fire

248
00:08:34,719 --> 00:08:38,159
the thing but if there was like

249
00:08:36,080 --> 00:08:39,200
you know two things that were at the

250
00:08:38,159 --> 00:08:42,240
same address a

251
00:08:39,200 --> 00:08:43,039
assembly execute and a write and if

252
00:08:42,240 --> 00:08:44,959
someone was

253
00:08:43,039 --> 00:08:46,160
self-modifying code or something like

254
00:08:44,959 --> 00:08:47,839
that

255
00:08:46,160 --> 00:08:49,680
then you know you could potentially get

256
00:08:47,839 --> 00:08:51,279
two different things

257
00:08:49,680 --> 00:08:52,880
but only one of them might have been the

258
00:08:51,279 --> 00:08:53,680
actual reason it might have only been a

259
00:08:52,880 --> 00:08:56,560
write to

260
00:08:53,680 --> 00:08:58,080
itself not an execute of itself and it

261
00:08:56,560 --> 00:09:00,880
would be the write breakpoint so

262
00:08:58,080 --> 00:09:01,680
anyways you're not making debuggers at

263
00:09:00,880 --> 00:09:03,680
the moment

264
00:09:01,680 --> 00:09:05,279
but just the kind of things that you end

265
00:09:03,680 --> 00:09:06,480
up seeing in the manual for little

266
00:09:05,279 --> 00:09:08,880
caveats

267
00:09:06,480 --> 00:09:11,360
there's then also the BD which is the

268
00:09:08,880 --> 00:09:12,640
corresponding status flag for the GD the

269
00:09:11,360 --> 00:09:14,560
general detect

270
00:09:12,640 --> 00:09:16,320
this tells you that yes it was actually

271
00:09:14,560 --> 00:09:18,000
the general detect

272
00:09:16,320 --> 00:09:20,800
breakpoint that just fired so someone

273
00:09:18,000 --> 00:09:22,640
was trying to move to a debug register

274
00:09:20,800 --> 00:09:25,200
so this could be used for you know the

275
00:09:22,640 --> 00:09:28,000
debuggers fighting amongst themselves to

276
00:09:25,200 --> 00:09:30,240
try to see who gets to control the debug

277
00:09:28,000 --> 00:09:32,800
registers

278
00:09:30,240 --> 00:09:34,640
then there's the BS flag which is set

279
00:09:32,800 --> 00:09:36,000
when you're single stepping now we're

280
00:09:34,640 --> 00:09:37,920
going to talk about single stepping

281
00:09:36,000 --> 00:09:39,839
in the next video so this is just to say

282
00:09:37,920 --> 00:09:41,519
that if you're single stepping this flag

283
00:09:39,839 --> 00:09:43,839
will be set

284
00:09:41,519 --> 00:09:45,760
all right now you can actually see a

285
00:09:43,839 --> 00:09:47,839
very strong correspondence

286
00:09:45,760 --> 00:09:50,399
in the WinDbg definition of hardware

287
00:09:47,839 --> 00:09:52,480
breakpoints the break on access

288
00:09:50,399 --> 00:09:54,959
and the things that we just saw about

289
00:09:52,480 --> 00:09:57,360
how the actual hardware registers work

290
00:09:54,959 --> 00:09:58,880
so the the formatting for break on

291
00:09:57,360 --> 00:10:02,399
access is break on access

292
00:09:58,880 --> 00:10:05,440
the access type the size and the address

293
00:10:02,399 --> 00:10:08,399
so the access types are r for read write

294
00:10:05,440 --> 00:10:09,279
w for write e for execute and i for port

295
00:10:08,399 --> 00:10:11,680
I/O

296
00:10:09,279 --> 00:10:12,720
the size is those things like the length

297
00:10:11,680 --> 00:10:15,760
field can it be

298
00:10:12,720 --> 00:10:16,959
1, 2, 4, 8 although WinDbg

299
00:10:15,760 --> 00:10:19,279
specifically says

300
00:10:16,959 --> 00:10:20,240
the size must be 1 for break on

301
00:10:19,279 --> 00:10:22,320
execute

302
00:10:20,240 --> 00:10:24,000
and then the address is the linear

303
00:10:22,320 --> 00:10:26,079
address where you're going to

304
00:10:24,000 --> 00:10:28,880
which the debugger is going to fill into

305
00:10:26,079 --> 00:10:28,880
the debug register

