1
00:00:00,320 --> 00:00:04,960
Muito bem, vamos rever o que temos até agora

2
00:00:03,120 --> 00:00:06,640
bem, nós temos pontos de parada de software que

3
00:00:04,960 --> 00:00:08,880
são nominalmente ilimitados

4
00:00:06,640 --> 00:00:10,719
mas requerem a substituição da montagem

5
00:00:08,880 --> 00:00:13,759
instruções com 0xCC

6
00:00:10,719 --> 00:00:14,639
INT3 bytes. Nós temos hardware

7
00:00:13,759 --> 00:00:17,279
pontos de interrupção

8
00:00:14,639 --> 00:00:17,760
mas isso é limitado a apenas 4 execuções

9
00:00:17,279 --> 00:00:21,199
ou

10
00:00:17,760 --> 00:00:23,439
pontos de interrupção de escrita ou de leitura/escrita ou de E/S.

11
00:00:21,199 --> 00:00:24,800
Nós também temos uma deteção geral

12
00:00:23,439 --> 00:00:26,960
ponto de parada que é o

13
00:00:24,800 --> 00:00:28,840
“a mãe Sally está a tentar escrever para a minha depuração

14
00:00:26,960 --> 00:00:32,320
registos outra vez”

15
00:00:28,840 --> 00:00:34,320
exceção mas o que é que ainda não vimos assim

16
00:00:32,320 --> 00:00:35,200
até agora não vimos uma boa maneira de separar

17
00:00:34,320 --> 00:00:37,200
passo

18
00:00:35,200 --> 00:00:38,879
então como é que um depurador faria um único passo com

19
00:00:37,200 --> 00:00:40,079
apenas essas coisas que você sabe certamente

20
00:00:38,879 --> 00:00:42,079
não vai ser

21
00:00:40,079 --> 00:00:43,360
configuração que você conhece um desses 4

22
00:00:42,079 --> 00:00:46,399
pontos de interrupção de hardware para

23
00:00:43,360 --> 00:00:48,320
cada um dos próximos rip e o software

24
00:00:46,399 --> 00:00:49,680
ponto de parada seria extremamente limitante

25
00:00:48,320 --> 00:00:52,320
também porque você teria que escrever

26
00:00:49,680 --> 00:00:52,960
passo escrever passo escrever passo então como fazer

27
00:00:52,320 --> 00:00:54,960
eles fazem isso

28
00:00:52,960 --> 00:00:56,160
bem, eles tipicamente usam o que é chamado de

29
00:00:54,960 --> 00:00:58,719
a bandeira de armadilha

30
00:00:56,160 --> 00:01:00,559
outra bandeira mágica especial no

31
00:00:58,719 --> 00:01:03,120
registo rflags

32
00:01:00,559 --> 00:01:04,159
então a presença da flag trap sendo

33
00:01:03,120 --> 00:01:07,280
definida como 1

34
00:01:04,159 --> 00:01:08,479
no registo rflags significa que cada

35
00:01:07,280 --> 00:01:10,240
único

36
00:01:08,479 --> 00:01:11,840
cada instrução que roda é

37
00:01:10,240 --> 00:01:12,880
vai causar uma exceção de depuração

38
00:01:11,840 --> 00:01:15,200
depois

39
00:01:12,880 --> 00:01:16,960
e referimo-nos a isto como modo de passo único

40
00:01:15,200 --> 00:01:18,960
agora isto é obviamente útil para coisas

41
00:01:16,960 --> 00:01:19,840
como entrar em um lugar onde você literalmente

42
00:01:18,960 --> 00:01:21,520
quer pisar

43
00:01:19,840 --> 00:01:23,680
uma instrução assembly de cada vez e

44
00:01:21,520 --> 00:01:25,920
quer seguir o fluxo de controlo como

45
00:01:23,680 --> 00:01:27,759
instruções de chamada e coisas do tipo

46
00:01:25,920 --> 00:01:28,960
mas também pode ser potencialmente útil

47
00:01:27,759 --> 00:01:31,920
para sair de onde

48
00:01:28,960 --> 00:01:33,600
essencialmente o depurador usaria apenas

49
00:01:31,920 --> 00:01:34,960
o sinalizador trap e daria um passo a passo

50
00:01:33,600 --> 00:01:36,880
e esperava até que

51
00:01:34,960 --> 00:01:37,759
eventualmente vê um conjunto de retorno

52
00:01:36,880 --> 00:01:39,439
instrução

53
00:01:37,759 --> 00:01:41,759
e uma vez que ele vê isso, ele sabe que

54
00:01:39,439 --> 00:01:42,720
pode parar de pisar depois de ter pisado

55
00:01:41,759 --> 00:01:44,720
através dela

56
00:01:42,720 --> 00:01:45,840
agora não posso falar com todos os depuradores mas

57
00:01:44,720 --> 00:01:48,560
algo como um passo

58
00:01:45,840 --> 00:01:50,720
over tem mais probabilidade de usar algo

59
00:01:48,560 --> 00:01:52,720
como um ponto de interrupção de software temporário, então

60
00:01:50,720 --> 00:01:55,360
você vai direto para o ponto de uma chamada

61
00:01:52,720 --> 00:01:57,040
e quer passar por cima de uma chamada que

62
00:01:55,360 --> 00:01:57,920
significa que você sabe que ela provavelmente não faz

63
00:01:57,040 --> 00:01:59,840
sentido para apenas

64
00:01:57,920 --> 00:02:02,000
sabe continuamente um único passo

65
00:01:59,840 --> 00:02:03,520
para sempre até voltar para

66
00:02:02,000 --> 00:02:05,119
a instrução de montagem após

67
00:02:03,520 --> 00:02:06,719
provavelmente faz mais sentido apenas

68
00:02:05,119 --> 00:02:08,319
escrever um ponto de interrupção de software

69
00:02:06,719 --> 00:02:10,160
para a instrução assembly após a instrução

70
00:02:08,319 --> 00:02:11,599
chamada apenas continue

71
00:02:10,160 --> 00:02:13,840
e deixá-lo voltar, mas sabe

72
00:02:11,599 --> 00:02:15,680
depuradores podem fazer isso como quiserem

73
00:02:13,840 --> 00:02:17,840
agora só para lembrar, quando nós estávamos

74
00:02:15,680 --> 00:02:19,120
falando sobre o registo de depuração 6 o

75
00:02:17,840 --> 00:02:22,720
registo de estado

76
00:02:19,120 --> 00:02:25,120
nós dissemos que o BS indica que ele tem

77
00:02:22,720 --> 00:02:25,920
teve uma exceção ocorrendo por causa de um único

78
00:02:25,120 --> 00:02:28,000
passo

79
00:02:25,920 --> 00:02:29,599
então basicamente essa parte aí é

80
00:02:28,000 --> 00:02:32,080
será o que lhe dirá

81
00:02:29,599 --> 00:02:33,760
ok, eu sou o depurador, estou a apanhar isto

82
00:02:32,080 --> 00:02:35,280
exceção que consigo ver BS

83
00:02:33,760 --> 00:02:37,200
está configurado e portanto esta coisa deve

84
00:02:35,280 --> 00:02:38,720
ter um único passo também vale a pena

85
00:02:37,200 --> 00:02:39,920
saber que o sinalizador de armadilha será

86
00:02:38,720 --> 00:02:43,120
automaticamente apagada

87
00:02:39,920 --> 00:02:45,440
quando a exceção de depuração é disparada

88
00:02:43,120 --> 00:02:46,560
então se o depurador quer de facto

89
00:02:45,440 --> 00:02:49,120
manter o passo único

90
00:02:46,560 --> 00:02:51,200
vai precisar de definir a bandeira trap

91
00:02:49,120 --> 00:02:52,239
nos rflags que são salvos no

92
00:02:51,200 --> 00:02:54,720
pilha e então

93
00:02:52,239 --> 00:02:56,480
iretq e então uma exceção acontece

94
00:02:54,720 --> 00:02:57,599
novamente e então ele coloca a flag trap e

95
00:02:56,480 --> 00:03:00,400
então iretq

96
00:02:57,599 --> 00:03:00,400
etc
