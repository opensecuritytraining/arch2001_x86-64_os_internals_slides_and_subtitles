1
00:00:00,640 --> 00:00:04,960
como um lembrete do tipo de taxonomia

2
00:00:03,360 --> 00:00:06,560
de condições que existem em algum lugar no

3
00:00:04,960 --> 00:00:07,440
o sistema que requerem o processador

4
00:00:06,560 --> 00:00:09,120
atenção

5
00:00:07,440 --> 00:00:11,040
tivemos interrupções do hardware e

6
00:00:09,120 --> 00:00:13,440
instruções e exceções

7
00:00:11,040 --> 00:00:15,280
que eram a falha que o rip

8
00:00:13,440 --> 00:00:17,680
aponta para a instrução de falha

9
00:00:15,280 --> 00:00:19,119
e o trap onde o rip aponta para a instrução

10
00:00:17,680 --> 00:00:20,560
instrução após o trapping

11
00:00:19,119 --> 00:00:22,160
instrução

12
00:00:20,560 --> 00:00:24,240
então vamos falar sobre o que acontece quando uma

13
00:00:22,160 --> 00:00:27,359
ponto de interrupção de hardware dispara

14
00:00:24,240 --> 00:00:29,599
ele realmente dispara a entrada idt um

15
00:00:27,359 --> 00:00:31,199
que o manual chama de exceção de depuração

16
00:00:29,599 --> 00:00:33,920
então você pode ver que

17
00:00:31,199 --> 00:00:36,239
um é de hardware e dois é de

18
00:00:33,920 --> 00:00:38,239
chamada de exceção, então essa noção de

19
00:00:36,239 --> 00:00:38,800
interrupções de hardware versus exceções

20
00:00:38,239 --> 00:00:42,160
não são

21
00:00:38,800 --> 00:00:44,320
mutuamente exclusivos, então o que acontece

22
00:00:42,160 --> 00:00:46,399
é que sempre que é uma execução

23
00:00:44,320 --> 00:00:48,800
ponto de parada ou uma deteção geral

24
00:00:46,399 --> 00:00:51,039
ponto de parada significando que havia aquele gde

25
00:00:48,800 --> 00:00:52,640
bit que é quando alguém faz um movimento

26
00:00:51,039 --> 00:00:53,840
tentando escrever na memória de depuração

27
00:00:52,640 --> 00:00:56,879
registos

28
00:00:53,840 --> 00:00:57,600
ele vai ser tratado como uma falha, então

29
00:00:56,879 --> 00:00:59,840
nesse caso

30
00:00:57,600 --> 00:01:01,359
Basicamente, a execução de um

31
00:00:59,840 --> 00:01:04,159
detecta que é uma falha

32
00:01:01,359 --> 00:01:05,760
parar a falha de hitler a apontar para o bobo

33
00:01:04,159 --> 00:01:08,799
a dizer que isto é tudo culpa tua

34
00:01:05,760 --> 00:01:12,080
rip aponta para a instrução de falha

35
00:01:08,799 --> 00:01:12,799
então também em

36
00:01:12,080 --> 00:01:14,880
pausa na escrita

37
00:01:12,799 --> 00:01:16,240
pausa na leitura escrita e pausa no io

38
00:01:14,880 --> 00:01:19,360
pontos de interrupção de hardware

39
00:01:16,240 --> 00:01:21,360
é uma armadilha, então basicamente há dois

40
00:01:19,360 --> 00:01:24,479
tipos diferentes de comportamento que o

41
00:01:21,360 --> 00:01:26,799
depurador tem que levar em conta, então

42
00:01:24,479 --> 00:01:28,000
praticamente falando, isso significa que se for

43
00:01:26,799 --> 00:01:30,799
uma pausa na escrita

44
00:01:28,000 --> 00:01:33,040
ou uma pausa na leitura e escrita do

45
00:01:30,799 --> 00:01:35,520
os dados serão realmente sobrescritos

46
00:01:33,040 --> 00:01:37,920
antes que a exceção seja gerada, então qualquer

47
00:01:35,520 --> 00:01:38,479
tipo de manipulador de depuração que queira

48
00:01:37,920 --> 00:01:40,400
de facto

49
00:01:38,479 --> 00:01:42,479
determinar o que o antes e o depois

50
00:01:40,400 --> 00:01:44,560
valores eram, na verdade, teria que

51
00:01:42,479 --> 00:01:46,000
ler o valor no momento da

52
00:01:44,560 --> 00:01:48,079
configuração inicial do ponto de interrupção

53
00:01:46,000 --> 00:01:50,560
por outro lado, uma instrução

54
00:01:48,079 --> 00:01:52,159
é de facto detectado antes de

55
00:01:50,560 --> 00:01:53,600
a instrução seja executada agora, isto é

56
00:01:52,159 --> 00:01:54,000
diferente do ponto de parada do software

57
00:01:53,600 --> 00:01:56,240
nós vimos

58
00:01:54,000 --> 00:01:58,799
antes de dizermos o ponto de interrupção do software

59
00:01:56,240 --> 00:02:01,439
de antes era na verdade uma armadilha

60
00:01:58,799 --> 00:02:02,799
e então, basicamente, antes ele iria

61
00:02:01,439 --> 00:02:05,920
depois destes

62
00:02:02,799 --> 00:02:08,239
hex cc a instrução assembly int 3

63
00:02:05,920 --> 00:02:09,039
mas ele sempre soube que poderia voltar atrás por um

64
00:02:08,239 --> 00:02:11,039
simples

65
00:02:09,039 --> 00:02:12,640
byte único para voltar ao que

66
00:02:11,039 --> 00:02:14,239
a instrução original seria

67
00:02:12,640 --> 00:02:16,160
para sobrescrever a instrução original

68
00:02:14,239 --> 00:02:17,520
instrução agora, neste caso, com o

69
00:02:16,160 --> 00:02:20,000
ponto de parada de hardware

70
00:02:17,520 --> 00:02:22,480
porque na verdade ele está quebrando antes de

71
00:02:20,000 --> 00:02:24,239
a instrução ocorre, você sabe

72
00:02:22,480 --> 00:02:25,599
um depurador que quereria, sabe

73
00:02:24,239 --> 00:02:27,280
passar por cima da instrução ou algo assim

74
00:02:25,599 --> 00:02:28,480
assim ele teria que descobrir

75
00:02:27,280 --> 00:02:30,480
sabe, nós teríamos que essencialmente

76
00:02:28,480 --> 00:02:33,360
desmontar a instrução de montagem

77
00:02:30,480 --> 00:02:34,720
por isso é um pouco indesejável

78
00:02:33,360 --> 00:02:36,560
em termos práticos

79
00:02:34,720 --> 00:02:39,360
o que vai acontecer é que o

80
00:02:36,560 --> 00:02:42,800
O depurador vai usar o sinalizador resume

81
00:02:39,360 --> 00:02:44,800
então, para retomar a execução sem

82
00:02:42,800 --> 00:02:46,400
reativar o ponto de interrupção de hardware

83
00:02:44,800 --> 00:02:47,920
apenas continuamente batendo no mesmo

84
00:02:46,400 --> 00:02:50,800
instrução de montagem

85
00:02:47,920 --> 00:02:52,720
ele usará a flag resume, então a flag resume

86
00:02:50,800 --> 00:02:53,360
era outro desses sistemas especiais

87
00:02:52,720 --> 00:02:57,200
bandeiras

88
00:02:53,360 --> 00:03:00,959
no registo r e r flags ou e flags

89
00:02:57,200 --> 00:03:03,360
bit 16. assim o comportamento do resume

90
00:03:00,959 --> 00:03:05,280
é que o processador ignora

91
00:03:03,360 --> 00:03:06,959
os pontos de parada de instrução durante o tempo

92
00:03:05,280 --> 00:03:08,879
da próxima instrução

93
00:03:06,959 --> 00:03:10,800
o processador então limpa automaticamente

94
00:03:08,879 --> 00:03:12,879
este sinalizador após a instrução

95
00:03:10,800 --> 00:03:14,959
retornou para has become has been

96
00:03:12,879 --> 00:03:16,800
executado com sucesso

97
00:03:14,959 --> 00:03:18,159
então se você quebrar o execute com um

98
00:03:16,800 --> 00:03:20,879
ponto de interrupção de hardware

99
00:03:18,159 --> 00:03:22,560
e então o rip está a apontar para o

100
00:03:20,879 --> 00:03:23,599
instrução de montagem que vai

101
00:03:22,560 --> 00:03:27,200
executar mas que

102
00:03:23,599 --> 00:03:29,760
ainda não executou o depurador

103
00:03:27,200 --> 00:03:31,360
deve definir o sinalizador de retomada e então ele pode

104
00:03:29,760 --> 00:03:33,120
retomar e então o sinalizador de retomada irá

105
00:03:31,360 --> 00:03:35,360
faz com que ele não quebre novamente

106
00:03:33,120 --> 00:03:37,920
exatamente na mesma instrução de montagem

107
00:03:35,360 --> 00:03:38,720
para de facto definir a bandeira resume

108
00:03:37,920 --> 00:03:41,360
o

109
00:03:38,720 --> 00:03:43,840
o depurador tem que manipular o r armazenado

110
00:03:41,360 --> 00:03:44,879
que está na pilha como resultado de

111
00:03:43,840 --> 00:03:48,000
a interrupção

112
00:03:44,879 --> 00:03:48,879
e depois a iret q a interrupção de retorno

113
00:03:48,000 --> 00:03:51,200
retorno

114
00:03:48,879 --> 00:03:51,920
vai de facto definir as bandeiras r

115
00:03:51,200 --> 00:03:53,920
porque

116
00:03:51,920 --> 00:03:55,680
nós conversamos no início se

117
00:03:53,920 --> 00:03:57,920
você se lembra do passado para a id da cpu

118
00:03:55,680 --> 00:03:58,879
nós dissemos que a id da cpu precisa ser capaz de definir

119
00:03:57,920 --> 00:04:01,200
e limpar o

120
00:03:58,879 --> 00:04:03,439
ou o software de processo precisa ser capaz de

121
00:04:01,200 --> 00:04:05,280
para definir e limpar o sinalizador id no

122
00:04:03,439 --> 00:04:07,040
os nossos sinalizadores registam-se para confirmar

123
00:04:05,280 --> 00:04:11,519
que o cpu id é de facto utilizável

124
00:04:07,040 --> 00:04:13,200
nós falamos sobre push f q e pop fq

125
00:04:11,519 --> 00:04:15,599
e essas eram as formas de palavras quadrangulares

126
00:04:13,200 --> 00:04:18,560
empurrando e saltando o registo das bandeiras r

127
00:04:15,599 --> 00:04:19,359
mas acontece que de facto o rf

128
00:04:18,560 --> 00:04:22,800
bandeira

129
00:04:19,359 --> 00:04:24,160
não é válido para ser definido pelo pop fq it

130
00:04:22,800 --> 00:04:26,960
só pode ser definido por este

131
00:04:24,160 --> 00:04:27,440
fila iret, então se você olhar no manual

132
00:04:26,960 --> 00:04:30,240
para

133
00:04:27,440 --> 00:04:32,160
e verá esta tabela que

134
00:04:30,240 --> 00:04:35,199
essencialmente diz

135
00:04:32,160 --> 00:04:36,800
a bandeira resume nunca é activada em qualquer

136
00:04:35,199 --> 00:04:38,639
tipo de condições aqui

137
00:04:36,800 --> 00:04:40,479
tendo a ver com o aparecimento destes outros

138
00:04:38,639 --> 00:04:43,759
sim, eles podem potencialmente ser configurados

139
00:04:40,479 --> 00:04:43,759
mas a bandeira de resumo não é
