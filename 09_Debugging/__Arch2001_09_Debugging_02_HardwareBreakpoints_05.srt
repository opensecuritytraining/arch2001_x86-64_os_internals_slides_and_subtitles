1
00:00:00,320 --> 00:00:04,960
okay so let's review what we have so far

2
00:00:03,120 --> 00:00:06,640
well we have software breakpoints which

3
00:00:04,960 --> 00:00:08,880
are nominally unlimited

4
00:00:06,640 --> 00:00:10,719
but they require overwriting assembly

5
00:00:08,880 --> 00:00:13,759
instructions with 0xCC

6
00:00:10,719 --> 00:00:14,639
INT3 bytes. We have hardware

7
00:00:13,759 --> 00:00:17,279
breakpoints

8
00:00:14,639 --> 00:00:17,760
but that's limited to only 4 execute

9
00:00:17,279 --> 00:00:21,199
or

10
00:00:17,760 --> 00:00:23,439
write or read/write or I/O breakpoints.

11
00:00:21,199 --> 00:00:24,800
We also have the one general detect

12
00:00:23,439 --> 00:00:26,960
breakpoint which is the

13
00:00:24,800 --> 00:00:28,840
"mom Sally's trying to write to my debug

14
00:00:26,960 --> 00:00:32,320
registers again"

15
00:00:28,840 --> 00:00:34,320
exception but what haven't we seen thus

16
00:00:32,320 --> 00:00:35,200
far we haven't seen a good way to single

17
00:00:34,320 --> 00:00:37,200
step

18
00:00:35,200 --> 00:00:38,879
so how would a debugger single step with

19
00:00:37,200 --> 00:00:40,079
just these things you know certainly

20
00:00:38,879 --> 00:00:42,079
it's not going to be

21
00:00:40,079 --> 00:00:43,360
setting you know one of these 4

22
00:00:42,079 --> 00:00:46,399
hardware break points to

23
00:00:43,360 --> 00:00:48,320
every single next rip and the software

24
00:00:46,399 --> 00:00:49,680
breakpoint would be extremely limiting

25
00:00:48,320 --> 00:00:52,320
as well because you'd have to write

26
00:00:49,680 --> 00:00:52,960
step write step write step so how do

27
00:00:52,320 --> 00:00:54,960
they do it

28
00:00:52,960 --> 00:00:56,160
well they typically use what's called

29
00:00:54,960 --> 00:00:58,719
the trap flag

30
00:00:56,160 --> 00:01:00,559
another special magic flag in the

31
00:00:58,719 --> 00:01:03,120
rflags register

32
00:01:00,559 --> 00:01:04,159
so the presence of the trap flag being

33
00:01:03,120 --> 00:01:07,280
set to 1

34
00:01:04,159 --> 00:01:08,479
in the rflags register means that every

35
00:01:07,280 --> 00:01:10,240
single

36
00:01:08,479 --> 00:01:11,840
every single instruction that runs is

37
00:01:10,240 --> 00:01:12,880
going to cause a debug exception

38
00:01:11,840 --> 00:01:15,200
afterwards

39
00:01:12,880 --> 00:01:16,960
and we refer to this as single step mode

40
00:01:15,200 --> 00:01:18,960
now this is of course useful for things

41
00:01:16,960 --> 00:01:19,840
like step into where you very literally

42
00:01:18,960 --> 00:01:21,520
want to step

43
00:01:19,840 --> 00:01:23,680
one assembly instruction at a time and

44
00:01:21,520 --> 00:01:25,920
you want to follow control flow like

45
00:01:23,680 --> 00:01:27,759
call instructions and things like that

46
00:01:25,920 --> 00:01:28,960
but it can also potentially be useful

47
00:01:27,759 --> 00:01:31,920
for step out of where

48
00:01:28,960 --> 00:01:33,600
essentially the debugger would just use

49
00:01:31,920 --> 00:01:34,960
the trap flag and it would step step

50
00:01:33,600 --> 00:01:36,880
step and it would wait till it

51
00:01:34,960 --> 00:01:37,759
eventually sees a return assembly

52
00:01:36,880 --> 00:01:39,439
instruction

53
00:01:37,759 --> 00:01:41,759
and once it sees that it knows that it

54
00:01:39,439 --> 00:01:42,720
can stop stepping after it has stepped

55
00:01:41,759 --> 00:01:44,720
through that

56
00:01:42,720 --> 00:01:45,840
now I can't speak to all debuggers but

57
00:01:44,720 --> 00:01:48,560
something like a step

58
00:01:45,840 --> 00:01:50,720
over is more likely to use something

59
00:01:48,560 --> 00:01:52,720
like a temporary software breakpoint so

60
00:01:50,720 --> 00:01:55,360
you step right up to the point of a call

61
00:01:52,720 --> 00:01:57,040
and you want to step over a call that

62
00:01:55,360 --> 00:01:57,920
means you know it probably doesn't make

63
00:01:57,040 --> 00:01:59,840
sense to just

64
00:01:57,920 --> 00:02:02,000
you know continuously single step

65
00:01:59,840 --> 00:02:03,520
forever until you make your way back to

66
00:02:02,000 --> 00:02:05,119
the assembly instruction after

67
00:02:03,520 --> 00:02:06,719
it probably makes more sense to just

68
00:02:05,119 --> 00:02:08,319
write a software breakpoint

69
00:02:06,719 --> 00:02:10,160
to the assembly instruction after the

70
00:02:08,319 --> 00:02:11,599
call just continue it

71
00:02:10,160 --> 00:02:13,840
and let it come back but you know

72
00:02:11,599 --> 00:02:15,680
debuggers could do it however they want

73
00:02:13,840 --> 00:02:17,840
now just as a reminder back when we were

74
00:02:15,680 --> 00:02:19,120
talking about the debug register 6 the

75
00:02:17,840 --> 00:02:22,720
status register

76
00:02:19,120 --> 00:02:25,120
we said that BS indicates that it has

77
00:02:22,720 --> 00:02:25,920
had a exception occur because of single

78
00:02:25,120 --> 00:02:28,000
stepping

79
00:02:25,920 --> 00:02:29,599
so basically that bit right there is

80
00:02:28,000 --> 00:02:32,080
going to be the one that tells you

81
00:02:29,599 --> 00:02:33,760
okay I'm the debugger I'm catching this

82
00:02:32,080 --> 00:02:35,280
exception I can see BS

83
00:02:33,760 --> 00:02:37,200
is set and therefore this thing must

84
00:02:35,280 --> 00:02:38,720
have single stepped it's also worth

85
00:02:37,200 --> 00:02:39,920
knowing that the trap flag will be

86
00:02:38,720 --> 00:02:43,120
automatically cleared

87
00:02:39,920 --> 00:02:45,440
when the debug exception is fired

88
00:02:43,120 --> 00:02:46,560
so if the debugger wants to actually

89
00:02:45,440 --> 00:02:49,120
keep single stepping

90
00:02:46,560 --> 00:02:51,200
it's going to need to set the trap flag

91
00:02:49,120 --> 00:02:52,239
in the rflags that are saved on the

92
00:02:51,200 --> 00:02:54,720
stack and then

93
00:02:52,239 --> 00:02:56,480
iretq and then an exception happens

94
00:02:54,720 --> 00:02:57,599
again and then it sets the trap flag and

95
00:02:56,480 --> 00:03:00,400
then iretq

96
00:02:57,599 --> 00:03:00,400
etc

