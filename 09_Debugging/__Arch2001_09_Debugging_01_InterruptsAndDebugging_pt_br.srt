1
00:00:00,240 --> 00:00:03,679
agora eu gostaria de falar sobre o

2
00:00:01,599 --> 00:00:04,880
relação entre interrupções que nós

3
00:00:03,679 --> 00:00:07,200
aprendemos há um tempo atrás

4
00:00:04,880 --> 00:00:08,240
e depuração, vamos começar com o

5
00:00:07,200 --> 00:00:10,639
forma mais simples

6
00:00:08,240 --> 00:00:11,840
que é a interrupção 3 o ponto de parada

7
00:00:10,639 --> 00:00:14,719
exceção

8
00:00:11,840 --> 00:00:16,720
isto também tem a sua própria interrupção separada

9
00:00:14,719 --> 00:00:19,920
instrução assembly INT3

10
00:00:16,720 --> 00:00:23,279
que tem uma forma de opcode de 1 byte

11
00:00:19,920 --> 00:00:25,119
0xCC enquanto a interrupção normal é 0xCD

12
00:00:23,279 --> 00:00:26,320
seguido de algum número para o

13
00:00:25,119 --> 00:00:29,039
número de interrupção

14
00:00:26,320 --> 00:00:30,880
então INT3 é na verdade o que os depuradores são

15
00:00:29,039 --> 00:00:32,880
usando quando dizem que estão configurando uma

16
00:00:30,880 --> 00:00:35,280
ponto de parada de software que é geralmente

17
00:00:32,880 --> 00:00:37,920
a forma padrão de pontos de parada

18
00:00:35,280 --> 00:00:39,040
então aqui vamos nós, o ponto de interrupção de exceção

19
00:00:37,920 --> 00:00:41,840
ou interrupção

20
00:00:39,040 --> 00:00:43,440
é do tipo trap e pode ver-se aqui

21
00:00:41,840 --> 00:00:46,480
ele tem a INT3

22
00:00:43,440 --> 00:00:48,399
não INT espaço 3. Então a primeira coisa

23
00:00:46,480 --> 00:00:50,320
que acontece quando algum depurador

24
00:00:48,399 --> 00:00:52,000
define um ponto de interrupção de software como visual

25
00:00:50,320 --> 00:00:53,039
studio se você está apenas clicando para

26
00:00:52,000 --> 00:00:54,800
o lado

27
00:00:53,039 --> 00:00:56,399
é que ele vai sobrescrever o

28
00:00:54,800 --> 00:00:58,399
primeiro byte do

29
00:00:56,399 --> 00:00:59,520
instrução assembly que corresponde a

30
00:00:58,399 --> 00:01:00,719
a linha onde está a tentar definir o

31
00:00:59,520 --> 00:01:02,000
ponto de parada ou o endereço onde você está

32
00:01:00,719 --> 00:01:03,600
tentando definir o ponto de parada

33
00:01:02,000 --> 00:01:05,040
ele vai sobrescrever o primeiro byte

34
00:01:03,600 --> 00:01:06,960
com 0xCC

35
00:01:05,040 --> 00:01:09,360
para que quando o processador

36
00:01:06,960 --> 00:01:10,320
normalmente executaria qualquer assembly

37
00:01:09,360 --> 00:01:13,200
instrução está lá

38
00:01:10,320 --> 00:01:14,880
em vez disso, ele executará a instrução INT3

39
00:01:13,200 --> 00:01:16,720
instrução de montagem

40
00:01:14,880 --> 00:01:18,560
ele vai ter que atualizar algum tipo de

41
00:01:16,720 --> 00:01:20,640
de lista de metadados que o depurador

42
00:01:18,560 --> 00:01:23,280
mantém o controlo para descobrir

43
00:01:20,640 --> 00:01:24,640
quais bytes foram realmente sobrescritos em

44
00:01:23,280 --> 00:01:27,040
para saber mais tarde

45
00:01:24,640 --> 00:01:29,040
qual byte ele precisa colocar de volta na ordem

46
00:01:27,040 --> 00:01:30,560
para executar a montagem original

47
00:01:29,040 --> 00:01:34,000
instrução

48
00:01:30,560 --> 00:01:35,840
agora uma vez que a INT3 é de facto atingida

49
00:01:34,000 --> 00:01:37,680
o depurador vai procurar o

50
00:01:35,840 --> 00:01:37,920
localização e vai ver que há

51
00:01:37,680 --> 00:01:40,240
a

52
00:01:37,920 --> 00:01:42,720
INT3 neste endereço específico e depois

53
00:01:40,240 --> 00:01:44,560
ele tem que basicamente substituir esse byte

54
00:01:42,720 --> 00:01:46,320
executar a instrução assembly e

55
00:01:44,560 --> 00:01:49,439
então assumindo que o

56
00:01:46,320 --> 00:01:51,280
a pessoa não desligou de facto o

57
00:01:49,439 --> 00:01:52,720
ponto de parada assumindo que não gostou do

58
00:01:51,280 --> 00:01:54,399
desfazer o ponto de parada

59
00:01:52,720 --> 00:01:55,920
mas eles vão escrever no

60
00:01:54,399 --> 00:01:57,600
ponto de parada lá atrás

61
00:01:55,920 --> 00:01:59,520
e então eles vão continuar

62
00:01:57,600 --> 00:02:01,520
e deixar o programa rodar normalmente

63
00:01:59,520 --> 00:02:02,719
então vamos fazer um laboratório rápido que é

64
00:02:01,520 --> 00:02:05,200
vai de facto

65
00:02:02,719 --> 00:02:07,280
tentar nos mostrar e expor a duplicidade

66
00:02:05,200 --> 00:02:09,840
de depuradores como o visual studio

67
00:02:07,280 --> 00:02:10,720
no sentido de que esta aplicação em particular

68
00:02:09,840 --> 00:02:13,680
ProofPudding

69
00:02:10,720 --> 00:02:14,959
lê os seus próprios bytes de montagem e imprime

70
00:02:13,680 --> 00:02:17,440
eles na tela

71
00:02:14,959 --> 00:02:19,200
então nós veremos especificamente o depurador

72
00:02:17,440 --> 00:02:19,440
tentando esconder de nós o facto de que ele

73
00:02:19,200 --> 00:02:23,120
é

74
00:02:19,440 --> 00:02:25,680
inseriu um 0xCC, então vamos em frente e pegar

75
00:02:23,120 --> 00:02:25,680
uma olhada nisso

76
00:02:25,840 --> 00:02:30,239
ProofPudding é apenas uma coisa simples

77
00:02:27,840 --> 00:02:33,040
que chama uma função assembly

78
00:02:30,239 --> 00:02:35,040
e o que essa função de montagem faz é

79
00:02:33,040 --> 00:02:37,840
ela pega o endereço de alguma etiqueta

80
00:02:35,040 --> 00:02:40,000
bla então ele pega o endereço de bla e

81
00:02:37,840 --> 00:02:41,440
ele o move para rax, então basicamente é

82
00:02:40,000 --> 00:02:44,160
apenas retornando

83
00:02:41,440 --> 00:02:46,000
o endereço de bla que, ao fazer isso, é

84
00:02:44,160 --> 00:02:46,400
essencialmente retornando o endereço de

85
00:02:46,000 --> 00:02:49,680
este

86
00:02:46,400 --> 00:02:52,959
instrução nop então no C

87
00:02:49,680 --> 00:02:56,080
ele vai dizer que a instrução

88
00:02:52,959 --> 00:02:57,519
dados apontados por este endereço de blah

89
00:02:56,080 --> 00:02:58,959
que está a vir de dentro deste

90
00:02:57,519 --> 00:03:00,480
função de montagem

91
00:02:58,959 --> 00:03:02,000
é o seguinte dado e ele de facto

92
00:03:00,480 --> 00:03:04,319
desreferencia-o

93
00:03:02,000 --> 00:03:05,440
como um ponteiro de byte, então ele é essencialmente

94
00:03:04,319 --> 00:03:07,360
indo para

95
00:03:05,440 --> 00:03:09,200
tratar o endereço do nop como um byte

96
00:03:07,360 --> 00:03:11,040
ele vai desreferenciá-lo e

97
00:03:09,200 --> 00:03:11,840
então ele vai nos dar 1 byte em

98
00:03:11,040 --> 00:03:14,400
esse endereço

99
00:03:11,840 --> 00:03:16,080
que deveria ser 0x90 porque é

100
00:03:14,400 --> 00:03:17,440
o valor de um conjunto nop

101
00:03:16,080 --> 00:03:19,360
instrução

102
00:03:17,440 --> 00:03:21,040
então vamos em frente e definir um ponto de interrupção

103
00:03:19,360 --> 00:03:22,480
antes de realmente entrarmos no código

104
00:03:21,040 --> 00:03:23,760
e vamos colocar um ponto de parada no final

105
00:03:22,480 --> 00:03:25,280
após as impressões

106
00:03:23,760 --> 00:03:28,239
certifique-se de que o define como um arranque

107
00:03:25,280 --> 00:03:30,560
projeto certifique-se de que ele está configurado para depuração

108
00:03:28,239 --> 00:03:31,440
e então nós vamos, nós vamos entrar no

109
00:03:30,560 --> 00:03:32,799
ele

110
00:03:31,440 --> 00:03:34,480
pode ver que ele vai receber o

111
00:03:32,799 --> 00:03:36,239
endereço do bla

112
00:03:34,480 --> 00:03:37,760
e então o endereço de bla deve apenas

113
00:03:36,239 --> 00:03:41,120
ser este nop

114
00:03:37,760 --> 00:03:42,080
e então se nós apenas continuarmos para vermos

115
00:03:41,120 --> 00:03:44,799
impresso

116
00:03:42,080 --> 00:03:45,200
que neste endereço o endereço do bla

117
00:03:44,799 --> 00:03:48,560


118
00:03:45,200 --> 00:03:50,959
1A1A o byte é 0x90. então isso é

119
00:03:48,560 --> 00:03:54,879
exatamente o que nós esperaríamos ver

120
00:03:50,959 --> 00:03:57,120
porque é um 0x90 naquele endereço

121
00:03:54,879 --> 00:03:58,799
mas agora vamos definir um ponto de parada certo

122
00:03:57,120 --> 00:04:00,159
aqui, vamos definir um ponto de parada aqui e

123
00:03:58,799 --> 00:04:01,120
o que eu estou dizendo sobre software

124
00:04:00,159 --> 00:04:02,640
pontos de interrupção

125
00:04:01,120 --> 00:04:04,720
é que o depurador vai

126
00:04:02,640 --> 00:04:07,519
de facto sobrescreve aquele 0x90

127
00:04:04,720 --> 00:04:08,400
com um 0xCC e é isso que vai

128
00:04:07,519 --> 00:04:10,159
causar o

129
00:04:08,400 --> 00:04:12,000
exceção de ponto de parada de fogo e o

130
00:04:10,159 --> 00:04:14,080
depurador a ser notificado

131
00:04:12,000 --> 00:04:15,120
e por fim ele vai parar em

132
00:04:14,080 --> 00:04:17,919
o depurador

133
00:04:15,120 --> 00:04:19,199
então vamos em frente e rodar o depurador

134
00:04:17,919 --> 00:04:22,240
continue

135
00:04:19,199 --> 00:04:22,720
ok, agora atingiu este ponto de paragem e

136
00:04:22,240 --> 00:04:24,639
então

137
00:04:22,720 --> 00:04:27,360
queremos ver se sabe ou não

138
00:04:24,639 --> 00:04:28,960
isto é 0x90 ou se isto é 0xCC

139
00:04:27,360 --> 00:04:30,639
então nós estamos quebrados bem aqui agora, então

140
00:04:28,960 --> 00:04:34,800
poderíamos simplesmente colocar

141
00:04:30,639 --> 00:04:37,840
rip e esse é o endereço 1A1A novamente

142
00:04:34,800 --> 00:04:41,600
e você pode ver que ele diz que 1A1A é

143
00:04:37,840 --> 00:04:43,360
90. e também rax é o valor

144
00:04:41,600 --> 00:04:45,120
que está a ser devolvido para o endereço de

145
00:04:43,360 --> 00:04:48,240
bla então poderíamos colocar

146
00:04:45,120 --> 00:04:53,360
rax e novamente é 1A1A e diz

147
00:04:48,240 --> 00:04:53,360
é 0x90. mas quando continuamos

148
00:04:53,440 --> 00:04:57,199
o que nós vemos é que ele imprime em

149
00:04:56,080 --> 00:05:01,360
1a1a

150
00:04:57,199 --> 00:05:03,759
é 0xCC, então basicamente o depurador mentiu

151
00:05:01,360 --> 00:05:05,680
para nós, o depurador disse que havia um

152
00:05:03,759 --> 00:05:07,919
0x90 naquele endereço de memória

153
00:05:05,680 --> 00:05:11,120
mas na realidade havia um 0xCC de

154
00:05:07,919 --> 00:05:11,120
o próprio depurador
