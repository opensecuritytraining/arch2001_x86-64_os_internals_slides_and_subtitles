1
00:00:00,320 --> 00:00:03,760
okay so the goal is to set some hardware

2
00:00:02,960 --> 00:00:06,879
breakpoints

3
00:00:03,760 --> 00:00:09,040
and interpret the registers

4
00:00:06,879 --> 00:00:10,240
so you could pick wherever you wanted

5
00:00:09,040 --> 00:00:14,559
I'm going to

6
00:00:10,240 --> 00:00:14,559
pick somewhere in the IDT

7
00:00:14,880 --> 00:00:19,600
so you could choose various locations

8
00:00:17,680 --> 00:00:22,000
and some of them would lead to heartache

9
00:00:19,600 --> 00:00:23,519
for instance you know I originally chose

10
00:00:22,000 --> 00:00:26,480
the entry point of the

11
00:00:23,519 --> 00:00:27,279
sysenter instruction and that didn't work

12
00:00:26,480 --> 00:00:29,119
out so well

13
00:00:27,279 --> 00:00:30,960
but instead this time I'm going to just

14
00:00:29,119 --> 00:00:34,160
pick a location in the IDT

15
00:00:30,960 --> 00:00:36,399
so break on access execute 1

16
00:00:34,160 --> 00:00:39,120
and that particular thing performance

17
00:00:36,399 --> 00:00:40,719
interrupt sounds relatively safe

18
00:00:39,120 --> 00:00:43,600
and I don't know I'm just going to go

19
00:00:40,719 --> 00:00:47,120
with the rsp as my location for the

20
00:00:43,600 --> 00:00:51,120
write breakpoint so break on

21
00:00:47,120 --> 00:00:54,160
write and let's say let's make it 8

22
00:00:51,120 --> 00:00:55,680
and this particular address now I said

23
00:00:54,160 --> 00:00:57,360
that you're not going to see it

24
00:00:55,680 --> 00:00:59,280
immediately get updated so you can see

25
00:00:57,360 --> 00:01:01,199
my debug registers are still 0

26
00:00:59,280 --> 00:01:04,000
despite having set those

27
00:01:01,199 --> 00:01:06,000
you can do bl to confirm that you know

28
00:01:04,000 --> 00:01:07,920
those are going to be set

29
00:01:06,000 --> 00:01:09,680
and so I said you have to actually go

30
00:01:07,920 --> 00:01:12,640
and then immediately break

31
00:01:09,680 --> 00:01:13,439
if you want to actually you know go

32
00:01:12,640 --> 00:01:15,439
interpret it

33
00:01:13,439 --> 00:01:17,280
I can see that actually this thing fired

34
00:01:15,439 --> 00:01:20,320
before I could hit the

35
00:01:17,280 --> 00:01:21,680
pause gui here in WinDbg so it did

36
00:01:20,320 --> 00:01:24,159
actually break on a

37
00:01:21,680 --> 00:01:27,200
write so let's go ahead and interpret

38
00:01:24,159 --> 00:01:29,600
the registers and see what we see

39
00:01:27,200 --> 00:01:30,640
so we need to back up and you know look

40
00:01:29,600 --> 00:01:32,640
at the

41
00:01:30,640 --> 00:01:34,479
control register 7 starting from the

42
00:01:32,640 --> 00:01:37,280
beginning do we see

43
00:01:34,479 --> 00:01:38,880
global enable do we see local enable

44
00:01:37,280 --> 00:01:42,240
let's find out

45
00:01:38,880 --> 00:01:45,439
so control register seven we could do dx

46
00:01:42,240 --> 00:01:47,200
at debug register 7

47
00:01:45,439 --> 00:01:48,960
said control register 7 but it's

48
00:01:47,200 --> 00:01:52,000
debug register 7

49
00:01:48,960 --> 00:01:55,119
so here you can see that bit 0 is set

50
00:01:52,000 --> 00:01:56,240
and bit 2 is set so that would

51
00:01:55,119 --> 00:01:59,840
correspond to

52
00:01:56,240 --> 00:02:01,759
local enable 0 and local enable 1

53
00:01:59,840 --> 00:02:03,600
and that makes sense because we have 2

54
00:02:01,759 --> 00:02:06,159
breakpoints that are set into debug

55
00:02:03,600 --> 00:02:09,360
register 0 and debug register 1

56
00:02:06,159 --> 00:02:10,800
first one being this address of this

57
00:02:09,360 --> 00:02:12,720
particular

58
00:02:10,800 --> 00:02:14,480
performance interrupt so that's our

59
00:02:12,720 --> 00:02:16,879
execute breakpoint and that's our write

60
00:02:14,480 --> 00:02:19,599
breakpoint

61
00:02:16,879 --> 00:02:21,599
we can see that the globals are not set

62
00:02:19,599 --> 00:02:23,760
in this output so the global

63
00:02:21,599 --> 00:02:26,239
enables are not set looks like we got a

64
00:02:23,760 --> 00:02:29,360
bit here so let's check what that bit is

65
00:02:26,239 --> 00:02:32,160
0 1 2 3 4 5 6

66
00:02:29,360 --> 00:02:34,160
7 8 9 10 I think that's 10

67
00:02:32,160 --> 00:02:36,319
it's coming out kind of correctly

68
00:02:34,160 --> 00:02:37,920
looks like it is because that's always

69
00:02:36,319 --> 00:02:41,280
set in debug register

70
00:02:37,920 --> 00:02:42,640
7 all right so bit 10 is set and

71
00:02:41,280 --> 00:02:44,400
then there's some other bits up here

72
00:02:42,640 --> 00:02:46,400
that are set so let's go see which bits

73
00:02:44,400 --> 00:02:50,480
those are so this was 10

74
00:02:46,400 --> 00:02:55,760
11 12 13 14 15 16 17 18

75
00:02:50,480 --> 00:02:58,879
19 20. so 19 and 20

76
00:02:55,760 --> 00:03:02,480
all right 19 20 is a boundary so

77
00:02:58,879 --> 00:03:02,480
you want to say 20 21.

78
00:03:02,879 --> 00:03:10,400
so this would be 20 and 21

79
00:03:06,080 --> 00:03:10,400
whereas 18 and 19 are 0

80
00:03:12,720 --> 00:03:18,000
so what we see then is 20 and 21

81
00:03:15,920 --> 00:03:20,239
are 01 we need to go interpret

82
00:03:18,000 --> 00:03:22,480
these right so 20 21

83
00:03:20,239 --> 00:03:24,799
are 0 1 and that's what we would

84
00:03:22,480 --> 00:03:28,480
expect we have a break on data write

85
00:03:24,799 --> 00:03:31,599
0 1 in 21 and 20

86
00:03:28,480 --> 00:03:35,120
then we have 0 0 in

87
00:03:31,599 --> 00:03:37,760
read/write 0 and length 0.

88
00:03:35,120 --> 00:03:39,200
so right these earlier bits are all 0

89
00:03:37,760 --> 00:03:40,799
here

90
00:03:39,200 --> 00:03:42,799
so the interpretation of that is that

91
00:03:40,799 --> 00:03:44,720
it's a break on instruction execution

92
00:03:42,799 --> 00:03:46,239
for breakpoint 0 which is exactly what

93
00:03:44,720 --> 00:03:49,599
we expect

94
00:03:46,239 --> 00:03:51,519
and then the length here is 1 byte for

95
00:03:49,599 --> 00:03:55,360
the instruction execution

96
00:03:51,519 --> 00:03:56,239
and for this one bits 22 and 23 we would

97
00:03:55,360 --> 00:03:59,519
expect would be

98
00:03:56,239 --> 00:04:00,319
1 0 because we set a 8 bit 8

99
00:03:59,519 --> 00:04:03,920
byte

100
00:04:00,319 --> 00:04:08,159
break on write so if this was

101
00:04:03,920 --> 00:04:08,159
sorry if this was 21

102
00:04:08,560 --> 00:04:14,959
let's see I was 20 21

103
00:04:11,840 --> 00:04:18,079
or 0 1 and so 22

104
00:04:14,959 --> 00:04:20,799
23 are 1 0 and

105
00:04:18,079 --> 00:04:22,479
1 0 is the 8 byte version so that's

106
00:04:20,799 --> 00:04:23,840
exactly what we would expect in the

107
00:04:22,479 --> 00:04:26,800
debug control register

108
00:04:23,840 --> 00:04:28,000
enablement of debug register 0 debug

109
00:04:26,800 --> 00:04:30,160
register 1

110
00:04:28,000 --> 00:04:31,600
some particular hard-coded bit there and

111
00:04:30,160 --> 00:04:34,560
then this said it was

112
00:04:31,600 --> 00:04:35,360
a break on execute it set a 1 byte

113
00:04:34,560 --> 00:04:37,759
this said

114
00:04:35,360 --> 00:04:38,880
a break on write and this said 8

115
00:04:37,759 --> 00:04:40,800
bytes

116
00:04:38,880 --> 00:04:42,400
okay so now I said that actually this

117
00:04:40,800 --> 00:04:44,560
breakpoint 1 hit

118
00:04:42,400 --> 00:04:47,600
this break on write to my stack address

119
00:04:44,560 --> 00:04:50,320
hit before I could actually

120
00:04:47,600 --> 00:04:51,680
hit the break so let's see what we have

121
00:04:50,320 --> 00:04:55,120
in debug register

122
00:04:51,680 --> 00:04:59,120
6 which is our status register so

123
00:04:55,120 --> 00:05:02,320
dx at dr6

124
00:04:59,120 --> 00:05:05,280
in binary so

125
00:05:02,320 --> 00:05:07,680
the zeroth bit is unset and the oneth bit

126
00:05:05,280 --> 00:05:11,120
is set so what does that mean

127
00:05:07,680 --> 00:05:13,280
so we need to move down to the debug

128
00:05:11,120 --> 00:05:13,680
status register the zeroth bit is not

129
00:05:13,280 --> 00:05:16,320
set

130
00:05:13,680 --> 00:05:17,360
and the oneth bit is set so this means

131
00:05:16,320 --> 00:05:20,160
that it is

132
00:05:17,360 --> 00:05:20,639
breakpoint 1 or debug register 1

133
00:05:20,160 --> 00:05:23,120
which

134
00:05:20,639 --> 00:05:24,960
is the thing that actually fired so you

135
00:05:23,120 --> 00:05:26,560
would have to a debugger would go back

136
00:05:24,960 --> 00:05:28,639
and would look at the control registers

137
00:05:26,560 --> 00:05:32,880
and it would say like okay for

138
00:05:28,639 --> 00:05:36,080
register 7 was you know entry

139
00:05:32,880 --> 00:05:38,800
1 was 0 sorry 0 was

140
00:05:36,080 --> 00:05:40,000
enabled and 1 was enabled it could be

141
00:05:38,800 --> 00:05:41,919
the case that you know the

142
00:05:40,000 --> 00:05:43,759
the breakpoint bit was set but this

143
00:05:41,919 --> 00:05:44,800
could actually be set to not enabled but

144
00:05:43,759 --> 00:05:46,880
it does see

145
00:05:44,800 --> 00:05:48,560
this corresponds to this which was

146
00:05:46,880 --> 00:05:49,440
enabled because this was the globals and

147
00:05:48,560 --> 00:05:52,160
the locals

148
00:05:49,440 --> 00:05:52,800
so what other bits are set here well we

149
00:05:52,160 --> 00:05:55,759
can see

150
00:05:52,800 --> 00:05:57,840
0 1 2 3 and then a whole

151
00:05:55,759 --> 00:06:01,039
bunch of ones how many ones

152
00:05:57,840 --> 00:06:03,600
1 2 3 4 5 6 7

153
00:06:01,039 --> 00:06:04,400
8 if it's a ones let's check here

154
00:06:03,600 --> 00:06:07,280
1 2

155
00:06:04,400 --> 00:06:09,199
3 4 5 6 7 8 if it's

156
00:06:07,280 --> 00:06:11,120
ones exactly as we'd expect

157
00:06:09,199 --> 00:06:14,160
and then we would have a hard-coded 0

158
00:06:11,120 --> 00:06:17,280
and then we might have something like

159
00:06:14,160 --> 00:06:18,960
the BD if the general detect was set and

160
00:06:17,280 --> 00:06:19,280
we didn't actually see general detect

161
00:06:18,960 --> 00:06:21,120
set

162
00:06:19,280 --> 00:06:22,479
up in the control register so we don't

163
00:06:21,120 --> 00:06:25,440
expect to see it here

164
00:06:22,479 --> 00:06:26,479
but this was 8 bits of ones and then

165
00:06:25,440 --> 00:06:29,039
a 0 and

166
00:06:26,479 --> 00:06:29,680
that is 0 so that means the break on

167
00:06:29,039 --> 00:06:32,720
general

168
00:06:29,680 --> 00:06:34,880
detect is not what just fired

169
00:06:32,720 --> 00:06:36,160
next one is BS that was break on single

170
00:06:34,880 --> 00:06:39,360
step that one

171
00:06:36,160 --> 00:06:42,639
is also 0 and then BT and

172
00:06:39,360 --> 00:06:43,919
RTM we didn't cover so we don't care

173
00:06:42,639 --> 00:06:46,240
all right so that's the way that you

174
00:06:43,919 --> 00:06:48,800
would actually interpret you know the

175
00:06:46,240 --> 00:06:50,560
values in the debug registers interpret

176
00:06:48,800 --> 00:06:52,319
dr6 as being the status

177
00:06:50,560 --> 00:06:53,680
when a breakpoint actually hits you may

178
00:06:52,319 --> 00:06:54,240
not have actually had your breakpoint

179
00:06:53,680 --> 00:06:55,680
fire

180
00:06:54,240 --> 00:06:57,520
it all depends on where you set the

181
00:06:55,680 --> 00:07:00,000
breakpoint but

182
00:06:57,520 --> 00:07:00,960
dr7 is the main thing controlling you

183
00:07:00,000 --> 00:07:03,199
know how it's set

184
00:07:00,960 --> 00:07:04,319
and you know we ultimately see that the

185
00:07:03,199 --> 00:07:08,240
things correspond to

186
00:07:04,319 --> 00:07:08,240
exactly what we said in WinDbg

