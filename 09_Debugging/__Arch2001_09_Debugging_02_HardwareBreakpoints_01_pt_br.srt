1
00:00:00,399 --> 00:00:04,960
para além de um número ilimitado de

2
00:00:02,879 --> 00:00:05,920
pontos de interrupção de software que podem ser definidos por

3
00:00:04,960 --> 00:00:08,320
um depurador

4
00:00:05,920 --> 00:00:09,760
inserindo um 0xCC no topo de alguns

5
00:00:08,320 --> 00:00:11,599
instrução de montagem

6
00:00:09,760 --> 00:00:13,280
há também suporte para o que é chamado de

7
00:00:11,599 --> 00:00:14,719
pontos de interrupção de hardware

8
00:00:13,280 --> 00:00:16,800
estas são coisas que estão incorporadas no

9
00:00:14,719 --> 00:00:19,039
hardware onde quando ele vê um determinado

10
00:00:16,800 --> 00:00:21,119
endereço, ele causará uma interrupção

11
00:00:19,039 --> 00:00:23,680
infelizmente só há suporte para

12
00:00:21,119 --> 00:00:26,240
4 pontos de interrupção de hardware

13
00:00:23,680 --> 00:00:29,119
então a limitação de 4 vem de

14
00:00:26,240 --> 00:00:32,000
alguns registos de depuração de hardware que existem

15
00:00:29,119 --> 00:00:33,040
onde os registos de depuração 0 a 3 têm um

16
00:00:32,000 --> 00:00:35,600
endereço linear

17
00:00:33,040 --> 00:00:37,760
onde algum ponto de parada deve ocorrer ali

18
00:00:35,600 --> 00:00:39,760
há um registo de depuração 4 e 5 mas eles são

19
00:00:37,760 --> 00:00:42,559
reservados e não usados por enquanto, então

20
00:00:39,760 --> 00:00:44,160
basicamente os registos de depuração 0 a 3 são

21
00:00:42,559 --> 00:00:45,440
as únicas coisas que podemos colocar endereços para

22
00:00:44,160 --> 00:00:47,920
pontos de parada

23
00:00:45,440 --> 00:00:49,920
o registo de depuração 6 é o estado

24
00:00:47,920 --> 00:00:51,600
onde quando acontece um ponto de paragem

25
00:00:49,920 --> 00:00:53,520
um depurador deve olhar para o status para

26
00:00:51,600 --> 00:00:54,320
ver que tipo de ponto de parada aconteceu e

27
00:00:53,520 --> 00:00:56,879
onde

28
00:00:54,320 --> 00:00:57,680
e o registo de depuração 7 é o controlo

29
00:00:56,879 --> 00:00:59,920
registo

30
00:00:57,680 --> 00:01:02,399
onde os depuradores definirão as coisas para

31
00:00:59,920 --> 00:01:04,799
ativar ou desativar pontos de interrupção

32
00:01:02,399 --> 00:01:05,840
agora todos os acessos ao registo de depuração

33
00:01:04,799 --> 00:01:09,040
requerem CPL

34
00:01:05,840 --> 00:01:10,960
0 e é feito através de um formulário especial

35
00:01:09,040 --> 00:01:13,200
da instrução mov assembly apenas

36
00:01:10,960 --> 00:01:15,200
como no caso de mover para registos de controlo

37
00:01:13,200 --> 00:01:18,000
nós também ou registos de segmento para isso

38
00:01:15,200 --> 00:01:20,080
também temos uma versão para passar para

39
00:01:18,000 --> 00:01:21,759
registos de depuração

40
00:01:20,080 --> 00:01:23,520
aqui está uma visão geral do que os registos de depuração

41
00:01:21,759 --> 00:01:25,119
se parecem, exceto pelo facto de que

42
00:01:23,520 --> 00:01:26,880
que há um erro no manual

43
00:01:25,119 --> 00:01:27,840
devia haver um registo de depuração 0 em baixo

44
00:01:26,880 --> 00:01:30,320
aqui também

45
00:01:27,840 --> 00:01:31,280
então de 0 a 3 apenas pegue 64-bit

46
00:01:30,320 --> 00:01:34,560
endereços lineares

47
00:01:31,280 --> 00:01:36,000
4 e 5 reservaram 6 para o estado, portanto

48
00:01:34,560 --> 00:01:38,840
alguns bits em particular vão dizer

49
00:01:36,000 --> 00:01:40,159
quais breakpoints foram disparados e 7 para

50
00:01:38,840 --> 00:01:43,759
controlo

51
00:01:40,159 --> 00:01:47,360
vamos então olhar para os primeiros bits de controlo

52
00:01:43,759 --> 00:01:48,799
L0 a L3 são os pontos de paragem locais

53
00:01:47,360 --> 00:01:51,360
permite

54
00:01:48,799 --> 00:01:53,520
estes sinalizadores são nominalmente apagados em

55
00:01:51,360 --> 00:01:55,840
trocas de tarefas, mas como dissemos

56
00:01:53,520 --> 00:01:57,200
ninguém usa tarefas e comutadores de tarefas para

57
00:01:55,840 --> 00:01:59,200
este propósito

58
00:01:57,200 --> 00:02:01,840
por isso, para todos os efeitos intensivos, estes podem

59
00:01:59,200 --> 00:02:04,640
ser usados como estão

60
00:02:01,840 --> 00:02:05,200
G0 a 3 onde a quebra global

61
00:02:04,640 --> 00:02:07,360
pontos

62
00:02:05,200 --> 00:02:08,399
que iriam sobreviver à tarefa

63
00:02:07,360 --> 00:02:10,160
interruptores

64
00:02:08,399 --> 00:02:11,920
mas novamente porque ninguém faz tarefas

65
00:02:10,160 --> 00:02:13,760
interruptores estes são

66
00:02:11,920 --> 00:02:15,760
não é a diferenciação entre local

67
00:02:13,760 --> 00:02:17,520
e global não tem sentido nesta altura

68
00:02:15,760 --> 00:02:19,360
e as pessoas tendem a usar apenas o local

69
00:02:17,520 --> 00:02:22,640
habilitar

70
00:02:19,360 --> 00:02:24,720
agora há também um LE e GE

71
00:02:22,640 --> 00:02:28,400
mas não nos preocupamos com eles em 64-bit

72
00:02:24,720 --> 00:02:31,120
porque eles não são suportados

73
00:02:28,400 --> 00:02:33,680
então há o sinalizador de deteção geral

74
00:02:31,120 --> 00:02:36,879
agora se esta estiver definida para 1

75
00:02:33,680 --> 00:02:39,519
isto está definido para 1 vamos lá

76
00:02:36,879 --> 00:02:40,160
tudo bem se isto está definido para 1 então

77
00:02:39,519 --> 00:02:42,640
na verdade

78
00:02:40,160 --> 00:02:44,879
usando essas instruções de montagem de movimento

79
00:02:42,640 --> 00:02:48,480
para mover para os registos de depuração

80
00:02:44,879 --> 00:02:50,800
para mudar DR0 DR1 etc

81
00:02:48,480 --> 00:02:52,560
se passar para esses registos e este

82
00:02:50,800 --> 00:02:53,280
estiver configurado, ele vai de facto causar um

83
00:02:52,560 --> 00:02:55,760
depuração

84
00:02:53,280 --> 00:02:56,480
exceção por si só é uma interrupção

85
00:02:55,760 --> 00:03:00,080
1

86
00:02:56,480 --> 00:03:02,239
e isso, de certa forma, é usado para

87
00:03:00,080 --> 00:03:03,519
tipo de manter o controlo sobre a depuração

88
00:03:02,239 --> 00:03:05,519
registos por isso

89
00:03:03,519 --> 00:03:06,879
se o depurador inicial entrar lá

90
00:03:05,519 --> 00:03:08,720
e quiser configurar o

91
00:03:06,879 --> 00:03:10,000
determinados controlos determinado hardware

92
00:03:08,720 --> 00:03:12,000
pontos de interrupção

93
00:03:10,000 --> 00:03:14,159
então se algum outro pedaço aleatório de

94
00:03:12,000 --> 00:03:14,560
software que aparece inesperadamente

95
00:03:14,159 --> 00:03:17,840
e

96
00:03:14,560 --> 00:03:19,840
tenta rabiscar por cima destes

97
00:03:17,840 --> 00:03:20,959
se esta bandeira for activada irá causar um

98
00:03:19,840 --> 00:03:23,680
interrupção que o

99
00:03:20,959 --> 00:03:24,319
software poderia então pegar e você sabe

100
00:03:23,680 --> 00:03:27,120
também

101
00:03:24,319 --> 00:03:28,720
negar ou permitir as sobrescritas particulares

102
00:03:27,120 --> 00:03:30,959
para os registos de depuração

103
00:03:28,720 --> 00:03:32,799
Na verdade, acabei de voltar atrás por um segundo

104
00:03:30,959 --> 00:03:33,760
porque eu percebi que eu não disse

105
00:03:32,799 --> 00:03:37,040
explicitamente

106
00:03:33,760 --> 00:03:37,840
que a habilitação L0 do sistema local

107
00:03:37,040 --> 00:03:39,920
ponto de parada

108
00:03:37,840 --> 00:03:41,599
vai ativar um ponto de paragem em

109
00:03:39,920 --> 00:03:42,799
qualquer que seja o endereço linear em debug

110
00:03:41,599 --> 00:03:45,599
registo 0.

111
00:03:42,799 --> 00:03:48,159
L1 é o registo de depuração 1 Registo de depuração L2

112
00:03:45,599 --> 00:03:51,040
2 L3 é o registo de depuração 3.

113
00:03:48,159 --> 00:03:52,879
então cada um deles está explicitamente associado

114
00:03:51,040 --> 00:03:55,680
com o número específico

115
00:03:52,879 --> 00:03:57,840
que é e que vai ser irritante

116
00:03:55,680 --> 00:04:02,480
tenho de esperar por isso outra vez

117
00:03:57,840 --> 00:04:02,480
vai, vai, vai, vai

118
00:04:03,920 --> 00:04:09,280
ok e então basicamente se isso

119
00:04:07,040 --> 00:04:10,480
leitura/escrita 0 é definido para um determinado

120
00:04:09,280 --> 00:04:13,120
valor aqui embaixo

121
00:04:10,480 --> 00:04:14,640
e se o endereço linear é se o

122
00:04:13,120 --> 00:04:15,519
ponto de parada nesse endereço linear em

123
00:04:14,640 --> 00:04:19,199
registo de depuração

124
00:04:15,519 --> 00:04:22,400
0 é ativado e depois R/W

125
00:04:19,199 --> 00:04:23,440
de 00 significaria que o registo de depuração 0 está

126
00:04:22,400 --> 00:04:26,639
tratado como

127
00:04:23,440 --> 00:04:29,840
um endereço linear para uma quebra na execução

128
00:04:26,639 --> 00:04:33,600
então 00 é uma instrução break on

129
00:04:29,840 --> 00:04:35,040
execução 01 é quebra nos dados

130
00:04:33,600 --> 00:04:38,080
escreve

131
00:04:35,040 --> 00:04:38,880
saltar para 11 é uma quebra nos dados

132
00:04:38,080 --> 00:04:41,199
lê ou

133
00:04:38,880 --> 00:04:43,040
escreve, então há uma pausa na escrita

134
00:04:41,199 --> 00:04:43,680
há uma pausa na leitura ou na escrita há

135
00:04:43,040 --> 00:04:47,040
não

136
00:04:43,680 --> 00:04:50,560
forma de somente leitura e então

137
00:04:47,040 --> 00:04:53,360
se CR4.DE

138
00:04:50,560 --> 00:04:53,919
para depurar extensões, então aqui nós temos

139
00:04:53,360 --> 00:04:56,000
temos

140
00:04:53,919 --> 00:04:57,120
registo de controlo 4 a fazer um inesperado

141
00:04:56,000 --> 00:05:00,560
aparição inesperada aqui

142
00:04:57,120 --> 00:05:04,160
estrela convidada se DE é igual a 1

143
00:05:00,560 --> 00:05:05,440
então 10 significa interrupção em E/S ou porta E/S

144
00:05:04,160 --> 00:05:07,520
e isso vai ser de facto o

145
00:05:05,440 --> 00:05:10,560
tópico para a próxima secção

146
00:05:07,520 --> 00:05:12,720
se DE é igual a 0 então

147
00:05:10,560 --> 00:05:14,400
10 é indefinido e isso não é um valor válido

148
00:05:12,720 --> 00:05:16,240
tipo de ponto de parada

149
00:05:14,400 --> 00:05:18,880
então foi meio que inesperado para

150
00:05:16,240 --> 00:05:19,840
me deparar com o CR4.DE aparecendo do nada

151
00:05:18,880 --> 00:05:22,800
o azul

152
00:05:19,840 --> 00:05:24,560
dizendo que sabes que sou relevante mas depois eu

153
00:05:22,800 --> 00:05:29,039
estava lendo isso e eu estava tipo

154
00:05:24,560 --> 00:05:30,639
o berço do berço do berço ao túmulo

155
00:05:29,039 --> 00:05:32,080
e então eu fui procurar por cradle to

156
00:05:30,639 --> 00:05:33,440
o túmulo para ter certeza de que não era você

157
00:05:32,080 --> 00:05:35,120
sabe uma referência a algo que eu não deveria

158
00:05:33,440 --> 00:05:36,400
estar referenciando, eu pensei nisso

159
00:05:35,120 --> 00:05:38,400
imagem fantástica

160
00:05:36,400 --> 00:05:40,320
e eu manipulei-o usando o meu louco

161
00:05:38,400 --> 00:05:40,639
habilidades de photoshop que na verdade são apenas

162
00:05:40,320 --> 00:05:43,759
como

163
00:05:40,639 --> 00:05:45,840
cortando pedaços de h's e

164
00:05:43,759 --> 00:05:46,880
e copiando e colando-os e

165
00:05:45,840 --> 00:05:50,479
lá vai

166
00:05:46,880 --> 00:05:53,520
esta é de longe a versão mais marota

167
00:05:50,479 --> 00:05:56,479
de registos de controlo que alguma vez viu ou

168
00:05:53,520 --> 00:05:56,479
que você verá

169
00:05:56,560 --> 00:06:00,319
tudo bem agora de volta ao não-badass

170
00:05:59,199 --> 00:06:03,520
olhando de

171
00:06:00,319 --> 00:06:07,039
bits particulares em registos

172
00:06:03,520 --> 00:06:09,600
então se esta é a interpretação de como

173
00:06:07,039 --> 00:06:10,160
o ponto de parada deve ocorrer

174
00:06:09,600 --> 00:06:13,600
baseado em

175
00:06:10,160 --> 00:06:15,759
executar, ler, ler/escrever ou escrever

176
00:06:13,600 --> 00:06:17,440
desculpe, baseado em executar eu vou

177
00:06:15,759 --> 00:06:18,479
mentalmente de cima para baixo baseado em

178
00:06:17,440 --> 00:06:21,600
executar

179
00:06:18,479 --> 00:06:24,800
baseado na escrita baseado na porta I/O

180
00:06:21,600 --> 00:06:28,240
ou baseado em leitura/escrita então

181
00:06:24,800 --> 00:06:31,440
LEN vai especificar o tamanho

182
00:06:28,240 --> 00:06:32,720
de qual tamanho o ponto de parada deve ser

183
00:06:31,440 --> 00:06:35,440
interpretado como

184
00:06:32,720 --> 00:06:37,280
agora os tamanhos importam principalmente no caso de

185
00:06:35,440 --> 00:06:38,800
travões nas leituras e travões nas escritas

186
00:06:37,280 --> 00:06:40,400
também é importante para a porta I/O

187
00:06:38,800 --> 00:06:42,080
não importa tanto para assembly

188
00:06:40,400 --> 00:06:44,319
instruções então

189
00:06:42,080 --> 00:06:46,479
os tamanhos podem ser interpretados como 1 byte 2

190
00:06:44,319 --> 00:06:48,240
byte 4 byte e 8 bytes

191
00:06:46,479 --> 00:06:50,240
e isso não costumava ser válido em

192
00:06:48,240 --> 00:06:52,240
sistemas não-64-bit

193
00:06:50,240 --> 00:06:54,080
então coisas como break on executam-no

194
00:06:52,240 --> 00:06:54,560
realmente não importa, você pode usar qualquer um dos

195
00:06:54,080 --> 00:06:57,120
estes

196
00:06:54,560 --> 00:06:59,039
é realmente apenas o endereço linear

197
00:06:57,120 --> 00:07:01,440
vai ser tratado como

198
00:06:59,039 --> 00:07:03,199
você sabe que a primeira instrução assembly

199
00:07:01,440 --> 00:07:04,880
então o primeiro endereço e ele não

200
00:07:03,199 --> 00:07:06,560
realmente se importa com o tamanho, mas o

201
00:07:04,880 --> 00:07:08,000
tamanho importa muito quando se começa a

202
00:07:06,560 --> 00:07:10,479
falando sobre leituras em

203
00:07:08,000 --> 00:07:11,120
quebras em leituras e escritas ou quebras em

204
00:07:10,479 --> 00:07:14,720
escritas

205
00:07:11,120 --> 00:07:17,599
então se definirmos o tamanho para 1 byte

206
00:07:14,720 --> 00:07:19,599
para uma pausa na escrita e se uma montagem

207
00:07:17,599 --> 00:07:21,520
instrução escreve 2 bytes

208
00:07:19,599 --> 00:07:23,280
então o hardware simplesmente não vai quebrar em

209
00:07:21,520 --> 00:07:25,039
isso porque ele assume que você só queria

210
00:07:23,280 --> 00:07:28,720
para saber se apenas 1 byte foi

211
00:07:25,039 --> 00:07:30,960
foi escrito, certo, então isso é

212
00:07:28,720 --> 00:07:33,039
o registo de controlo este é o estado

213
00:07:30,960 --> 00:07:36,240
pode ver que é muito mais simples

214
00:07:33,039 --> 00:07:38,720
você tem seu 0 a 3 e quando

215
00:07:36,240 --> 00:07:39,599
o ponto de parada realmente acontece estes

216
00:07:38,720 --> 00:07:42,400
dirão a você

217
00:07:39,599 --> 00:07:44,080
qual dos registos de depuração 0 a 3

218
00:07:42,400 --> 00:07:45,840
na verdade tinha uma pausa nele, então

219
00:07:44,080 --> 00:07:46,800
tecnicamente você poderia colocar o mesmo

220
00:07:45,840 --> 00:07:48,720
endereço linear em

221
00:07:46,800 --> 00:07:50,240
todos os 4 registos, pode ativar todos

222
00:07:48,720 --> 00:07:51,919
4 registos que podia

223
00:07:50,240 --> 00:07:54,319
definir todos os mesmos comprimentos e todos os

224
00:07:51,919 --> 00:07:56,000
mesmos bits R/W

225
00:07:54,319 --> 00:07:57,840
e quando esse ponto de parada acontece você

226
00:07:56,000 --> 00:07:59,440
veria todos esses conjuntos porque eles

227
00:07:57,840 --> 00:08:02,160
eram todos tecnicamente

228
00:07:59,440 --> 00:08:03,440
razões pelas quais o ponto de interrupção ocorreu, mas

229
00:08:02,160 --> 00:08:05,120
praticamente falando, estes serão

230
00:08:03,440 --> 00:08:06,879
tipicamente os seus registos de depuração serão

231
00:08:05,120 --> 00:08:08,479
diferentes ou você conhece valores que são

232
00:08:06,879 --> 00:08:10,319
simplesmente não preenchidos

233
00:08:08,479 --> 00:08:12,160
e então estes são apenas para vos dizer, ok?

234
00:08:10,319 --> 00:08:13,680
foi o ponto de rutura 0 que

235
00:08:12,160 --> 00:08:14,720
disparou desta vez o ponto de parada 3 que

236
00:08:13,680 --> 00:08:16,800
disparou naquela hora

237
00:08:14,720 --> 00:08:19,039
um pouco complicado para isto, sabe

238
00:08:16,800 --> 00:08:19,680
depuradores é que esses bits vão

239
00:08:19,039 --> 00:08:22,080
ser configurados

240
00:08:19,680 --> 00:08:23,919
mesmo que isto o registo de controlo diga

241
00:08:22,080 --> 00:08:25,840
que uma coisa em particular não é de facto

242
00:08:23,919 --> 00:08:27,360
habilitado no momento para que você saiba que o

243
00:08:25,840 --> 00:08:29,039
o hardware está apenas fazendo algo

244
00:08:27,360 --> 00:08:30,800
super trivial e está apenas dizendo oh

245
00:08:29,039 --> 00:08:31,599
sim, tu sabes que este linear em particular

246
00:08:30,800 --> 00:08:34,719
endereço

247
00:08:31,599 --> 00:08:36,080
e sabe que ele não dispararia de facto

248
00:08:34,719 --> 00:08:38,159
a coisa, mas se houvesse com

249
00:08:36,080 --> 00:08:39,200
sabe duas coisas que estavam no

250
00:08:38,159 --> 00:08:42,240
mesmo endereço a

251
00:08:39,200 --> 00:08:43,039
assembly executa e uma escrita e se

252
00:08:42,240 --> 00:08:44,959
alguém estava

253
00:08:43,039 --> 00:08:46,160
código auto-modificador ou algo do tipo

254
00:08:44,959 --> 00:08:47,839
que

255
00:08:46,160 --> 00:08:49,680
então você sabe que pode potencialmente conseguir

256
00:08:47,839 --> 00:08:51,279
duas coisas diferentes

257
00:08:49,680 --> 00:08:52,880
mas apenas uma delas pode ter sido a

258
00:08:51,279 --> 00:08:53,680
razão real pode ter sido apenas um

259
00:08:52,880 --> 00:08:56,560
escrever para

260
00:08:53,680 --> 00:08:58,080
ele mesmo, não uma execução dele mesmo e ele

261
00:08:56,560 --> 00:09:00,880
seria o ponto de parada de escrita, então

262
00:08:58,080 --> 00:09:01,680
De qualquer forma, não está a fazer depuradores em

263
00:09:00,880 --> 00:09:03,680
o momento

264
00:09:01,680 --> 00:09:05,279
mas apenas o tipo de coisas que você acaba

265
00:09:03,680 --> 00:09:06,480
e que acabamos vendo no manual para pequenos

266
00:09:05,279 --> 00:09:08,880
advertências

267
00:09:06,480 --> 00:09:11,360
há também o BD que é o

268
00:09:08,880 --> 00:09:12,640
sinalizador de status correspondente para o GD o

269
00:09:11,360 --> 00:09:14,560
deteção geral

270
00:09:12,640 --> 00:09:16,320
isto diz-lhe que sim, era de facto

271
00:09:14,560 --> 00:09:18,000
a deteção geral

272
00:09:16,320 --> 00:09:20,800
ponto de interrupção que acabou de disparar para que alguém

273
00:09:18,000 --> 00:09:22,640
estava a tentar passar para um registo de depuração

274
00:09:20,800 --> 00:09:25,200
então isso poderia ser usado para

275
00:09:22,640 --> 00:09:28,000
depuradores lutando entre si para

276
00:09:25,200 --> 00:09:30,240
tentar ver quem consegue controlar o depurador

277
00:09:28,000 --> 00:09:32,800
registos

278
00:09:30,240 --> 00:09:34,640
depois há a flag BS que é colocada

279
00:09:32,800 --> 00:09:36,000
quando você está dando um único passo agora nós estamos

280
00:09:34,640 --> 00:09:37,920
vamos falar sobre passo único

281
00:09:36,000 --> 00:09:39,839
no próximo vídeo, então isso é só para dizer

282
00:09:37,920 --> 00:09:41,519
que se você é solteiro e está pisando nesta bandeira

283
00:09:39,839 --> 00:09:43,839
será acionado

284
00:09:41,519 --> 00:09:45,760
agora você pode realmente ver um

285
00:09:43,839 --> 00:09:47,839
correspondência muito forte

286
00:09:45,760 --> 00:09:50,399
na definição de hardware do WinDbg

287
00:09:47,839 --> 00:09:52,480
aponta a quebra no acesso

288
00:09:50,399 --> 00:09:54,959
e as coisas que acabamos de ver sobre

289
00:09:52,480 --> 00:09:57,360
como os registos de hardware funcionam

290
00:09:54,959 --> 00:09:58,880
então a formatação para break on

291
00:09:57,360 --> 00:10:02,399
acesso é break on access

292
00:09:58,880 --> 00:10:05,440
o tipo de acesso o tamanho e o endereço

293
00:10:02,399 --> 00:10:08,399
então os tipos de acesso são r para leitura e escrita

294
00:10:05,440 --> 00:10:09,279
w para escrever e para executar e i para porta

295
00:10:08,399 --> 00:10:11,680
E/S

296
00:10:09,279 --> 00:10:12,720
o tamanho são aquelas coisas como o comprimento

297
00:10:11,680 --> 00:10:15,760
o campo pode ser

298
00:10:12,720 --> 00:10:16,959
1, 2, 4, 8 embora WinDbg

299
00:10:15,760 --> 00:10:19,279
diz especificamente

300
00:10:16,959 --> 00:10:20,240
o tamanho tem de ser 1 para o intervalo em

301
00:10:19,279 --> 00:10:22,320
executar

302
00:10:20,240 --> 00:10:24,000
e então o endereço é o linear

303
00:10:22,320 --> 00:10:26,079
endereço onde você vai

304
00:10:24,000 --> 00:10:28,880
que o depurador vai preencher no

305
00:10:26,079 --> 00:10:28,880
o registo de depuração
