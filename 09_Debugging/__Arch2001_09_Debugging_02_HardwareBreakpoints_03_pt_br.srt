1
00:00:00,320 --> 00:00:03,760
Ok, então o objetivo é definir algum hardware

2
00:00:02,960 --> 00:00:06,879
pontos de parada

3
00:00:03,760 --> 00:00:09,040
e interpretar os registos

4
00:00:06,879 --> 00:00:10,240
para poder escolher onde quisesse

5
00:00:09,040 --> 00:00:14,559
Eu vou

6
00:00:10,240 --> 00:00:14,559
escolher algum lugar no IDT

7
00:00:14,880 --> 00:00:19,600
para poder escolher vários locais

8
00:00:17,680 --> 00:00:22,000
e alguns deles levariam à dor de cabeça

9
00:00:19,600 --> 00:00:23,519
por exemplo, sabem que eu escolhi originalmente

10
00:00:22,000 --> 00:00:26,480
o ponto de entrada do

11
00:00:23,519 --> 00:00:27,279
instrução sysenter e isso não funcionou

12
00:00:26,480 --> 00:00:29,119
muito bem

13
00:00:27,279 --> 00:00:30,960
mas desta vez eu vou apenas

14
00:00:29,119 --> 00:00:34,160
escolher um local no IDT

15
00:00:30,960 --> 00:00:36,399
então interrompa no acesso execute 1

16
00:00:34,160 --> 00:00:39,120
e esse desempenho específico

17
00:00:36,399 --> 00:00:40,719
a interrupção parece relativamente segura

18
00:00:39,120 --> 00:00:43,600
e eu não sei, eu vou apenas

19
00:00:40,719 --> 00:00:47,120
com o rsp como minha localização para o

20
00:00:43,600 --> 00:00:51,120
escreva o ponto de parada, então parta para

21
00:00:47,120 --> 00:00:54,160
write e digamos que seja 8

22
00:00:51,120 --> 00:00:55,680
e este endereço em particular agora eu disse

23
00:00:54,160 --> 00:00:57,360
que não o vai ver

24
00:00:55,680 --> 00:00:59,280
imediatamente atualizado para que possa ver

25
00:00:57,360 --> 00:01:01,199
os meus registos de depuração ainda estão a 0

26
00:00:59,280 --> 00:01:04,000
apesar de os ter definido

27
00:01:01,199 --> 00:01:06,000
pode fazer bl para confirmar que sabe

28
00:01:04,000 --> 00:01:07,920
esses vão ser definidos

29
00:01:06,000 --> 00:01:09,680
e então eu disse que você tem que realmente ir

30
00:01:07,920 --> 00:01:12,640
e depois quebrar imediatamente

31
00:01:09,680 --> 00:01:13,439
se quiser realmente ir

32
00:01:12,640 --> 00:01:15,439
interpretá-lo

33
00:01:13,439 --> 00:01:17,280
Eu posso ver que de facto esta coisa disparou

34
00:01:15,439 --> 00:01:20,320
antes que eu pudesse acertar o

35
00:01:17,280 --> 00:01:21,680
pause gui aqui no WinDbg, então ele fez

36
00:01:20,320 --> 00:01:24,159
na verdade quebrou em um

37
00:01:21,680 --> 00:01:27,200
escrever, então vamos em frente e interpretar

38
00:01:24,159 --> 00:01:29,600
os registos e ver o que vemos

39
00:01:27,200 --> 00:01:30,640
por isso precisamos de voltar atrás e ver

40
00:01:29,600 --> 00:01:32,640
no

41
00:01:30,640 --> 00:01:34,479
registo de controlo 7 a partir do

42
00:01:32,640 --> 00:01:37,280
início nós vemos

43
00:01:34,479 --> 00:01:38,880
global enable do we see local enable

44
00:01:37,280 --> 00:01:42,240
vamos descobrir

45
00:01:38,880 --> 00:01:45,439
assim, no registo de controlo sete podemos fazer dx

46
00:01:42,240 --> 00:01:47,200
no registo de depuração 7

47
00:01:45,439 --> 00:01:48,960
disse registo de controlo 7 mas é

48
00:01:47,200 --> 00:01:52,000
registo de depuração 7

49
00:01:48,960 --> 00:01:55,119
então aqui pode ver que o bit 0 está definido

50
00:01:52,000 --> 00:01:56,240
e o bit 2 está definido, o que significa que

51
00:01:55,119 --> 00:01:59,840
corresponde a

52
00:01:56,240 --> 00:02:01,759
habilitação local 0 e habilitação local 1

53
00:01:59,840 --> 00:02:03,600
e isso faz sentido porque temos 2

54
00:02:01,759 --> 00:02:06,159
pontos de parada que são colocados em depuração

55
00:02:03,600 --> 00:02:09,360
registo 0 e registo de depuração 1

56
00:02:06,159 --> 00:02:10,800
sendo o primeiro o endereço deste

57
00:02:09,360 --> 00:02:12,720
particular

58
00:02:10,800 --> 00:02:14,480
interrupção de desempenho, então essa é a nossa

59
00:02:12,720 --> 00:02:16,879
ponto de parada de execução e esse é o nosso ponto de escrita

60
00:02:14,480 --> 00:02:19,599
ponto de parada

61
00:02:16,879 --> 00:02:21,599
podemos ver que os globais não estão definidos

62
00:02:19,599 --> 00:02:23,760
nesta saída, então o global

63
00:02:21,599 --> 00:02:26,239
não estão definidas parece que temos um

64
00:02:23,760 --> 00:02:29,360
bit aqui, então vamos verificar qual é esse bit

65
00:02:26,239 --> 00:02:32,160
0 1 2 3 4 5 6

66
00:02:29,360 --> 00:02:34,160
7 8 9 10 Acho que são 10

67
00:02:32,160 --> 00:02:36,319
está a sair mais ou menos corretamente

68
00:02:34,160 --> 00:02:37,920
parece que sim porque é sempre assim

69
00:02:36,319 --> 00:02:41,280
definido no registo de depuração

70
00:02:37,920 --> 00:02:42,640
7 tudo bem, então o bit 10 está definido e

71
00:02:41,280 --> 00:02:44,400
então há alguns outros bits aqui em cima

72
00:02:42,640 --> 00:02:46,400
que estão configurados, então vamos ver quais bits

73
00:02:44,400 --> 00:02:50,480
esses são, então esse foi 10

74
00:02:46,400 --> 00:02:55,760
11 12 13 14 15 16 17 18

75
00:02:50,480 --> 00:02:58,879
19 20. então 19 e 20

76
00:02:55,760 --> 00:03:02,480
tudo bem 19 20 é um limite então

77
00:02:58,879 --> 00:03:02,480
você quer dizer 20 21.

78
00:03:02,879 --> 00:03:10,400
então isto seria 20 e 21

79
00:03:06,080 --> 00:03:10,400
enquanto 18 e 19 são 0

80
00:03:12,720 --> 00:03:18,000
então o que vemos é 20 e 21

81
00:03:15,920 --> 00:03:20,239
são 01 que precisamos interpretar

82
00:03:18,000 --> 00:03:22,480
estes corretos, então 20 21

83
00:03:20,239 --> 00:03:24,799
são 0 1 e é isso que nós faríamos

84
00:03:22,480 --> 00:03:28,480
esperamos ter uma quebra na escrita de dados

85
00:03:24,799 --> 00:03:31,599
0 1 em 21 e 20

86
00:03:28,480 --> 00:03:35,120
então temos 0 0 em

87
00:03:31,599 --> 00:03:37,760
leitura/escrita 0 e comprimento 0.

88
00:03:35,120 --> 00:03:39,200
então estes bits anteriores são todos 0

89
00:03:37,760 --> 00:03:40,799
aqui

90
00:03:39,200 --> 00:03:42,799
então a interpretação disso é que

91
00:03:40,799 --> 00:03:44,720
é uma quebra na execução da instrução

92
00:03:42,799 --> 00:03:46,239
para o ponto de interrupção 0 que é exatamente o que

93
00:03:44,720 --> 00:03:49,599
nós esperamos

94
00:03:46,239 --> 00:03:51,519
e então o comprimento aqui é 1 byte para

95
00:03:49,599 --> 00:03:55,360
a execução da instrução

96
00:03:51,519 --> 00:03:56,239
e para este, bits 22 e 23, teríamos

97
00:03:55,360 --> 00:03:59,519
esperamos que sejam

98
00:03:56,239 --> 00:04:00,319
1 0 porque definimos um 8 bit 8

99
00:03:59,519 --> 00:04:03,920
byte

100
00:04:00,319 --> 00:04:08,159
quebra na escrita, então se isso fosse

101
00:04:03,920 --> 00:04:08,159
desculpe se isso foi 21

102
00:04:08,560 --> 00:04:14,959
vejamos, eu tinha 20 21

103
00:04:11,840 --> 00:04:18,079
ou 0 1 e assim 22

104
00:04:14,959 --> 00:04:20,799
23 são 1 0 e

105
00:04:18,079 --> 00:04:22,479
1 0 é a versão de 8 bytes, então isso é

106
00:04:20,799 --> 00:04:23,840
exatamente o que esperaríamos no

107
00:04:22,479 --> 00:04:26,800
registo de controlo de depuração

108
00:04:23,840 --> 00:04:28,000
ativação do registo de depuração 0 debug

109
00:04:26,800 --> 00:04:30,160
registo 1

110
00:04:28,000 --> 00:04:31,600
algum bit específico codificado ali e

111
00:04:30,160 --> 00:04:34,560
então isto disse que era

112
00:04:31,600 --> 00:04:35,360
uma pausa na execução, ele colocou um 1 byte

113
00:04:34,560 --> 00:04:37,759
isto disse

114
00:04:35,360 --> 00:04:38,880
uma pausa na escrita e this said 8

115
00:04:37,759 --> 00:04:40,800
bytes

116
00:04:38,880 --> 00:04:42,400
ok então agora eu disse que na verdade isso

117
00:04:40,800 --> 00:04:44,560
ponto de parada 1 atingido

118
00:04:42,400 --> 00:04:47,600
esta quebra ao escrever no endereço da minha pilha

119
00:04:44,560 --> 00:04:50,320
atingiu antes que eu pudesse de facto

120
00:04:47,600 --> 00:04:51,680
fazer a pausa, então vamos ver o que temos

121
00:04:50,320 --> 00:04:55,120
no registo de depuração

122
00:04:51,680 --> 00:04:59,120
6 que é o nosso registo de estado, portanto

123
00:04:55,120 --> 00:05:02,320
dx em dr6

124
00:04:59,120 --> 00:05:05,280
em binário assim

125
00:05:02,320 --> 00:05:07,680
o zerésimo bit está desativado e o primeiro bit

126
00:05:05,280 --> 00:05:11,120
está definido, então o que isso significa

127
00:05:07,680 --> 00:05:13,280
então nós precisamos ir para o debug

128
00:05:11,120 --> 00:05:13,680
registo de estado o bit zero não é

129
00:05:13,280 --> 00:05:16,320
definido

130
00:05:13,680 --> 00:05:17,360
e o primeiro bit está definido, o que significa que

131
00:05:16,320 --> 00:05:20,160
que é

132
00:05:17,360 --> 00:05:20,639
ponto de paragem 1 ou registo de depuração 1

133
00:05:20,160 --> 00:05:23,120
que

134
00:05:20,639 --> 00:05:24,960
é a coisa que realmente disparou, então você

135
00:05:23,120 --> 00:05:26,560
teria que um depurador voltar

136
00:05:24,960 --> 00:05:28,639
e olharia para os registos de controlo

137
00:05:26,560 --> 00:05:32,880
e ele diria ok para

138
00:05:28,639 --> 00:05:36,080
o registo 7 era a entrada

139
00:05:32,880 --> 00:05:38,800
1 era 0 desculpe 0 era

140
00:05:36,080 --> 00:05:40,000
habilitado e 1 estava habilitado, poderia ser

141
00:05:38,800 --> 00:05:41,919
o caso em que você sabe que o

142
00:05:40,000 --> 00:05:43,759
o bit do ponto de parada foi configurado, mas este

143
00:05:41,919 --> 00:05:44,800
poderia ser configurado para não habilitado mas

144
00:05:43,759 --> 00:05:46,880
ele vê

145
00:05:44,800 --> 00:05:48,560
isto corresponde a isto que foi

146
00:05:46,880 --> 00:05:49,440
habilitado porque este era o globals e o

147
00:05:48,560 --> 00:05:52,160
os locais

148
00:05:49,440 --> 00:05:52,800
então que outros bits estão definidos aqui bem nós

149
00:05:52,160 --> 00:05:55,759
podemos ver

150
00:05:52,800 --> 00:05:57,840
0 1 2 3 e então um inteiro

151
00:05:55,759 --> 00:06:01,039
um monte de uns quantos uns

152
00:05:57,840 --> 00:06:03,600
1 2 3 4 5 6 7

153
00:06:01,039 --> 00:06:04,400
8 se for um ones vamos verificar aqui

154
00:06:03,600 --> 00:06:07,280
1 2

155
00:06:04,400 --> 00:06:09,199
3 4 5 6 7 8 se for

156
00:06:07,280 --> 00:06:11,120
exatamente como esperávamos

157
00:06:09,199 --> 00:06:14,160
e então nós teríamos um 0 codificado

158
00:06:11,120 --> 00:06:17,280
e então poderíamos ter algo como

159
00:06:14,160 --> 00:06:18,960
a BD se a deteção geral foi definida e

160
00:06:17,280 --> 00:06:19,280
nós não vimos a deteção geral

161
00:06:18,960 --> 00:06:21,120
conjunto

162
00:06:19,280 --> 00:06:22,479
no registo de controlo para que nós não

163
00:06:21,120 --> 00:06:25,440
esperamos ver isso aqui

164
00:06:22,479 --> 00:06:26,479
mas isso era 8 bits de uns e depois

165
00:06:25,440 --> 00:06:29,039
um 0 e

166
00:06:26,479 --> 00:06:29,680
que é 0, então isso significa que a quebra em

167
00:06:29,039 --> 00:06:32,720
geral

168
00:06:29,680 --> 00:06:34,880
detetar não é o que acabou de disparar

169
00:06:32,720 --> 00:06:36,160
o próximo é o BS que foi quebrado num único

170
00:06:34,880 --> 00:06:39,360
passo esse

171
00:06:36,160 --> 00:06:42,639
também é 0 e então BT e

172
00:06:39,360 --> 00:06:43,919
RTM não cobrimos, portanto não nos interessa

173
00:06:42,639 --> 00:06:46,240
tudo bem, então essa é a maneira que você

174
00:06:43,919 --> 00:06:48,800
interpretarias de facto, sabes, o

175
00:06:46,240 --> 00:06:50,560
valores nos registos de depuração interpretam

176
00:06:48,800 --> 00:06:52,319
dr6 como sendo o estado

177
00:06:50,560 --> 00:06:53,680
quando um ponto de paragem chega de facto, pode

178
00:06:52,319 --> 00:06:54,240
não ter tido seu ponto de parada

179
00:06:53,680 --> 00:06:55,680
disparar

180
00:06:54,240 --> 00:06:57,520
tudo depende de onde você coloca o

181
00:06:55,680 --> 00:07:00,000
ponto de parada, mas

182
00:06:57,520 --> 00:07:00,960
dr7 é a principal coisa que o está a controlar

183
00:07:00,000 --> 00:07:03,199
saiba como ele é configurado

184
00:07:00,960 --> 00:07:04,319
e sabe que acabamos por ver que o

185
00:07:03,199 --> 00:07:08,240
coisas correspondem a

186
00:07:04,319 --> 00:07:08,240
exatamente o que dissemos no WinDbg
