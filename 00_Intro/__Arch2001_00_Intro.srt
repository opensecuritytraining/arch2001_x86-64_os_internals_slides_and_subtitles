1
00:00:01,199 --> 00:00:05,359
Welcome to Architecture 2001

2
00:00:03,280 --> 00:00:06,879
in this class we'll dig into Intel

3
00:00:05,359 --> 00:00:07,680
architectural features which are

4
00:00:06,879 --> 00:00:10,160
utilized by

5
00:00:07,680 --> 00:00:12,639
operating systems, virtualization systems,

6
00:00:10,160 --> 00:00:14,559
and to a lesser extent firmware

7
00:00:12,639 --> 00:00:16,400
In this class we're going to use Windows

8
00:00:14,559 --> 00:00:18,400
kernel debugging as a means to the end

9
00:00:16,400 --> 00:00:20,480
to see how these particular hardware

10
00:00:18,400 --> 00:00:22,080
mechanisms are used in practice

11
00:00:20,480 --> 00:00:23,600
but the goal of the class is not to give

12
00:00:22,080 --> 00:00:24,320
you a deep understanding of Windows

13
00:00:23,600 --> 00:00:26,800
internals

14
00:00:24,320 --> 00:00:28,560
that will be covered in a future class

15
00:00:26,800 --> 00:00:30,640
we're also going to come across

16
00:00:28,560 --> 00:00:32,719
a variety of new assembly instructions

17
00:00:30,640 --> 00:00:33,440
in this class but the point is again not

18
00:00:32,719 --> 00:00:35,040
to really

19
00:00:33,440 --> 00:00:37,040
understand assembly instructions for

20
00:00:35,040 --> 00:00:38,719
their own sake it's more that as we

21
00:00:37,040 --> 00:00:40,160
learn about the architectural features

22
00:00:38,719 --> 00:00:41,040
we'll have to learn about the assembly

23
00:00:40,160 --> 00:00:43,120
instructions that

24
00:00:41,040 --> 00:00:45,200
get and set values relevant to these

25
00:00:43,120 --> 00:00:46,879
features there's only two assembly

26
00:00:45,200 --> 00:00:50,000
instructions that we're covering just

27
00:00:46,879 --> 00:00:51,360
for the heck of it so what I hope you

28
00:00:50,000 --> 00:00:53,440
get out of this class

29
00:00:51,360 --> 00:00:56,079
is a better understanding of how these

30
00:00:53,440 --> 00:00:57,920
Intel hardware mechanisms actually work

31
00:00:56,079 --> 00:01:00,320
and that will give you a good

32
00:00:57,920 --> 00:01:02,960
understanding and a good base for future

33
00:01:00,320 --> 00:01:05,280
classes on virtualization and firmware

34
00:01:02,960 --> 00:01:07,280
I also hope that you learn about how

35
00:01:05,280 --> 00:01:10,240
hardware security mechanisms

36
00:01:07,280 --> 00:01:12,240
exist in the hardware and when they are

37
00:01:10,240 --> 00:01:12,799
or are not used by given operating

38
00:01:12,240 --> 00:01:14,560
systems

39
00:01:12,799 --> 00:01:17,360
and what this says about the security of

40
00:01:14,560 --> 00:01:19,280
different operating systems

41
00:01:17,360 --> 00:01:21,759
also hopefully this should give you a

42
00:01:19,280 --> 00:01:24,320
good base of understanding that you can

43
00:01:21,759 --> 00:01:25,360
start exploring the Intel manual and

44
00:01:24,320 --> 00:01:27,360
seeing what other

45
00:01:25,360 --> 00:01:29,520
mechanisms exist there and what other

46
00:01:27,360 --> 00:01:30,880
things operating systems virtualization

47
00:01:29,520 --> 00:01:33,360
systems and firmware

48
00:01:30,880 --> 00:01:35,280
could potentially be using if you work

49
00:01:33,360 --> 00:01:37,119
in one of these fields such as operating

50
00:01:35,280 --> 00:01:39,200
systems virtualization or firmware

51
00:01:37,119 --> 00:01:41,439
hopefully it'll give you a sufficient

52
00:01:39,200 --> 00:01:42,880
base to go off and explore and say

53
00:01:41,439 --> 00:01:44,560
well what about this new feature why are

54
00:01:42,880 --> 00:01:45,920
we not using that maybe we should use

55
00:01:44,560 --> 00:01:48,479
that

56
00:01:45,920 --> 00:01:49,840
and of course as with other classes I

57
00:01:48,479 --> 00:01:51,840
hope that you get out of this

58
00:01:49,840 --> 00:01:53,680
a satisfaction that comes from

59
00:01:51,840 --> 00:01:54,000
understanding how things work at a very

60
00:01:53,680 --> 00:01:57,759
deep

61
00:01:54,000 --> 00:02:00,640
level. So in the Architecture 1001

62
00:01:57,759 --> 00:02:01,759
class we covered the rflags register or

63
00:02:00,640 --> 00:02:03,360
eflags register

64
00:02:01,759 --> 00:02:05,600
and we hit a bunch of these different

65
00:02:03,360 --> 00:02:07,920
flags but most of these were

66
00:02:05,600 --> 00:02:09,840
status flags indicating whether or not

67
00:02:07,920 --> 00:02:11,760
you know the zero flag was set or the

68
00:02:09,840 --> 00:02:14,160
sign flag was set.

69
00:02:11,760 --> 00:02:16,400
In this class we're going to see a

70
00:02:14,160 --> 00:02:18,640
variety of system flags

71
00:02:16,400 --> 00:02:20,879
and these are things that speak to the

72
00:02:18,640 --> 00:02:23,599
current operation of the system current

73
00:02:20,879 --> 00:02:25,440
privileges of certain actions whether or

74
00:02:23,599 --> 00:02:27,120
not certain features are supported on

75
00:02:25,440 --> 00:02:28,959
this hardware and so forth

76
00:02:27,120 --> 00:02:30,640
so these are we're going to dig in more

77
00:02:28,959 --> 00:02:32,640
and see more flags from the rflags

78
00:02:30,640 --> 00:02:35,040
register

79
00:02:32,640 --> 00:02:36,080
and here's a overview of 64-bit

80
00:02:35,040 --> 00:02:38,160
execution mode

81
00:02:36,080 --> 00:02:39,120
but in this particular class we're only

82
00:02:38,160 --> 00:02:40,879
going to be adding

83
00:02:39,120 --> 00:02:43,280
this little bits the rest of these are

84
00:02:40,879 --> 00:02:44,560
things like uh vector instructions and

85
00:02:43,280 --> 00:02:46,080
floating point which again

86
00:02:44,560 --> 00:02:48,400
you know we don't care about that much

87
00:02:46,080 --> 00:02:49,040
in this class so you've already seen the

88
00:02:48,400 --> 00:02:51,040
16

89
00:02:49,040 --> 00:02:52,080
general purpose registers the rflags

90
00:02:51,040 --> 00:02:53,360
and the rip

91
00:02:52,080 --> 00:02:56,160
and in this class what we'll be

92
00:02:53,360 --> 00:02:58,239
introducing is the segment registers

93
00:02:56,160 --> 00:03:00,239
and we'll be talking a lot about address

94
00:02:58,239 --> 00:03:02,800
spaces and virtual memory logical

95
00:03:00,239 --> 00:03:05,040
addresses and so forth

96
00:03:02,800 --> 00:03:07,040
but this is the nitty-gritty and this is

97
00:03:05,040 --> 00:03:09,040
the picture which should scare you

98
00:03:07,040 --> 00:03:10,080
to a first approximation this is all the

99
00:03:09,040 --> 00:03:13,280
various

100
00:03:10,080 --> 00:03:16,480
internal data structures and and

101
00:03:13,280 --> 00:03:18,319
fields and tables which the hardware

102
00:03:16,480 --> 00:03:19,920
automatically uses and which software

103
00:03:18,319 --> 00:03:20,560
sets up and this is what we're going to

104
00:03:19,920 --> 00:03:22,400
learn

105
00:03:20,560 --> 00:03:24,159
and so I would understand if you said

106
00:03:22,400 --> 00:03:29,680
this is madness

107
00:03:24,159 --> 00:03:29,680
but this is architecture 2001.

